import json
from src.planner_common import feature_flags
import os
import shlex
import subprocess
import sys
from http import HTTPStatus
from json.decoder import JSONDecodeError
from pathlib import Path
from pprint import pformat

# Since this tasks.py is not in a python project, the dependencies are managed in flake.nix
# directly. The tasks.py files in the subprojects are managed via poetry.
from invoke.tasks import task
from invoke.exceptions import Exit, UnexpectedExit

if "PYTHONDEVMODE" in os.environ:
    # invoke leaks file descriptors and it's not too important here
    del os.environ["PYTHONDEVMODE"]

repo = Path(__file__).parent


@task
def branch_pre_commit(c):
    """Make sure pre-commit runs fine for all files that have been changed in this branch"""
    c.run(
        "git diff `git merge-base HEAD origin/main` --name-only "
        "| xargs pre-commit run --show-diff-on-failure --files",
        pty=True,
        env={"SKIP": "isort,make-migrations"},  # flaky, we're checking this explicitly
    )


@task
def make_migrations(c):
    """Create forgotten django migrations"""
    try:
        c.run(
            "direnv exec . ./manage.py makemigrations --noinput",
            hide=True,
            in_stream=False,
        )
    except UnexpectedExit:
        print(
            "\n"
            "---"
            "`./manage.py makemigrations` failed.\n"
            "  This usually happens when the migration needs some input.\n"
            "  Try to go in the project directory "
            'and run "./manage.py makemigrations" manually',
            file=sys.stderr,
        )
        raise

    unstaged_migrations = c.run(
        r"git status --porcelain=v1 | awk '/^\?\? .*\/migrations\//{print $2}' ",
        hide="stdout",
    )
    if unstaged_migrations.stdout.strip():
        filenames = "\n".join(
            f"  {filename}" for filename in unstaged_migrations.stdout.split("\n")
        )
        print(f"Unstaged migrations:\n{filenames}", file=sys.stderr)
        raise Exit("", 1)


class SecretPusher:
    # Docs are at https://docs.gitlab.com/ee/api/project_level_variables.html
    project_id = "20207631"
    url = f"https://gitlab.com/api/v4/projects/{project_id}"

    ENV_VAR = "env_var"
    FILE = "file"

    def __init__(self, c):
        self.c = c

    __auth_headers_value = None

    @property
    def __auth_headers(self):
        if not self.__auth_headers_value:
            gitlab_token = self.get_gopass(
                "seawatch-crewingdb-personal/gitlab_personal_access_token"
            ).strip()
            self.__auth_headers_value = {"PRIVATE-TOKEN": gitlab_token}
        return self.__auth_headers_value

    def get_gopass(self, path):
        result = subprocess.run(
            ["gopass", "cat", shlex.quote(path)],
            stdout=subprocess.PIPE,
        )
        result.check_returncode()
        return result.stdout.decode()

    def push_all_secrets(self):
        for key, gopass_key in {
            "hcloud_token": "seawatch-crewingdb/staging/hcloud_token",
            "vault_password": "seawatch-crewingdb/staging/ansible-vault",
        }.items():
            self.push_secret(
                key, self.get_gopass(gopass_key).strip(), type=self.ENV_VAR
            )

        self.push_secret(
            "deploy_ssh_key_pub",
            self.get_gopass("seawatch-crewingdb/staging/deploy_ssh_key.pub").strip(),
            type=self.ENV_VAR,
            masked=False,
        )
        self.push_secret(
            "deploy_ssh_key",
            # Don't strip, stuff breaks without a trailing newline:
            #   https://github.com/ansible/ansible-runner/issues/544#issuecomment-719004397
            self.get_gopass("seawatch-crewingdb/staging/deploy_ssh_key"),
            type=self.FILE,
            masked=False,
        )

    def push_secret(self, key, value, /, type, masked=True):
        import requests

        data = {
            "id": self.project_id,
            "key": key,
            "value": value,
            "variable_type": type,
            "protected": False,  # TODO: toggle to true once we're running on main
            "masked": masked,  # can't mask files =/
            "environment_scope": "staging",
        }
        response = requests.put(
            f"{self.url}/variables/{data['key']}",
            headers=self.__auth_headers,
            data=data,
        )
        if HTTPStatus(response.status_code) == HTTPStatus.NOT_FOUND:
            print(f"Variable \"{data['key']}\" not found, creating...", file=sys.stderr)
            response = requests.post(
                f"{self.url}/variables", headers=self.__auth_headers, data=data
            )
        if not response.ok:
            self.__report_error_response(response)

    def __report_error_response(self, response):
        print(
            f"~~ Got status {response.status_code} ({HTTPStatus(response.status_code).phrase}) "
            " from the gitlab api :(",
            file=sys.stderr,
        )
        try:
            print(pformat(json.loads(response.content)), file=sys.stderr)
        except JSONDecodeError:
            print("Also, I couldn't json-parse the response:", file=sys.stderr)
            print(repr(response.content))


@task
def push_ci_secrets(c):
    SecretPusher(c).push_all_secrets()


@task
def feature_flag_guardrail(c):
    enabled_flags = [
        flag
        for flag, value in feature_flags.production_disabled_features.items()
        if value
    ]
    if enabled_flags:
        print(
            f"Accidentally enabled or forgot to disable feature flags? ({', '.join(enabled_flags)})",
            file=sys.stderr,
        )
        raise Exit("", 1)


@task
def test(c):
    "Run the tests"
    run_with_passthrough(c, "pytest", pty=True)


@task
def watch(c):
    "Continuously re-run all tests whenever files are changed"
    import watchfiles

    # Do a bash-dance to avoid the terminal not showing input if pdb gets interrupted by watchfiles
    watchfiles.run_process(
        "./src", target=f"bash -c 'stty echo; pytest \"$@\"' -- {passthrough_args()}"
    )


@task
def tdd_reset(c):
    """
    Clear caches and everything.

    Shouldn't usually be necessary, try this if tdd is weird
    """
    c.run(
        "rm -rf .pytest_cache .testmondata .testmondata-shm .testmondata-wal",
        echo="both",
    )


@task
def tdd(c):
    "Continuousy re-run affected tests whenever files are changed"
    import watchfiles

    # Do a bash-dance to avoid the terminal not showing input if pdb gets interrupted by watchfiles
    watchfiles.run_process(
        "./src",
        target=f"bash -c 'stty echo; pytest \"$@\"' -- --testmon {passthrough_args()}",
    )


@task
def gc(c):
    c.run(
        # This is fine, don't think about it too much
        r"""
        psql -Atc "select 'DROP DATABASE \"' || datname || '\";' from pg_database where datname like 'test_%';" | psql
        """
    )


#####################################################################
## Helper functions


def run_with_passthrough(c, cmd, *args, **kwargs):
    return c.run(" ".join([cmd, passthrough_args()]), *args, **kwargs)


def passthrough_args() -> str:
    try:
        dashdash_position = sys.argv.index("--")
    except ValueError:
        return ""
    return " ".join(map(shlex.quote, sys.argv[dashdash_position + 1 :]))
