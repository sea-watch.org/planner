resource "hcloud_ssh_key" "deploy" {
  name       = "deploy"
  public_key = var.deploy_ssh_key_pub
}

resource "hcloud_ssh_key" "simon" {
  name       = "simon"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOAOtv9Uc6RYFIuEqwcxAMYEoqdKMX4j2VPnrMq9NJ1S simon@sea-watch.org"
}

resource "hcloud_server" "staging" {
  # 49.13.87.26
  name        = "staging"
  server_type = "cx21"
  image       = "debian-12"
  location    = "fsn1"
  ssh_keys = [
    hcloud_ssh_key.deploy.name,
    hcloud_ssh_key.simon.name,
  ]

  lifecycle {
    # The ip is allow-listed in the mailserver! Do not remove unless that is changed!
    prevent_destroy = true
  }
}

# ################################
# ## Ansible inventory information

resource "ansible_host" "staging" {
  inventory_hostname = hcloud_server.staging.name
  vars = {
    ansible_user = "root"
    ansible_host = hcloud_server.staging.ipv4_address
  }
}
