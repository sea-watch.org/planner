#!/usr/bin/env ansible-playbook

- hosts: staging
  vars:
    image_tag: "{{ lookup('env', 'image_tag') | mandatory }}"
  tasks:
    - fail:
        msg: "Define the image_tag environment variable"
      when: not image_tag

    #################
    ## Basic server stuff

    - name: Install unattended-upgrades
      package:
        name: unattended-upgrades
        state: present
        update_cache: yes
        # This sadly breaks deployment on fresh VMs, since the relevant mtime is set on provisioning
        # cache_valid_time: 86400 # Don't apt update if we already did it today

    - name: Mail on upgrade failures
      lineinfile:
        dest: /etc/apt/apt.conf.d/50unattended-upgrades
        regexp: "Unattended-Upgrade::Mail "
        line: 'Unattended-Upgrade::Mail "root";'

    - name: Ensure that SSH Password Auth is disabled
      become: true
      notify: Reload ssh
      lineinfile:
        dest: /etc/ssh/sshd_config
        regexp: "^#?PasswordAuthentication"
        line: "PasswordAuthentication no"

    - name: SSH Keys
      ansible.posix.authorized_key:
        user: root
        state: present
        key: "{{ item }}"
      loop:
        - "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOAOtv9Uc6RYFIuEqwcxAMYEoqdKMX4j2VPnrMq9NJ1S simon@sea-watch.org"

    - name: Install mail transfer agent
      package:
        name: "{{ item }}"
        state: present
      loop:
        - msmtp
        - msmtp-mta

    - name: Enable mta
      service:
        name: msmtpd
        state: started
        enabled: true

    - name: Configure system mailing
      template:
        src: templates/msmtprc.j2
        dest: /etc/msmtprc

    - name: Mail aliases
      copy:
        dest: /etc/aliases
        content: |
          root: simon@sea-watch.org

    #########
    ## Podman

    - name: Install podman
      package:
        name: podman
        state: present

    ###########
    ## Database

    - name: Install postgresql and related packages
      package:
        name: "{{ item }}"
        state: present
      loop:
        - postgresql
        - postgresql-client
        - python3-psycopg2

    - name: Create database user
      become: true
      become_user: postgres
      community.postgresql.postgresql_user:
        name: planner
        password: "{{ secret_planner_db_password | mandatory }}"

    - name: Create database
      become: true
      become_user: postgres
      community.postgresql.postgresql_db:
        name: planner
        owner: planner

    - name: Allow database users to connect to socket
      become: true
      become_user: postgres
      community.postgresql.postgresql_pg_hba:
        dest: /etc/postgresql/15/main/pg_hba.conf
        contype: local
        users: "planner"
        databases: "planner"
        method: scram-sha-256
      notify: Reload postgresql

    ########
    ## MinIO

    - name: Copy MinIO environment file
      template:
        src: "systemd/minio.environment.j2"
        dest: "/etc/minio.environment"
        mode: 0600
      notify: Restart minio

    - name: Ensure minio state directory exists
      ansible.builtin.file:
        path: /var/lib/minio
        state: directory

    - name: Copy MinIO systemd service file
      copy:
        src: "minio.service"
        dest: "/etc/systemd/system/minio.service"
      notify: Restart minio

    - name: Start minio
      systemd:
        service: minio.service
        enabled: true
        state: started
        daemon-reload: yes

    - name: Copy MinIO-create-bucket environment
      tags:
        - minio-create-bucket
      template:
        src: "systemd/minio-create-bucket.environment.j2"
        dest: "/etc/minio-create-bucket.environment"
        mode: 0600
      notify: Run minio-create-bucket

    - name: Copy MinIO-create-bucket unit
      tags:
        - minio-create-bucket
      copy:
        src: minio-create-bucket.service
        dest: /etc/systemd/system/minio-create-bucket.service

    #########
    ## ClamAV

    - name: Install clamav
      package:
        name: "{{ item }}"
        state: present
      with_items:
        - clamav
        - clamav-daemon

    ##############
    ## Application

    - name: Setup planner systemd unit
      template:
        src: "systemd/planner.service.j2"
        dest: "/etc/systemd/system/planner.service"
      notify: Restart planner

    - name: Setup planner worker systemd unit
      template:
        src: "systemd/planner-worker.service.j2"
        dest: "/etc/systemd/system/planner-worker.service"
      notify: Restart planner worker

    - name: Copy planner environment file
      template:
        src: "systemd/planner.environment.j2"
        dest: "/etc/planner.environment"
        mode: 0600
      notify:
        - Restart planner
        - Restart planner worker

    - name: Enable services
      ansible.builtin.systemd_service:
        name: "{{ item }}"
        enabled: yes
        state: started
        daemon-reload: yes
      loop:
        - planner
        - planner-worker

    #########################
    ## Podman regular cleanup
    - name: Setup "Prune podman images" timer and service
      ansible.builtin.copy:
        src: "{{ item }}"
        dest: "/etc/systemd/system/{{ item }}"
      loop:
        - podman-image-prune.service
        - podman-image-prune.timer

    - name: Active "Prune podman images" timer
      ansible.builtin.systemd_service:
        name: podman-image-prune.timer
        state: started
        enabled: yes

    ################
    ## Reverse proxy
    - name: Caddy repo dependencies
      package:
        name:
          - debian-keyring
          - debian-archive-keyring
          - apt-transport-https
        state: present

    - name: Caddy repo key
      ansible.builtin.apt_key:
        data: "{{ lookup('file', 'templates/caddy-stable.asc') }}"
        state: present

    - name: Caddy repos
      ansible.builtin.apt_repository:
        repo: "{{ item }}"
        state: present
        update_cache: yes
      with_items:
        - deb https://dl.cloudsmith.io/public/caddy/stable/deb/debian any-version main
        - deb-src https://dl.cloudsmith.io/public/caddy/stable/deb/debian any-version main

    - name: Install caddy
      package:
        name: caddy
        state: present

    - name: Caddyfile
      ansible.builtin.template:
        src: templates/Caddyfile.j2
        dest: /etc/caddy/Caddyfile
      notify: Reload caddy

    - name: Enable caddy service
      ansible.builtin.systemd_service:
        name: caddy.service
        enabled: yes
        state: started
        daemon-reload: yes

  handlers:
    - name: Reload ssh
      service:
        name: ssh
        state: reloaded

    - name: Reload postgresql
      service:
        name: postgresql
        state: reloaded

    - name: Restart planner
      ansible.builtin.systemd_service:
        name: planner.service
        state: restarted

    - name: Restart planner worker
      ansible.builtin.systemd_service:
        name: planner-worker.service
        state: restarted

    - name: Reload caddy
      ansible.builtin.systemd_service:
        name: caddy.service
        state: reloaded

    - name: Restart minio
      ansible.builtin.systemd_service:
        name: minio.service
        state: restarted

    - name: Run minio-create-bucket
      ansible.builtin.systemd_service:
        name: minio-create-bucket.service
        state: started
        daemon-reload: yes
