# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.50.0"
  constraints = "1.50.0"
  hashes = [
    "h1:+qK+M2TCzFMhWM3KsnRm9RUb/iRmfYzCo8PYpKKSBKE=",
    "h1:5ml5MoESSVODwbYCNbAG9djGupOkmG3qDen8+sUi7+s=",
    "h1:BttNgkmi36JI8Zq/oSK3cw0sTZVEve2Y3qQlfJWlcR0=",
    "h1:J8hlBTTUVPklgOxLnK8oW33bFlQDHySbexL6aGq6jPU=",
    "h1:MET0JcOv9V1qZa6Ot/Utn+Tm35XcWjM3917vz4RLRuE=",
    "h1:OIE9fP+QNdPWxA1pKw9gFVqi6ZMHF4HuRcdAFfO8Hb4=",
    "h1:WtwegMMk1dDjm1Z03dLzBRQU8xhMCipYfYEXRNpa76o=",
    "h1:hgym1GEv8sMxDjl6IV9+8Qm8TG/HP9k4YDnsrvc5tNc=",
    "h1:iL8G0KPdIC1Ds/xI2pPq8BL1qlm6pksyptJ5a+gXgcg=",
    "h1:py6baTaTZyReCUPViqfGpKQ+ETO+KYYSc06MnVDUARc=",
    "h1:txFVTG8RI/X3NnyR1RST2cYZiseoDsCVjZt/v2Z6Q/I=",
    "h1:xW72PNXRrdwWpgokDvIqKCS2aD1+SJASTHWmgRzcZxE=",
    "h1:yaOrN9EoZv0S+MorEvXaPYtBUaCOdCg1BCG8Nu0EZRM=",
    "h1:z5J9wgkt9xIKlr699hWCjHSS7K4bYKWWnGCg2T/YNmg=",
    "zh:0bd650fb52e272f74eda5053a7bb62f0fd92182f57ad3ef742abe165cb8cac98",
    "zh:1c36667aa89b672a96c0df3d3c613e80916a2d0944b1a1f9112065f40630b689",
    "zh:21f90683890ea7a184b0ac55efd52911694ba86c58898bc8bbe87ee2507bb1eb",
    "zh:24349d483a6ff97420d847433553fa031f68f99b9ead4ebb3592fc8955ef521f",
    "zh:3fffd83c450bea2b382a986501ae51a4d3e6530eda48ed9ca74d518e4a909c37",
    "zh:43d7de1dc4c50fae99d6c4ab4bb394608948091f5b53ddb29bc65deead9dc8a6",
    "zh:47a37d5fec79dd8bc9cab2c892bc59e135b86cb51eebe2b01cdb40afac7ed777",
    "zh:6efeb9530b8f57618c43f0b294b983d06cce43e9423bdd737eed81db913edb80",
    "zh:7511ace4b33baddfc452ef95a634d83b92bfbfaa23cb30403899e95b64727075",
    "zh:7bade77104ed8788c9b5171c7daae6ab6c011b3c40b152274fda803bf0bf2707",
    "zh:83bce3ff9a1bd52a340a6ebdd2e2b731ec6fb86811ef0ed8a8264daf9d7beb61",
    "zh:a09d5fce4c8d33e10b9a19318c965076db2d8ed5f62f5feb3e7502416f66d7bf",
    "zh:c942832b80270eb982eeb9cc14f30a437db5fd28faf37d6aa32ec2cd345537d6",
    "zh:e2c1812f2e1f9fac17c7551d4ab0efb713b6d751087c18b84b8acd542f587459",
  ]
}

provider "registry.terraform.io/nbering/ansible" {
  version     = "1.0.4"
  constraints = "1.0.4"
  hashes = [
    "h1:G9OI+CZMXBdfe0Oa+/B7zOVronatrovHxDlvpuy5VOE=",
    "zh:0601b46fc129828b7351662c46946a099e57c97a35eefdba111e97feaed00be1",
    "zh:0a6754c2b93146bbb50a6e7ac6d9b596da352bf43474ac65ebd0214ac703ce92",
    "zh:16cc61e49ab806a7e7b1e510d915c3563c89147e7f49d1cb857b4a98555d58e2",
    "zh:3ad5b27cc4ae71c0794b6c3a0abb5bbc74d750b34ab83bed0a232c722514dabe",
    "zh:4dca1f8fcf5a0749994346f1dc6c7f7284469b80d87370b5bae7e812234d1eba",
    "zh:587ef471ea30c9505fa39570c49c5ed2222d4569e5909982965a963209f5c67f",
    "zh:5cfaae8e4ef9754156008418fc74224461b7ad101fa86812af7315a7e79fd33f",
    "zh:720510a04d17c4593969dd31b44063a0ecca53d930e9616a7714c372b4973e13",
    "zh:84d57af64bfbd007440c6916c40a9bed6c5226b72549e8c408c80981bc9aa13e",
    "zh:e1fe7f6c9826f905333813332e027529d2dae30043c3903afcff05be302b940c",
    "zh:fe5e9e7a8550e97933e50d5471b4e13338754aff76524ecb1cb50bd6bce61145",
  ]
}
