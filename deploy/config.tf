##############
## Basic setup
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging"
    lock_address   = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging/lock"
    lock_method    = "POST"
    unlock_address = "https://gitlab.com/api/v4/projects/20207631/terraform/state/planner-staging/lock"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.50.0"
    }
    ansible = {
      source  = "nbering/ansible"
      version = "1.0.4"
    }
  }
}

variable "hcloud_token" { sensitive = true }
variable "deploy_ssh_key_pub" {}

provider "hcloud" { token = var.hcloud_token }
provider "ansible" {}
