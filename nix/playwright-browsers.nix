{
  pkgs,
  system,
  fetchzip,
  stdenv,
  cacert,
  lib,
  playwright-driver,
}:
let
  new-driver =
    let
      version = "1.50.0";
      suffix =
        {
          x86_64-linux = "linux";
          aarch64-linux = "linux-arm64";
          x86_64-darwin = "mac";
          aarch64-darwin = "mac-arm64";
        }
        .${system};
      filename = "playwright-${version}-${suffix}.zip";
    in
    playwright-driver.overrideAttrs {
      inherit version;
      src = fetchzip {
        url = "https://playwright.azureedge.net/builds/driver/${filename}";
        stripRoot = false;
        sha256 =
          {
            # To update these, run `nix repl` folowed by these commands:
            #   :lf .
            #   pkgs = inputs.legacyPackages.x86_64-linux # or whatever your current system is
            #   :b pkgs.fetchurl { stripRoot = false; url = "https://playwright.azureedge.net/builds/driver/playwright-1.50.0-linux.zip"; }
            #   :b pkgs.fetchurl { stripRoot = false; url = "https://playwright.azureedge.net/builds/driver/playwright-1.50.0-linux-arm64.zip"; }
            #   :b pkgs.fetchurl { stripRoot = false; url = "https://playwright.azureedge.net/builds/driver/playwright-1.50.0-mac.zip"; }
            #   :b pkgs.fetchurl { stripRoot = false; url = "https://playwright.azureedge.net/builds/driver/playwright-1.50.0-mac-arm64.zip"; }
            # Each of the `:b` commands will print the sha to put here.
            # x86_64-linux = "sha256-eH879IDuzvWZzyP2XzCOQZ0jIAJAA7IEjkhpmTqw2tQ=";
            # aarch64-linux = "sha256-uS68f5Zasc28kBBHf0L6HDM28gKV1g8Td7ml7fvZKdg=";
            # x86_64-darwin = "sha256-0x5nSgBhgWzY0pDC5bai8LnXCjGDK57o8310xSe/Jbc=";
            # aarch64-darwin = "sha256-/9UJKj8GURXzQaJ9O53/DSKNS0v5+hmk6LaZsC86eQs=";
            x86_64-linux = "sha256-fp+cX9FTRzIEglc2W8CKHlkoY/FzsTqFJRroVGSez64=";
            aarch64-linux = "sha256-Pt3/mTENpJ59PUT6GB1q5Fu4kcFjXh3CSeKbIZGfI4I=";
            x86_64-darwin = "sha256-IhChZSt2Rmk0szs0iMg70LFUUZPwDeFcRlUkSm7H8Ts=";
            aarch64-darwin = "sha256-QSA/qr8jisub3zHAJ1tmCtreC+IEQ42/Zu9IPjaYfPk=";
          }
          .${system};
      };
      postPatch = ''
        rm node
        patchShebangs package/bin/*.sh
      '';
      installPhase = ''
        runHook preInstall

        mkdir $out
        mv package $out/

        runHook postInstall
      '';
    };
  playwright-browsers-mac = stdenv.mkDerivation {
    pname = "playwright-browsers";
    inherit (new-driver) version;

    dontUnpack = true;

    nativeBuildInputs = [ cacert ];

    installPhase = ''
      runHook preInstall

      export PLAYWRIGHT_BROWSERS_PATH=$out
      ${new-driver}/bin/playwright install
      rm -r $out/.links

      runHook postInstall
    '';

    meta.platforms = lib.platforms.darwin;
  };

  playwright-browsers-linux =
    with pkgs;
    let
      fontconfig = makeFontsConf { fontDirectories = [ ]; };
    in
    runCommand "playwright-browsers-chromium"
      {
        nativeBuildInputs = [
          makeWrapper
          jq
        ];
      }
      (''
        BROWSERS_JSON=${new-driver}/package/browsers.json
        CHROMIUM_REVISION=$(jq -r '.browsers[] | select(.name == "chromium").revision' $BROWSERS_JSON)
        mkdir -p $out/chromium-$CHROMIUM_REVISION/chrome-linux

        # See here for the Chrome options:
        # https://github.com/NixOS/nixpkgs/issues/136207#issuecomment-908637738
        makeWrapper ${chromium}/bin/chromium $out/chromium-$CHROMIUM_REVISION/chrome-linux/chrome \
          --set SSL_CERT_FILE /etc/ssl/certs/ca-bundle.crt \
          --set FONTCONFIG_FILE ${fontconfig}
        FFMPEG_REVISION=$(jq -r '.browsers[] | select(.name == "ffmpeg").revision' $BROWSERS_JSON)
        mkdir -p $out/ffmpeg-$FFMPEG_REVISION
        ln -s ${ffmpeg}/bin/ffmpeg $out/ffmpeg-$FFMPEG_REVISION/ffmpeg-linux
      '');
in
{
  x86_64-linux = playwright-browsers-linux;
  aarch64-linux = playwright-browsers-linux;
  x86_64-darwin = playwright-browsers-mac;
  aarch64-darwin = playwright-browsers-mac;
}
.${system}
