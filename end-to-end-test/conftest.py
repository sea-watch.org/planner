import os
from typing import Dict, Optional, Generator

import playwright
import pytest
from playwright.sync_api import BrowserContext

from planner_end_to_end_test.testing_api import BACKOFFICE_URL


@pytest.fixture(scope="session")
def browser_type_launch_args(
    browser_name: Optional[str], browser_type_launch_args: Dict
) -> Dict:
    if expected_version := os.environ.get("PLANNER_PLAYWRIGHT_VERSION"):
        assert (
            playwright._repo_version.version == expected_version  # type: ignore
        ), "Mismatched playwright browsers, did you update playwright without fixing the version in flake.nix?"
    return browser_type_launch_args


@pytest.fixture()
def logged_in_context(
    context: BrowserContext,
) -> Generator[BrowserContext, None, None]:
    page = context.new_page()
    page.goto(BACKOFFICE_URL)
    page.fill("[name=username]", "admin")
    page.fill("[name=password]", "admin")
    page.click('button:text("Login")')
    page.close()

    yield context
