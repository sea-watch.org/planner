# Not sure why pyright doesn't see the task import, but it complains :shrug:
from invoke import task  # type: ignore


@task
def up(c):
    """Bring up the application using docker compose"""
    c.run("docker compose -f docker-compose.yml down --volumes", pty=True)
    c.run(
        "docker compose -f docker-compose.yml up --pull always --build --renew-anon-volumes --force-recreate",
        pty=True,
    )


@task
def like_ci(c):
    """Run the tests like in CI"""
    image_name = (
        c.run("git branch --show-current", hide="stdout")
        .stdout.strip()
        .replace("/", "__")
    )
    env = {
        "PLANNER_IMAGE": f"registry.gitlab.com/sea-watch.org/planner/planner:{image_name}-candidate",
    }
    compose = "docker compose -f docker-compose.yml -f docker-compose.ci.yml"
    c.run(f"{compose} down --volumes", pty=True, env=env)
    c.run(
        f"{compose} up --pull always --renew-anon-volumes --force-recreate",
        pty=True,
        env=env,
    )
