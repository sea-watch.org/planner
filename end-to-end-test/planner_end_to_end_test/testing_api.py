import os
from typing import TypedDict

import requests

BACKOFFICE_URL = os.environ.get("BACKOFFICE_URL", "http://backoffice.localhost:9000")
PUBLIC_BACKEND_URL = os.environ.get(
    "PUBLIC_BACKEND_URL", "http://public.localhost:9000"
)
TIMEOUT_SECONDS = 3


class Volunteer(TypedDict):
    first_name: str
    last_name: str
    email: str
    positions: list[str]  # the names of the positions, not the keys


class VolunteerResult(TypedDict):
    id: int


def create_volunteer(volunteer: Volunteer) -> VolunteerResult:
    response = requests.post(
        f"{BACKOFFICE_URL}/_testing/create_volunteer/",
        json=volunteer,
        timeout=TIMEOUT_SECONDS,
    )
    response.raise_for_status()
    return response.json()


class Operation(TypedDict):
    name: str
    start_date: str
    end_date: str


def delete_all_operations():
    response = requests.post(
        f"{BACKOFFICE_URL}/_testing/delete_all_operations/", timeout=TIMEOUT_SECONDS
    )
    response.raise_for_status()


def create_operations(operations: list[Operation]):
    for operation in operations:
        response = requests.post(
            f"{BACKOFFICE_URL}/_testing/create_operation/",
            json=operation,
            timeout=TIMEOUT_SECONDS,
        )
        response.raise_for_status()


def set_operations(operations: list[Operation]):
    delete_all_operations()
    create_operations(operations)
