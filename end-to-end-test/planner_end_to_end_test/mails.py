from collections.abc import Callable
import os
from dataclasses import dataclass
from typing import cast
import requests
from waiting import wait


@dataclass
class Mail:
    fromAddress: str
    toAddresses: list[str]
    subject: str
    body: str


def fetch_matching(
    predicate: Callable[[Mail], bool], *, timeout_seconds: int = 3
) -> Mail:
    WEBMAIL_URL = os.environ.get("WEBMAIL_URL", "http://localhost:8085")

    def fn():
        data = requests.get(f"{WEBMAIL_URL}/mail", timeout=180).json()
        if "mailItems" not in data:
            raise Exception(f"invalid response from {WEBMAIL_URL}")
        mails = [
            Mail(
                **{k: v for k, v in mail_dict.items() if k in Mail.__dataclass_fields__}
            )
            for mail_dict in data["mailItems"]
        ]
        matching = [mail for mail in mails if predicate(mail)]
        return matching[0]

    wait_result = wait(
        fn, timeout_seconds=timeout_seconds, waiting_for="Mail matching predicate"
    )

    # If there's no match, it will throw a TimeoutError
    return cast(Mail, wait_result)
