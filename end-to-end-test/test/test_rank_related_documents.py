import hashlib
import os
import re
from pathlib import Path

import requests
from playwright.sync_api import BrowserContext, expect

from planner_end_to_end_test import mails, testing_api


def test_rank_related_documents_form_submits_and_documents_present_in_backoffice(
    logged_in_context: BrowserContext,
):
    # given we have a volunteer
    email = "pedro@email.com"
    volunteer_result = testing_api.create_volunteer(
        {
            "first_name": "Pedro",
            "last_name": "Last",
            "email": email,
            "positions": ["Bosun", "Cook"],
        }
    )
    volunteer_id = volunteer_result["id"]

    # we click request documents in backoffice
    page = logged_in_context.new_page()
    page.goto(f"{testing_api.BACKOFFICE_URL}/volunteers/{volunteer_id}/documents/")
    page.locator("role=button", has_text="Request documents").click()
    page.locator("role=button", has_text="Send inquiry").click()

    # user gets a email, we find form link
    mail = mails.fetch_matching(lambda m: email in m.toAddresses)
    link_match = re.search(r"\bhttps://\S+\b", mail.body)
    assert link_match
    link = link_match[0].replace(
        "https://planner-public", testing_api.PUBLIC_BACKEND_URL
    )

    # we fill and sumbit form with 3 documents and validity dates
    page.goto(link)

    data_dir = Path(__file__).parent.parent / "data"
    bs_path = data_dir / "example_BS.pdf"
    mf_path = data_dir / "example_MF.pdf"
    wk_path = data_dir / "example_WK.pdf"

    for file_path, field, m, d, y in [
        (bs_path, "basic_safety", "2", "2", "2032"),
        (mf_path, "medical_fitness", "3", "3", "2033"),
        (wk_path, "watchkeeping_license", "4", "4", "2034"),
    ]:
        page.locator(f'[name="{field}_certificate"]').set_input_files(file_path)
        page.locator(f'[name="{field}_certificate_validity_date_month"]').select_option(
            value=m
        )
        page.locator(f'[name="{field}_certificate_validity_date_day"]').select_option(
            value=d
        )
        page.locator(f'[name="{field}_certificate_validity_date_year"]').select_option(
            value=y
        )

    page.get_by_text("Submit").click()
    expect(page.locator("body")).to_contain_text("Thank you")

    page.goto(f"{testing_api.BACKOFFICE_URL}/volunteers/{volunteer_id}/documents/")

    for field, file_path in [
        ("stcw_basic_safety", bs_path),
        ("medical_fitness", mf_path),
        ("stcw_watchkeeping", wk_path),
    ]:
        link = page.locator("a", has_text=field).get_attribute("href")
        assert link

        if not os.environ.get("DIRECT_MINIO_ACCESS"):
            # if we're not in the test-runner container, we need to adjust the link to access minio
            link = link.replace("minio:9000", "localhost:29446")

        response = requests.get(link)
        response.raise_for_status()
        downloaded_md5 = hashlib.md5(response.content).hexdigest()

        with file_path.open("rb") as input:
            input_md5 = hashlib.md5(input.read()).hexdigest()

        assert downloaded_md5 == input_md5
