import hashlib
from datetime import date
import os
import random
import re
from pathlib import Path

import requests
from playwright.sync_api import BrowserContext, Page, expect

from planner_common.playwright_utils import select_in_choices_js
from planner_end_to_end_test import mails, testing_api
from planner_end_to_end_test.testing_api import BACKOFFICE_URL, PUBLIC_BACKEND_URL


### Submit form in public-frontend(client)
def submit_application_form(
    context: BrowserContext,
    name: str,
    other_certificate: str = "",
    cv_file: Path | None = None,
) -> None:
    page = context.new_page()
    page.goto(PUBLIC_BACKEND_URL)
    page.wait_for_selector("application-form")

    page.locator("#input_first_name").fill(name)
    page.locator("#input_last_name").fill("lastname_" + name)
    page.locator("#input_phone_number").fill("+4912345678910")
    page.locator("#input_email").fill(name + "@example.com")

    application_form = page.locator("application-form")
    date_picker = application_form.locator("date-picker[name='date_of_birth']")

    date_picker.locator("select[name='year']").select_option("1995")
    date_picker.locator("select[name='month']").select_option("10")
    date_picker.locator("select[name='day']").select_option("25")
    application_form.locator("select[id='input_gender']").select_option("none_other")
    # Select element is hidden - not sure if possible to select hidden elements with playwright
    # This seems overengineered, but '.fill' does not work here
    # lets discuss if theres a simpler/better way
    select_in_choices_js(
        application_form.locator("multi-select[name='nationalities']"), "Palau"
    )

    # See comments above nationalities
    select_in_choices_js(
        application_form.locator("language-mapping[name='spoken_languages']"), "Urdu"
    )

    # See comments above nationalities
    select_in_choices_js(
        application_form.locator("multi-select[name='positions']"), "Head of Mission"
    )

    # Other certificates: TODO: to be clarified - in js test this is a dictionary - is this necessary
    if other_certificate != "":
        select_in_choices_js(
            page.locator("multi-select[name=certificates]"), "Other (free text)"
        )
        page.locator("input#input_other_certificate").fill(other_certificate)

    if cv_file is not None:
        page.locator("input[type=file][id='cv_file']").set_input_files(cv_file)

    text_area_counter_motivation = application_form.locator(
        "textarea-counter[id='motivation']"
    )
    text_area_counter_motivation.locator("textarea").fill("motivated!!")

    text_area_counter_qualification = application_form.locator(
        "textarea-counter[id='qualification']"
    )
    text_area_counter_qualification.locator("textarea").fill("qualified!")

    application_form.locator("input[type='submit']").click()

    expect(page.locator("text=Thanks a lot for your interest!")).to_be_visible()


#########################
## Application process ##
#########################


def test_imports_application_to_backoffice_after_submission(
    logged_in_context: BrowserContext,
) -> None:
    random_id = random.randint(10000, 99999)
    peter = f"Peter{random_id}"
    anna = f"Anna{random_id}"
    ## Given 2 Applications
    submit_application_form(logged_in_context, name=peter)
    submit_application_form(logged_in_context, name=anna)

    page = logged_in_context.new_page()
    page.goto(BACKOFFICE_URL + "/applications/?sort=-created")

    expect(page.locator("table")).to_contain_text(peter)
    expect(page.locator("table")).to_contain_text(anna)


def test_processes_applications_with_free_text_other_certificates(
    logged_in_context: BrowserContext,
) -> None:
    random_id = random.randint(10000, 99999)
    charlotte = f"Charlotte{random_id}"

    ## Given an application with other certificate is created
    submit_application_form(
        logged_in_context,
        name=charlotte,
        other_certificate="superspecial qualification cert",
    )

    ## Then the application should appear in the list
    page = logged_in_context.new_page()
    page.goto(BACKOFFICE_URL + "/applications/?sort=-created")
    expect(page.locator("table")).to_contain_text(charlotte)

    link = page.locator("table a", has_text=charlotte)
    link.click()
    pageContent = page.locator("body")
    expect(pageContent).to_contain_text("Personal Data")
    expect(pageContent.locator("input[name=certificates__other]")).to_have_value(
        "superspecial qualification cert"
    )


def test_application_form_links_to_position_description(page: Page):
    page.goto(PUBLIC_BACKEND_URL)
    select_in_choices_js(
        page.locator("multi-select[name=positions]"), "Head of Mission"
    )
    with page.expect_popup() as page_info:
        page.locator(
            "a:near(textarea-counter[name=qualification])", has_text="Head of Mission"
        ).click()
    popup = page_info.value
    # This visits the actual seawatch site and is slow and brittle, but it does catch bugs
    expect(
        popup.locator(
            "p",
            has_text="As the Head of Mission, you are responsible for the team on board.",
        )
    ).to_be_visible()


def test_processes_applications_with_cv(logged_in_context: BrowserContext) -> None:
    ## Given an application with cv is created

    klaus = f"Klaus-{random.randint(10000, 99999)}"

    input_cv = Path(__name__).parent.parent / "data" / "example_CV.pdf"
    with input_cv.open("rb") as input:
        input_md5 = hashlib.md5(input.read()).hexdigest()

    submit_application_form(
        logged_in_context,
        name=klaus,
        cv_file=input_cv,
    )

    ## Then the application should appear in the list
    page = logged_in_context.new_page()
    page.goto(BACKOFFICE_URL + "/applications/?sort=-created")
    page.locator("table a", has_text=klaus).click()

    link = page.locator(
        ".planner--card:has-text('CV or Portfolio') .field:has-text('Uploaded File') a"
    ).get_attribute("href")
    assert link

    if not os.environ.get("DIRECT_MINIO_ACCESS"):
        # if we're not in the test-runner container, we need to adjust the link to access minio
        link = link.replace("minio:9000", "localhost:29446")

    # We would prefer to download the CV with the browser, but it opens in a tab instead of
    # downloading immediately (which is good) and playwright has no way of getting the bytes from
    # that. Luckily, the download is behind a signed link and doesn't require us to be logged in.
    response = requests.get(link)
    response.raise_for_status()
    downloaded_md5 = hashlib.md5(response.content).hexdigest()

    assert downloaded_md5 == input_md5


def test_request_availabilities(logged_in_context: BrowserContext):
    #######################################
    ## GIVEN a volunteer and two operations

    next_year = str(date.today().year + 1)

    name = f"example-{random.randint(10000, 99999)}"
    email = f"{name}@example.com"

    testing_api.create_volunteer(
        {"first_name": name, "last_name": "foo", "email": email, "positions": ["Bosun"]}
    )
    testing_api.set_operations(
        [
            {
                "name": "Jan-Operation",
                "start_date": f"{next_year}-01-01",
                "end_date": f"{next_year}-01-25",
            },
            {
                "name": "Feb-Operation",
                "start_date": f"{next_year}-02-01",
                "end_date": f"{next_year}-02-25",
            },
        ]
    )

    #################################################################
    ## WHEN I click the request availabilities button for the volunteer

    page = logged_in_context.new_page()
    page.goto(f"{BACKOFFICE_URL}/volunteers/?sort=-created")
    page.locator(
        f"tr:has-text('{name}') >> role=button >> text=Request Availabilities"
    ).click()

    ###########################
    ## AND I confirm the dialog
    page.get_by_role("dialog").get_by_role("button", name="Send inquiry").click()
    expect(page.locator(".planner-modal-body")).to_contain_text(
        f"Sent availabilities request to {name} at {email}"
    )

    #########################################
    ## AND I visit the url from the mail that was sent to the volunteer

    mail = mails.fetch_matching(
        lambda m: email in m.toAddresses
        and m.subject == "Operation availability request"
    )
    link_match = re.search(r"\bhttps://\S+\b", mail.body)
    assert link_match
    link = link_match[0].replace("https://planner-public", PUBLIC_BACKEND_URL)
    page.goto(link)

    ###############################
    ## AND I fill out the form
    expect(page.locator("legend", has_text="Availability")).to_be_visible()
    for label, value in [
        (f"January 1st, {next_year} to January 25th, {next_year}", "yes"),
        (f"February 1st, {next_year} to February 25th, {next_year}", "maybe"),
    ]:
        page.get_by_label(label).select_option(value)

    page.locator("role=button", has_text="Submit").click()
    expect(page.locator(":root")).to_contain_text("Thank you")

    #############################################
    ## THEN the availabilities are visible there
    page.goto(f"{BACKOFFICE_URL}/volunteers/?sort=-created")

    # The availabilities can only be viewed in the dialog which shows when clicking
    # "Request Availabilities" right now
    page.locator(
        f"tr:has-text('{name}') >> role=button >> text=Request Availabilities"
    ).click()

    expect(
        page.locator("tr", has_text="Jan-Operation").locator("td:last-child")
    ).to_contain_text("Yes")
    expect(
        page.locator("tr", has_text="Feb-Operation").locator("td:last-child")
    ).to_contain_text("Maybe")
