services:
  db:
    image: postgres:alpine
    expose:
      - "5432"
    environment:
      POSTGRES_PASSWORD: "insecure-db-password"
      POSTGRES_DB: "planner"
      POSTGRES_USER: "planner"
    healthcheck:
      test: "pg_isready -U planner"
      interval: "10s"
      timeout: "3s"
      start_period: "180s"
      retries: 3

  clamav:
    image: docker.io/clamav/clamav:stable
    expose:
      - "3310"

  planner:
    build: &planner-build
      context: ..
      dockerfile: Dockerfile
    environment: &planner-environment
      PLANNER_ADMINS: "admin@planner.test"
      PLANNER_AWS_S3_ACCESS_KEY_ID: "minioadmin"
      PLANNER_AWS_S3_ENDPOINT_URL: "http://minio:9000"
      PLANNER_AWS_S3_SECRET_ACCESS_KEY: "minioadmin"
      PLANNER_AWS_STORAGE_BUCKET_NAME: "planner-files"
      PLANNER_BACKOFFICE_HOSTS: "planner-backoffice,backoffice.localhost"
      PLANNER_BASE_URL: "http://planner-backoffice"
      PLANNER_CLAMD_URL: "tcp://clamav:3310"
      PLANNER_DATABASE_URL: "postgresql://planner:insecure-db-password@db:5432/planner"
      PLANNER_DEFAULT_FROM_EMAIL: "info@planner.test"
      PLANNER_EMAIL_URL: "smtp://mailslurper:2500"
      PLANNER_EMAIL_URL_HOST: "planner-public"
      PLANNER_ENABLE_TESTING_API: "I 100% know what I'm doing by enabling this"
      PLANNER_ERROR_EMAIL_FROM: "error@planner.test"
      PLANNER_EXTERNAL_EMAIL_FROM: "external@planner.test"
      PLANNER_PUBLIC_HOSTS: "planner-public,public.localhost"
      PLANNER_SECRET_KEY: "insecure :("
      PLANNER_SHOW_ENV_WARNING: "dev"
    ports:
      - "9000:9000"
    networks:
      default:
        aliases:
          - planner-backoffice
          - planner-public
    depends_on:
      db:
        condition: service_healthy
    command:
      - sh
      - -c
      - >-
        set -x
        && django-admin migrate
        && django-admin shell -c "from django.contrib.auth.models import User; u = User.objects.get_or_create(username='admin', defaults={'email': 'admin@example.com'})[0]; u.is_superuser = True; u.set_password('admin'); u.save()"
        && waitress-serve --port=9000 seawatch_planner.wsgi:application
    healthcheck:
      test: "curl -sS planner-backoffice:9000"
      interval: "10s"
      timeout: "3s"
      start_period: "180s"
      retries: 3

  worker:
    build: *planner-build
    environment: *planner-environment
    depends_on:
      planner:
        condition: service_healthy
      db:
        condition: service_healthy
    command:
      - sh
      - -c
      - >-
        django-admin procrastinate worker

  mailslurper:
    image: adampresley/mailslurper
    command:
      - sh
      - -c
      - /home/go/src/github.com/mailslurper/mailslurper/cmd/mailslurper/mailslurper -loglevel error
    expose:
      - "2500"
    ports:
      - "8080:8080"
      - "8085:8085"
      - "9080:8080"
      - "9085:8085"
    healthcheck:
      test: "echo -e 'GET /mail HTTP/1.1\nHost: localhost:8085\n\n' | nc localhost:8085 | head -1 | grep -q 200"
      interval: "10s"
      timeout: "3s"
      start_period: "180s"
      retries: 3

  test_runner:
    # playwright doesn't support alpine or bookworm yet
    # https://github.com/microsoft/playwright/issues/13530
    image: python:3.13-bullseye
    depends_on:
      planner:
        condition: service_healthy
      mailslurper:
        condition: service_healthy
      minio-create-bucket:
        condition: service_healthy
    volumes:
      - ..:/test
    working_dir: /test/
    environment:
      PUBLIC_BACKEND_URL: "http://planner-public:9000"
      BACKOFFICE_URL: "http://planner-backoffice:9000"
      WEBMAIL_URL: "http://mailslurper:8085"
      DIRECT_MINIO_ACCESS: "true"
      PLANNER_DATABASE_URL: "sqlite:///:memory:"
    command:
      - sh
      - -c
      - >-
        set -xeu
        && apt-get update && apt-get install -y python3-ldap libldap2-dev libsasl2-dev
        && pip install poetry
        && poetry install
        && poetry run playwright install-deps
        && poetry run playwright install
        && cd end-to-end-test
        && poetry run pytest --verbose --screenshot only-on-failure -ra

  minio:
    image: quay.io/minio/minio:RELEASE.2023-11-06T22-26-08Z
    command:
      - server
      - /data
      - --console-address
      - ":9001"
    ports:
      - "29446:9000"
      - "29447:9001"
    tmpfs:
      - /data
    healthcheck:
      test: ["CMD", "mc", "ready", "local"]
      interval: "5s"
      timeout: "5s"

  minio-create-bucket:
    image: quay.io/minio/mc:RELEASE.2023-11-06T04-19-23Z
    depends_on:
      minio:
        condition: service_healthy
    healthcheck:
      test: "test -e /tmp/done"
    entrypoint: >
      /bin/sh -c "
      /usr/bin/mc config host add minio http://minio:9000 minioadmin minioadmin;
      /usr/bin/mc mb minio/planner-files;
      touch /tmp/done;
      while true; do sleep 100; done;
      "
