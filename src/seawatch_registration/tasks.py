from datetime import datetime, timedelta, UTC
from procrastinate.contrib.django import app

from applications.models import SarApplication
from volunteers.models import Volunteer


# every morning at 04:00
@app.periodic(cron="0 4 * * *")
@app.task
def delete_old_rejected_applications(timestamp: int):
    SarApplication.objects.filter(
        closed__lt=datetime.fromtimestamp(timestamp, tz=UTC) - timedelta(days=365),
        decision=SarApplication.Decision.REJECT,
    ).delete()

    Volunteer.objects.filter(sarapplication=None, sarprofile=None).delete()
