from django.forms import ModelMultipleChoiceField

from .models import Position


class PositionsField(ModelMultipleChoiceField):
    class iterator(ModelMultipleChoiceField.iterator):
        queryset: Position.QuerySet

        def __iter__(self):
            if self.field.empty_label is not None:
                yield ("", self.field.empty_label)
            by_group = self.queryset.by_group()
            for group, positions in by_group.items():
                if group == "":
                    group = "Common Positions"
                yield (group, [self.choice(p) for p in positions])
