import os.path
from typing import Any, List

from countryinfo import CountryInfo
from django import template
from django.core.files import File
from django.db.models import FileField
from django.utils.translation import pgettext
from django_countries.fields import Country

from planner_common import data
from seawatch_registration import html
from volunteers.models import ExperienceLevels

register = template.Library()


language_names = {
    lang["code"]: lang["name"] for lang in data.languages if lang["backoffice"]
}
levels = {
    0: "A1-A2",
    1: "B1-B2",
    2: "C1-C2",
    3: pgettext("As in native speaker", "Native"),
}


@register.filter
def format_spoken_languages(languages):
    return ", ".join(
        sorted(
            f"{language_names[code]} ({levels[points]})"
            for code, points in languages.items()
        )
    )


@register.filter
def format_list_of_spoken_language_objects(languages):
    return format_spoken_languages(
        {language.code: language.points for language in languages}
    )


def _format_nationality(nationality: str) -> str:
    try:
        demonym = CountryInfo(nationality).demonym()
        if demonym is None:
            raise AssertionError(f"CountryInfo doesn't seem to support {nationality}")
        return demonym
    except KeyError:
        return Country(nationality).name


@register.filter
def format_nationalities(nationalities: List[str]) -> str:
    return ", ".join(sorted(_format_nationality(n) for n in nationalities))


@register.filter
def basename(file: FileField | File) -> str:
    return os.path.basename(file.name)


@register.filter
def method(items: List[Any], name: str) -> List[Any]:
    return [getattr(i, name)() for i in items]


@register.filter
def join_comma_and(items: List[Any]):
    length = len(items)
    if length == 0:
        return ""
    items = [str(i) for i in items]
    if length == 1:
        return items[0]
    return " and ".join([", ".join(items[:-1]), items[-1]])


@register.filter
def attr(items: List[Any], name: str) -> List[Any]:
    return [getattr(i, name) for i in items]


@register.filter
def check_experiences(volunteer):
    if volunteer.has_experience == ExperienceLevels.NO_EXPERIENCE:
        return "No"
    elif volunteer.has_experience == ExperienceLevels.HAS_EXPERIENCE:
        return "Yes"
    return "Unknown"


@register.filter()
def has_current_url_name(request, url_name) -> bool:
    if request.resolver_match:
        return request.resolver_match.url_name == url_name
    else:
        return False


@register.simple_tag
def field(*args, **kwargs):
    return html.field(*args, **kwargs)
