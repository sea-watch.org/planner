from django.forms import BoundField
import htpy as h


def field(field: BoundField, *, cls: str | None = None, tooltip: str | None = None):
    # if field.name.lower() == "gender":
    #     breakpoint()
    # tooltip_tag = None
    # if tooltip:
    #     tooltip_id = uuid4().hex
    #     tooltip_tag = h.div()[
    #
    #     ]
    # field.__str__
    return h.div(
        class_=[
            "field",
            cls,
            {
                "errors": bool(field.errors),
                "required": field.field.required,
            },
        ]
    )[
        field.label_tag(),
        h.small[field.help_text] if field.help_text else "",
        str(field),
        str(field.errors),
        tooltip,
    ]
