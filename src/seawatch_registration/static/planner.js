///////////////////////
// HTMX Error Listeners

document.body.addEventListener("htmx:responseError", (event) => {
  console.log("server response error", event);
  alert("Server response error. Please reload the page and try again.");
});
document.body.addEventListener("htmx:sendError", (event) => {
  console.log("send error", event);
  alert("Error sending rqeuest to the server. Please reload the page and try again.");
});

/////////////////////////
// Dialog close on escape

document.body.addEventListener("keydown", function (e) {
  if (e.key == "Escape") {
    for (const modal of document.querySelectorAll(".planner-modal.visible")) {
      modal.classList.remove("visible");
    }
  }
});

///////////////////////////
// Custom elements

class ChoicesJS extends HTMLSelectElement {
  constructor() {
    super();
    this._choices = new window.Choices(this, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      shouldSort: false,
    });
    this.dispatchEvent(new Event("initialized"));
  }
}
customElements.define("choices-js", ChoicesJS, { extends: "select" });

class AutosavingCertificates extends HTMLElement {
  get _select() {
    return this.querySelector("select");
  }
  get _otherLabel() {
    return this.querySelector("label.other-certificates");
  }
  get _otherInput() {
    return this.querySelector("input.other-certificates");
  }

  constructor() {
    super();

    // Setup choices.js
    const label = this._select.labels[0];
    if (!label.id) {
      // Generate an id real quick
      var arr = new Uint8Array(20);
      window.crypto.getRandomValues(arr);
      label.id = Array.from(arr, (d) => d.toString(16).padStart(2, "0")).join("");
    }
    this._choices = new window.Choices(this._select, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      allowHTML: false,
      shouldSort: false,
      labelId: label.id,
    });

    // Update "other" visibility depending on selected elements
    this._select.addEventListener("change", this._update_other_visibility.bind(this));
    this._select.addEventListener(
      "initialized",
      this._update_other_visibility.bind(this),
      { once: true },
    );

    // auto-save the new value on change or enter
    this._select.addEventListener(
      "change",
      this._submit.bind(this, ".choices-container"),
    );
    this._otherInput.addEventListener(
      "change",
      this._submit.bind(this, ".input-container"),
    );
    this._update_other_visibility();
  }

  _update_other_visibility() {
    if (this._choices.getValue().find((e) => e.value == "other")) {
      // show
      this._otherInput.removeAttribute("disabled");
      this._otherInput.removeAttribute("aria-hidden");
      this._otherLabel.removeAttribute("aria-hidden");
    } else {
      this._otherInput.setAttribute("disabled", "");
      this._otherInput.setAttribute("aria-hidden", "true");
      this._otherLabel.setAttribute("aria-hidden", "true");
    }
  }

  _resetIndicators() {
    Array.from(this.querySelectorAll(".autosave-indicator")).forEach((indicator) => {
      indicator.removeAttribute("style");
    });
  }

  _make_visible(/** @type Element */ e) {
    e.style.opacity = 1;
    e.style.visibility = "visible";
  }

  _reportError(container, errorOrResponse) {
    this._resetIndicators();
    this._make_visible(this.querySelector(`${container} .autosave-indicator-error`));
    console.log(errorOrResponse);
    alert("Error saving certificates. Please reload the page and try again");
  }

  _reportSuccess(container) {
    this._resetIndicators();
    this._make_visible(this.querySelector(`${container} .autosave-indicator-saved`));
    setTimeout(() => {
      this.querySelector(`${container} .autosave-indicator-saved`).removeAttribute(
        "style",
      );
    }, 3000);
  }

  _submit(container) {
    this._resetIndicators();
    this._make_visible(this.querySelector(`${container} .autosave-indicator-saving`));
    const responseP = this._doFetch();

    responseP.catch((error) => this._reportError(container, error));

    responseP.then((response) => {
      if (response.ok) {
        this._reportSuccess(container);
      } else {
        this._reportError(container, response);
      }
    });
  }

  _doFetch() {
    const /** @type string[] */ selectedCertificates = this._choices.getValue(true);

    return fetch("autosave/", {
      method: "POST",
      headers: {
        "X-CSRFToken": document.body.dataset.csrfToken,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        certificates: selectedCertificates.map((cert) =>
          // If the "other" input has a value, we append it to the "other" entry in the multiselect,
          // otherwise we just leave a potential "other" alone.
          cert == "other" && this._otherInput.value
            ? `other: ${this._otherInput.value}`
            : cert,
        ),
      }),
      credentials: "same-origin",
    });
  }
}
customElements.define("autosaving-certificates", AutosavingCertificates);
