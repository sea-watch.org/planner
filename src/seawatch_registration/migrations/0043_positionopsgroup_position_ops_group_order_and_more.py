# Generated by Django 5.1.2 on 2024-10-25 19:11

import django.db.models.deletion
import seawatch_registration.models
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("seawatch_registration", "0042_split_2nd_3rd_engineer"),
    ]

    operations = [
        migrations.CreateModel(
            name="PositionOpsGroup",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("title", seawatch_registration.models.ShortTextField()),
                ("order", models.IntegerField(default=0)),
            ],
        ),
        migrations.AddField(
            model_name="position",
            name="ops_group_order",
            field=models.IntegerField(blank=True, default=0),
        ),
        migrations.AddField(
            model_name="position",
            name="ops_group",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.SET_DEFAULT,
                to="seawatch_registration.positionopsgroup",
            ),
        ),
    ]
