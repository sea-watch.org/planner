from __future__ import annotations

from typing import TYPE_CHECKING, ClassVar

from django import forms
from django.contrib.auth.models import User
from django.core import validators
from django.db import models
from django.db.models.fields import BooleanField
from django.utils.translation import gettext_lazy as _

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager

    from applications.models import SarApplicationManager
    from volunteers.models import SarProfile


class ShortTextField(models.TextField):
    description = _("TextField that uses a forms.TextInput by default")

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                **({} if self.choices is not None else {"widget": forms.TextInput}),
                **kwargs,
            }
        )


class URLField(ShortTextField):
    """like django.db.models.fields.URLField, but backed by text, not varchar"""

    default_validators = [validators.URLValidator()]
    description = _("URL")

    def formfield(self, **kwargs):
        return super().formfield(
            **{
                "form_class": forms.URLField,
                **kwargs,
            }
        )


class PositionOpsGroup(models.Model):
    """Internal grouping, currently for operation details page"""

    title = ShortTextField()
    order = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.title


class Position(models.Model):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        ordering = ["key"]

    id = models.AutoField(primary_key=True)

    key = ShortTextField(unique=True)
    name = ShortTextField(verbose_name=_("Name"))

    open_for_applications = BooleanField()
    """open_for_applications determines whether the position is considered an "additional positon" or not"""

    group = ShortTextField(blank=True, default="")

    ops_group = models.ForeignKey(
        PositionOpsGroup,
        on_delete=models.SET_DEFAULT,
        null=True,
        blank=True,
        default=None,
    )
    ops_group_order = models.IntegerField(default=0, blank=True)

    if TYPE_CHECKING:
        sarprofile_set: RelatedManager[SarProfile]
        sarapplication_set: SarApplicationManager
        userprofile_set: RelatedManager[UserProfile]

    def __str__(self):
        return self.name

    class QuerySet(models.QuerySet["Position"]):
        def by_group(self) -> dict[str, list[Position]]:
            result: dict[str, list[Position]] = {}
            for position in self.order_by("name"):
                group = result.setdefault(position.group, [])
                group.append(position)
            return result

    if TYPE_CHECKING:

        class PositionManager(QuerySet):
            def by_group(self) -> dict[str, list[Position]]: ...

    objects: ClassVar[  # pyright: ignore [reportIncompatibleVariableOverride]
        PositionManager
    ] = models.Manager.from_queryset(QuerySet)()  # pyright: ignore [reportAssignmentType]


class UserProfile(models.Model):
    def __str__(self):
        return f"User profile for {self.user.username}"

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    positions = models.ManyToManyField(Position)
