from django.urls import reverse
from playwright.sync_api import BrowserContext, expect

from applications.tests.factories import SarApplicationFactory
from planner_common.feature_flags import features
from volunteers.tests.factories import VolunteerFactory


def test_escape_key_closes_modal(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )

    if features["keep_rejected_applications"]:
        page.locator("role=button").get_by_text("Reject for all positions").click()
    else:
        page.locator("role=button").get_by_text("Delete application").click()

    expect(page.locator("#planner-modal-reject-application")).to_be_visible()
    page.keyboard.press("Escape")
    expect(page.locator("#planner-modal-reject-application")).not_to_be_visible()
