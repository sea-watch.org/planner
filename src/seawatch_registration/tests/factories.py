from typing import Callable, ClassVar, Iterable
from typing import Sequence as SequenceType
from typing import Type, Union, cast

from django.contrib.auth.models import Permission, User
from django.utils.text import slugify
from django.views import View
from factory import Faker, LazyAttribute, Maybe, Sequence, SubFactory, post_generation  # type: ignore
from factory.django import DjangoModelFactory

from planner_common.test.factories import DeprecateDirectCallMetaclass

from ..models import Position, PositionOpsGroup


class UserFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = User
        django_get_or_create = ("email",)
        skip_postgeneration_save = True  # will be default soon

    create: ClassVar[Callable[..., User]]
    create_batch: ClassVar[Callable[..., list[User]]]
    build: ClassVar[Callable[..., User]]
    build_batch: ClassVar[Callable[..., list[User]]]

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    username = Faker("user_name")
    is_active = True
    is_superuser = False

    @post_generation
    def permission(obj: User, create: bool, extracted: str, **kwargs):  # type: ignore
        if create and extracted is not None:
            obj.user_permissions.set([get_permission(extracted)])

    @post_generation
    def permissions(obj: User, create: bool, extracted: SequenceType[str], **kwargs):  # type: ignore
        if create and extracted is not None:
            obj.user_permissions.set(map(get_permission, extracted))

    @post_generation
    def view_permissions(
        obj: User,  # type: ignore
        create: bool,
        view: Union[Type[View], Iterable[Type[View]]],
        **kwargs,
    ):
        """This is meant to work with django.contrib.auth.mixins.PermissionRequiredMixin"""
        if create and view is not None:
            views = [view] if type(view) is type else view

            permissions = []
            for v in cast(Iterable[Type[View]], views):
                view_permissions = getattr(v, "permission_required", [])
                if isinstance(view_permissions, str):
                    view_permissions = [view_permissions]
                permissions.extend(view_permissions)
            obj.user_permissions.add(*map(get_permission, permissions))


def get_permission(permission_str: str) -> Permission:
    if "." not in permission_str:
        return Permission.objects.get(codename=permission_str)
    else:
        app_label, codename = permission_str.split(".")
        return Permission.objects.get(
            content_type__app_label=app_label, codename=codename
        )


class PositionOpsGroupFactory(
    DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass
):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = PositionOpsGroup

    create: ClassVar[Callable[..., PositionOpsGroup]]
    create_batch: ClassVar[Callable[..., list[PositionOpsGroup]]]
    build: ClassVar[Callable[..., PositionOpsGroup]]
    build_batch: ClassVar[Callable[..., list[PositionOpsGroup]]]

    title = Faker("word")
    order = Faker("random_int", min=0, max=10000)


class PositionFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Position

    class Params:
        name_base = Faker("job")
        name_uniquifier = Sequence(lambda i: str(i))

        with_ops_group = Faker("boolean", chance_of_getting_true=80)

    create: ClassVar[Callable[..., Position]]
    create_batch: ClassVar[Callable[..., list[Position]]]
    build: ClassVar[Callable[..., Position]]
    build_batch: ClassVar[Callable[..., list[Position]]]

    name = LazyAttribute(lambda p: f"{p.name_base} ({p.name_uniquifier})")
    key = LazyAttribute(lambda p: slugify(p.name))
    open_for_applications = True

    ops_group = Maybe(
        "with_ops_group",
        SubFactory(PositionOpsGroupFactory),  # type: ignore
    )
