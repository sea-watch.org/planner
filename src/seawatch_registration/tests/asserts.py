from inspect import isclass
from typing import Callable

from django.core.exceptions import PermissionDenied
from django.http import HttpRequest, HttpResponse
from django.test.client import RequestFactory
from django.views import View
from pytest import raises

from .factories import UserFactory


def assert_requires_profile(rf: RequestFactory, view):
    """Asserts that the view requires the logged-in user to have a profile

    The view argument can be either a subclass of django.views.View or a classic
    django view function.
    """
    request = rf.get("ignored_url")
    request.user = UserFactory.create(profile=None)

    view_func: Callable[[HttpRequest], HttpResponse]
    if isclass(view) and issubclass(view, View):
        view_func = view.as_view()  # type: ignore
    else:
        view_func = view  # type: ignore

    with raises(PermissionDenied):
        view_func(request)
