from pytest import mark

from seawatch_registration.management.commands.load_ngos_and_ships import (
    Command as LoadDataCommand,
)
from volunteers.models import NGO, Ship


@mark.django_db
def test_does_not_add_anything_by_default():
    LoadDataCommand().handle(go=False)
    assert not NGO.objects.all().exists()
    assert not Ship.objects.all().exists()


@mark.django_db
def test_does_add_entries_with_go():
    LoadDataCommand().handle(go=True)
    assert NGO.objects.filter(name="Mission Lifeline").exists()
    assert Ship.objects.filter(name="Sea-Watch 3").exists()


@mark.django_db
def test_is_idempotent():
    cmd = LoadDataCommand()
    cmd.handle(go=True)
    cmd.handle(go=True)
    assert Ship.objects.filter(name="Sea-Watch 3").exists()


@mark.django_db
def test_reports_missing_ships(capsys):
    cmd = LoadDataCommand()
    cmd.handle(go=True)
    NGO.objects.filter(name="Sea-Watch").delete()
    Ship.objects.filter(name="Sea-Watch 3").delete()
    cmd.handle(go=False)

    expected = "\n".join(
        [
            "Would add these NGOs:",
            "\tSea-Watch",
            "",
            "Would add these Ships:",
            "\tSea-Watch 3",
        ]
    )
    assert capsys.readouterr().out[: len(expected)] == expected
