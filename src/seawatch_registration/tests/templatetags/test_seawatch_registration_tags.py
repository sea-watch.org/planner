from pytest import mark

from seawatch_registration.templatetags.seawatch_registration_tags import (
    format_nationalities,
    join_comma_and,
)


@mark.parametrize(
    "input,expected",
    [
        ([], ""),
        (["x"], "x"),
        (["x", "y"], "x and y"),
        (["x", "y", "z"], "x, y and z"),
    ],
)
def test_join_comma_and(input, expected):
    assert join_comma_and(input) == expected


def test_format_nationalities_supports_andorra():
    # Andorra is not supported by countryinfo
    assert format_nationalities(["ad"]) == "Andorra"
