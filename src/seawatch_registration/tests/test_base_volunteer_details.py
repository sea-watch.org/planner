import pytest
from pytest import mark
from volunteers.tests.factories import VolunteerFactory
from playwright.sync_api import BrowserContext, expect


@pytest.fixture
def volunteer():
    return VolunteerFactory.create()


@mark.django_db
def test_toggle_navbar(
    volunteer, logged_in_context: BrowserContext, backoffice_url: str
):
    page = logged_in_context.new_page()
    page.set_viewport_size({"width": 1920, "height": 1080})
    page.goto(f"{backoffice_url}/volunteers/{volunteer.id}")
    sideBarToggle = page.locator('//*[@id="sideBarToggleButton"]')

    def check_all_visible(should_be_visible: bool, locator: str, element_count: int):
        elements = page.locator(locator)
        for element in range(element_count):
            if should_be_visible:
                expect(elements.nth(element)).to_be_visible()
            else:
                expect(elements.nth(element)).not_to_be_visible()

    check_all_visible(True, ".description", 5)
    sideBarToggle.click()
    check_all_visible(False, ".description", 5)

    page.set_viewport_size({"width": 1400, "height": 1080})
    page.reload()

    check_all_visible(False, ".description", 5)
    sideBarToggle.click()
    check_all_visible(True, ".description", 5)
