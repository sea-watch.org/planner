from urllib.parse import parse_qs, urlparse
from playwright.sync_api import Page
from seawatch_registration.models import Position
from seawatch_registration.tests.factories import PositionFactory


def ensure_positions_exists(keys):
    # XXX: The factories should ideally create the positions itself but it was moved from the
    # public backend, so that is still TODO
    for key in keys:
        if key == "rib-driver":
            key = "rhib-driver"
        if not Position.objects.filter(key=key).exists():
            PositionFactory.create(key=key)


def wait_for_queryarg(page: Page, key: str, value: str) -> None:
    def matcher(url: str) -> bool:
        parsed = urlparse(url)
        qs = parse_qs(parsed.query)
        return qs.get(key) == [value]

    page.wait_for_url(matcher)


def htmlnormalize_text(string: str):
    return " ".join(s.strip() for s in string.split("\n") if s.strip())
