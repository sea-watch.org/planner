from pytest import mark

from seawatch_registration.models import Position, UserProfile
from seawatch_registration.tests.factories import PositionFactory, UserFactory


@mark.django_db
def test_user_profile_string():
    profile = UserProfile(user=UserFactory.create(username="anna"))
    assert str(profile) == "User profile for anna"


@mark.django_db
def test_position_by_group():
    # We probably have the default positions at this point, let's just get rid of them
    Position.objects.all().delete()

    without_1 = PositionFactory.create(name="1", group="")
    without_2 = PositionFactory.create(name="2", group="")
    a_1 = PositionFactory.create(name="a1", group="a")
    a_2 = PositionFactory.create(name="a2", group="a")
    b = PositionFactory.create(group="b")

    assert Position.objects.by_group() == {
        "": [without_1, without_2],
        "a": [a_1, a_2],
        "b": [b],
    }
