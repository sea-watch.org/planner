from http import HTTPStatus

from django.test import Client
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains

from seawatch_registration.models import Position
from seawatch_registration.tests.factories import PositionFactory, UserFactory


@mark.django_db
def test_PositionAdmin_allows_creation(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True, permissions=["seawatch_registration.add_position"]
        )
    )
    response = backoffice_client.post(
        reverse("admin:seawatch_registration_position_add"),
        {"key": "test-position", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    position = Position.objects.get(key="test-position")
    assert position.name == "Test Position"


@mark.django_db
def test_PositionAdmin_rejects_modifications_to_key(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True, permissions=["seawatch_registration.change_position"]
        )
    )
    position = PositionFactory.create(key="foo", open_for_applications=False)

    response = backoffice_client.post(
        reverse(
            "admin:seawatch_registration_position_change",
            kwargs={"object_id": position.pk},
        ),
        {"key": "bar", "name": "whatever"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND

    position.refresh_from_db()
    assert position.key == "foo"


@mark.django_db
def test_PositionAdmin_only_shows_additional_positions(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True, permissions=["seawatch_registration.view_position"]
        )
    )
    should_not_show = PositionFactory.create(open_for_applications=True)
    should_show = PositionFactory.create(open_for_applications=False)
    response = backoffice_client.get(
        reverse("admin:seawatch_registration_position_changelist"),
    )
    assertContains(response, should_show.name, html=True)
    assertNotContains(response, should_not_show.name, html=True)


@mark.django_db
def test_PositionAdmin_forms_dont_include_additional_positions_field(
    backoffice_client: Client,
):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True,
            permissions=[
                f"seawatch_registration.{x}_position" for x in ["add", "change"]
            ],
        )
    )
    response = backoffice_client.get(
        reverse("admin:seawatch_registration_position_add")
    )
    assertNotContains(response, "open_for_applications")


@mark.django_db
def test_PositionAdmin_doesnt_allow_changing_positions_that_are_open_for_applications(
    backoffice_client: Client,
):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True, permissions=["seawatch_registration.change_position"]
        )
    )
    position = PositionFactory.create(key="k", name="n", open_for_applications=True)
    response = backoffice_client.post(
        reverse(
            "admin:seawatch_registration_position_change",
            kwargs={"object_id": position.id},
        ),
        {"key": "test-position", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.FOUND
    position.refresh_from_db()
    assert position.key == "k"
    assert position.name == "n"


@mark.django_db
def test_PositionAdmin_validates_key_as_slug(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(
            is_staff=True, permissions=["seawatch_registration.add_position"]
        )
    )
    response = backoffice_client.post(
        reverse("admin:seawatch_registration_position_add"),
        {"key": "invalid key", "name": "Test Position"},
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.OK
    assert response.context["adminform"].errors == {
        "key": [
            "Enter a valid “slug” consisting of letters, numbers, underscores or hyphens."
        ]
    }
