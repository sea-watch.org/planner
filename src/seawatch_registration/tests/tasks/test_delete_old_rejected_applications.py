from datetime import UTC, datetime, timedelta
from pytest import mark

from applications.models import SarApplication
from applications.tests.factories import SarApplicationFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import SarProfileFactory
from ...tasks import delete_old_rejected_applications

fake_now = datetime(2024, 7, 3, 12, 13, 14, tzinfo=UTC)
cutoff = fake_now - timedelta(days=365)
recently = cutoff + timedelta(days=1)
long_ago = cutoff - timedelta(days=1)


@mark.django_db
def test_all_positions_rejected():
    SarApplicationFactory.create(
        closed=long_ago, decision=SarApplication.Decision.REJECT
    )

    delete_old_rejected_applications(round(fake_now.timestamp()))

    assert not SarApplication.objects.exists()
    assert not Volunteer.objects.exists()


@mark.django_db
def test_keeps_volunteers_with_profile():
    sa = SarApplicationFactory.create(
        closed=long_ago, decision=SarApplication.Decision.REJECT
    )
    SarProfileFactory.create(volunteer=sa.volunteer)

    delete_old_rejected_applications(round(fake_now.timestamp()))

    assert not SarApplication.objects.filter(id=sa.id).exists()
    assert Volunteer.objects.exists()


@mark.django_db
def test_keeps_volunteers_with_open_application():
    sa = SarApplicationFactory.create(
        closed=long_ago, decision=SarApplication.Decision.REJECT
    )
    SarApplicationFactory.create(is_closed=False, volunteer=sa.volunteer)

    delete_old_rejected_applications(round(fake_now.timestamp()))

    assert not SarApplication.objects.filter(id=sa.id).exists()
    assert Volunteer.objects.exists()


@mark.django_db
@mark.parametrize(
    "_name, factory_kwargs",
    [
        (
            "recently rejected",
            {"closed": recently, "decision": SarApplication.Decision.REJECT},
        ),
        ("not closed", {"closed": None, "decision": SarApplication.Decision.REJECT}),
        ("accepted", {"closed": long_ago, "decision": SarApplication.Decision.ACCEPT}),
        ("new", {"closed": None, "decision": SarApplication.Decision.NEW}),
        (
            "in-progress",
            {"closed": None, "decision": SarApplication.Decision.IN_PROGRESS},
        ),
        # closed but open shouldn't exist, but let's make sure the code doesnt delete it either way
        (
            "expired-but-new",
            {"closed": long_ago, "decision": SarApplication.Decision.NEW},
        ),
        (
            "expired-but-in-progress",
            {"closed": long_ago, "decision": SarApplication.Decision.IN_PROGRESS},
        ),
    ],
)
def test_keeps_relevant_sarapplications(_name, factory_kwargs):
    sa = SarApplicationFactory.create(**factory_kwargs)

    delete_old_rejected_applications(round(fake_now.timestamp()))

    assert SarApplication.objects.filter(id=sa.id).exists()
    assert Volunteer.objects.filter(id=sa.volunteer.id).exists()
