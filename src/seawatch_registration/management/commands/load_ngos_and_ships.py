from django.core.management import BaseCommand

from volunteers.models import NGO, Ship


def get_from_fra():
    """Helper function/documentation on where the data is from.

    This crawls the fra.europa.eu website and thus shouldn't be used in any regular functionality.
    """
    import bs4
    import requests

    try:
        get_ipython()  # type: ignore  # noqa
    except NameError:
        raise Exception("This is supposed to only be run in ipython") from None

    page = requests.get(
        "https://fra.europa.eu/en/publication/2022/june-2022-update-ngo-ships-sar-activities#publication-tab-1",
        timeout=180,
    )
    html = bs4.BeautifulSoup(page.content, features="html.parser")
    tbody = html.find("tbody")
    assert isinstance(  # nosec the assert is 90% type checker and 10% consistency check
        tbody, bs4.Tag
    )
    rows = [
        e
        for e in tbody.contents
        if isinstance(e, bs4.Tag) and e.name == "tr" and e.text.strip()
    ]
    rows_cells = [r(["th", "td"]) for r in rows]

    def fix_ngo(ngo: str) -> str:
        for org in [
            "Sea-Watch",
            "Mediterranea",
            "Médecins Sans Frontières",
            "SOS Méditerranée",
        ]:
            if ngo.startswith(org):
                return org
        return ngo

    # XXX: We're calling them ships here, but there are planes and stuff in the list.

    def fix_ship(ship: str) -> str:
        # the ships have a lot of \n\n\t\t\t\t or stuff like that
        ship = " ".join(ship.split())

        if ship.startswith("Sea Watch"):
            ship = f"Sea-Watch{ship.removeprefix('Sea Watch')}"
        if ship == "Sea-Watch":
            ship = "Sea-Watch 1"

        return ship

    ngos = sorted(list({fix_ngo(cells[2].text.strip()) for cells in rows_cells}))
    ships = sorted(list({fix_ship(cells[0].text.strip()) for cells in rows_cells}))

    return {"ngos": ngos, "ships": ships}


data = {
    "ngos": [
        "Association Pilotes Volontaires",
        "Humanitarian Maritime Rescue Association (SMH)",
        "Jugend Rettet",
        "LifeBoat",
        "M.V. Louise Michel",
        "Mare Liberum",
        "Mediterranea",
        "Migrant Offshore Aid Station (MOAS)",
        "Mission Lifeline",
        "Médecins Sans Frontières",
        "ProActiva Open Arms",
        "RESQSHIP",
        "Refugee Rescue",
        "ResQ – People Saving People",
        "SOS Méditerranée",
        "Save the Children",
        "Sea-Eye",
        "Sea-Watch",
    ],
    "ships": [
        "Aita Mari",
        "Alex Mediterranea",
        "Aquarius",
        "Astral",
        "Aurora",
        "Bourbon Argos (operated by MSF Belgium)",
        "Colibri 2 (reconnaissance aircraft)",
        "Dignity I (operated by MSF Spain)",
        "Eleonore",
        "Geo Barents",
        "Golfo Azzurro",
        "Iuventa",
        "Josefa (used only for monitoring and observation in SAR zones)",
        "Lifeline",
        "Louise Michel",
        "Mare Jonio",
        "Mare Liberum (used only for human rights monitoring)",
        "Minden",
        "Mo Chara",
        "Moonbird (reconnaissance aircraft)",
        "Nadir",
        "Ocean Viking",
        "Open Arms",
        "Phoenix",
        "ResQpeople",
        "Rise Above",
        "Sea-Eye 4",
        "Sea-Watch 1",
        "Sea-Watch 2",
        "Sea-Watch 3",
        "Sea-Watch 4 (In collaboration with Médecins Sans Frontières until 2020)",
        "Seabird 1 (reconnaissance aircraft)",
        "Seabird 2 (reconnaissance aircraft)",
        "The Sea-Eye",
        "The Seefuchs",
        "Vos Hestia",
        "Vos Prudence (operated by MSF Belgium)",
        "Alan Kurdi (previously ‘Professor Albrecht Penck’)",
    ],
}


class Command(BaseCommand):
    help = "Load static base data"

    def add_arguments(self, parser):
        parser.add_argument(
            "--go", action="store_true", help="Actually perform the load"
        )

    def handle(self, *args, **options):
        ngos_in_db = {ngo.name for ngo in NGO.objects.all()}
        ships_in_db = {ship.name for ship in Ship.objects.all()}

        ngos_to_add = (set(data["ngos"]) | {"Other"}) - ngos_in_db
        ships_to_add = (set(data["ships"]) | {"Other"}) - ships_in_db

        if options["go"]:
            NGO.objects.bulk_create(NGO(name=name) for name in ngos_to_add)
            Ship.objects.bulk_create(Ship(name=name) for name in ships_to_add)
        else:
            if ngos_to_add:
                print(
                    "\n".join(
                        [
                            "Would add these NGOs:",
                            *[f"\t{ngo}" for ngo in ngos_to_add],
                            "",
                        ]
                    )
                )
            if ships_to_add:
                print(
                    "\n".join(
                        [
                            "Would add these Ships:",
                            *[f"\t{ship}" for ship in ships_to_add],
                            "",
                        ]
                    )
                )
            if ngos_to_add or ships_to_add:
                print("Use --go to do it")
            else:
                print("Nothing to do :)")
