from django import template
from django.utils.safestring import mark_safe
from inline_static.templatetags.inline_static_tags import inline_staticfile

register = template.Library()


@register.simple_tag
def inline_svg(name):
    # This only loads static files, i.e. files that are not derived from user input, so we're good
    # to use mark_safe here
    return mark_safe(inline_staticfile(name))  # nosec b308 b700
