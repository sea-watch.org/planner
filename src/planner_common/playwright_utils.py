from playwright.sync_api import Locator, expect


class SingleChoicesJS:
    def __init__(self, element: Locator):
        self._element = element
        self._choices = self._element.locator(".choices")

    def select(self, option: str) -> None:
        self._choices.click()
        self._element.get_by_text(option).click()
        # If there's a remove button, the test is suffixed by "Remove Item" so we only check the
        # start
        expect(self._choices.locator(".choices__list--dropdown")).not_to_be_visible()
        expect(self._choices.locator(".choices__item:visible")).to_contain_text(option)

    def deselect(self) -> None:
        self._choices.get_by_role("button", name="Remove item").click()
        expect(self._choices.locator("select")).to_have_value("")


class MultiChoicesJS:
    def __init__(self, element: Locator):
        self.element = element

    def deselect(self, option: str) -> None:
        if (
            not self.element.locator("css=.choices__inner .choices__item")
            .get_by_text(option)
            .is_visible()
        ):
            raise AssertionError("expected option to be selected")
        self.element.locator(
            ".choices__inner .choices__list .choices__item", has_text=option
        ).get_by_role("button").click()

    def deselect_all(self) -> None:
        button_loc = self.element.locator(
            ".choices__inner .choices__list .choices__button[aria-label^=Remove]"
        ).first
        expect(button_loc).to_be_visible()
        while button_loc.is_visible():
            button_loc.click()

    def select(self, option: str) -> None:
        selected_loc = self.element.locator(
            "css=.choices__inner .choices__item", has_text=option
        )
        if selected_loc.is_visible():
            raise AssertionError("expected option to not already be selected")
        input_box = self.element.locator("input.choices__input >> visible=true")
        input_box.focus()
        input_box.type(option)
        option_loc = self.element.locator(
            "css=.choices__list--dropdown .choices__item", has_text=option
        )
        expect(option_loc).to_be_visible()
        option_loc.click()

        expect(selected_loc).to_be_visible()
        input_box.blur()

    def select_value(self, value: str) -> None:
        selected_loc = self.element.locator(
            f"css=.choices__inner .choices__item[data-value='{value}']"
        )
        if selected_loc.is_visible():
            raise AssertionError("expected option to not already be selected")
        input_box = self.element.locator("input.choices__input >> visible=true")
        input_box.focus()
        option_loc = self.element.locator(
            f"css=.choices__list--dropdown .choices__item[data-value='{value}']"
        )
        option_loc.click()

        expect(selected_loc).to_be_visible()
        self.element.page.focus("body")


def select_in_choices_js(element: Locator, option_text: str) -> None:
    if "choices" in element.evaluate("e => Array.from(e.classList)"):
        choices = element
    else:
        choices = element.locator(".choices")
    match choices.evaluate("e => e.dataset.type"):
        case "select-one":
            SingleChoicesJS(element).select(option_text)
        case "select-multiple":
            MultiChoicesJS(element).select(option_text)
