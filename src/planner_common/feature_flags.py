import os
import warnings

__all__ = ["features", "production_disabled_features"]

# Put new features flags in here, commit them as False and set them to True while working
# OR (experimental) set the PLANNER_FEATURES=my_feature;my_other_feature in the repo-root .env to
# enable features called my_feature and my_other_feature locally
production_disabled_features = {
    "application_wizard": False,  # gitlab #262 # This is actually an effort to get rid of the public_client now...
    "operation_planning": False,  # gitlab #276
    "check_if_document_is_present": False,  # gitlab #274
    "daisy_ui_migration": False,  # Added feature flag for new feature
}

# To enable a flag in production, add it here
production_enabled_features = [
    "keep_rejected_applications",  # gitlab #245
]


# Use this variable to check whether a flag is enabled
features = production_disabled_features | {
    flag: True for flag in production_enabled_features
}

for feature in os.environ.get("PLANNER_FEATURES", "").split(";"):
    if feature == "":
        # ''.split(';') == [''], which is annoying here
        continue

    if feature in features:
        features[feature] = True
    else:
        warnings.warn(f"Unknown feature {feature}")
