import json
from pathlib import Path


def load_json_file(name):
    with (Path(__file__).parent / f"{name}.json").open("r") as f:
        return json.load(f)


availability = load_json_file("availability")
initial_application = load_json_file("initialApplication")
