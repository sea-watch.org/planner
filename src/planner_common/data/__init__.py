import json
from importlib.resources import files

_data = files("planner_common.data")

with _data.joinpath("certificates.json").open("r") as f:
    certificates = json.load(f)

with _data.joinpath("languages.json").open("r") as f:
    languages = json.load(f)
