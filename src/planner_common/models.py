from django.db import models
from django.utils.translation import gettext_lazy as _


class Gender(models.TextChoices):
    MALE = "male", _("Male")
    FEMALE = "female", _("Female")
    NONE_OTHER = "none_other", _("None/Other")
    PREFER_NOT_SAY = "prefer_not_say", _("Prefer not to say")
