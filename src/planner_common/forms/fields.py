from typing import Any

from django.forms import MultipleChoiceField

from .. import data
from .widgets import CertificatesWidget

_certificates_choices = [
    (g["group_label"], [(e["value"], e["label"]) for e in g["certificates"]])
    for g in data.certificates
]
_certificates_choices[0][1].append(("other", "Other (free text)"))


class CertificatesField(MultipleChoiceField):
    widget = CertificatesWidget

    def __init__(self, *, choices=_certificates_choices, **kwargs):
        # ModelForm passes those for JSONFields
        kwargs.pop("encoder", None)
        kwargs.pop("decoder", None)

        super().__init__(choices=choices, **kwargs)

    def valid_value(self, value: Any) -> bool:
        if str(value).startswith("other: "):
            return True
        else:
            return super().valid_value(value)
