import json

from django import forms
from django.http.request import QueryDict

from .. import data
from . import ChoicesJSMedia


class ChoicesJSWidget(forms.widgets.SelectMultiple):
    template_name = "planner_common/forms/widgets/choices_js.html"

    Media = ChoicesJSMedia


class CertificatesWidget(forms.SelectMultiple):
    Media = ChoicesJSMedia
    template_name = "planner_common/forms/widgets/certificates.html"

    def value_from_datadict(self, data, files, name):
        # The declared type is different so we explicitly assert
        if not isinstance(data, QueryDict):
            raise AssertionError("We assume that django gives us a query dict here")
        return [
            f"other: {data[f'{name}__other']}" if cert == "other" else cert
            for cert in data.getlist(name)
        ]

    def get_context(self, name, value, attrs):
        value = value if value else []
        other = None
        for idx, cert in enumerate(value):
            if cert.startswith("other: "):
                other = cert
                value[idx] = "other"
        ctx = super().get_context(name, value, attrs)
        if other:
            ctx["other"] = other.removeprefix("other: ")
        return ctx


class LanguagesWidget(forms.TextInput):
    template_name = "planner_common/forms/widgets/languages.html"

    class Media:
        css = {
            "all": [
                *ChoicesJSMedia.css["all"],
                # This should morally be here, but the css is for the new application form (#262)
                # and breaks the backoffice, so we include it there until we can get around to make
                # the css work in both cases (or split it properly)
                # "languages_widget.css",
            ],
        }
        js = ChoicesJSMedia.js

    def get_context(self, name, value, attrs):
        ctx = super().get_context(name, value, attrs)
        if isinstance(value, str):
            value = json.loads(value)
        if value is None:
            value = {}
        ctx["value_json"] = json.dumps(value)
        ctx["available_languages"] = [
            (lang["code"], lang["name"], lang["code"] in value)
            for lang in data.languages
            if lang["backoffice"]
        ]
        return ctx

    def format_value(self, value):
        if isinstance(value, str):
            value = json.loads(value)
        return value
