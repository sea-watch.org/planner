import logging
from typing import Union
from urllib.parse import urlparse

import clamd

logger = logging.getLogger(__name__)


class NullClamd:
    def __init__(self):
        logger.warning("Skipping malware scanning")

    def instream(self, buffer) -> dict[str, tuple[str, str | None]]:
        return {"stream": ("OK", None)}


ClamdOrNull = Union[NullClamd, clamd.ClamdNetworkSocket]


def clamd_from_url(url: str) -> clamd.ClamdNetworkSocket:
    parsed = urlparse(url)
    if parsed.scheme == "unix":
        return clamd.ClamdUnixSocket(path=parsed.path or "/var/run/clamav/clamd.ctl")
    elif parsed.scheme == "tcp":
        return clamd.ClamdNetworkSocket(
            host=parsed.hostname or "127.0.0.1", port=parsed.port or 3310
        )
    else:
        raise Exception(f'Unknown url scheme "{parsed.scheme}" for clamd')
