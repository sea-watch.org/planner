from typing import TypeVar


T = TypeVar("T")


def unwrap(t: T | None) -> T:
    if t is None:
        raise ValueError("unwrap(None)")
    return t
