# This module needs to be imported in conftest. it cannot be a pytest plugin because that way,
# overriding the playwright fixtures doesn't work
import os

import playwright
from pytest import fixture


@fixture(scope="session")
def browser_type_launch_args(
    browser_name: str | None, browser_type_launch_args: dict
) -> dict:
    # Django does this weird check where it errors out it an event loop is running because it
    # assumes it's called from an async context. That's not neccessarily correct though.
    # Specifically, the playwright fixtures create async contexts (and thus an event loop) while the
    # other fixtures are run in the usual context, so we disable the check and hope for the best.
    os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "True"

    if expected_version := os.environ.get("PLANNER_PLAYWRIGHT_VERSION"):
        assert (  # nosec B101
            playwright._repo_version.version == expected_version  # type: ignore
        ), "Mismatched playwright browsers, did you update playwright without fixing the version in flake.nix?"

    return browser_type_launch_args
