import logging
import os
from io import BytesIO

import clamd
from pytest import LogCaptureFixture, mark

from planner_common.clamav import NullClamd, clamd_from_url


def test_NullClamd_just_logs(caplog: LogCaptureFixture):
    cd = NullClamd()
    assert cd
    assert cd.instream(BytesIO(clamd.EICAR)) == {"stream": ("OK", None)}
    assert caplog.record_tuples == [
        (
            "planner_common.clamav",
            logging.WARNING,
            "Skipping malware scanning",
        )
    ]


@mark.skipif(
    os.environ.get("PLANNER_CLAMD_URL") is None,
    reason="needs clamav running, set PLANNER_CLAMD_URL env variable",
)
def test_clamd_or_stub_returns_stub_if_DEBUG_is_set():
    cd = clamd_from_url(os.environ["PLANNER_CLAMD_URL"])
    assert cd
    assert cd.instream(BytesIO(clamd.EICAR)) == {
        "stream": ("FOUND", "Win.Test.EICAR_HDB-1")
    }
