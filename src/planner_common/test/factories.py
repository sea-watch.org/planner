import dataclasses
import json
from typing import Callable, ClassVar, List, TypeVar
from warnings import warn

import django_countries
from factory import Factory, LazyAttribute, SelfAttribute, SubFactory  # type: ignore
from factory.base import FactoryMetaClass
from factory.faker import Faker

from planner_common import data, schema
from planner_common.models import Gender

available_positions: list[str] = schema.initial_application["properties"]["positions"][
    "items"
]["allOf"][0]["enum"]
assert "master" in available_positions


# fmt: off
available_countries = ['af', 'ax', 'al', 'dz', 'as', 'ad', 'ao', 'ai', 'aq', 'ag',
 'ar', 'am', 'aw', 'au', 'at', 'az', 'bs', 'bh', 'bd', 'bb', 'by', 'be', 'bz', 'bj',
 'bm', 'bt', 'bo', 'bq', 'ba', 'bw', 'bv', 'br', 'io', 'bn', 'bg', 'bf', 'bi', 'cv',
 'kh', 'cm', 'ca', 'ky', 'cf', 'td', 'cl', 'cn', 'cx', 'cc', 'co', 'km', 'cg', 'cd',
 'ck', 'cr', 'ci', 'hr', 'cu', 'cw', 'cy', 'cz', 'dk', 'dj', 'dm', 'do', 'ec', 'eg',
 'sv', 'gq', 'er', 'ee', 'sz', 'et', 'fk', 'fo', 'fj', 'fi', 'fr', 'gf', 'pf', 'tf',
 'ga', 'gm', 'ge', 'de', 'gh', 'gi', 'gr', 'gl', 'gd', 'gp', 'gu', 'gt', 'gg', 'gn',
 'gw', 'gy', 'ht', 'hm', 'va', 'hn', 'hk', 'hu', 'is', 'in', 'id', 'ir', 'iq', 'ie',
 'im', 'il', 'it', 'jm', 'jp', 'je', 'jo', 'kz', 'ke', 'ki', 'kw', 'kg', 'la', 'lv',
 'lb', 'ls', 'lr', 'ly', 'li', 'lt', 'lu', 'mo', 'mg', 'mw', 'my', 'mv', 'ml', 'mt',
 'mh', 'mq', 'mr', 'mu', 'yt', 'mx', 'fm', 'md', 'mc', 'mn', 'me', 'ms', 'ma', 'mz',
 'mm', 'na', 'nr', 'np', 'nl', 'nc', 'nz', 'ni', 'ne', 'ng', 'nu', 'nf', 'kp', 'mk',
 'mp', 'no', 'om', 'pk', 'pw', 'ps', 'pa', 'pg', 'py', 'pe', 'ph', 'pn', 'pl', 'pt',
 'pr', 'qa', 're', 'ro', 'ru', 'rw', 'bl', 'sh', 'kn', 'lc', 'mf', 'pm', 'vc', 'ws',
 'sm', 'st', 'sa', 'sn', 'rs', 'sc', 'sl', 'sg', 'sx', 'sk', 'si', 'sb', 'so', 'za',
 'gs', 'kr', 'ss', 'es', 'lk', 'sd', 'sr', 'sj', 'se', 'ch', 'sy', 'tw', 'tj', 'tz',
 'th', 'tl', 'tg', 'tk', 'to', 'tt', 'tn', 'tr', 'tm', 'tc', 'tv', 'ug', 'ua', 'ae',
 'gb', 'um', 'us', 'uy', 'uz', 'vu', 've', 'vn', 'vg', 'vi', 'wf', 'eh', 'ye', 'zm',
 'zw']
# fmt: on


T = TypeVar("T")


# This works in my (Simon) neovim pyright setup, but it doesn't work with pyright on the
# command-line, so it's no good. Migrated to DeprecateDirectCallMetaclass.

# class TypedFactoryMetaClass(Generic[T], FactoryMetaClass):
#     __call__: Callable[..., T]


class UniqueFaker(Faker):
    """Faker that doesnt repeat values

    Taken from https://github.com/FactoryBoy/factory_boy/issues/305
    """

    def evaluate(self, instance, step, extra):
        locale = extra.pop("locale")
        subfaker = self._get_faker(locale)
        unique_proxy = subfaker.unique
        return unique_proxy.format(self.provider, **extra)


class DeprecateDirectCallMetaclass(FactoryMetaClass):
    def __call__(self, *args, **kwargs):
        # pyright intentionally doesn't support returning different types from __new__, so we need
        # this as a workaround: https://github.com/microsoft/pyright/issues/625
        warn(
            "use .create and remove the type annotation",
            DeprecationWarning,
            stacklevel=2,
        )
        return super().__call__(*args, **kwargs)

    # Also add the following to the factory definition:
    #
    # create: ClassVar[Callable[..., YourType]]
    # build: ClassVar[Callable[..., YourType]]


##################
## Applications ##
##################


class CertificatesListFactory(Factory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = list
        inline_args = ["elements"]

    create: ClassVar[Callable[..., list]]
    create_batch: ClassVar[Callable[..., list]]
    build: ClassVar[Callable[..., list]]
    build_batch: ClassVar[Callable[..., list]]

    class Params:
        count = Faker("random_int", min=0, max=3)

    elements = Faker(
        "random_sample",
        elements=[
            certificate["value"]
            for group in data.certificates
            for certificate in group["certificates"]
        ],
        length=SelfAttribute("..count"),
    )


@dataclasses.dataclass
class ApplicationData:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    date_of_birth: str
    gender: str
    nationalities: list
    spoken_languages: list
    motivation: str
    qualification: str
    positions: List[str]
    certificates: List[str]
    other_certificate: str
    homepage: str | None

    @staticmethod
    def valid_positions():
        return available_positions

    def to_dict(self):
        return dataclasses.asdict(self)

    def as_json(self):
        return json.dumps(self.to_dict())


class ApplicationDataFactory(Factory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = ApplicationData

    class Params:
        homepage_ = Faker("uri")
        has_homepage = Faker("boolean")

        nationality_count = Faker("random_int", min=1, max=6)

    create: ClassVar[Callable[..., ApplicationData]]
    create_batch: ClassVar[Callable[..., list[ApplicationData]]]
    build: ClassVar[Callable[..., ApplicationData]]
    build_batch: ClassVar[Callable[..., list[ApplicationData]]]

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    phone_number = Faker("phone_number")
    date_of_birth = Faker("date", pattern="%Y-%m-%d")
    gender = Faker("random_element", elements=Gender.values)
    positions = Faker(
        "random_elements", elements=ApplicationData.valid_positions(), unique=True
    )
    # @todo make this use faker
    nationalities = Faker(
        "random_elements",
        unique=True,
        length=SelfAttribute("..nationality_count"),
        elements=[country.code for country in django_countries.countries],
    )
    spoken_languages = [{"language": "de", "points": 5}]
    motivation = Faker("paragraph")
    qualification = Faker("paragraph")
    phone_number = Faker("bothify", text="+############")
    certificates = SubFactory(CertificatesListFactory)
    other_certificate = Faker("job")
    homepage = LazyAttribute(lambda self: self.homepage_ if self.has_homepage else "")
