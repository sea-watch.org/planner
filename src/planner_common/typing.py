from typing import Any, Type


def assert_is[T](t: Type[T], value: Any) -> T:
    """Assert that the value is of the given type."""
    assert isinstance(value, t)
    return value
