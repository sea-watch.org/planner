from django.urls import path

from . import views

urlpatterns = [
    path("", views.ApplicationListView.as_view(), name="application_list"),
    path(
        "rejected/",
        views.RejectedApplicationListView.as_view(),
        name="application_list_rejected",
    ),
    path(
        "<int:volunteer_id>/",
        views.ApplicationUpdateView.as_view(),
        name="application_update",
    ),  # needs to be updated, it's called with the volunteer id now
    path(
        "<int:volunteer_id>/reject/",
        views.application_reject,
        name="application_reject",
    ),
    path(
        "<int:volunteer_id>/accept/",
        views.accept_application,
        name="application_accept",
    ),
    path(
        "<int:volunteer_id>/add_comment/",
        views.add_comment,
        name="application_add_comment",
    ),
    path(
        "<int:volunteer_id>/decide_position/",
        views.decide_position,
        name="application_decide_position",
    ),
    path(
        "<int:volunteer_id>/update_additional_positions/",
        views.update_additional_positions,
        name="application_update_additional_positions",
    ),
]
