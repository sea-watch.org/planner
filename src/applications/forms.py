import django_countries
from django import forms
from django.template.loader import render_to_string
from django.utils.translation import gettext as _

from planner_common import data
from planner_common.forms.fields import CertificatesField
from planner_common.forms.widgets import ChoicesJSWidget, LanguagesWidget
from seawatch_planner import context_processors
from seawatch_registration.forms import PositionsField
from seawatch_registration.models import Position
from volunteers.models import Document, Volunteer


class CSVField(forms.MultipleChoiceField):
    def __init__(self, *args, separator=";", **kwargs):
        super().__init__(*args, **kwargs)
        self.__separator = separator

    def clean(self, value):
        v = super().clean(value)
        return self.__separator.join(sorted(v))

    def prepare_value(self, value):
        if value is None:
            return []
        if isinstance(value, list):
            return value
        else:
            return value.split(self.__separator)


class WarnOnLeavingIfChangedMixin:
    class Media:
        js = [
            "seawatch_registration/js/disable_submit_until_first_edit.js",
            "seawatch_registration/js/warn_on_leave_if_changed.js",
        ]


certificates = [
    (g["group_label"], [(e["value"], e["label"]) for e in g["certificates"]])
    for g in data.certificates
]
certificates[0][1].append(("other", "Other (free text)"))


class ApplicationForm(WarnOnLeavingIfChangedMixin, forms.ModelForm):
    """Form for a Volunteer object from an application perspective"""

    def __init__(self, instance: Volunteer, **kwargs):
        if instance:
            instance.nationalities = [n.upper() for n in instance.nationalities]
        kwargs.setdefault("label_suffix", "")
        super().__init__(instance=instance, **kwargs)

        self.fields["nationalities"].choices = django_countries.countries

        if instance:
            positions = [sa.position for sa in instance.sarapplication_set.open()]
            self.initial["positions"] = [
                p.id for p in positions if p.open_for_applications
            ]
            self.initial["additional_positions"] = [
                p.id for p in positions if not p.open_for_applications
            ]

    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Volunteer
        label_suffix = ""
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",
            "date_of_birth",
            "gender",
            "nationalities",
            "languages",
            "positions",
            "certificates",
            "homepage",
        ]
        field_classes = {
            "certificates": CertificatesField,
            # ModelForm passes de/encoder because the model has a json field
            "nationalities": lambda *a,
            encoder=None,
            decoder=None,
            **k: forms.MultipleChoiceField(
                *a,
                choices=django_countries.countries,
                **k,
            ),
        }
        help_texts = {
            "languages": "We only ask for specific languages, applicants might know others"
        }
        labels = {
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "phone_number": _("Phone Number"),
            "email": _("Email Address"),
            "date_of_birth": _("Date of Birth"),
            "gender": _("Gender"),
            "languages": _("Spoken Languages"),
            "nationalities": _("Nationalities"),
            "motivation": _("Why do you want to work for Sea-Watch?"),
            "qualification": _("What qualifies you for the chosen positions?"),
        }
        widgets = {
            "positions": ChoicesJSWidget,
            "nationalities": ChoicesJSWidget,
            "languages": LanguagesWidget,
        }

    template_name = "applications/forms/application_form.html"

    positions = PositionsField(
        queryset=Position.objects.filter(open_for_applications=True),
        label=_("Positions"),
        widget=ChoicesJSWidget,
    )

    # This one is only for rendering the additional positions field, not parsing. Additional
    # positions are saved using applications.views.update_additional_positions via htmx
    additional_positions = PositionsField(
        queryset=Position.objects.filter(open_for_applications=False),
        label=_("Additional Positions"),
        widget=ChoicesJSWidget,
        required=False,
    )

    def get_context(self, **kwargs):
        return {
            **super().get_context(**kwargs),
            "cv": self.instance.document_set.filter(type=Document.Type.cv).first(),
            **context_processors.features(),
        }

    def save(self, *args, **kwargs) -> Volunteer:
        instance = super().save(*args, **kwargs)
        positions = set(self.cleaned_data["positions"])
        current_positions = {
            sa.position
            for sa in instance.sarapplication_set.open().filter(
                position__open_for_applications=True
            )
        }
        instance.sarapplication_set.open().filter(
            position__in=current_positions - positions
        ).delete()
        for p in positions - current_positions:
            instance.sarapplication_set.create(position=p)
        return instance


class ApplicationCustomEmailForm(WarnOnLeavingIfChangedMixin, forms.Form):
    template_name = "applications/forms/application_reject_form.html"

    subject = forms.CharField(widget=forms.TextInput)
    body = forms.CharField(widget=forms.Textarea(attrs={"style": "min-height: 300px;"}))


class ApplicationRejectForm(ApplicationCustomEmailForm):
    def __init__(self, volunteer: Volunteer, **kwargs):
        kwargs["initial"] = {
            "subject": render_to_string(
                "applications/mail/reject_application.subject.txt",
            ).strip(),
            "body": render_to_string(
                "applications/mail/reject_application.message.txt",
                {
                    "volunteer": volunteer,
                    "positions": Position.objects.filter(
                        sarapplication__volunteer=volunteer, open_for_applications=True
                    ),
                },
            ).strip(),
            **kwargs.get("initial", {}),
        }
        super().__init__(**kwargs)


class ApplicationAcceptForm(ApplicationCustomEmailForm):
    def __init__(self, volunteer: Volunteer, **kwargs):
        kwargs["initial"] = {
            "subject": render_to_string(
                "applications/mail/accept_application.subject.txt",
            ).strip(),
            "body": render_to_string(
                "applications/mail/accept_application.message.txt",
                {
                    "volunteer": volunteer,
                    "positions": Position.objects.filter(
                        sarapplication__volunteer=volunteer, open_for_applications=True
                    ),
                },
            ).strip(),
            **kwargs.get("initial", {}),
        }
        super().__init__(**kwargs)
