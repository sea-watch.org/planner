from datetime import datetime
from django.utils.safestring import mark_safe
import django_tables2 as tables
from django.db.models import Q, FilteredRelation, Max, Min, QuerySet
from django.template.loader import render_to_string
from django.urls import reverse

from applications.models import SarApplication
from volunteers.models import Volunteer


class OpenApplicationsTable(tables.Table):
    # Similar to other application-related stuff, this works on Volunteer objects with
    # non-accepted SarApplication's

    name = tables.Column(
        accessor="name_reversed",
        linkify=lambda record: reverse(
            "application_update", kwargs={"volunteer_id": record.id}
        ),  # type: ignore
        order_by=("last_name", "first_name"),
        attrs={"header": True},
    )

    positions = tables.Column(empty_values=[])

    def render_positions(self, record: Volunteer):
        return render_to_string(
            "applications/application_list_table_positions_cell.html",
            context={"sar_applications": record.sarapplication_set.open()},
        )

    def order_positions(self, queryset: QuerySet[Volunteer], is_descending: bool):
        return (
            queryset.annotate(
                position_for_sort=Min("sarapplication__position__name")
            ).order_by(
                f'{"-" if is_descending else ""}position_for_sort',
            ),
            True,
        )

    class Meta:
        model = Volunteer
        template_name = "applications/application_list_table.html"
        fields = ["applied_at"]
        sequence = ["name", "positions", "applied_at"]
        attrs = {"style": "min-width: 500px;"}
        empty_text = "There are no open applications at the moment."


class RejectedApplicationsTable(tables.Table):
    # Similar to other application-related stuff, this works on Volunteer objects with
    # non-accepted SarApplication's

    @staticmethod
    def _link(record: Volunteer):
        base = reverse("application_update", kwargs={"volunteer_id": record.id})
        return f"{base}?rejected"

    name = tables.Column(
        accessor="name_reversed",
        # Has to be a lambda or there's a weird error 🤷
        linkify=lambda record: RejectedApplicationsTable._link(record),  # type: ignore
        order_by=("last_name", "first_name"),
        attrs={"header": True},
    )

    positions = tables.Column(empty_values=[])
    rejected_at = tables.Column(empty_values=[])

    def render_positions(self, record: Volunteer):
        return render_to_string(
            "applications/application_list_table_positions_cell.html",
            context={"sar_applications": record.sarapplication_set.all()},
        )

    def render_rejected_at(self, record: Volunteer):
        def assert_not_none(dt: datetime | None) -> datetime:
            assert dt is not None
            return dt

        dates = sorted(
            [
                assert_not_none(sar_application.closed)
                for sar_application in record.sarapplication_set.rejected()
            ],
            reverse=True,
        )
        date_strings = {d.strftime("%d.%m.%Y") for d in dates if d is not None}
        return mark_safe("<br>".join(date_strings))  # nosec b308 b703

    def order_rejected_at(self, queryset: QuerySet[Volunteer], is_descending: bool):
        return (
            queryset.annotate(
                application_for_rejected_sort=FilteredRelation(
                    "sarapplication",
                    condition=Q(
                        sarapplication__closed__isnull=False,
                        sarapplication__decision=SarApplication.Decision.REJECT,
                    ),
                )
            )
            .annotate(rejected_at_for_sort=Max("application_for_rejected_sort__closed"))
            .order_by(f'{"-" if is_descending else ""}rejected_at_for_sort'),
            True,
        )

    def order_positions(self, queryset: QuerySet[Volunteer], is_descending: bool):
        return (
            queryset.annotate(
                position_for_sort=Min("sarapplication__position__name")
            ).order_by(
                f'{"-" if is_descending else ""}position_for_sort',
            ),
            True,
        )

    class Meta:
        model = Volunteer
        template_name = "applications/application_list_table.html"
        fields = ["applied_at"]
        sequence = ["name", "positions", "rejected_at", "applied_at"]
        attrs = {"style": "min-width: 500px;"}
        empty_text = "There are no rejected applications at the moment."
