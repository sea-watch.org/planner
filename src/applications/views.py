import json
import logging
from collections.abc import Callable
from datetime import datetime, timezone
from typing import Any
from urllib.parse import urlencode

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import FilteredRelation, Q, QuerySet
from django.http import Http404
from django.http.request import HttpRequest
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    HttpResponseRedirect,
    HttpResponseRedirectBase,
)
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST
from django.views.generic.edit import UpdateView
from django_tables2 import SingleTableView

from planner_common.feature_flags import features
from seawatch_registration.models import Position
from volunteers.models import Volunteer

from . import forms
from .models import Import, SarApplication
from .tables import OpenApplicationsTable, RejectedApplicationsTable

logger = logging.getLogger(__name__)


def get_position_or_none(key):
    try:
        return Position.objects.get(key=key)
    except Position.DoesNotExist:
        return None


@require_POST
def add_comment(request, volunteer_id):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    text = request.POST.get("comment")
    if not text:
        return HttpResponseBadRequest('"comment" POST parameter required')

    comment = volunteer.comments.create(
        author=request.user,
        text=text,
    )

    return TemplateResponse(
        request,
        "applications/application_form_add_comment.html",
        {"comment": comment, "volunteer": volunteer},
    )


persisted_get_params_session_key = "ApplicationListView__persisted_flags"


def common_filter_context() -> dict:
    positions_by_group = Position.objects.by_group()
    return {
        "positions_without_group": positions_by_group.pop("", []),
        "positions_with_group": positions_by_group,
    }


persisted_get_params = {"search", "position", "only_ready_to_close", "sort"}


def redirect_based_on_saved_search_if_appropriate(
    request: HttpRequest, current_url_name: str
) -> HttpResponseRedirect | None:
    if not any(k in request.GET for k in persisted_get_params) and (
        session_data := request.session.get(persisted_get_params_session_key)
    ):
        try:
            query = {
                k: session_data[k] for k in persisted_get_params if session_data.get(k)
            }
            # Not sure in which situations the session data would be empty, but lets check and
            # fall through anyhow
            if query:
                return HttpResponseRedirect(
                    f"{reverse(current_url_name)}?{urlencode(query)}"
                )
        except Exception:
            logger.exception(
                f"Failed to retrieve old application list settings from session (data in session: {session_data})"
            )
            del request.session[persisted_get_params_session_key]


def persist_query_parameters(request: HttpRequest) -> None:
    request.session[persisted_get_params_session_key] = {
        k: request.GET.get(k) for k in persisted_get_params
    }


class ApplicationListView(SingleTableView):
    table_class = OpenApplicationsTable
    template_name = "applications/application_list.html"

    search: str | None = None
    position: str | None = None
    only_ready_to_close: bool | None = None
    sort: str | None = None

    def get_queryset(self):
        # Getting this query to work right was a bit of a struggle tbh.
        # It was hard to make sure all the conditions apply to the same SarApplication entry
        qs = Volunteer.objects.annotate(
            application=FilteredRelation(
                "sarapplication",
                condition=Q(
                    # sarapplication__isnull=False,
                    sarapplication__closed=None,
                ),
            )
        ).filter(application__isnull=False)
        if self.position:
            qs = qs.filter(application__position__key=self.position)
        if self.only_ready_to_close:
            qs = qs.exclude(
                application__decision__in=[
                    SarApplication.Decision.NEW,
                    SarApplication.Decision.IN_PROGRESS,
                ],
            )
        if self.search:
            search_without_spaces = self.search.split(" ")
            for word in search_without_spaces:
                qs = qs.filter(
                    Q(first_name__icontains=word) | Q(last_name__icontains=word)
                )

        # Use `qs.query.sql_with_params()` to inspect the query
        return qs.distinct()

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["last_import"] = Import.get_last_imported()

        ctx = {**ctx, **common_filter_context()}

        ctx["search"] = self.search or ""
        ctx["selected_position"] = get_position_or_none(self.position)
        ctx["only_ready_to_close"] = self.only_ready_to_close
        ctx["sort_filter"] = self.request.GET.get(
            "sort"
        )  # needed to preserve sort after filtering
        return ctx

    @method_decorator(
        permission_required(
            [
                "applications.view_sarapplication",
                "volunteers.view_volunteer",
            ],
            raise_exception=True,
        )
    )
    def get(self, request, *args, **kwargs):
        if redirect := redirect_based_on_saved_search_if_appropriate(
            request, "application_list"
        ):
            return redirect

        self.position = request.GET.get("position")
        self.search = request.GET.get("search")
        self.only_ready_to_close = bool(request.GET.get("only_ready_to_close"))

        persist_query_parameters(request)

        return super().get(request, *args, **kwargs)


class RejectedApplicationListView(SingleTableView):
    table_class = RejectedApplicationsTable
    template_name = "applications/application_list_rejected.html"

    def get_queryset(self) -> QuerySet[Volunteer]:
        qs = Volunteer.objects.annotate(
            application=FilteredRelation(
                "sarapplication",
                condition=Q(
                    sarapplication__closed__isnull=False,
                    sarapplication__decision=SarApplication.Decision.REJECT,
                ),
            )
        ).filter(application__isnull=False)
        if self.position:
            qs = qs.filter(application__position__key=self.position)
        if self.search:
            search_without_spaces = self.search.split(" ")
            for word in search_without_spaces:
                qs = qs.filter(
                    Q(first_name__icontains=word) | Q(last_name__icontains=word)
                )
        return qs.distinct()

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        ctx = super().get_context_data(**kwargs)
        ctx = {**ctx, **common_filter_context()}

        ctx["search"] = self.search or ""
        ctx["selected_position"] = get_position_or_none(self.position)
        ctx["sort_filter"] = self.request.GET.get(
            "sort"
        )  # needed to preserve sort after filtering
        return ctx

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        if redirect := redirect_based_on_saved_search_if_appropriate(
            request, "application_list_rejected"
        ):
            return redirect

        self.position = request.GET.get("position")
        self.search = request.GET.get("search")

        persist_query_parameters(request)

        return super().get(request, *args, **kwargs)


class ApplicationUpdateView(PermissionRequiredMixin, UpdateView):
    """
    Don't let the name fool you, the core object here is a volunteers.models.Volunteer, but
    we're looking at it as an application.
    """

    permission_required = [
        "applications.view_sarapplication",
        "applications.change_sarapplication",
    ]
    form_class = forms.ApplicationForm
    template_name = "applications/application_form.html"

    # This is still None before django.views.generic.edit.BaseUpdateView sets
    # `self.object = self.get_object()` in .get() and .post(), but we declare it as non-None for
    # simplicity
    object: Volunteer

    # Same here, it's not set before get_object is called but we set it for convenience
    old_history_dict: dict[str, Any]

    def get_object(self, queryset=None) -> Volunteer:
        object = get_object_or_404(Volunteer, pk=self.kwargs["volunteer_id"])
        self.old_history_dict = object.to_history_dict()
        return object

    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        ctx["rejected"] = "rejected" in self.request.GET
        ctx["rejected_positions"] = [
            sa.position
            for sa in SarApplication.objects.filter(
                volunteer=self.object,
                closed__isnull=False,
                decision=SarApplication.Decision.REJECT,
            )
        ]
        ctx["accepted_positions_count"] = SarApplication.objects.filter(
            volunteer=self.object,
            closed__isnull=False,
            decision=SarApplication.Decision.ACCEPT,
        ).count()

        ctx["current_positions_count"] = SarApplication.objects.filter(
            volunteer=self.object,
            closed__isnull=True,
        ).count()

        if reject_form_data := self.request.GET.get("reject-form-data"):
            ctx["reject_form_visible"] = True
            ctx["reject_form"] = forms.ApplicationRejectForm(
                self.object, data=json.loads(reject_form_data)
            )
        else:
            ctx["reject_form_visible"] = False
            ctx["reject_form"] = forms.ApplicationRejectForm(self.object)

        if accept_form_data := self.request.GET.get("accept-form-data"):
            ctx["accept_form_visible"] = True
            ctx["accept_form"] = forms.ApplicationAcceptForm(
                self.object, data=json.loads(accept_form_data)
            )
        else:
            ctx["accept_form_visible"] = False
            ctx["accept_form"] = forms.ApplicationAcceptForm(self.object)
        return ctx

    def form_valid(self, form):
        response = super().form_valid(form)
        self.object.refresh_from_db()
        self.object.history.create_from_dicts(
            old=self.old_history_dict, new=self.object.to_history_dict()
        )
        messages.success(self.request, _("The application was successfully updated"))
        return response

    def get_success_url(self):
        return reverse("application_update", kwargs={"volunteer_id": self.object.id})


@permission_required(
    ["applications.delete_sarapplication", "volunteers.delete_volunteer"],
    raise_exception=True,
)
def application_reject(request, volunteer_id, send_mail=send_mail):
    match request.POST:
        case {"reject-with-email": _}:
            should_send_mail = True
        case {"reject-without-email": _}:
            should_send_mail = False
        case _:
            return HttpResponseBadRequest(
                "Expected either 'reject-without-email' or 'reject-with-email'"
            )

    volunteer = get_object_or_404(Volunteer, id=volunteer_id)
    if not volunteer.sarapplication_set.open().exists():
        return HttpResponseNotFound()

    # Helper functions
    def perform_delete_or_reject():
        if features["keep_rejected_applications"]:
            for sarapplication in volunteer.sarapplication_set.open():
                sarapplication.reject()
        else:
            if volunteer.sarprofile_set.exists():
                volunteer.sarapplication_set.open().delete()
            else:
                volunteer.delete()

    def rerender_form():
        url = reverse("application_update", kwargs={"volunteer_id": volunteer.id})
        # We need to tramsit the POST data as GET because we need to redirect. The
        # ApplicationUpdateView will pass them to the form to re-render
        querystring = urlencode(
            {
                "reject-form-data": json.dumps(
                    {
                        k: v
                        for k, v in request.POST.items()
                        if k
                        not in {
                            "csrfmiddlewaretoken",
                            "reject-with-email",
                            "reject-without-email",
                        }
                    }
                )
            }
        )
        return HttpResponseRedirect(f"{url}?{querystring}")

    # Actual view logic

    if should_send_mail:
        form = forms.ApplicationRejectForm(volunteer=volunteer, data=request.POST)
        if form.is_valid():
            perform_delete_or_reject()
            send_mail(
                subject=form.cleaned_data["subject"],
                message=form.cleaned_data["body"],
                from_email=settings.EXTERNAL_EMAIL_FROM,
                recipient_list=[volunteer.email],
            )
        else:
            return rerender_form()
    else:
        perform_delete_or_reject()

    messages.info(
        request,
        (
            f"{volunteer.first_name}'s application has been rejected."
            if features["keep_rejected_applications"]
            else f"{volunteer.first_name}'s application has been deleted."
        ),
    )

    return HttpResponseRedirect(reverse("application_list"))


@permission_required(
    [
        "applications.change_sarapplication",
        "applications.delete_sarapplication",
        "volunteers.add_sarprofile",
    ],
    raise_exception=True,
)
def accept_application(
    request: HttpRequest, volunteer_id: int, send_mail: Callable = send_mail
) -> HttpResponseRedirectBase:
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)

    # Check everything is fine
    if volunteer.sarapplication_set.open().undecided().exists():
        return _handle_application_with_undecided_positions(request, volunteer)
    if (
        not volunteer.sarapplication_set.open()
        .filter(decision=SarApplication.Decision.ACCEPT)
        .exists()
    ):
        return _handle_no_accepted_applications(request, volunteer)

    # Update SarApplications
    accepted_applications = (
        volunteer.sarapplication_set.open()
        .filter(decision=SarApplication.Decision.ACCEPT)
        .select_related("position")
    )
    # Need to grab the positions here because the update will make the query return 0 results
    # later
    accepted_positions = [sa.position for sa in accepted_applications]
    for sa in accepted_applications:
        sa.accept()

    for sa in volunteer.sarapplication_set.open().filter(
        decision=SarApplication.Decision.REJECT
    ):
        if features["keep_rejected_applications"]:
            sa.reject()
        else:
            sa.delete()

    # Update volunteer
    if volunteer.joined_at is None:
        volunteer.joined_at = datetime.now(tz=timezone.utc)
        volunteer.save()

    volunteer.history.create(
        change_json=[
            {
                "type": "applications_accepted",
                "positions": [p.key for p in accepted_positions],
            }
        ]
    )

    return _send_acceptance_email_and_redirect(request, volunteer, send_mail)


def _send_acceptance_email_and_redirect(
    request: HttpRequest, volunteer: Volunteer, send_mail: Callable
) -> HttpResponseRedirectBase:
    # Helper function

    def rerender_form():
        url = reverse("application_update", kwargs={"volunteer_id": volunteer.id})
        # We need to tramsit the POST data as GET because we need to redirect. The
        # ApplicationUpdateView will pass them to the form to re-render
        querystring = urlencode(
            {
                "accept-form-data": json.dumps(
                    {
                        k: v
                        for k, v in request.POST.items()
                        if k not in {"csrfmiddlewaretoken"}
                    }
                )
            }
        )
        return HttpResponseRedirect(f"{url}?{querystring}")

    # Actual logic

    try:
        form = forms.ApplicationAcceptForm(volunteer=volunteer, data=request.POST)
        if form.is_valid():
            send_mail(
                subject=form.cleaned_data["subject"],
                message=form.cleaned_data["body"],
                from_email=settings.EXTERNAL_EMAIL_FROM,
                recipient_list=[volunteer.email],
            )
        else:
            return rerender_form()
        messages.info(
            request,
            f"Application accepted, {volunteer.first_name} is now in the volunteer pool",
        )
    except Exception:
        logger.exception(
            "Error sending acceptance email",
            extra={"request": request},
        )
        messages.warning(
            request,
            (
                "The application was successfully accepted, but an error occured while sending the mail to the volunteer. "
                "Please notify them yourself.\n"
                "We have already been informed about the error and will look into the cause as soon "
                "as possible. Sorry for the inconvenience."
            ),
        )

    return HttpResponseRedirect(reverse("application_list"))


def _handle_application_with_undecided_positions(
    request: HttpRequest, volunteer: Volunteer
) -> HttpResponseRedirectBase:
    logging.warning(
        "Accept view has been reached while some positions are still undecided",
        extra={"request": request},
    )
    messages.warning(
        request,
        "Cannot accept application before all positions have been decided",
    )
    return HttpResponseRedirect(
        reverse("application_update", kwargs={"volunteer_id": volunteer.id})
    )


def _handle_no_accepted_applications(
    request: HttpRequest, volunteer: Volunteer
) -> HttpResponseRedirectBase:
    logging.warning(
        "Accept view has been reached for application that has no decision: accept",
        extra={"request": request},
    )
    messages.warning(
        request, "Cannot accept application without positions we would accept"
    )
    return HttpResponseRedirect(
        reverse("application_update", kwargs={"volunteer_id": volunteer.id})
    )


@permission_required("applications.change_sarapplication", raise_exception=True)
@require_POST
def decide_position(request: HttpRequest, volunteer_id):
    position_key = request.POST.get("position")
    decision = request.POST.get("decision")

    if not position_key or not decision:
        return HttpResponseBadRequest()

    try:
        sarapplication = get_object_or_404(
            SarApplication.objects.open(),
            volunteer_id=volunteer_id,
            position__key=position_key,
        )
    except Http404:
        if not Position.objects.filter(key=position_key).exists():
            raise Http404("Position does not exist")
        if Volunteer.objects.filter(id=volunteer_id).exists():
            return HttpResponseBadRequest("Volunteer didn't apply for that position")
        raise
    if sarapplication.closed is not None:
        return HttpResponseBadRequest("The application has already been accepted")

    if not isinstance(request.user, User):
        raise AssertionError(
            "Assumed that this view was only accessible when logged in"
        )
    sarapplication.by = request.user
    sarapplication.decision = SarApplication.Decision(decision)
    sarapplication.time = datetime.now(tz=timezone.utc)
    sarapplication.save()

    return TemplateResponse(
        request,
        "applications/application_decide_position_response.html",
        context={
            "sarapplication": sarapplication,
            "accept_form_visible": False,
            "accept_form": forms.ApplicationAcceptForm(sarapplication.volunteer),
        },
    )


@permission_required("volunteers.change_volunteer", raise_exception=True)
@require_POST
def update_additional_positions(
    request: HttpRequest, volunteer_id: int
) -> HttpResponse:
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    new_positions = {
        get_object_or_404(Position, pk=pos_id)
        for pos_id in request.POST.getlist("additional_positions")
    }
    current_positions = set(
        sa.position
        for sa in volunteer.sarapplication_set.open().filter(
            position__open_for_applications=False
        )
    )

    volunteer.sarapplication_set.open().filter(
        position__in=(current_positions - new_positions)
    ).delete()

    for p in new_positions - current_positions:
        volunteer.sarapplication_set.create(position=p)

    return TemplateResponse(
        request,
        "applications/application_update_additional_positions_response.html",
        context={
            "volunteer": volunteer,
            "accept_form_visible": False,
            "accept_form": forms.ApplicationAcceptForm(volunteer),
        },
    )
