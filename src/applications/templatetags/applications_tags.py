from django import template

from volunteers.models import Volunteer

from ..models import SarApplication

register = template.Library()


@register.filter
def positions_with_open_application_and_decision_accept(volunteer: Volunteer):
    return [
        sa.position
        for sa in volunteer.sarapplication_set.open().filter(
            decision=SarApplication.Decision.ACCEPT
        )
    ]


@register.filter
def all_open_sarapplications_decided(volunteer: Volunteer):
    return not volunteer.sarapplication_set.open().undecided().exists()


@register.filter
def has_open_sarapplication_with_decision_accept(volunteer: Volunteer):
    return (
        volunteer.sarapplication_set.open()
        .filter(decision=SarApplication.Decision.ACCEPT)
        .exists()
    )
