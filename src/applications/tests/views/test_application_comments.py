from datetime import datetime, timezone
from http import HTTPStatus
from typing import Literal, assert_never, cast

import faker
from _pytest.python_api import raises
from bs4 import BeautifulSoup, Tag
from django.http.response import Http404
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from django.utils.timezone import localtime
from freezegun import freeze_time
from playwright.sync_api import BrowserContext, expect
from pytest import mark

from applications.tests.factories import SarApplicationFactory
from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory

from ...views import ApplicationUpdateView, add_comment


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    backoffice_client.force_login(UserFactory.create())
    volunteer = VolunteerFactory.create()
    response = backoffice_client.get(
        f"/applications/{volunteer.id}/add_comment/",
    )
    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED


@mark.django_db
def test_can_add_comment_through_browser(
    logged_in_context: BrowserContext, backoffice_url
):
    page = logged_in_context.new_page()
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, is_closed=False)
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )
    comments_section = page.locator("#comments-section")
    form = comments_section.locator("form")
    comment = faker.Faker().paragraph()
    form.locator("textarea").fill(comment)
    form.get_by_role("button").click()

    expect(comments_section).to_contain_text(comment)


@mark.django_db
@mark.parametrize("scenario", ["on-update-page", "on-post-response"])
def test_shows_comments(
    scenario: Literal["on-update-page", "on-post-response"], rf: RequestFactory
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)
    author = UserFactory.create(username="rumpel")
    comment = volunteer.comments.create(
        text="lirum lorom",
        author=author,
        date=datetime(2021, 2, 3, 14, 15, 16, tzinfo=timezone.utc),
    )
    match scenario:
        case "on-update-page":
            request = rf.get("")
            request.user = UserFactory.create(view_permissions=ApplicationUpdateView)
            response = ApplicationUpdateView.as_view()(
                request, volunteer_id=volunteer.id
            )
            assert response.status_code == HTTPStatus.OK, response.content
        case "on-post-response":
            request = rf.post("", {"comment": "the new stuff"})
            request.user = UserFactory.create()
            response = add_comment(request, volunteer_id=volunteer.id)
            assert response.status_code == HTTPStatus.OK, response.content
        case other:
            assert_never(other)

    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content,
        features="html.parser",
    )
    comments = html.find(id="comments")
    assert isinstance(comments, Tag)
    for it in [
        comment.text,
        author.username,
        # I want to re-use the format here, but this doesn't work and I give up
        # Just using SHORT_DATETIME_FORMAT with the python standard function doesn't work
        # because django wants to "make it easier for php developers" and uses a different
        # formatting specifier
        # dateformat.format(comment.date, settings.SHORT_DATETIME_FORMAT)
        f"{localtime(comment.date):%d.%m.%Y, %H:%M}",
    ]:
        assert it in comments.text


@mark.django_db
def test_saves_comment(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)
    author = UserFactory.create()

    comment_text = "lets goOOooOOOoOO"
    comment_time = datetime(2022, 8, 1, 13, 45, 31, tzinfo=timezone.utc)
    request = rf.post("", {"comment": comment_text})
    request.user = author

    with freeze_time(comment_time):
        response = add_comment(request, volunteer_id=volunteer.id)
    assert response.status_code == HTTPStatus.OK, response.content

    assert [(c.text, c.author, c.date) for c in volunteer.comments.all()] == [
        (
            comment_text,
            author,
            comment_time,
        )
    ]


@mark.django_db
def test_returns_bad_request_if_missing_comment_value(rf: RequestFactory):
    data = {}  # no "comment" entry

    request = rf.post("", data)
    request.user = UserFactory.create()

    response = add_comment(request, volunteer_id=VolunteerFactory.create().id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_returns_not_found_if_application_does_not_exist(rf: RequestFactory):
    with raises(Http404):
        add_comment(rf.post("", {}), volunteer_id=4223)
