from http import HTTPStatus
from typing import cast
from urllib.parse import urlparse

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.mail.message import EmailMessage
from django.http.response import Http404, HttpResponseRedirect, HttpResponseRedirectBase
from django.template.response import TemplateResponse
from django.test.utils import override_settings
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark, raises

from applications.models import SarApplication
from planner_common.feature_flags import features
from seawatch_planner.tests.request_factory import (
    RequestFactory,
    WSGIRequestWithStubMessages,
)
from seawatch_registration.tests.factories import UserFactory
from volunteers.models import Document, SarProfile, Volunteer
from volunteers.tests.factories import (
    DocumentFactory,
    SarProfileFactory,
    VolunteerFactory,
)

from ...views import ApplicationUpdateView, application_reject
from ..factories import SarApplicationFactory

userfactory_permission_kwargs = {
    "permissions": [
        "applications.delete_sarapplication",
        "volunteers.delete_volunteer",
    ],
    "view_permissions": ApplicationUpdateView,
}


def mk_request(
    rf: RequestFactory, with_email, subject="...", body="..."
) -> WSGIRequestWithStubMessages:
    request = rf.post(
        "",
        {
            "reject-with-email" if with_email else "reject-without-email": "...",
            "subject": subject,
            "body": body,
        },
    )
    request.user = UserFactory.create(**userfactory_permission_kwargs)
    return request


@mark.django_db
@mark.parametrize("with_email", [True, False])
def test_deletes_volunteer_and_sarapplications(with_email: bool, rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    sarapplication = SarApplicationFactory.create(volunteer=volunteer, is_closed=False)
    SarApplicationFactory.create(volunteer=volunteer, is_closed=False)

    request = mk_request(rf, with_email=with_email)
    request.user = UserFactory.create(**userfactory_permission_kwargs)
    response = application_reject(request, volunteer_id=volunteer.id)

    assert isinstance(response, HttpResponseRedirect), response.content
    assert response.url == reverse("application_list")

    if features["keep_rejected_applications"]:
        # Note that the decision is now REJECT independently of what it was before
        for sa in volunteer.sarapplication_set.all():
            assert sa.decision == SarApplication.Decision.REJECT
            assert sa.closed is not None
    else:
        assert not SarApplication.objects.filter(id=sarapplication.id).exists()
        assert not Volunteer.objects.filter(id=volunteer.id).exists()


@mark.skipif(
    features["keep_rejected_applications"],
    reason="This test doesnt make sense anymore with the new feature",
)
@mark.django_db
@mark.parametrize("with_email", [True, False])
def test_only_deletes_sarapplications_but_not_volunteer_if_volunteer_has_sarprofiles(
    with_email: bool,
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create()
    sarapplication = SarApplicationFactory.create(volunteer=volunteer)
    sarprofile = SarProfileFactory.create(volunteer=volunteer)

    request = mk_request(rf, with_email=with_email)
    request.user = UserFactory.create(**userfactory_permission_kwargs)
    response = application_reject(request, volunteer_id=volunteer.id)

    assert isinstance(response, HttpResponseRedirect), response.content
    assert response.url == reverse("application_list")

    assert not SarApplication.objects.filter(id=sarapplication.id).exists()
    assert Volunteer.objects.filter(id=volunteer.id).exists()
    assert SarProfile.objects.filter(id=sarprofile.id).exists()


@mark.skipif(
    features["keep_rejected_applications"],
    reason="This test doesnt make sense anymore with the new feature",
)
@mark.django_db(transaction=True)
def test_delete_application_also_deletes_cv_file(rf: RequestFactory):
    cv = DocumentFactory.create(type=Document.Type.cv)
    SarApplicationFactory.create(volunteer=cv.volunteer)

    # call delete application
    request = mk_request(rf, with_email=False)
    application_reject(request, volunteer_id=cv.volunteer.id)

    # check that file and volunteer is deleted
    assert not Volunteer.objects.filter(id=cv.volunteer.id).exists()
    assert not Document.objects.filter(id=cv.id).exists()
    path = cv.file.name
    assert default_storage.exists(path) is False


@mark.django_db
@mark.parametrize("missing_permission", userfactory_permission_kwargs["permissions"])
def test_delete_application_requires_permission(
    missing_permission: str, rf: RequestFactory
):
    request = rf.post("")
    permissions = {
        "applications.delete_sarapplication",
        "volunteers.delete_volunteer",
    } - {missing_permission}
    request.user = UserFactory.create(permissions=permissions)

    with raises(PermissionDenied):
        application_reject(request, volunteer_id=1234)


@mark.django_db
def test_delete_responds_404_with_missing_volunteer(rf: RequestFactory):
    request = mk_request(rf, with_email=False)
    with raises(Http404):
        application_reject(request, volunteer_id=93123)


@mark.django_db
def test_delete_responds_404_for_volunteer_with_no_applications(
    rf: RequestFactory,
):
    request = mk_request(rf, with_email=False)
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, is_closed=True)

    response = application_reject(request, volunteer_id=volunteer.id)
    assert response.status_code == HTTPStatus.NOT_FOUND


@mark.django_db
def test_delete_application_shows_message(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    request = mk_request(rf, with_email=False)
    application_reject(request, volunteer_id=volunteer.id)

    messages = request._messages.messages
    assert messages == [
        (
            20,
            (
                f"{volunteer.first_name}'s application has been rejected."
                if features["keep_rejected_applications"]
                else f"{volunteer.first_name}'s application has been deleted."
            ),
            "",
        )
    ]


@mark.django_db
def test_delete_rerenders_form_if_not_valid(
    rf: RequestFactory,
    mailoutbox: list[EmailMessage],
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position__name="Foo")
    SarApplicationFactory.create(volunteer=volunteer, position__name="Bar")

    request = mk_request(rf, with_email=True, subject="", body="test-body")
    user = request.user

    response = application_reject(request, volunteer_id=volunteer.id)
    assert len(mailoutbox) == 0
    assert isinstance(response, HttpResponseRedirectBase)

    assert response.url.startswith(
        f"{reverse('application_update', kwargs={'volunteer_id': volunteer.id})}"
    )
    query = urlparse(response.url).query

    request = rf.get(f"?{query}")
    request.user = user
    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    dialog = html.find(id="planner-modal-reject-application")
    assert isinstance(dialog, Tag)
    assert "visible" in dialog.attrs["class"]
    assert (
        cast(Tag, dialog.find("input", attrs={"name": "subject"})).attrs.get(
            "value", ""
        )
        == ""
    )
    assert (
        cast(Tag, dialog.find("textarea", attrs={"name": "body"})).text.strip()
        == "test-body"
    )
    assert "This field is required" in dialog.text


@mark.django_db
def test_delete_sends_email_if_with_email_is_given(
    rf: RequestFactory,
    mailoutbox: list[EmailMessage],
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    request = mk_request(rf, with_email=True, subject="test-subject", body="test-body")

    from_email = "test-external-from@example.com"

    with override_settings(EXTERNAL_EMAIL_FROM=from_email):
        application_reject(request, volunteer_id=volunteer.id)

    assert [(m.subject, m.body, m.to, m.from_email) for m in mailoutbox] == [
        ("test-subject", "test-body", [volunteer.email], from_email)
    ]


@mark.django_db
def test_delete_does_not_send_email_if_without_email_is_given(
    rf: RequestFactory, mailoutbox: list
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    # not using mk_request on purpose to verify that the missing subject and body fields don't
    # matter
    request = rf.post("", {"reject-without-email": "..."})
    request.user = UserFactory.create(**userfactory_permission_kwargs)

    application_reject(request, volunteer_id=volunteer.id)
    assert mailoutbox == []


@mark.django_db
@mark.parametrize("data", [None, {"foo": "bar"}, {}])
def test_returns_bad_request_if_unspecified_whether_to_send_mail(
    data, rf: RequestFactory
):
    # TODO: this duplicates the new test
    request = rf.post("", data)
    request.user = UserFactory.create(**userfactory_permission_kwargs)

    response = application_reject(request, volunteer_id=42)

    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db()
def test_delete_application_dialog_flow(
    logged_in_context: BrowserContext, mailoutbox: list[EmailMessage], backoffice_url
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )
    if features["keep_rejected_applications"]:
        page.get_by_role("button", name="Reject for all positions").click()
        dialog = page.get_by_role("dialog")
        dialog.wait_for(state="visible")
        expect(dialog.locator("form")).to_be_visible()
        dialog.get_by_role("button", name="Send & Reject", exact=True).click()
        expect(dialog).to_contain_text("application has been rejected")
    else:
        page.get_by_role("button", name="Delete application").click()
        dialog = page.get_by_role("dialog")
        dialog.wait_for(state="visible")
        expect(dialog.locator("form")).to_be_visible()
        dialog.get_by_role("button", name="Send & Delete", exact=True).click()
        expect(dialog).to_contain_text("application has been deleted")

    assert page.url.endswith(reverse("application_list"))

    if features["keep_rejected_applications"]:
        assert all(
            sa.decision == SarApplication.Decision.REJECT and sa.closed is not None
            for sa in volunteer.sarapplication_set.all()
        )
    else:
        assert not Volunteer.objects.filter(id=volunteer.id).exists()

    [mail] = mailoutbox
    assert mail.to == [volunteer.email]
    assert (
        mail.subject
        == "Update \N{EN DASH}\N{NO-BREAK SPACE}Your application for Sea-Watch Crew"
    )

    assert "after carefully reviewing your application we cannot accept" in mail.body
