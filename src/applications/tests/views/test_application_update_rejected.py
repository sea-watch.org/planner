import re
from pytest_subtests import SubTests
from bs4 import BeautifulSoup, Tag
from django.template.response import TemplateResponse
from django.test import RequestFactory
from pytest import mark

from applications.models import SarApplication
from applications.tests.factories import SarApplicationFactory
from applications.views import ApplicationUpdateView
from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory


@mark.django_db
def test_lists_rejected_positions_in_infobox(rf: RequestFactory):
    volunteer = VolunteerFactory.create(first_name="Jesse")
    sarapplications = SarApplicationFactory.create_batch(
        size=2,
        volunteer=volunteer,
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get("?rejected")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    warningbox = html.select_one(".alert.alert-warning")
    assert warningbox

    assert re.search(
        (
            r"was rejected for the following positions:\s*"
            + r"\s*".join([re.escape(sa.position.name) for sa in sarapplications])
        ),
        warningbox.text,
    )


@mark.django_db
def test_is_inert(rf: RequestFactory, subtests: SubTests):
    sarapplication = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )

    request = rf.get("?rejected")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(
        request, volunteer_id=sarapplication.volunteer.id
    )
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    def assert_is_inert(el: Tag):
        assert any(element.has_attr("inert") for element in [el, *el.parents])

    with subtests.test("form"):
        form = html.select_one("#application-update-form")
        assert form
        assert_is_inert(form)

    with subtests.test("internal"):
        internal = html.select_one(".application-internal")
        assert internal
        assert_is_inert(internal)

    with subtests.test("button"):
        (button,) = [
            button
            for button in html.select("button")
            if button.text.strip() in ["Reject for all positions", "Delete application"]
        ]
        assert_is_inert(button)


@mark.django_db
def test_warningbox_includes_accepted_position_count(rf: RequestFactory):
    sarapplication = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )

    SarApplicationFactory.create_batch(
        size=7,
        volunteer=sarapplication.volunteer,
        is_closed=True,
        decision=SarApplication.Decision.ACCEPT,
    )

    request = rf.get("?rejected")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(
        request, volunteer_id=sarapplication.volunteer.id
    )
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    warningbox = html.select_one(".alert.alert-warning")
    assert warningbox

    assert "has been accepted for 7 positions" in warningbox.text


@mark.django_db
def test_warningbox_includes_current_application_count(rf: RequestFactory):
    sarapplication = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )

    SarApplicationFactory.create_batch(
        size=7,
        volunteer=sarapplication.volunteer,
        is_closed=False,
        # decision is irrelevant here
    )

    request = rf.get("?rejected")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(
        request, volunteer_id=sarapplication.volunteer.id
    )
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    warningbox = html.select_one(".alert.alert-warning")
    assert warningbox

    assert "is currently applying for 7 positions" in warningbox.text
