from http import HTTPStatus
import time
from typing import cast

from _pytest.python_api import raises
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse
from django.test.client import Client
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark

from applications.tests.factories import SarApplicationFactory
from applications.views import update_additional_positions
from planner_common.playwright_utils import MultiChoicesJS
from seawatch_planner.tests.request_factory import RequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.tests.factories import VolunteerFactory

required_permissions = ["volunteers.change_volunteer"]


@mark.django_db
def test_is_reachable_and_requires_post(client: Client):
    client.force_login(UserFactory.create(permissions=required_permissions))
    response = client.get(
        "/applications/42/update_additional_positions/",
        headers={"Host": settings.BACKOFFICE_HOSTS[0]},  # type: ignore
    )

    assert HTTPStatus(response.status_code) == HTTPStatus.METHOD_NOT_ALLOWED


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    request = rf.get("")  # permissions are more important than http method

    request.user = UserFactory.create()  # No permissions

    with raises(PermissionDenied):
        update_additional_positions(request, volunteer_id=1323)


@mark.django_db
def test_saves_the_position(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="regular",
        position__open_for_applications=True,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="to_be_removed",
        position__open_for_applications=False,
    )

    additional_position = PositionFactory.create(
        name="to_be_added", open_for_applications=False
    )

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )
    section = page.locator(
        "section:has(> h2:has-text('Position Specific Recommendations'))"
    )
    additional_positions = MultiChoicesJS(
        section.locator("label:has-text('Additional Positions') + .choices")
    )

    additional_positions.select(additional_position.name)
    expect(section.get_by_label(additional_position.name)).to_be_visible()
    additional_positions.deselect("to_be_removed")
    expect(section.get_by_label("to_be_removed")).not_to_be_visible()

    assert {sa.position.key for sa in volunteer.sarapplication_set.open()} == {
        "regular",
        "to_be_added",
    }


@mark.django_db
def test_returns_position_selects(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position__key="first")
    SarApplicationFactory.create(volunteer=volunteer, position__key="second")

    request = rf.post("")
    request.user = UserFactory.create(permissions=required_permissions)

    response = update_additional_positions(request, volunteer_id=volunteer.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    assert {
        cast(str, select["id"]) for select in cast(list[Tag], html.find_all("select"))
    } == {
        "position-decision-first",
        "position-decision-second",
    }


@mark.django_db
def test_shows_accept_mail_form_after_deciding_to_accept_without_reload_with_additional_position(
    logged_in_context: BrowserContext, backoffice_url
):
    # This is a regression test. Bug originally discovered 2023-06-02
    page = logged_in_context.new_page()

    # For debugging
    # networks = []
    # page.on("request", lambda request: networks.append((">>", request)))
    # page.on("response", lambda response: networks.append(("<<", response)))
    # # Listen for all console logs
    # logs = []
    # page.on("console", lambda msg: logs.append(msg))

    application = SarApplicationFactory.create(decided=False)
    new_position = PositionFactory.create(name="Muad'Dib", open_for_applications=False)

    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": application.volunteer.id})}'
    )

    # The order of the next few operations is somewhat important, because playwright implicitly
    # waits until the controls are available. If we do too many changes too quickly
    page.get_by_label(application.position.name).select_option("Accept")
    additional_positions = MultiChoicesJS(
        page.get_by_label("Additional Positions").and_(page.get_by_role("combobox"))
    )
    additional_positions.select(new_position.name)

    # I think this fixes the issue maybe? Accepting the new position doesn't trigger the network
    # sometimes, maybe because the htmx event handlers aren't se up yet?
    time.sleep(0.1)

    page.get_by_label(new_position.name).select_option("Accept")

    # NOTE: This next action is occasionally failing for me recently, but I don't really know why :(
    try:
        page.get_by_role("button").get_by_text("Create profile as").click()
    except Exception as e:
        raise Exception("The weird thing happened again") from e

    assert page.get_by_role("dialog").get_by_label("Subject").is_visible()
