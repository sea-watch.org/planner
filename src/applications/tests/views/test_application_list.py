import os.path
from uuid import uuid4
from collections.abc import Callable, Iterable
from seawatch_registration.tests.util import wait_for_queryarg
from dataclasses import dataclass
from datetime import datetime
from http import HTTPStatus
from typing import Any, Literal, cast
from urllib.parse import parse_qs, urlparse

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.conf import settings
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains

from planner_common.feature_flags import features
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory

from ...models import SarApplication
from ...views import ApplicationListView, persisted_get_params_session_key
from ..factories import SarApplicationFactory

get_permissions = ["applications.view_sarapplication", "volunteers.view_volunteer"]


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    backoffice_client.force_login(UserFactory.create(permissions=get_permissions))
    response = backoffice_client.get("/applications/")
    assertContains(response, "Applications")


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf,
        ApplicationListView,
        ["applications.view_sarapplication", "volunteers.view_volunteer"],
    )


@mark.django_db
def test_empty(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "There are no open applications at the moment")


@mark.django_db
def test_application_list(rf: RequestFactory):
    SarApplicationFactory.create(volunteer__first_name="foo")
    SarApplicationFactory.create(volunteer__first_name="bar")

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "foo")
    assertContains(response, "bar")


@mark.django_db
def test_doesnt_show_volunteers_without_open_applications(rf: RequestFactory):
    VolunteerFactory.create(last_name="Foo")
    SarApplicationFactory.create(
        is_closed=True,
        volunteer__last_name="long-test-name-to-avoid-sporadic-failures",
    )
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertNotContains(response, "Foo")
    assertNotContains(response, "long-test-name-to-avoid-sporadic-failures")


@mark.django_db
def test_filter_applies_to_open_applications_only(rf: RequestFactory):
    position_show = PositionFactory.create(name="shown position")
    position_hide = PositionFactory.create(name="hidden position")
    # The volunteer has one accepted application, which should be disregarded, and one open
    # application. We're now filtering on the position of the accepted one, so the volunteer
    # shouldn't show up.
    volunteer = VolunteerFactory.create(last_name="Foo")
    SarApplicationFactory.create(
        volunteer=volunteer, position=position_show, is_closed=True
    )
    SarApplicationFactory.create(volunteer=volunteer, position=position_hide)

    request = rf.get(f"?position={position_show.key}")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertNotContains(response, "Foo")


@mark.django_db
def test_shows_volunteers_with_multiple_sarapplications_only_once(rf: RequestFactory):
    volunteer = VolunteerFactory.create(last_name="Yolocus Kwak")
    SarApplicationFactory.create(volunteer=volunteer)
    SarApplicationFactory.create(volunteer=volunteer)

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    matching_trs = html.select('tbody tr:-soup-contains("Yolocus")')
    assert len(matching_trs) == 1


@mark.django_db
def test_includes_filtered_out_sarapplications_when_evaluating_ready_to_close(
    rf: RequestFactory,
):
    position_show = PositionFactory.create(name="shown position")
    # The volunteer has one accepted application, which should be disregarded, one open and one
    # in-progress application.
    # We're now filtering on the position of the accepted one, so the volunteer shouldn't show up.
    volunteer = VolunteerFactory.create(last_name="Foo")
    SarApplicationFactory.create(volunteer=volunteer, position=position_show)
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.NEW
    )
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.IN_PROGRESS
    )

    request = rf.get(
        f"?position={position_show.key}&only_ready_to_close=on",
        headers={"Host": settings.BACKOFFICE_HOSTS[0]},  # type: ignore
    )
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertNotContains(response, "Foo")


@mark.django_db
def test_filters_by_position(rf: RequestFactory):
    SarApplicationFactory.create(
        volunteer__first_name="Henry Cook", position__key="foo"
    )
    SarApplicationFactory.create(
        volunteer__first_name="Meister Eder", position__key="bar"
    )

    request = rf.get("", {"position": "foo"})
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assertContains(response, "Henry Cook")
    assertNotContains(response, "Meister Eder")


@mark.django_db
def test_position_select_shows_all_when_not_filtered(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    assert not position_select.find("option", selected=True)
    assert cast(Tag, position_select.find("option")).text.strip() == "All Positions"


@mark.django_db
def test_position_select_shows_position_when_filtered(rf: RequestFactory):
    position = PositionFactory.create(key="pos-key", name="Pos name")
    request = rf.get("", {"position": position.key})
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    selected = position_select.find("option", selected=True)
    assert isinstance(selected, Tag)
    assert selected.text.strip() == position.name


@mark.django_db
def test_applications_list_shows_positions(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="wizard",
        decision=SarApplication.Decision.NEW,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="arcanist",
        decision=SarApplication.Decision.IN_PROGRESS,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="sorcerer",
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="warlock",
        decision=SarApplication.Decision.ACCEPT,
    )
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )

    headers = [td.text.strip() for td in html.select("thead th")]
    positions_index = headers.index("Positions")
    rows = html.select(
        f"tbody tr > td:nth-child({positions_index + 1}) li"  # css counts from 1
    )

    def icon_for_line(line: Tag):
        img = line.select_one("img")
        if img:
            src = img.attrs["src"]
            assert isinstance(src, str)
            return os.path.basename(src)
        else:
            return None

    assert {row.text.strip(): icon_for_line(row) for row in rows} == {
        "wizard": None,
        # TODO: do we want a symbol for in-progress? #289
        "arcanist": None,
        "warlock": "check.svg",
        "sorcerer": "close.svg",
    }


@mark.django_db
def test_search_one_application_match_one(rf: RequestFactory):
    # given application and search param
    search_param = "alon"
    not_search_param = "simontheunique"
    SarApplicationFactory.create(volunteer__first_name=search_param)
    SarApplicationFactory.create(volunteer__first_name=not_search_param)

    request = rf.get("", {"search": search_param})

    # wehn an a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)

    # response should contain the search application and not include the opther user
    assert isinstance(response, TemplateResponse)
    assert search_param in response.rendered_content
    assert not_search_param not in response.rendered_content


@mark.django_db
def test_search_one_application_match_one_when_position_exists(rf: RequestFactory):
    # given application and search param
    search_param = "Ashley"
    SarApplicationFactory.create(volunteer__first_name=search_param)

    request = rf.get("", {"search": search_param, "position": "test"})

    # when a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    # then the response should not contain any result
    assert isinstance(response, TemplateResponse)
    assert "There are no open applications at the moment." in response.rendered_content


@mark.django_db
def test_all_closed_filter_returns_only_ready_applications(rf: RequestFactory):
    # given application and search param
    only_closed_filter_param = "true"
    SarApplicationFactory.create(
        volunteer__first_name="George",
        position__key="gremlin",
        decision=SarApplication.Decision.ACCEPT,
    )

    SarApplicationFactory.create(
        volunteer__first_name="BeckaTheUniqueName",
        position__key="mediator",
        decided=False,
    )

    request = rf.get("", {"only_ready_to_close": only_closed_filter_param})

    # when a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    # then the response should contain one result, George
    assert isinstance(response, TemplateResponse)
    assert "George" in response.rendered_content
    assert "BeckaTheUniqueName" not in response.rendered_content


@mark.django_db
def test_all_closed_filter_combined_with_search_filter_returns_only_one_applications(
    rf: RequestFactory,
):
    # given application and search param
    only_closed_filter_param = "true"
    search_param = "Becka"
    position = PositionFactory.create(key="gremlin")

    SarApplicationFactory.create(
        volunteer__first_name="GeorgeTheUniqueName",
        position=position,
        decision=SarApplication.Decision.ACCEPT,
    )

    SarApplicationFactory.create(
        volunteer__first_name="Becka",
        position=position,
        decision=SarApplication.Decision.ACCEPT,
    )

    SarApplicationFactory.create(
        volunteer__first_name="BettyTheUniqueName", position=position
    )

    request = rf.get(
        "", {"only_ready_to_close": only_closed_filter_param, "search": search_param}
    )

    # when a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    # then the response should contain one result, Becka
    assert isinstance(response, TemplateResponse)
    assert "GeorgeTheUniqueName" not in response.rendered_content
    assert "BettyTheUniqueName" not in response.rendered_content
    assert "Becka" in response.rendered_content


@mark.django_db
def test_application_filter_workflow_should_work_with_only_ready_to_close_filter(
    rf: RequestFactory,
):
    def mk_volunteer(decisions: Iterable[SarApplication.Decision]) -> Volunteer:
        volunteer = VolunteerFactory.create(first_name=str(uuid4()))
        for decision in decisions:
            SarApplicationFactory.create(volunteer=volunteer, decision=decision)
        return volunteer

    always_visible = [
        mk_volunteer([SarApplication.Decision.ACCEPT]),
        mk_volunteer([SarApplication.Decision.REJECT]),
    ]
    not_visible_when_only_ready_to_close = [
        mk_volunteer([SarApplication.Decision.NEW]),
        mk_volunteer([SarApplication.Decision.IN_PROGRESS]),
        mk_volunteer([SarApplication.Decision.ACCEPT, SarApplication.Decision.NEW]),
        mk_volunteer(
            [SarApplication.Decision.ACCEPT, SarApplication.Decision.IN_PROGRESS]
        ),
    ]

    request = rf.get("", {"only_ready_to_close": "true"})
    request.user = UserFactory.create(permissions=get_permissions)
    response = ApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)

    for volunteer in always_visible:
        assertContains(response, volunteer.first_name)

    for volunteer in not_visible_when_only_ready_to_close:
        assertNotContains(response, volunteer.first_name)


@dataclass
class SortField:
    label: str
    sort: str
    to_sortable: Callable[[str], Any]


sortable_fields = [
    # Postgresql seems to sort case-insensitive by default
    SortField("Name", "name", lambda p: p.lower().split(",")),
    SortField(
        "Applied at", "applied_at", lambda s: datetime.strptime(s, "%d.%m.%Y, %H:%M")
    ),
    SortField(
        "Positions",
        "positions",
        # We only care about the first position here, that's the best sort we can do
        lambda ps: ps.lower().replace(" ", "").replace(",", ""),
    ),
]


@mark.django_db
@mark.parametrize("field", sortable_fields)
def test_shows_sort_links(field: SortField, rf: RequestFactory):
    SarApplicationFactory.create_batch(10)

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)

    response = ApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    thead = cast(Tag, html.find("thead"))
    sort_link = thead.select_one(f'th a:-soup-contains("{field.label}")')
    assert isinstance(sort_link, Tag)
    assert sort_link["href"] == f"?sort={field.sort}"


@mark.django_db
@mark.parametrize("field", sortable_fields)
@mark.parametrize("direction", ["asc", "desc"])
def test_sorts_by_fields(
    field: SortField, direction: Literal["asc", "desc"], rf: RequestFactory
):
    SarApplicationFactory.create_batch(10)

    request = rf.get(f"?sort={'-' if direction == 'desc' else ''}{field.sort}")
    request.user = UserFactory.create(permissions=get_permissions)
    html = BeautifulSoup(
        cast(TemplateResponse, ApplicationListView.as_view()(request)).rendered_content,
        features="html.parser",
    )
    headers = [th.text.strip() for th in html.select("thead th")]
    field_index = headers.index(field.label)
    values = [
        e.text.strip()
        for e in html.select(
            f"tbody tr > :nth-child({field_index + 1})"  # css starts counting at 1
        )
    ]
    assert values, "values shouldn't be empty here!"
    assert values == sorted(values, key=field.to_sortable, reverse=direction == "desc")


def test_application_list_filters_work_together_with_table_sort_filters(
    logged_in_context: BrowserContext, backoffice_url
):
    PositionFactory.create(key="position-one", name="Position One")

    page = logged_in_context.new_page()
    page.goto(f"{backoffice_url}/applications")

    # check that we are on applications list page and filter form is available
    title = page.locator('h1:has-text("Open Applications")')
    expect(title).to_be_visible()
    form = page.locator("id=application_list_filter_form")
    expect(form).to_be_visible()

    # add search filter and sort table by name, check url contains the right filters
    search_input = page.locator("id=search")
    search_input.fill("Julia")
    search_button = page.locator("id=search_button")
    search_button.click()

    name_sort_a = page.locator('a:has-text("Name")')
    name_sort_a.click()

    wait_for_queryarg(page, "search", "Julia")
    wait_for_queryarg(page, "sort", "name")

    # add position filter and check that sort filter is still passed in url
    position_dropdown = page.locator("id=position-filter")
    position_dropdown.select_option("position-one")

    wait_for_queryarg(page, "search", "Julia")
    wait_for_queryarg(page, "sort", "name")
    wait_for_queryarg(page, "position", "position-one")


##############################
## Preserve filters in session


@mark.django_db
def test_redirects_to_data_from_session_if_no_filters_or_sort_specified(
    rf: RequestFactory,
):
    # setup: select search, position and sort filter
    position = PositionFactory.create(key="position-one", name="Position One")
    SarApplicationFactory.create(
        volunteer__first_name="Julia",
        volunteer__last_name="Position1",
        position=position,
        decided=False,
    )
    SarApplicationFactory.create(
        volunteer__first_name="Julia2",
        volunteer__last_name="Position1",
        position=position,
        decision=SarApplication.Decision.ACCEPT,
    )

    session = {}

    filter_values = {
        "sort": ["-name"],
        "only_ready_to_close": ["on"],
        "search": ["test-search"],
        "position": [position.key],
    }

    user = UserFactory.create(permissions=get_permissions)
    request = rf.get("", data=filter_values)
    request.user = user
    request.session = session  # type: ignore
    ApplicationListView.as_view()(request)

    request = rf.get("")
    request.user = user
    request.session = session  # type: ignore

    response = ApplicationListView.as_view()(request)
    assert response.status_code // 100 == 3
    location = urlparse(response["Location"])
    assert parse_qs(location.query) == filter_values


@mark.django_db
@mark.parametrize(
    "key,value",
    [
        ("sort", "name"),
        ("only_ready_to_close", "on"),
        ("position", "position-key"),
        ("search", "foo"),
    ],
)
def test_renders_page_if_filters_are_given(key, value, rf: RequestFactory):
    PositionFactory.create(key="position-key")
    user = UserFactory.create(permissions=get_permissions)

    request = rf.get("", {key: value})
    request.user = user

    response = ApplicationListView.as_view()(request)
    assert response.status_code == HTTPStatus.OK


@mark.django_db
@mark.parametrize("session_data", ["garbage", ["list", "garbage"], {"unknown": "keys"}])
def test_works_with_unexpected_data_in_session(session_data, rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    request.session = {  # type: ignore
        persisted_get_params_session_key: session_data
    }

    response = ApplicationListView.as_view()(request)
    assert response.status_code == HTTPStatus.OK


##############################
## More tests


@mark.django_db
@mark.skipif(not features["keep_rejected_applications"], reason="work in progress")
def test_links_to_open_and_rejected_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)

    response = ApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    links = {a.text.strip(): a["href"] for a in html.select("a")}
    assert links["Rejected"] == reverse("application_list_rejected")
    assert links["Open"] == reverse("application_list")
