import datetime
import json
from typing import Type, cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark, raises
from pytest_django.asserts import assertContains

from applications.models import SarApplication
from planner_common.models import Gender
from planner_common.playwright_utils import select_in_choices_js
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Document
from volunteers.tests.factories import DocumentFactory, VolunteerFactory

from ...views import ApplicationListView, ApplicationUpdateView
from ..factories import SarApplicationFactory


def ass[T](obj, type: Type[T]) -> T:
    assert isinstance(obj, type)
    return obj


@mark.django_db
def test_requires_permissions(rf: RequestFactory):
    request = rf.get("/applications/ignored")
    request.user = UserFactory.create()

    with raises(PermissionDenied):
        ApplicationUpdateView.as_view()(request, volunteer_id=1234)


@mark.django_db
def test_reachable(client: Client):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)
    client.force_login(UserFactory.create(view_permissions=ApplicationUpdateView))
    response = client.get(
        reverse(
            "application_update",
            kwargs={"volunteer_id": volunteer.id},
        ),
        headers={"Host": settings.BACKOFFICE_HOSTS[0]},  # type: ignore
    )
    assert response.status_code == 200


@mark.django_db
def test_is_reachable_via_link_from_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=["applications.view_sarapplication", "volunteers.view_volunteer"]
    )

    volunteer = VolunteerFactory.create(first_name="Molly", last_name="Millions")
    SarApplicationFactory.create(volunteer=volunteer)

    response = ApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    def filter(s: str):
        return s is not None and s.strip() == "Millions, Molly"

    link = html.find("a", string=filter)
    assert isinstance(link, Tag)
    assert link["href"] == reverse(
        "application_update", kwargs={"volunteer_id": volunteer.id}
    )


@mark.django_db
def test_shows_fields(rf: RequestFactory):
    positions = PositionFactory.create_batch(2)

    volunteer = VolunteerFactory.create(
        first_name="Peter",
        email="ghjkmail@test.com",
        phone_number="999888777",
        motivation="Motivation Oh yeah!",
        qualification="Qualification Seriously!",
        date_of_birth=datetime.date(2000, 1, 1),
        nationalities=["AX", "PL", "IT"],
        gender=Gender.NONE_OTHER,
        languages={"deu": 2, "eng": 1},
        homepage="https://never.ever.clever.land",
    )
    for p in positions:
        SarApplicationFactory.create(volunteer=volunteer, position=p)

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    assertContains(response, "Peter")
    assertContains(response, "ghjkmail@test.com")
    assertContains(response, "999888777")
    assertContains(response, "Motivation Oh yeah!")
    assertContains(response, "Qualification Seriously!")
    assertContains(response, "2000-01-01")
    assertContains(response, "https://never.ever.clever.land")

    nationalities_select = html.find("select", attrs={"name": "nationalities"})
    assert isinstance(nationalities_select, Tag)

    assert {
        (ass(t, Tag).attrs["value"], t.text)
        for t in nationalities_select.find_all("option", selected=True)
    } == {("AX", "Åland Islands"), ("IT", "Italy"), ("PL", "Poland")}

    positions_select = html.find("select", attrs={"name": "positions"})
    assert isinstance(positions_select, Tag)
    assert {
        (ass(t, Tag).attrs["value"], t.text)
        for t in positions_select.find_all("option", selected=True)
    } == {(str(p.id), p.name) for p in positions}

    languages_select = html.find("select", attrs={"name": "languages__choices"})
    assert isinstance(languages_select, Tag)
    assert {
        (ass(t, Tag).attrs["value"], t.text.strip())
        for t in languages_select.find_all("option", selected=True)
    } == {("deu", "German"), ("eng", "English")}
    languages_data = html.find("input", attrs={"name": "languages"})
    assert isinstance(languages_data, Tag)
    assert json.loads(cast(str, languages_data["value"])) == {"deu": 2, "eng": 1}

    assertContains(response, "None/Other")


@mark.django_db
def test_creates_history_item(rf: RequestFactory):
    volunteer = VolunteerFactory.create(last_name="Kirk")
    SarApplicationFactory.create(volunteer=volunteer)

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)
    form_response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    form = BeautifulSoup(
        cast(TemplateResponse, form_response).rendered_content,
        features="html.parser",
    ).select_one("#application-update-form")
    assert isinstance(form, Tag)

    original_data = {
        **{
            e["name"]: e["value"]
            for e in form.select("input")
            if e.has_attr("value") and e.has_attr("name")
        },
        **{
            s["name"]: [o["value"] for o in s.select("option[selected]")]
            for s in form.select("select")
            if s.has_attr("name")
        },
    }

    request = rf.post("", {**original_data, "last_name": "Spock"})
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    assert (
        response.status_code == 302
    ), f"didn't get redirect, probably form errors: {response.context_data['form'].errors!r}"  # type: ignore

    h = volunteer.history.last()
    assert h
    assert h.change_json == [{"field": "last_name", "old": "Kirk", "new": "Spock"}]


def test_submitting_the_form_updates_the_data(
    logged_in_context: BrowserContext, backoffice_url
):
    current_position = PositionFactory.create()
    volunteer = VolunteerFactory.create(
        gender=Gender.MALE,
        certificates=[],
        nationalities=["de"],
        languages={"deu": 3},
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position=current_position,
    )
    new_position = PositionFactory.create()

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )

    new_data = {
        "first_name": "new-first-name",
        "last_name": "new-last-name",
        "phone_number": "+491709684061",
        "email": "new@test.com",
        "date_of_birth": datetime.date(2004, 1, 1),
        "homepage": "https://foo.test.com",
        "gender": Gender.FEMALE.value,
    }

    form = page.locator("form:has(legend:has-text('Personal Data'))")
    for key, label in [
        ("first_name", "First Name"),
        ("last_name", "Last Name"),
        ("phone_number", "Phone Number"),
        ("email", "Email Address"),
        ("homepage", "Homepage"),
    ]:
        form.get_by_label(label).fill(new_data[key])

    form.get_by_label("Date of Birth").fill(new_data["date_of_birth"].isoformat())

    form.get_by_label("Gender").select_option(new_data["gender"])

    select_in_choices_js(
        form.locator(".field:has(label:has-text('Nationalities'))"), "Palau"
    )
    select_in_choices_js(
        form.locator(".field:has(label:has-text('Positions'))"), new_position.name
    )
    select_in_choices_js(
        form.locator(".field:has(label:has-text('Spoken Languages'))"),
        "Sudanese Arabic",
    )
    form.get_by_label("Sudanese Arabic").select_option("2")
    certificates = form.locator(".field:has(label:has-text('Certificates'))")
    select_in_choices_js(certificates, "STCW VI/4 (2) Medical Care")
    select_in_choices_js(certificates, "Other (free text)")
    form.get_by_label(
        "Other Certificate (free text)",
    ).fill("my fancy personal certificate")

    form.locator("role=button >> text=Save Changes").click()

    expect(
        page.locator(".planner-modal-body >> visible=true"),
    ).to_contain_text("The application was successfully updated")

    # Verify the data has been saved

    volunteer.refresh_from_db()
    expected = {
        **new_data,
        "nationalities": ["de", "pw"],
        "languages": {"deu": 3, "apd": 2},
        "certificates": [
            "STCW-VI/4-2",
            "other: my fancy personal certificate",
        ],
    }
    assert {k: getattr(volunteer, k) for k in expected.keys()} == expected
    assert set(sa.position for sa in volunteer.sarapplication_set.open()) == {
        current_position,
        new_position,
    }


@mark.parametrize("decision", list(SarApplication.Decision))
@mark.django_db
def test_shows_decisions_for_positions(decision, rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__key="test-position-key",
        position__name="test-position",
        decision=decision,
    )

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)
    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)

    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    label = html.find("label", string="test-position")
    assert isinstance(label, Tag)
    select = html.find(id=label.attrs["for"])
    assert isinstance(select, Tag)

    assert {
        ass(option.attrs["value"], str)
        for option in select.select("option")
        if "selected" in option.attrs
    } == {decision.value}


@mark.django_db
@mark.parametrize("cv_present", [True, False])
def test_displays_cv_file(cv_present: bool, rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    cv = (
        DocumentFactory.create(
            volunteer=volunteer, type=Document.Type.cv, filename="the_cv.pdf"
        )
        if cv_present
        else None
    )

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)
    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    field = html.select_one(
        '.application-data .field:has(label:-soup-contains("Uploaded File"))'
    )
    assert field
    if cv_present:
        download_link = field.select_one("a")
        assert download_link
        expected = {
            "text": "the_cv.pdf",
            "attrs": {
                "href": cast(Document, cv).file.url,
                "download": "",
                "target": "_blank",
            },
        }
        assert {
            "text": download_link.text.strip(),
            "attrs": {k: download_link.attrs[k] for k in expected["attrs"].keys()},
        } == expected
    else:
        assert "No CV uploaded for this application" in field.text


@mark.django_db
def test_returns_not_found_on_nonexistant_volunteer(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)
    with raises(Http404):
        ApplicationUpdateView.as_view()(request, volunteer_id=123901234)


@mark.django_db
def test_shows_accept_mail_form_after_deciding_to_accept_without_reload(
    logged_in_context: BrowserContext, backoffice_url
):
    # This is a regression test. Bug originally discovered 2023-06-02
    page = logged_in_context.new_page()

    application = SarApplicationFactory.create(is_closed=False, decided=False)

    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": application.volunteer.id})}'
    )

    page.locator("select[name=decision]").select_option("Accept")
    page.get_by_role("button").get_by_text("Create profile as").click()

    expect(page.get_by_role("dialog").get_by_label("Subject")).to_be_visible()


@mark.django_db
@mark.parametrize("was_rejected", [True, False])
def test_shows_infobox_iff_volunteer_was_rejected(was_rejected, rf: RequestFactory):
    volunteer = VolunteerFactory.create(first_name="Jesse")
    SarApplicationFactory.create(volunteer=volunteer, is_closed=False)

    if was_rejected:
        SarApplicationFactory.create(
            volunteer=volunteer, is_closed=True, decision=SarApplication.Decision.REJECT
        )

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    infobox = html.select_one(".alert.alert-info")
    if was_rejected:
        assert infobox
        assert infobox.text.strip().startswith("Jesse was rejected for 1 position")
        link = infobox.select_one("a")
        assert link
        assert link["href"] == "?rejected"
    else:
        assert not infobox
