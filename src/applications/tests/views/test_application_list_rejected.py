from dataclasses import dataclass
from datetime import datetime
from http import HTTPStatus
from datetime import timezone
from typing import Any, Callable, Literal, cast
from urllib.parse import parse_qs, urlparse
from bs4 import BeautifulSoup, Tag
from django.conf import settings
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains

from applications.tests.factories import SarApplicationFactory
from planner_common.feature_flags import features
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from seawatch_registration.tests.util import wait_for_queryarg
from volunteers.tests.factories import VolunteerFactory

from ...models import SarApplication
from ...views import RejectedApplicationListView, persisted_get_params_session_key
import os.path

get_permissions = ["applications.view_sarapplication", "volunteers.view_volunteer"]

post_permissions = [
    "applications.add_sarapplication",
    "volunteers.add_volunteer",
    "volunteers.change_volunteer",
]


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory.create(permissions=get_permissions))
    response = client.get(
        "/applications/rejected/",
        headers={"Host": settings.BACKOFFICE_HOSTS[0]},  # type: ignore
    )
    assertContains(response, "Applications")


@mark.django_db
def test_empty(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "There are no rejected applications at the moment")


@mark.django_db
def test_lists_volunteers_with_rejected_applications(rf: RequestFactory):
    SarApplicationFactory.create(
        volunteer__first_name="foo",
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )
    sa2 = SarApplicationFactory.create(
        volunteer__first_name="bar",
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer=sa2.volunteer,
        is_closed=True,
        decision=SarApplication.Decision.ACCEPT,
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "foo")
    assertContains(response, "bar")


@mark.django_db
def test_includes_position(rf: RequestFactory):
    SarApplicationFactory.create(
        position__name="Superhero deluxe",
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "Superhero deluxe")


@mark.django_db
def test_includes_rejection_dates(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    for rejection_date in [
        datetime(2020, 1, 1, 17, 0, 0, tzinfo=timezone.utc),
        datetime(2023, 1, 1, 13, 0, 0, tzinfo=timezone.utc),
        datetime(2023, 1, 1, 18, 0, 0, tzinfo=timezone.utc),
    ]:
        SarApplicationFactory.create(
            volunteer=volunteer,
            closed=rejection_date,
            decision=SarApplication.Decision.REJECT,
        )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "01.01.2020")
    assertContains(response, "01.01.2023", count=1)


@mark.django_db
def test_doesnt_show_volunteers_without_rejected_applications(rf: RequestFactory):
    name = "von Schweinebacke"

    VolunteerFactory.create(last_name=name)
    SarApplicationFactory.create(
        is_closed=True,
        decision=SarApplication.Decision.ACCEPT,
        volunteer__last_name="long-test-name-to-avoid-sporadic-failures",
    )
    SarApplicationFactory.create(
        is_closed=False,
        decision=SarApplication.Decision.REJECT,
        volunteer__last_name="marry-poppins",
    )
    sa = SarApplicationFactory.create(
        is_closed=True,
        decision=SarApplication.Decision.ACCEPT,
        volunteer__last_name="barben-heimer",
    )
    SarApplicationFactory.create(
        is_closed=False, decision=SarApplication.Decision.REJECT, volunteer=sa.volunteer
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertNotContains(response, name)
    assertNotContains(response, "long-test-name-to-avoid-sporadic-failures")
    assertNotContains(response, "marry-poppins")
    assertNotContains(response, "barben-heimer")


@mark.django_db
@mark.skipif(not features["keep_rejected_applications"], reason="work in progress")
def test_volunteers_with_multiple_sarapplications_are_listed_only_once(
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create(first_name="banana-rama-lama")
    SarApplicationFactory.create(
        volunteer=volunteer, is_closed=True, decision=SarApplication.Decision.REJECT
    )
    SarApplicationFactory.create(
        volunteer=volunteer, is_closed=True, decision=SarApplication.Decision.REJECT
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "banana-rama-lama", count=1)


@mark.django_db
@mark.skip("todo")
def test_filter_applies_to_open_applications_only(rf: RequestFactory):
    position_show = PositionFactory.create(name="shown position")
    position_hide = PositionFactory.create(name="hidden position")
    # The volunteer has one accepted application, which should be disregarded, and one open
    # application. We're now filtering on the position of the accepted one, so the volunteer
    # shouldn't show up.
    volunteer = VolunteerFactory.create(last_name="Foo")
    SarApplicationFactory.create(
        volunteer=volunteer, position=position_show, is_closed=True
    )
    SarApplicationFactory.create(volunteer=volunteer, position=position_hide)

    request = rf.get(f"?position={position_show.key}")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertNotContains(response, "Foo")


@mark.django_db
def test_filters_by_position(rf: RequestFactory):
    SarApplicationFactory.create(
        volunteer__first_name="Henry Cook",
        position__key="foo",
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer__first_name="Meister Eder",
        position__key="bar",
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get("", {"position": "foo"})
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assertContains(response, "Henry Cook")
    assertNotContains(response, "Meister Eder")


@mark.django_db
def test_position_select_shows_all_when_not_filtered(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    assert not position_select.find("option", selected=True)
    assert cast(Tag, position_select.find("option")).text.strip() == "All Positions"


@mark.django_db
def test_position_select_shows_position_when_filtered(rf: RequestFactory):
    position = PositionFactory.create(key="pos-key", name="Pos name")
    request = rf.get("", {"position": position.key})
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    position_select = html.find("select", id="position-filter")
    assert isinstance(position_select, Tag)
    selected = position_select.find("option", selected=True)
    assert isinstance(selected, Tag)
    assert selected.text.strip() == position.name


@mark.django_db
def test_applications_list_shows_positions(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="sorcerer",
        decision=SarApplication.Decision.REJECT,
        is_closed=True,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__name="warlock",
        decision=SarApplication.Decision.ACCEPT,
        is_closed=True,
    )
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )

    headers = [td.text.strip() for td in html.select("thead th")]
    positions_index = headers.index("Positions")
    lines = html.select(
        f"tbody tr > td:nth-child({positions_index + 1}) li"  # css counts from 1
    )

    def icon_for_line(line: Tag):
        img = line.select_one("img")
        if img:
            src = img.attrs["src"]
            assert isinstance(src, str)
            return os.path.basename(src)
        else:
            return None

    assert {line.text.strip(): icon_for_line(line) for line in lines} == {
        "warlock": "check.svg",
        "sorcerer": "close.svg",
    }


@mark.django_db
def test_search_one_application_match_one(rf: RequestFactory):
    # given application and search param
    search_param = "alon"
    not_search_param = "simontheunique"
    SarApplicationFactory.create(
        volunteer__first_name=search_param,
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer__first_name=not_search_param,
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get("", {"search": search_param})

    # wehn an a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)

    # response should contain the search application and not include the opther user
    assert isinstance(response, TemplateResponse)
    assert search_param in response.rendered_content
    assert not_search_param not in response.rendered_content


@mark.django_db
def test_search_one_application_match_one_when_position_exists(rf: RequestFactory):
    # given application and search param
    search_param = "Ashley"
    SarApplicationFactory.create(
        volunteer__first_name=search_param,
        is_closed=True,
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get("", {"search": search_param, "position": "test"})

    # when a search is being done
    request.user = UserFactory.create(permissions=get_permissions)
    response = RejectedApplicationListView.as_view()(request)
    # then the response should not contain any result
    assert isinstance(response, TemplateResponse)
    assert (
        "There are no rejected applications at the moment." in response.rendered_content
    )


@dataclass
class SortField:
    label: str
    sort: str
    to_sortable: Callable[[str], Any]


sortable_fields = [
    # Postgresql seems to sort case-insensitive by default
    SortField("Name", "name", lambda p: p.lower().replace(" ", "").split(",")),
    SortField(
        "Applied at", "applied_at", lambda s: datetime.strptime(s, "%d.%m.%Y, %H:%M")
    ),
    SortField(
        "Positions",
        "positions",
        # We only care about the first position here, that's the best sort we can do
        lambda ps: ps.lower().replace(" ", "").replace(",", ""),
    ),
]


@mark.django_db
@mark.parametrize("field", sortable_fields)
def test_shows_sort_links(field: SortField, rf: RequestFactory):
    SarApplicationFactory.create_batch(
        10, is_closed=True, decision=SarApplication.Decision.REJECT
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)

    response = RejectedApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    thead = cast(Tag, html.find("thead"))
    sort_link = thead.select_one(f'th a:-soup-contains("{field.label}")')
    assert isinstance(sort_link, Tag)
    assert sort_link["href"] == f"?sort={field.sort}"


@mark.django_db
@mark.parametrize("field", sortable_fields)
@mark.parametrize("direction", ["asc", "desc"])
def test_sorts_by_fields(
    field: SortField, direction: Literal["asc", "desc"], rf: RequestFactory
):
    SarApplicationFactory.create_batch(
        10, is_closed=True, decision=SarApplication.Decision.REJECT
    )

    request = rf.get(f"?sort={'-' if direction == 'desc' else ''}{field.sort}")
    request.user = UserFactory.create(permissions=get_permissions)
    html = BeautifulSoup(
        cast(
            TemplateResponse, RejectedApplicationListView.as_view()(request)
        ).rendered_content,
        features="html.parser",
    )
    headers = [th.text.strip() for th in html.select("thead th")]
    field_index = headers.index(field.label)
    values = [
        e.text.strip()
        for e in html.select(
            f"tbody tr > :nth-child({field_index + 1})"  # css starts counting at 1
        )
    ]
    assert values, "values shouldn't be empty here!"
    assert values == sorted(values, key=field.to_sortable, reverse=direction == "desc")


@mark.django_db
@mark.parametrize("direction", ["asc", "desc"])
def test_sorts_by_rejected_prioritizing_last_rejection_date(
    direction: Literal["asc", "desc"], rf: RequestFactory
):
    vol1 = VolunteerFactory.create(first_name="2022", last_name="Lastname")
    vol2 = VolunteerFactory.create(first_name="2023", last_name="Lastname")
    SarApplicationFactory.create(
        volunteer=vol1,
        closed=datetime(2022, 1, 1, tzinfo=timezone.utc),
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer=vol2,
        closed=datetime(2021, 1, 1, tzinfo=timezone.utc),
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer=vol2,
        closed=datetime(2023, 1, 1, tzinfo=timezone.utc),
        decision=SarApplication.Decision.REJECT,
    )

    request = rf.get(f"?sort={'-' if direction == 'desc' else ''}rejected_at")
    request.user = UserFactory.create(permissions=get_permissions)
    html = BeautifulSoup(
        cast(
            TemplateResponse, RejectedApplicationListView.as_view()(request)
        ).rendered_content,
        features="html.parser",
    )
    names = [e.text.strip() for e in html.select("tbody tr > :first-child")]
    match direction:
        case "asc":
            assert names == ["Lastname, 2022", "Lastname, 2023"]
        case "desc":
            assert names == ["Lastname, 2023", "Lastname, 2022"]


def test_application_list_filters_work_together_with_table_sort_filters(
    logged_in_context: BrowserContext, backoffice_url: str
):
    PositionFactory.create(key="position-one", name="Position One")

    page = logged_in_context.new_page()
    page.goto(f"{backoffice_url}/applications")

    # check that we are on applications list page and filter form is available
    title = page.locator('h1:has-text("Open Applications")')
    expect(title).to_be_visible()
    form = page.locator("id=application_list_filter_form")
    expect(form).to_be_visible()

    # add search filter and sort table by name, check url contains the right filters
    search_input = page.locator("id=search")
    search_input.fill("Julia")
    search_button = page.locator("id=search_button")
    search_button.click()

    name_sort_a = page.locator('a:has-text("Name")')
    name_sort_a.click()

    wait_for_queryarg(page, "search", "Julia")
    wait_for_queryarg(page, "sort", "name")

    # add position filter and check that sort filter is still passed in url
    position_dropdown = page.locator("id=position-filter")
    position_dropdown.select_option("position-one")

    wait_for_queryarg(page, "search", "Julia")
    wait_for_queryarg(page, "sort", "name")
    wait_for_queryarg(page, "position", "position-one")


##############################
## Preserve filters in session


@mark.django_db
def test_redirects_to_data_from_session_if_no_filters_or_sort_specified(
    rf: RequestFactory,
):
    # setup: select search, position and sort filter
    position = PositionFactory.create(key="position-one", name="Position One")
    SarApplicationFactory.create(
        volunteer__first_name="Julia",
        volunteer__last_name="Position1",
        position=position,
        decision=SarApplication.Decision.REJECT,
    )
    SarApplicationFactory.create(
        volunteer__first_name="Julia2",
        volunteer__last_name="Position1",
        position=position,
        decision=SarApplication.Decision.REJECT,
    )

    session = {}

    filter_values = {
        "sort": ["-name"],
        "only_ready_to_close": ["on"],
        "search": ["test-search"],
        "position": [position.key],
    }

    user = UserFactory.create(permissions=get_permissions)
    request = rf.get("", data=filter_values)
    request.user = user
    request.session = session  # type: ignore
    RejectedApplicationListView.as_view()(request)

    request = rf.get("")
    request.user = user
    request.session = session  # type: ignore

    response = RejectedApplicationListView.as_view()(request)
    assert (
        response.status_code // 100 == 3
    ), "expected to redirect to list with saved filters"
    location = urlparse(response["Location"])
    assert parse_qs(location.query) == filter_values


@mark.django_db
@mark.parametrize(
    "key,value",
    [
        ("sort", "name"),
        ("only_ready_to_close", "on"),
        ("position", "position-key"),
        ("search", "foo"),
    ],
)
def test_renders_page_if_filters_are_given(key, value, rf: RequestFactory):
    PositionFactory.create(key="position-key")
    user = UserFactory.create(permissions=get_permissions)

    request = rf.get("", {key: value})
    request.user = user

    response = RejectedApplicationListView.as_view()(request)
    assert response.status_code == HTTPStatus.OK


@mark.django_db
@mark.parametrize("session_data", ["garbage", ["list", "garbage"], {"unknown": "keys"}])
def test_works_with_unexpected_data_in_session(session_data, rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)
    request.session = {  # type: ignore
        persisted_get_params_session_key: session_data
    }

    response = RejectedApplicationListView.as_view()(request)
    assert response.status_code == HTTPStatus.OK


@mark.django_db
@mark.skipif(not features["keep_rejected_applications"], reason="work in progress")
def test_links_to_rejected_details(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)

    sarapplication = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )

    response = RejectedApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    link = html.select_one("tbody th:first-child a")
    assert link

    assert link["href"] == (
        f'{reverse("application_update", kwargs={"volunteer_id": sarapplication.volunteer.id})}?rejected'
    )


@mark.django_db
@mark.skipif(not features["keep_rejected_applications"], reason="work in progress")
def test_links_to_rejected_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=get_permissions)

    response = RejectedApplicationListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    links = {a.text.strip(): a["href"] for a in html.select("a")}
    assert links["Rejected"] == reverse("application_list_rejected")
