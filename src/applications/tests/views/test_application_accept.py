from datetime import datetime, timezone
from typing import Literal, assert_never, cast
from urllib.parse import urlparse

from bs4 import BeautifulSoup, Tag
from django.conf import settings
from django.contrib import messages
from django.core.mail.message import EmailMessage
from django.http.response import HttpResponseRedirect, HttpResponseRedirectBase
from django.template.response import TemplateResponse
from django.test import Client, override_settings
from django.urls import reverse
from freezegun.api import freeze_time
from playwright.sync_api import BrowserContext, expect
from pytest import LogCaptureFixture, mark
from pytest_django.asserts import assertRedirects

from applications.models import SarApplication
from planner_common.feature_flags import features
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_planner.tests.request_factory import RequestFactory
from seawatch_registration.tests.factories import UserFactory
from volunteers.models import SarProfile, Volunteer
from volunteers.tests.factories import VolunteerFactory

from ...views import ApplicationUpdateView, accept_application
from ..factories import SarApplicationFactory

required_permissions = [
    "applications.change_sarapplication",
    "applications.delete_sarapplication",
    "volunteers.add_sarprofile",
]


@mark.django_db
def test_creates_profile_for_accepted_application(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    application = SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.post("", {"subject": "foo", "body": "bar"})
    request.user = UserFactory.create(permissions=required_permissions)

    frozen_time = datetime(2002, 12, 24, 18, tzinfo=timezone.utc)
    with freeze_time(frozen_time):
        response = accept_application(request, volunteer_id=volunteer.id)

    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse("application_list")

    application.refresh_from_db()
    assert application.closed == frozen_time
    assert volunteer.sarprofile_set.filter(position=application.position).exists()

    # Message
    assert request._messages.messages == [
        (
            messages.INFO,
            f"Application accepted, {volunteer.first_name} is now in the volunteer pool",
            "",
        )
    ]


@mark.django_db
def test_sends_email_to_volunteer(rf: RequestFactory, mailoutbox: list[EmailMessage]):
    from_email = "test-external-from@example.com"
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.post("", {"subject": "foo", "body": "bar"})
    request.user = UserFactory.create(permissions=required_permissions)

    with override_settings(EXTERNAL_EMAIL_FROM=from_email):
        accept_application(request, volunteer_id=volunteer.id)

    # Mail
    assert [(m.subject, m.body, m.to, m.from_email) for m in mailoutbox] == [
        (
            "foo",
            "bar",
            [volunteer.email],
            from_email,
        )
    ]


@mark.django_db
@mark.parametrize(
    ["previous_joined_at", "should_write"],
    [(None, True), (datetime(2023, 5, 1, tzinfo=timezone.utc), False)],
)
def test_sets_joined_at_on_volunteer_iff_not_already_set(
    previous_joined_at, should_write, rf: RequestFactory
):
    volunteer = VolunteerFactory.create(joined_at=previous_joined_at)
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.post("", {"subject": "foo", "body": "bar"})
    request.user = UserFactory.create(permissions=required_permissions)
    now = datetime(2024, 8, 1, 13, 15, 15, tzinfo=timezone.utc)
    with freeze_time(now):
        response = accept_application(request, volunteer_id=volunteer.id)
    assert cast(HttpResponseRedirect, response).url == reverse(
        "application_list"
    ), "accepting seems to not have worked?!?"

    volunteer.refresh_from_db()

    if should_write:
        assert volunteer.joined_at == now
    else:
        assert volunteer.joined_at == previous_joined_at


@mark.django_db
def test_logs_and_messages_but_still_accepts_on_email_sending_failure(
    rf: RequestFactory,
    caplog: LogCaptureFixture,
):
    volunteer = VolunteerFactory.create()
    application = SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.post("", {"subject": "foo", "body": "bar"})
    request.user = UserFactory.create(permissions=required_permissions)

    exception = Exception("mock error")

    def mock_send_mail(*a, **k):
        raise exception

    response = accept_application(
        request, volunteer_id=volunteer.id, send_mail=mock_send_mail
    )
    assert isinstance(response, HttpResponseRedirect)

    application.refresh_from_db()
    assert application.closed

    # Just checking membership because the accept message should also be there
    assert (
        messages.WARNING,
        (
            "The application was successfully accepted, but an error occured while sending the mail to the volunteer. "
            "Please notify them yourself.\n"
            "We have already been informed about the error and will look into the cause as soon "
            "as possible. Sorry for the inconvenience."
        ),
        "",
    ) in request._messages.messages

    assert [
        (r.levelname, r.message, getattr(r, "request")) for r in caplog.records
    ] == [
        (
            "ERROR",
            "Error sending acceptance email",
            request,
        )
    ]
    assert caplog.records[0].exc_info[1] == exception  # type: ignore


@mark.django_db
def test_deletes_rejected_applications(rf: RequestFactory):
    application = SarApplicationFactory.create(decision=SarApplication.Decision.REJECT)
    SarApplicationFactory.create(
        volunteer=application.volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.post("")
    request.user = UserFactory.create(permissions=required_permissions)
    response = accept_application(
        request, volunteer_id=cast(Volunteer, application.volunteer).id
    )
    assert isinstance(response, HttpResponseRedirect)

    if features["keep_rejected_applications"]:
        # TODO: Make sure to change the test name when removing the feature flag!
        assert SarApplication.objects.filter(id=application.id).exists()
        sa = SarApplication.objects.get(id=application.id)
        assert sa.is_rejected()
        assert sa.closed is not None
        assert not SarProfile.objects.filter(position=application.position).exists()
    else:
        assert not SarApplication.objects.filter(id=application.id).exists()
        assert not SarProfile.objects.filter(position=application.position).exists()


@mark.django_db
@mark.parametrize(
    ["scenario", "expected_log", "expected_message"],
    [
        (
            "none-accepted",
            "Accept view has been reached for application that has no decision: accept",
            "Cannot accept application without positions we would accept",
        ),
        (
            "some-still-undecided",
            "Accept view has been reached while some positions are still undecided",
            "Cannot accept application before all positions have been decided",
        ),
    ],
)
def test_logs_shows_message_and_redirects_back_if_not_ready_to_accept(
    scenario: Literal["none-accepted", "some-still-undecided"],
    expected_log: str,
    expected_message: str,
    rf: RequestFactory,
    caplog: LogCaptureFixture,
):
    volunteer = VolunteerFactory.create()
    match scenario:
        case "none-accepted":
            SarApplicationFactory.create(
                volunteer=volunteer, decision=SarApplication.Decision.REJECT
            )
        case "some-still-undecided":
            SarApplicationFactory.create(volunteer=volunteer, decided=True)
            SarApplicationFactory.create(volunteer=volunteer, decided=False)
        case other:
            assert_never(other)

    request = rf.post("")
    request.user = UserFactory.create(permissions=required_permissions)
    response = accept_application(request, volunteer_id=volunteer.id)
    assert isinstance(response, HttpResponseRedirect)

    # Redirect
    assert response.url == reverse(
        "application_update",
        kwargs={"volunteer_id": volunteer.id},
    )

    # Log
    assert [
        (r.levelname, r.message, getattr(r, "request")) for r in caplog.records
    ] == [("WARNING", expected_log, request)]

    # Message
    assert request._messages.messages == [(messages.WARNING, expected_message, "")]


@mark.django_db
def test_creates_history_item(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        decision=SarApplication.Decision.ACCEPT,
        position__key="foo",
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        decision=SarApplication.Decision.ACCEPT,
        position__key="bar",
    )
    before_count = volunteer.history.count()

    request = rf.post("", {"subject": "bar", "body": "foo"})
    request.user = UserFactory.create(permissions=required_permissions)

    frozen_time = datetime(2013, 4, 13, 9, 15, tzinfo=timezone.utc)

    with freeze_time(frozen_time):
        response = accept_application(request, volunteer.id)
    assert response.url == reverse(
        "application_list"
    ), "this should have been true, something unrelated probably went wrong"

    assert volunteer.history.count() == before_count + 1
    last = volunteer.history.last()
    assert last
    assert last.change_json == [
        {"type": "applications_accepted", "positions": ["foo", "bar"]}
    ]


@mark.django_db
def test_reachable(backoffice_client: Client):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    backoffice_client.force_login(UserFactory.create(permissions=required_permissions))
    response = backoffice_client.post(
        reverse("application_accept", kwargs={"volunteer_id": volunteer.id}),
        {"subject": "sss", "body": "bbb"},
    )
    assertRedirects(
        response,
        reverse("application_list", urlconf=settings.BACKOFFICE_URLCONF),
        fetch_redirect_response=False,
    )


@mark.django_db
def test_accept_requires_permissions(rf: RequestFactory):
    # Missing "add_volunteer"
    assert_requires_permissions(
        rf,
        accept_application,
        [
            "applications.change_sarapplication",
            "applications.delete_sarapplication",
            "volunteers.add_sarprofile",
        ],
    )


@mark.django_db
def test_is_reachable_from_details_page(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    form = html.find(
        "form",
        method="post",
        action=reverse("application_accept", kwargs={"volunteer_id": volunteer.id}),
    )
    assert form
    assert form.find("input", attrs={"name": "csrfmiddlewaretoken"})  # type: ignore
    button = html.find("button", id="acceptApplication")
    assert isinstance(button, Tag)
    assert button.text.strip().startswith("Create profile as")


@mark.django_db()
def test_accept_with_customized_email(
    logged_in_context: BrowserContext, mailoutbox: list[EmailMessage], backoffice_url
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("application_update", kwargs={"volunteer_id": volunteer.id})}'
    )
    page.get_by_role("button", name="Create profile").click()

    dialog = page.get_by_role("dialog")
    dialog.wait_for(state="visible")
    expect(dialog.locator("form")).to_be_visible()
    dialog.locator("form").get_by_label("Subject").fill("Hallo")
    dialog.locator("form").get_by_label("Body").fill("it's me")
    dialog.get_by_role("button", name="Create profile").click()

    assert [(m.subject, m.body) for m in mailoutbox] == [("Hallo", "it's me")]

    expect(dialog).to_contain_text("Application accepted")
    assert page.url.endswith(reverse("application_list"))

    assert volunteer.sarprofile_set.exists()


@mark.parametrize("empty_field", ["subject", "body"])
@mark.django_db
def test_rerenders_on_missing_data(
    empty_field, rf: RequestFactory, mailoutbox: list[EmailMessage]
):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer, decision=SarApplication.Decision.ACCEPT
    )

    user = UserFactory.create(
        view_permissions=ApplicationUpdateView,
        permissions=[
            "applications.change_sarapplication",
            "applications.delete_sarapplication",
            "volunteers.add_sarprofile",
        ],
    )
    post_data = {"subject": "test-subject", "body": "test-body"}
    post_data[empty_field] = ""
    request = rf.post("", post_data)
    request.user = user

    response = accept_application(request, volunteer_id=volunteer.id)

    assert len(mailoutbox) == 0
    assert isinstance(response, HttpResponseRedirectBase)
    assert response.url.startswith(
        f"{reverse('application_update', kwargs={'volunteer_id': volunteer.id})}"
    )

    # The form data is passed as a GET parameter, so we simulate the redirect by passing the query
    # string ourselves

    query = urlparse(response.url).query
    request = rf.get(f"?{query}")
    request.user = user
    response = ApplicationUpdateView.as_view()(request, volunteer_id=volunteer.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    dialog = html.select_one("#planner-modal-accept-application")
    assert dialog
    assert "visible" in dialog.attrs["class"]
    assert "This field is required" in dialog.text
