from datetime import datetime, timezone
from http import HTTPStatus
from typing import cast

from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.test.client import Client
from freezegun.api import freeze_time
from pytest import mark, raises

from applications.models import SarApplication
from applications.tests.factories import SarApplicationFactory
from applications.views import decide_position
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.tests.factories import VolunteerFactory

required_permissions = ["applications.change_sarapplication"]


@mark.django_db
def test_is_reachable(client: Client):
    client.force_login(UserFactory.create(permissions=required_permissions))

    assert (
        HTTPStatus(
            client.post(
                "/applications/42/decide_position/",
                data={"position": "foo", "decision": SarApplication.Decision.REJECT},
            ).status_code
        )
        == HTTPStatus.NOT_FOUND
    )


@mark.django_db
def test_requires_post(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=required_permissions)

    assert (
        HTTPStatus(decide_position(request, "42").status_code)
        == HTTPStatus.METHOD_NOT_ALLOWED
    )


@mark.django_db
def test_stores_decision(rf: RequestFactory):
    position = PositionFactory.create()
    decision = SarApplication.Decision.REJECT

    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(
        volunteer=volunteer,
        position__key="foo",
        decision=SarApplication.Decision.NEW,
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position=position,
        decision=SarApplication.Decision.NEW,
    )

    request = rf.post(
        "",
        data={"position": position.key, "decision": decision},
    )
    request.user = UserFactory.create(permissions=required_permissions)
    time = datetime(2010, 5, 3, 18, tzinfo=timezone.utc)
    with freeze_time(time):
        decide_position(request, volunteer_id=volunteer.id)

    volunteer.refresh_from_db()
    # We didn't touch that one
    assert (
        volunteer.sarapplication_set.open().get(position__key="foo").decision
        == SarApplication.Decision.NEW
    )

    sa = volunteer.sarapplication_set.open().get(position=position)
    assert (sa.decision, sa.by, sa.time) == (decision, request.user, time)


@mark.django_db
@freeze_time("2022-09-01T13:00")
def test_renders_last_updated(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position__key="gremlin")

    request = rf.post(
        "", data={"position": "gremlin", "decision": SarApplication.Decision.ACCEPT}
    )
    request.user = UserFactory.create(
        username="personMcExampleName", permissions=required_permissions
    )
    response = decide_position(request, volunteer_id=volunteer.id)

    assert HTTPStatus(response.status_code) == HTTPStatus.OK
    assert (
        "Updated 01.09.2022 by personMcExampleName"
        in cast(TemplateResponse, response).rendered_content
    )


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    request = rf.get("")  # permissions are more important than http method

    request.user = UserFactory.create()  # No permissions

    with raises(PermissionDenied):
        decide_position(request, "1323")


@mark.django_db
def test_pad_request_on_invalid_POST(rf: RequestFactory):
    request = rf.post("", data={"yooo": "lmao"})
    request.user = UserFactory.create(permissions=required_permissions)

    # note that in particular, this is prioritized over 404. We verify inputs first and fetch data
    # second.
    response = decide_position(request, "124")

    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_NOT_FOUND_on_nonexisting_position(rf: RequestFactory):
    request = rf.post(
        "", data={"position": "magus", "decision": SarApplication.Decision.ACCEPT}
    )
    request.user = UserFactory.create(permissions=required_permissions)

    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position__key="witch")

    with raises(Http404):
        decide_position(request, volunteer_id=volunteer.id)


@mark.django_db
def test_BAD_REQUEST_if_sarapplication_is_already_closed(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    sarapplication = SarApplicationFactory.create(volunteer=volunteer, is_closed=True)
    request = rf.post(
        "",
        data={
            "position": sarapplication.position.key,
            "decision": SarApplication.Decision.REJECT,
        },
    )
    request.user = UserFactory.create(permissions=required_permissions)
    response = decide_position(request, volunteer_id=volunteer.id)
    assert response.status_code == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_BAD_REQUEST_if_volunteer_didnt_apply_for_that_position(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)
    request = rf.post(
        "",
        data={
            "position": PositionFactory.create().key,
            "decision": SarApplication.Decision.ACCEPT,
        },
    )
    request.user = UserFactory.create(permissions=required_permissions)
    response = decide_position(request, volunteer_id=volunteer.id)
    assert response.status_code == HTTPStatus.BAD_REQUEST
