from datetime import timezone
from typing import Callable, ClassVar

from factory import Faker, LazyAttribute, Maybe, SubFactory  # type: ignore
from factory.django import DjangoModelFactory

from planner_common.test.factories import DeprecateDirectCallMetaclass
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.tests.factories import VolunteerFactory

from ..models import SarApplication


class SarApplicationFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = SarApplication

    class Params:
        decided = Maybe("is_closed", True, Faker("boolean"))  # type: ignore
        updated_even_though_undecided = Faker("boolean")
        updated = LazyAttribute(
            lambda self: self.decided or self.updated_even_though_undecided
        )
        is_closed = False

    create: ClassVar[Callable[..., SarApplication]]
    create_batch: ClassVar[Callable[..., list[SarApplication]]]
    build: ClassVar[Callable[..., SarApplication]]
    build_batch: ClassVar[Callable[..., list[SarApplication]]]

    volunteer = SubFactory(VolunteerFactory)
    position = SubFactory(PositionFactory)

    decision = Maybe(
        "decided",
        yes_declaration=Faker(  # type: ignore
            "random_element",
            elements=[
                SarApplication.Decision.ACCEPT,
                SarApplication.Decision.REJECT,
            ],
        ),
        no_declaration=Faker(  # type: ignore
            "random_element",
            elements=[
                SarApplication.Decision.NEW,
                SarApplication.Decision.IN_PROGRESS,
            ],
        ),
    )

    created = Faker("past_datetime", tzinfo=timezone.utc)
    by = Maybe(
        "updated",
        yes_declaration=SubFactory(UserFactory),  # type: ignore
    )
    time = Maybe(
        "updated",
        yes_declaration=Faker("past_datetime", tzinfo=timezone.utc),  # type: ignore
    )
    closed = Maybe(
        "is_closed",
        yes_declaration=Faker("past_datetime", tzinfo=timezone.utc),  # type: ignore
    )
