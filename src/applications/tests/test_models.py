from datetime import datetime, timezone

from freezegun import freeze_time
from pytest import mark, raises

from applications.models import AlreadyClosedException, Import, SarApplication

from .factories import SarApplicationFactory


@mark.django_db
def test_get_absolute_url():
    sarapplication = SarApplicationFactory.create(volunteer__id=94)
    url = sarapplication.get_absolute_url()
    assert url == "/applications/94/"


@mark.django_db
@freeze_time("2021-02-02 01:02:03")
def test_Import_updating_allows_later_retrieval():
    Import.update_to_now()
    assert Import.get_last_imported() == datetime.now(timezone.utc)


@mark.django_db
def test_Import_get_last_imported_returns_None_if_never_imported():
    assert Import.get_last_imported() is None


@mark.django_db
def test_SarApplicationManager_open():
    SarApplicationFactory.create(is_closed=True)
    a = SarApplicationFactory.create(is_closed=False)
    assert list(SarApplication.objects.open()) == [a]


@mark.django_db
def test_SarApplicationManager_accepted():
    a = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.ACCEPT
    )
    SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )
    SarApplicationFactory.create(is_closed=False)
    assert list(SarApplication.objects.accepted()) == [a]


@mark.django_db
def test_SarApplicationManager_all():
    a = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.ACCEPT
    )
    b = SarApplicationFactory.create(
        is_closed=True, decision=SarApplication.Decision.REJECT
    )
    c = SarApplicationFactory.create(is_closed=False)
    assert set(SarApplication.objects.all()) == {a, b, c}


@mark.django_db
def test_SarApplication_accept_raises_when_already_closed():
    sa = SarApplicationFactory.create(is_closed=True)
    with raises(AlreadyClosedException):
        sa.accept()


@mark.django_db
def test_SarApplication_reject_raises_when_already_closed():
    sa = SarApplicationFactory.create(is_closed=True)
    with raises(AlreadyClosedException):
        sa.reject()


@mark.django_db
@mark.parametrize("decision", SarApplication.Decision)
def test_SarApplication_accept_closes_and_sets_decision_accept(decision):
    sa = SarApplicationFactory.create(is_closed=False, decision=decision)
    sa.accept()
    assert sa.decision == SarApplication.Decision.ACCEPT
    assert sa.closed is not None


@mark.django_db
@mark.parametrize("decision", SarApplication.Decision)
def test_SarApplication_reject_closes_and_sets_decision_reject(decision):
    sa = SarApplicationFactory.create(is_closed=False, decision=decision)
    sa.reject()
    assert sa.decision == SarApplication.Decision.REJECT
    assert sa.closed is not None
