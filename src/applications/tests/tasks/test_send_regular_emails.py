from datetime import datetime, timezone

from django.core.mail.message import EmailMessage
from django.test import override_settings
from django.urls import reverse
from pytest import mark

from applications.models import SarApplication
from applications.tests.factories import SarApplicationFactory
from seawatch_registration.models import UserProfile
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.tests.factories import VolunteerFactory
# from ...tasks import send_regular_emails


def send_regular_emails(*a, **k):
    raise NotImplementedError("Temporarily disabled")


pytestmark = mark.skip


fake_now = datetime(2023, 2, 8, tzinfo=timezone.utc)


@mark.django_db
def test_includes_open_application_count_relevant_positions(
    mailoutbox: list[EmailMessage],
):
    medics = PositionFactory.create(name="Medic test")
    officers = PositionFactory.create(name="1st Officer test")
    cooks = PositionFactory.create(name="Cook test")
    superhero = PositionFactory.create(name="Superhero test")

    SarApplicationFactory.create(
        position=officers, decision=SarApplication.Decision.NEW
    )
    SarApplicationFactory.create(
        position=superhero, decision=SarApplication.Decision.IN_PROGRESS
    )
    SarApplicationFactory.create(position=medics)

    user = UserFactory.create()
    profile = UserProfile(user=user)
    profile.save()
    profile.positions.set([officers, cooks, superhero])

    # Hi Crewing Team Member,
    #   1st Officer: 1
    #   Cook: 0

    # In the database: 1x 1st officer, 1x medics

    from_email = "official@sea-watch.gov"
    with override_settings(DEFAULT_FROM_EMAIL=from_email):
        send_regular_emails(round(fake_now.timestamp()))

    assert len(mailoutbox) != 0
    assert mailoutbox[0].from_email == from_email
    assert "1st Officer test: 1" in mailoutbox[0].body
    assert "Superhero test: 1" in mailoutbox[0].body
    assert "Cook test: 0" in mailoutbox[0].body
    assert "Medic test" not in mailoutbox[0].body


@mark.django_db
def test_doesnt_send_mails_to_users_without_profiles(
    mailoutbox: list[EmailMessage],
):
    UserFactory.create(email="test@sea-watch.de")

    from_email = "official@sea-watch.gov"
    with override_settings(DEFAULT_FROM_EMAIL=from_email):
        send_regular_emails(round(fake_now.timestamp()))

    assert len(mailoutbox) == 0


@mark.django_db
def test_sent_to_all_relevant_users(
    mailoutbox: list[EmailMessage],
):
    user = UserFactory.create(email="test@sea-watch.de")
    profile = UserProfile(user=user)
    profile.save()
    profile.positions.set([PositionFactory.create()])

    user_without_positions = UserFactory.create(email="test1@sea-watch.de")
    profile = UserProfile(user=user_without_positions)
    profile.save()

    from_email = "official@sea-watch.gov"
    with override_settings(DEFAULT_FROM_EMAIL=from_email):
        send_regular_emails(round(fake_now.timestamp()))

    assert [mail.recipients() for mail in mailoutbox] == [[user.email]]


@mark.django_db
def test_include_application_links(
    mailoutbox: list[EmailMessage],
):
    position = PositionFactory.create()

    user = UserFactory.create(email="test@sea-watch.de")
    profile = UserProfile(user=user)
    profile.save()
    profile.positions.set([position, PositionFactory.create()])

    from_email = "official@sea-watch.gov"
    with override_settings(
        DEFAULT_FROM_EMAIL=from_email, BASE_URL="http://backoffice.test"
    ):
        send_regular_emails(round(fake_now.timestamp()))

    assert (
        f"http://backoffice.test{reverse('application_list')}?position={position.key}"
        in mailoutbox[0].body
    )


@mark.django_db
def test_shows_new_applications(
    mailoutbox: list[EmailMessage],
):
    medics = PositionFactory.create(name="Medic test")
    cooks = PositionFactory.create(name="Cook test")

    user = UserFactory.create(email="test@sea-watch.de")
    profile = UserProfile(user=user)
    profile.save()
    profile.positions.set([medics, cooks, PositionFactory.create()])

    v = VolunteerFactory.create()
    for decision in [SarApplication.Decision.NEW, SarApplication.Decision.IN_PROGRESS]:
        SarApplicationFactory.create(
            volunteer=v,
            created=datetime(2022, 3, 21, tzinfo=timezone.utc),
            position=medics,
            decision=decision,
        )
    SarApplicationFactory.create(
        volunteer=v,
        created=datetime(2022, 3, 21, tzinfo=timezone.utc),
        position=cooks,
        decision=SarApplication.Decision.ACCEPT,
    )

    for decision in [
        SarApplication.Decision.NEW,
        SarApplication.Decision.IN_PROGRESS,
        SarApplication.Decision.ACCEPT,
    ]:
        SarApplicationFactory.create(
            created=datetime(2023, 2, 3, tzinfo=timezone.utc),
            position=medics,
            decision=decision,
        )

    from_email = "official@sea-watch.gov"
    with override_settings(DEFAULT_FROM_EMAIL=from_email):
        send_regular_emails(round(fake_now.timestamp()))

    assert "Medic test: 4 pending 5 total (3 in the last week)" in mailoutbox[0].body
    assert "Cook test: 0 pending 1 total" in mailoutbox[0].body


@mark.django_db
def test_sends_only_one_mail_per_user(
    mailoutbox: list[EmailMessage],
):
    profile = UserProfile(user=UserFactory.create())
    profile.save()
    profile.positions.set(PositionFactory.create_batch(2))

    send_regular_emails(round(fake_now.timestamp()))

    assert len(mailoutbox) == 1


@mark.django_db
def test_doesnt_include_closed_positions(
    mailoutbox: list[EmailMessage],
):
    profile = UserProfile(user=UserFactory.create())
    profile.save()
    position = PositionFactory.create(name="Jedi")
    profile.positions.set([position])

    SarApplicationFactory.create(position=position, is_closed=True)

    send_regular_emails(round(fake_now.timestamp()))
    assert "Jedi: 0 pending 0 total" in mailoutbox[0].body
