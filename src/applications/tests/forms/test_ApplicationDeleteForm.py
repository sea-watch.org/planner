from pytest import mark

from applications.forms import ApplicationRejectForm
from applications.tests.factories import SarApplicationFactory
from seawatch_registration.tests.factories import PositionFactory
from volunteers.tests.factories import VolunteerFactory


@mark.django_db
def test_ApplicationDeleteForm_pulls_initial_from_application():
    volunteer = VolunteerFactory.create()

    for position in [
        PositionFactory.create(name="Foo"),
        PositionFactory.create(name="Bar"),
        PositionFactory.create(name="should-not-be-there", open_for_applications=False),
    ]:
        SarApplicationFactory.create(volunteer=volunteer, position=position)

    form = ApplicationRejectForm(volunteer)

    assert "your application for sea-watch crew" in form.initial["subject"].lower()
    body = form.initial["body"]
    assert "we cannot accept you" in body
    assert "positions of Bar and Foo" in body
    assert "should-not-be-there" not in body.lower()
