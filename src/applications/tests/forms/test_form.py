from typing import cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.http.request import QueryDict
from django.template.response import TemplateResponse
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark

from applications.forms import ApplicationForm
from applications.tests.factories import SarApplicationFactory
from applications.views import ApplicationUpdateView
from planner_common.playwright_utils import MultiChoicesJS
from planner_common.typing import assert_is
from seawatch_planner.tests.request_factory import RequestFactory
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory


@mark.django_db
def test_shows_additional_positions_separately(rf: RequestFactory):
    external_position = PositionFactory.create(key="ext", open_for_applications=True)
    additional_position = PositionFactory.create(key="int", open_for_applications=False)
    v = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=v, position=external_position)
    SarApplicationFactory.create(volunteer=v, position=additional_position)

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=ApplicationUpdateView)

    response = ApplicationUpdateView.as_view()(request, volunteer_id=v.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )

    positions = html.select_one("select[id='id_positions']")
    assert {
        assert_is(str, o["value"])
        for o in cast(Tag, positions).select("option[selected]")
    } == {str(external_position.id)}

    additional_positions = html.find("select", id="id_additional_positions")
    assert {
        assert_is(str, o["value"])
        for o in cast(Tag, additional_positions).select("option[selected]")
    } == {str(additional_position.id)}


def default_form_data(volunteer: Volunteer) -> QueryDict:
    # Use the form to get the data, this should decouple us somewhat from changes in the data layout
    data = QueryDict("", mutable=True)
    data.update(
        {
            k: v
            for k, v in ApplicationForm(instance=volunteer).initial.items()
            if k != "additional_positions"
        }
    )
    # Fixup list-typed entries because they are put as a list of list with the above
    for field in ["positions", "nationalities", "certificates"]:
        data.setlist(field, cast(list, data[field]))

    assert not ApplicationForm(
        instance=volunteer, data=data
    ).errors, "something is wrong with the helper function, this should be fine"
    return data


@mark.django_db
def test_saves_positions():
    # this is tested explicitly because the positions aren't really a field but manually managed
    # through SarApplications
    to_keep = PositionFactory.create(
        name="keep", key="to_keep", open_for_applications=True
    )
    to_add = PositionFactory.create(
        name="add", key="to_add", open_for_applications=True
    )

    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position=to_keep)

    data = default_form_data(volunteer)
    data.setlist("positions", [str(to_add.id), *data.getlist("positions")])

    form = ApplicationForm(instance=volunteer, data=data)
    assert not form.errors
    form.save()

    volunteer.refresh_from_db()
    assert {sa.position for sa in volunteer.sarapplication_set.open()} == {
        to_add,
        to_keep,
    }


@mark.django_db
def test_preserves_additional_positions():
    should_be_removed = PositionFactory.create(
        name="removed", open_for_applications=True
    )
    should_be_added = PositionFactory.create(name="added", open_for_applications=True)
    should_be_kept = PositionFactory.create(name="kept", open_for_applications=False)
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer, position=should_be_removed)
    SarApplicationFactory.create(volunteer=volunteer, position=should_be_kept)

    data = default_form_data(volunteer)
    data["positions"] = str(should_be_added.id)

    form = ApplicationForm(
        instance=volunteer,
        data=data,
    )
    assert not form.errors
    form.save()

    volunteer.refresh_from_db()
    assert "kept" in {sa.position.name for sa in volunteer.sarapplication_set.open()}


@mark.django_db
def test_save_returns_updated_object():
    volunteer = VolunteerFactory.create()
    SarApplicationFactory.create(volunteer=volunteer)

    data = default_form_data(volunteer)
    data["first_name"] = "one bucket of coffee please"

    form = ApplicationForm(instance=volunteer, data=data)
    assert form.is_valid, "consistency check failed"

    new_volunteer = form.save()
    assert new_volunteer == volunteer
    assert "coffee" in new_volunteer.first_name


@mark.django_db
def test_languages_render_properly(logged_in_context: BrowserContext, backoffice_url):
    # The actual wiring is already checked in
    # applications.tests.views.test_application_update.test_shows_fields
    languages = [
        {"code": "swa", "label": "Swahili", "level": 2},
        {"code": "ita", "label": "Italian", "level": 0},
    ]
    volunteer = VolunteerFactory.create(
        languages={language["code"]: language["level"] for language in languages}
    )
    SarApplicationFactory.create(volunteer=volunteer)

    page = logged_in_context.new_page()
    page.goto(
        f"{backoffice_url}{reverse('application_update', kwargs={'volunteer_id': volunteer.id})}"
    )
    languages_field = page.locator(".field:has(input[name='languages'])")
    levels = languages_field.locator(".language-mapper--levels")

    # Choices is rendered
    expect(
        languages_field.locator("select[name='languages__choices']")
    ).not_to_be_visible()
    expect(languages_field.locator(".choices")).to_be_visible()

    # Language selects are rendered properly
    assert {
        e.inner_text(): e.input_value()
        for e in levels.locator("label").element_handles()
    } == {"Swahili": "2", "Italian": "0"}

    # Adding a language
    choices = MultiChoicesJS(languages_field.locator(".choices"))
    choices.select("English")

    assert {
        e.inner_text(): e.input_value()
        for e in levels.locator("label").element_handles()
    } == {"Swahili": "2", "Italian": "0", "English": ""}
    languages_field.get_by_label("English").select_option(value="3")

    # Removing a language
    choices.deselect("Italian")
    assert {
        e.inner_text(): e.input_value()
        for e in levels.locator("label").element_handles()
    } == {"Swahili": "2", "English": "3"}

    page.locator("section.application-data").get_by_role("button").get_by_text(
        "Save changes"
    ).click()
    expect(page.locator("#modal-message")).to_contain_text("successfully updated")

    volunteer.refresh_from_db()
    assert volunteer.languages == {"swa": 2, "eng": 3}
