from bs4 import BeautifulSoup
from pytest import mark

from applications.forms import ApplicationAcceptForm
from applications.tests.factories import SarApplicationFactory


@mark.django_db
def test_correct_initial_data():
    volunteer = SarApplicationFactory.create().volunteer
    form = ApplicationAcceptForm(volunteer=volunteer)
    html = BeautifulSoup(str(form), features="html.parser")
    subject = html.select_one("[name=subject]")
    assert (
        subject
        and subject.attrs["value"] == "Accepted – Your Application for Sea-Watch Crew"
    )
    body = html.select_one("[name=body]")
    assert (
        body
        and "This E-Mail is the confirmation that you are now part of our crewing pool."
        in body.text
    )
