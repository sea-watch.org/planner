from pytest import mark

from applications.models import SarApplication
from applications.templatetags.applications_tags import (
    all_open_sarapplications_decided,
    has_open_sarapplication_with_decision_accept,
    positions_with_open_application_and_decision_accept,
)
from applications.tests.factories import SarApplicationFactory
from seawatch_registration.tests.factories import PositionFactory
from volunteers.tests.factories import VolunteerFactory


@mark.django_db
def test_positions_with_open_application_and_decision_accept():
    volunteer = VolunteerFactory.create()
    positions = PositionFactory.create_batch(2)
    SarApplicationFactory.create(
        volunteer=volunteer,
        decision=SarApplication.Decision.ACCEPT,
        position=positions[0],
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        decision=SarApplication.Decision.ACCEPT,
        position=positions[1],
    )
    for decision in [
        SarApplication.Decision.NEW,
        SarApplication.Decision.IN_PROGRESS,
        SarApplication.Decision.REJECT,
    ]:
        # These shouldn't be included
        SarApplicationFactory.create(volunteer=volunteer, decision=decision)

    assert positions_with_open_application_and_decision_accept(volunteer) == positions


@mark.django_db
@mark.parametrize(
    ["decisions", "expected"],
    [
        ([SarApplication.Decision.ACCEPT, SarApplication.Decision.NEW], False),
        ([SarApplication.Decision.ACCEPT, SarApplication.Decision.IN_PROGRESS], False),
        ([SarApplication.Decision.ACCEPT, SarApplication.Decision.REJECT], True),
    ],
)
def test_all_open_sarapplications_decided(
    decisions: list[SarApplication.Decision], expected: bool
):
    volunteer = VolunteerFactory.create()
    for d in decisions:
        SarApplicationFactory.create(volunteer=volunteer, decision=d)
    assert all_open_sarapplications_decided(volunteer) == expected


@mark.django_db
@mark.parametrize(
    ["decisions", "expected"],
    [
        ([SarApplication.Decision.ACCEPT, SarApplication.Decision.NEW], True),
        ([SarApplication.Decision.ACCEPT, SarApplication.Decision.IN_PROGRESS], True),
        ([SarApplication.Decision.NEW, SarApplication.Decision.REJECT], False),
    ],
)
def test_has_sarapplications_with_decision_accept(
    decisions: list[SarApplication.Decision], expected: bool
):
    volunteer = VolunteerFactory.create()
    for d in decisions:
        SarApplicationFactory.create(volunteer=volunteer, decision=d)
    assert has_open_sarapplication_with_decision_accept(volunteer) == expected
