from __future__ import annotations

from collections.abc import Callable
from datetime import datetime, timezone
from typing import TYPE_CHECKING, ClassVar, Optional

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from seawatch_registration.models import Position, ShortTextField
from volunteers.models import Volunteer


if TYPE_CHECKING:

    class SarApplicationManager(models.Manager["SarApplication"]):
        def open(self) -> SarApplicationQuerySet: ...
        def closed(self) -> SarApplicationQuerySet: ...
        def undecided(self) -> SarApplicationQuerySet: ...
        def accepted(self) -> SarApplicationQuerySet: ...
        def rejected(self) -> SarApplicationQuerySet: ...


class SarApplicationQuerySet(models.QuerySet["SarApplication"]):
    def open(self) -> SarApplicationQuerySet:
        return self.filter(closed=None)

    def closed(self) -> SarApplicationQuerySet:
        return self.filter(closed__isnull=False)

    def undecided(self) -> SarApplicationQuerySet:
        return self.filter(
            models.Q(decision=SarApplication.Decision.NEW)
            | models.Q(decision=SarApplication.Decision.IN_PROGRESS)
        )

    def accepted(self) -> SarApplicationQuerySet:
        return self.closed().filter(decision=SarApplication.Decision.ACCEPT)

    def rejected(self) -> SarApplicationQuerySet:
        return self.closed().filter(decision=SarApplication.Decision.REJECT)


def now():
    return datetime.now(timezone.utc)


class AlreadyClosedException(Exception):
    pass


class SarApplication(models.Model):
    """A volunteer's application to join sea-watch on a search-and-rescue operation.

    This is position-specific, so if someone applies for multiple positions there will be one
    SarApplication for each one.

    The lifecycle looks like this:
        1. Open
            - Determined by .closed == None
            - .decision does not immediately affect the state
        2.1 Accepted
            - Determined by .closed != None and .decision = ACCEPT
            - Makes the volunteer show up in the volunteer pool and stuff
        2.2 Rejected
            - .closed != None and .decision = REJECT
            - Volunteer shows up in rejected list
            - SarApplication will be auto-deleted after some time
    """

    if TYPE_CHECKING:
        objects: ClassVar[SarApplicationManager]
    objects = SarApplicationQuerySet.as_manager()  # type: ignore
    all_objects: ClassVar[models.Manager[SarApplication]] = models.Manager()

    id = models.AutoField(primary_key=True)

    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)

    position = models.ForeignKey(Position, on_delete=models.PROTECT)

    class Decision(models.TextChoices):
        NEW = "new", _("New")
        IN_PROGRESS = "in_progress", _("In progress")
        REJECT = "reject", _("Reject")
        ACCEPT = "accept", _("Accept")

    decision = ShortTextField(choices=Decision.choices, default=Decision.NEW)
    get_decision_display: Callable[[], str]  # added by django

    created = models.DateTimeField(default=now)
    closed = models.DateTimeField(null=True)
    by = models.ForeignKey(User, on_delete=models.PROTECT, default=None, null=True)
    time = models.DateTimeField(default=None, null=True)

    def __str__(self) -> str:
        return f"{self.volunteer.name} as {self.position.name}: {self.get_decision_display()}"

    def is_new(self):
        return self.decision == self.Decision.NEW

    def is_in_progress(self):
        return self.decision == self.Decision.IN_PROGRESS

    def is_accepted(self):
        # TODO: with the addition of closed, this name became ambiguous
        return self.decision == self.Decision.ACCEPT

    def is_rejected(self):
        # TODO: with the addition of closed, this name became ambiguous
        return self.decision == self.Decision.REJECT

    def is_closed(self):
        return self.closed is not None

    def get_absolute_url(self):
        return reverse("application_update", kwargs={"volunteer_id": self.volunteer.pk})

    def accept(self):
        if self.is_closed():
            raise AlreadyClosedException()
        self.volunteer.sarprofile_set.create(position=self.position)
        self.decision = SarApplication.Decision.ACCEPT
        self.closed = datetime.now(tz=timezone.utc)
        self.save()

    def reject(self):
        if self.is_closed():
            raise AlreadyClosedException()
        self.decision = SarApplication.Decision.REJECT
        self.closed = datetime.now(tz=timezone.utc)
        self.save()


class Import(models.Model):
    time = models.DateTimeField(default=now)

    @classmethod
    def update_to_now(cls) -> None:
        cls.objects.update_or_create(defaults={"time": datetime.now(timezone.utc)})

    @classmethod
    def get_last_imported(cls) -> Optional[datetime]:
        try:
            return cls.objects.get().time
        except cls.DoesNotExist:
            return None
