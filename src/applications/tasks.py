# from datetime import UTC, datetime, timedelta
# from django.conf import settings
# from django.contrib.auth.models import User
# from django.core.mail import send_mail
# from django.urls import reverse
# from procrastinate.contrib.django import app
#
# from applications.models import SarApplication
#
#
# @app.periodic(
#     cron="0 11 * * 4"
# )  # Ideally we'd have 1pm german time always but it's just gonna be an our earlier in winter
# @app.task
# def send_regular_emails(timestamp: int):
#     users = User.objects.filter(profile__positions__isnull=False).distinct()
#     one_week_ago = datetime.fromtimestamp(timestamp, tz=UTC) - timedelta(days=7)
#     for user in users:
#         email = user.email
#         profile = user.profile  # type: ignore
#
#         positions = []
#         for position in profile.positions.all():
#             all = position.sarapplication_set.open()
#             new = all.filter(created__gte=one_week_ago)
#             pending = all.filter(
#                 decision__in=[
#                     SarApplication.Decision.NEW,
#                     SarApplication.Decision.IN_PROGRESS,
#                 ]
#             )
#             if new.count() > 0:
#                 positions.append(
#                     f"{position.name}: {pending.count()} pending {all.count()} total ({new.count()} in the last week)\n"
#                     f"    {settings.BASE_URL}{reverse('application_list')}?position={position.key}"
#                 )
#             else:
#                 positions.append(
#                     f"{position.name}: {pending.count()} pending {all.count()} total\n"
#                     f"    {settings.BASE_URL}{reverse('application_list')}?position={position.key}"
#                 )
#
#         positions = "\n\n".join(positions)
#
#         send_mail(
#             subject="Weekly update from the Crewing DB",
#             message=f"Hello, \n\n{positions}",
#             from_email=settings.DEFAULT_FROM_EMAIL,
#             recipient_list=[email],
#         )
