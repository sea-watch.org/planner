# Generated by Django 4.1.7 on 2023-03-20 14:08

from django.db import connection, migrations, models


def create_Volunteer_for_each_Application(apps, schema_editor):
    Application = apps.get_model("applications", "Application")
    Volunteer = apps.get_model("volunteers", "Volunteer")

    Volunteer.objects.bulk_create(
        [
            Volunteer(
                # Previously existing fields
                first_name=application.first_name,
                last_name=application.last_name,
                phone_number=application.phone_number,
                email=application.email,
                date_of_birth=application.date_of_birth,
                gender=application.gender,
                certificates=application.certificates_csv.split(";"),
                languages={l.code: l.points for l in application.languages.all()},
                nationalities=application.nationalities.split(";"),
                homepage=application.homepage,
                application=application,
                # fields added in last volunteers/00035_application_removal_1
                cv=application.cv_file,
                applied_at=application.created,
                # joined_at = None for now since these volunteers aren't accepted yet
                motivation=application.motivation,
                qualification=application.qualification,
            )
            for application in Application.objects.filter(volunteer=None)
        ]
    )

    if Application.objects.filter(volunteer=None).exists():
        raise AssertionError(
            "Expected all applications to have volunteers at this point, something went wrong"
        )


def set_sarapplication_accepted(apps, schema_editor):
    apps.get_model("applications", "SarApplication")
    with connection.cursor() as cursor:
        cursor.execute(
            """
            UPDATE applications_sarapplication as sa
                SET accepted = a.accepted
            FROM applications_application as a
                WHERE sa.application_id = a.id
            """
        )


class Migration(migrations.Migration):
    dependencies = [
        ("applications", "0042_migrate_positiondecision_to_sarapplication"),
        ("volunteers", "0035_application_removal_1_new_volunteer_fields"),
    ]

    operations = [
        migrations.RunPython(create_Volunteer_for_each_Application),
        migrations.AddField(
            model_name="sarapplication",
            name="accepted",
            field=models.DateTimeField(null=True),
        ),
        migrations.RunPython(set_sarapplication_accepted),
    ]
