document
  .querySelector('form[name="add_comment"]')
  .addEventListener("htmx:afterRequest", (event) => {
    const messageDiv = document.querySelector("#comment-error-message");
    if (event.detail.failed) {
      messageDiv.style.display = "block";
    } else {
      messageDiv.style.display = "none";
    }
  });
