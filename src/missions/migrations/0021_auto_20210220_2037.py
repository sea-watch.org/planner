# Generated by Django 3.1.6 on 2021-02-20 20:37

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("volunteers", "0001_initial"),
        ("missions", "0020_mission_version"),
    ]

    operations = [
        migrations.CreateModel(
            name="Availability",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("mission_version", models.IntegerField()),
                (
                    "mission",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="missions.mission",
                    ),
                ),
                (
                    "volunteer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="volunteers.volunteer",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="mission",
            name="available",
            field=models.ManyToManyField(
                through="missions.Availability", to="volunteers.Volunteer"
            ),
        ),
    ]
