# Generated by Django 4.2 on 2023-05-30 12:40

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("missions", "0028_alter_availability_availability"),
        ("operations", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="mission",
            name="available",
        ),
        migrations.DeleteModel(
            name="Availability",
        ),
        migrations.DeleteModel(
            name="Mission",
        ),
    ]
