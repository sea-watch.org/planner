# Generated by Django 4.0 on 2022-01-11 19:45

import django.db.models.deletion
from django.db import migrations, models

import history.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
    ]

    operations = [
        migrations.CreateModel(
            name="HistoryItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("object_id", models.PositiveIntegerField()),
                ("created", models.DateTimeField(default=history.models._nowutc)),
                ("actor", models.TextField(default=history.models._current_actor)),
                ("change_json", models.JSONField()),
                (
                    "content_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="contenttypes.contenttype",
                    ),
                ),
            ],
            options={
                "ordering": ["created"],
            },
        ),
    ]
