# Generated by Django 4.0 on 2022-02-18 17:40

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("history", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="historyitem",
            name="id",
            field=models.AutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
