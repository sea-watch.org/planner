from history.models import actor_context


def history_actor_middleware(get_response):
    def handler(request):
        if request.user.is_authenticated:
            with actor_context(request.user):
                return get_response(request)
        else:
            return get_response(request)

    return handler
