from history.utils import diff


def test_diff_reports_removed_fields():
    assert diff({"a": 42}, {}) == [{"field": "a", "old": 42, "new": None}]


def test_diff_reports_added_fields():
    assert diff({}, {"a": 42}) == [{"field": "a", "old": None, "new": 42}]


def test_diff_doesnt_report_on_same_value():
    assert diff({"a": 4}, {"a": 4}) == []


def test_diff_reports_changed_fields():
    assert diff({"a": 1}, {"a": 3}) == [{"field": "a", "old": 1, "new": 3}]
