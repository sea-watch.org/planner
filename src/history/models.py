import threading
from contextlib import contextmanager
from datetime import datetime, timezone
from typing import ClassVar, Iterator, Literal, Optional, Union

from django.contrib.auth.models import User
from django.db import models

from history.utils import diff
from volunteers.models import Volunteer

_threadlocal = threading.local()

Actor = Union[User, Literal["system"], Literal["testing_api"], Literal["pytest"]]


@contextmanager
def actor_context(actor: Actor) -> Iterator[None]:
    previous = getattr(_threadlocal, "actor", None)
    _threadlocal.actor = actor
    yield
    _threadlocal.actor = previous


def unsafe_set_actor_context(actor: Actor):
    _threadlocal.actor = actor


def unsafe_get_actor_context() -> Optional[Actor]:
    return getattr(_threadlocal, "actor", None)


def _nowutc():
    return datetime.now(timezone.utc)


def _current_actor():
    actor = getattr(_threadlocal, "actor", None)
    if not actor:
        raise Exception("HistoryItems can only be saved in an actor_context block")
    if isinstance(actor, User):
        return actor.username
    else:
        return actor


class HistoryItemManager(models.Manager["HistoryItem"]):
    def create_from_dicts(self, volunteer: Volunteer | None = None, *, old, new):
        """We allow volunteer=None to make it work over related fields

        This way, these two are equivalent:
            HistoryItem.objects.create_from_dicts(volunteer=volunteer, old=o, new=n)
            volunteer.history.create_from_dicts(old=o, new=n)

        You _will_ get a database error if you pass volunteer=None in the first version.
        """
        return self.create(volunteer=volunteer, change_json=diff(old, new))


class HistoryItem(models.Model):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        ordering = ["created"]

    objects: ClassVar[  # pyright: ignore [reportIncompatibleVariableOverride]
        HistoryItemManager
    ] = HistoryItemManager()

    volunteer = models.ForeignKey(
        Volunteer, on_delete=models.CASCADE, related_name="history"
    )

    created = models.DateTimeField(default=_nowutc)
    actor = models.TextField(default=_current_actor)
    change_json = models.JSONField()
