from importlib import import_module
import string
import random
from urllib.parse import urlparse, urlunparse

from django.test import Client
import pytest
from django.conf import settings
from django.contrib.auth import login
from django.http.request import HttpRequest
from playwright._impl._api_structures import SetCookieParam
from playwright.sync_api import BrowserContext
from pytest_playwright.pytest_playwright import CreateContextCallback
from pytest_django.lazy_django import skip_if_no_django
from pytest_django.live_server_helper import LiveServer

from history.models import unsafe_set_actor_context

# importing to bring fixtures into scope
from planner_common.pytest import *  # noqa: F403
from seawatch_planner.tests.request_factory import RequestFactory
from seawatch_registration.tests.factories import UserFactory
import procrastinate.testing as procrastinate_testing
from procrastinate.contrib.django import app as procrastinate_app


@pytest.fixture(scope="session", autouse=True)
def history_actor():
    unsafe_set_actor_context("pytest")


@pytest.fixture
def rf() -> RequestFactory:
    skip_if_no_django()
    return RequestFactory()


@pytest.fixture
def context(context: BrowserContext):
    context.set_default_timeout(3000)
    return context


@pytest.fixture(scope="session")
def django_db_modify_db_settings(
    django_db_modify_db_settings_parallel_suffix: None,
) -> None:
    """Random suffix to avoid issues when running multiple tests in parallel"""
    from django.conf import settings

    suffix = "".join(random.choices(string.ascii_lowercase, k=8))
    for db_settings in settings.DATABASES.values():
        test_name = db_settings.get("TEST", {}).get("NAME")

        if not test_name:
            test_name = f"test_{db_settings['NAME']}"

        db_settings.setdefault("TEST", {})
        db_settings["TEST"]["NAME"] = f"{test_name}_{suffix}"


@pytest.fixture()
def logged_in_context(
    new_context: CreateContextCallback,
    live_server: LiveServer,
    backoffice_url: str,
) -> BrowserContext:
    context = new_context(
        base_url=backoffice_url, viewport={"width": 1600, "height": 1080}
    )
    admin_user = UserFactory.create(is_superuser=True, is_staff=True)
    admin_user.set_password("password")
    admin_user.save()

    ## Explicit (slow) way to login in
    # page = context.new_page()
    # page.goto(live_server.url + reverse("login"))
    # page.fill("[name=username]", admin_user.username)
    # page.fill("[name=password]", "password")
    # page.click('button:text("Login")')
    # page.close()

    ## Forced (fast) way to in. Mostly copied from django/test/client.py::Client.force_login()
    request = HttpRequest()
    request.session = import_module(settings.SESSION_ENGINE).SessionStore()
    login(request, user=admin_user, backend=None)
    request.session.save()

    cookie: SetCookieParam = {
        "name": settings.SESSION_COOKIE_NAME,
        "value": request.session.session_key,
        "domain": settings.BACKOFFICE_HOSTS[0],
        "path": "/",
        # "expires": -1,
    }
    context.add_cookies([cookie])

    return context


@pytest.fixture
def backoffice_url(live_server):
    parts = urlparse(live_server.url)
    assert (
        parts.netloc.split(":")[0] == "localhost"
    ), "this code expects the live_server to run on localhost!"
    return urlunparse(
        parts._replace(netloc=f"backoffice.{parts.netloc}"),
    )


@pytest.fixture()
def minimal_pdf():
    return """
        %PDF-1.1
        %¥±ë

        1 0 obj
        << /Type /Catalog
        /Pages 2 0 R
        >>
        endobj

        2 0 obj
        << /Type /Pages
        /Kids [3 0 R]
        /Count 1
        /MediaBox [0 0 300 144]
        >>
        endobj

        3 0 obj
        <<  /Type /Page
        /Parent 2 0 R
        /Resources
        << /Font
        << /F1
        << /Type /Font
        /Subtype /Type1
        /BaseFont /Times-Roman
        >>
        >>
        >>
        /Contents 4 0 R
        >>
        endobj

        4 0 obj
        << /Length 55 >>
        stream
        BT
        /F1 18 Tf
        0 0 Td
        (Hello World) Tj
        ET
        endstream
        endobj

        xref
        0 5
        0000000000 65535 f
        0000000018 00000 n
        0000000077 00000 n
        0000000178 00000 n
        0000000457 00000 n
        trailer
        <<  /Root 1 0 R
        /Size 5
        >>
        startxref
        565
        %%EOF
        """.encode()


@pytest.fixture
def backoffice_client() -> Client:
    skip_if_no_django()
    return Client(headers={"Host": settings.BACKOFFICE_HOSTS[0]})


@pytest.fixture
def public_client_with_csrf() -> Client:
    skip_if_no_django()
    return Client(enforce_csrf_checks=True, headers={"Host": settings.PUBLIC_HOSTS[0]})


@pytest.fixture
def public_client() -> Client:
    skip_if_no_django()
    return Client(headers={"Host": settings.PUBLIC_HOSTS[0]})


@pytest.fixture
def procrastinate():
    in_memory = procrastinate_testing.InMemoryConnector()

    with procrastinate_app.replace_connector(in_memory):
        yield procrastinate_app
