from inspect import cleandoc
from django.contrib import admin
from django.contrib.admin.widgets import AdminTextInputWidget
from django.forms import ModelForm
from django.http import HttpRequest

from seawatch_registration.models import ShortTextField

from . import models


@admin.register(models.Operation)
class OperationAdmin(admin.ModelAdmin):
    exclude = ("version",)
    list_display = ["id", "name", "start_date", "end_date"]


@admin.register(models.PermanentCrewMember)
class PermanentCrewMemberAdmin(admin.ModelAdmin):
    formfield_overrides = {ShortTextField: {"widget": AdminTextInputWidget}}
    list_display = ["name"]

    def get_form(
        self,
        request: HttpRequest,
        obj: models.PermanentCrewMember | None = None,
        change=False,
        **kwargs,
    ) -> type[ModelForm]:
        form = super().get_form(request, obj, change, **kwargs)
        return form

    def formfield_for_dbfield(self, db_field, *args, **kwargs):
        field = super().formfield_for_dbfield(db_field, *args, **kwargs)
        if field is None:
            return field

        match db_field.name:
            case "languages":
                field.help_text = languages_help_text
        return field


languages_help_text = (
    cleandoc(
        """
    The field has the format like {"de": 3, "fr": 0}.
    The value  are:
    &nbsp;&nbsp;&nbsp;&nbsp;0: "Basic Knowledge (A1-A2)",
    &nbsp;&nbsp;&nbsp;&nbsp;1: "Good (B1-B2)",
    &nbsp;&nbsp;&nbsp;&nbsp;2: "Fluent (C1-C2)",
    &nbsp;&nbsp;&nbsp;&nbsp;3: "Native Speaker",
    and the available languages and their codes are:
    &nbsp;&nbsp;&nbsp;&nbsp;Amharic (amh)
    &nbsp;&nbsp;&nbsp;&nbsp;Arabic (ara)
    &nbsp;&nbsp;&nbsp;&nbsp;Bengali (ben)
    &nbsp;&nbsp;&nbsp;&nbsp;Classic Arabic (ara-classic)
    &nbsp;&nbsp;&nbsp;&nbsp;Colloquial Arabic (car)
    &nbsp;&nbsp;&nbsp;&nbsp;Egyptian Arabic (arz)
    &nbsp;&nbsp;&nbsp;&nbsp;English (eng)
    &nbsp;&nbsp;&nbsp;&nbsp;French (fre)
    &nbsp;&nbsp;&nbsp;&nbsp;German (deu)
    &nbsp;&nbsp;&nbsp;&nbsp;Italian (ita)
    &nbsp;&nbsp;&nbsp;&nbsp;Levantine Arabic (ara-levantine)
    &nbsp;&nbsp;&nbsp;&nbsp;Maghrebi Arabic (ara-maghrebi)
    &nbsp;&nbsp;&nbsp;&nbsp;Mesopotamian Arabic (acm)
    &nbsp;&nbsp;&nbsp;&nbsp;Modern Standard Arabic (arb)
    &nbsp;&nbsp;&nbsp;&nbsp;Peninsular Arabic (ara-peninsular)
    &nbsp;&nbsp;&nbsp;&nbsp;Portuguese (por)
    &nbsp;&nbsp;&nbsp;&nbsp;Somali (som)
    &nbsp;&nbsp;&nbsp;&nbsp;Spanish (spa)
    &nbsp;&nbsp;&nbsp;&nbsp;Sudanese Arabic (apd)
    &nbsp;&nbsp;&nbsp;&nbsp;Swahili (swa)
    &nbsp;&nbsp;&nbsp;&nbsp;Tigrinya (tir)
    &nbsp;&nbsp;&nbsp;&nbsp;Urdu (urd)
    """
    )
    .replace("\n", "<br>")
    .strip()
)
