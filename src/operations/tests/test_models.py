from datetime import date

from pytest import mark

from volunteers.tests.factories import VolunteerFactory

from ..models import Availability
from .factories import OperationFactory


@mark.django_db
def test_operation_increments_on_save():
    # given I created an operation
    operation = OperationFactory.create()
    version = operation.version
    # when I change the date
    new_time = date.today()
    operation.start_date = new_time
    operation.save()
    # I expect the date to be changed and the version to be incremented
    assert operation.start_date == new_time
    assert operation.version == version + 1


@mark.django_db
def test_operation_name_is_changeable():
    # given I created an operation
    operation = OperationFactory.create()
    name = operation.name
    # when I change the name
    operation.name = "TEST OPERATION - 231.0"
    operation.save()
    # I expect the name to be changed
    assert operation.name != name


@mark.django_db
def test_adding_available_volunteer_to_operation_stores_version_in_relation():
    # given I created an operation
    # and there is one volunteer available
    operation = OperationFactory.create()
    volunteer = VolunteerFactory.create()
    version = operation.version
    # When i add the volunteer to an operation by adding an Availability relation
    availability = Availability(operation=operation, volunteer=volunteer)
    availability.save()
    # I expect that the version of the operation I added the vol to is present
    assert availability.operation_version == version


@mark.django_db
def test_adding_available_volunteer_to_operation_defaults_to_UNKNOWN():
    # given I created an operation
    # and there is one volunteer available
    operation = OperationFactory.create()
    volunteer = VolunteerFactory.create()
    # When i add the volunteer to an operation by adding an Availability relation
    availability = Availability(operation=operation, volunteer=volunteer)
    availability.save()
    # I expect that the availability of the volunteer for this operation to be unknown
    assert availability.availability == Availability.Choices.UNKNOWN


@mark.django_db
def test_operations_contain_name():
    # given a new operation
    name = "test_operation"
    operation = OperationFactory.create(name=name)
    # when an operation created in the DB
    # then a name should be part of the operation string
    assert operation.name == name
    assert name in str(operation)
