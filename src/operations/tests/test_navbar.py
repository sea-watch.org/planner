from typing import cast

from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test.client import RequestFactory
from django.urls import reverse
from pytest import mark

from applications.views import ApplicationListView
from planner_common.feature_flags import features
from seawatch_registration.tests.factories import UserFactory


@mark.skipif(not features["operation_planning"], reason="needs feature flag")
@mark.django_db
def test_navbar_has_operations_link(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
        ]
    )
    response = ApplicationListView.as_view()(request)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, "html.parser"
    )

    nav_links = html.select("nav a.nav-link")
    assert ("Operations", reverse("operation_list")) in [
        (x.text, x["href"]) for x in nav_links
    ]

    # TODO: write test looking for active tab #276
