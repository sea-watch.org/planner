from http import HTTPStatus
from bs4 import BeautifulSoup
from django.conf import settings
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from pytest import mark

from operations.models import Operation
from seawatch_planner.tests.site import site
from seawatch_registration.tests.factories import UserFactory

from .factories import OperationFactory


@mark.django_db
def test_operations_admin(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(is_staff=True, permission="view_operation")
    )
    assert (
        backoffice_client.get(
            reverse("admin:operations_operation_changelist")
        ).status_code
        == 200
    )


@mark.django_db
def test_operation_with_dates_in_list(backoffice_client: Client):
    # given there is one operation in the databse
    # and I am logged in with a staff user that is allowed to view operations
    test_operation: Operation = OperationFactory.create()
    backoffice_client.force_login(
        UserFactory.create(is_staff=True, permission="view_operation")
    )
    # when i navigate to the operation view
    response = backoffice_client.get(reverse("admin:operations_operation_changelist"))
    assert isinstance(response, TemplateResponse)
    with site("backoffice"):  # Needed because the template is rendered lazily...
        html = BeautifulSoup(response.rendered_content, features="html.parser")
    detailsUrl = reverse(
        "admin:operations_operation_change",
        kwargs={"object_id": test_operation.id},
        urlconf=settings.BACKOFFICE_URLCONF,
    )
    # I expect that there is link on the page that leads me to the operation detail view
    # and  I expect that the table contains the start and end date
    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert (
        test_operation.start_date.strftime("%d.%m.%Y")
        in html.find_all("td", class_="field-start_date")[0].text
    )
    assert (
        test_operation.end_date.strftime("%d.%m.%Y")
        in html.find_all("td", class_="field-end_date")[0].text
    )


@mark.django_db
def test_includes_permanent_crew(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(is_staff=True, permission="view_permanentcrewmember")
    )

    assert (
        backoffice_client.get(
            reverse("admin:operations_permanentcrewmember_changelist")
        ).status_code
        == HTTPStatus.OK
    )
