from datetime import timedelta
from random import randint
from typing import Callable, ClassVar, cast
from uuid import uuid4

from factory import LazyFunction, Maybe, SubFactory, post_generation  # type: ignore
from factory.faker import Faker
from factory.declarations import LazyAttribute, RelatedFactoryList, SelfAttribute
from factory.django import DjangoModelFactory

from planner_common import data
from planner_common.models import Gender
from planner_common.test.factories import DeprecateDirectCallMetaclass, UniqueFaker
from seawatch_registration.tests.factories import PositionFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import SarProfileFactory, VolunteerFactory

from ..models import Availability, Operation, OperationLine, PermanentCrewMember


class OperationFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Operation

    start_date = Faker("date_between", start_date="+30d", end_date="+1y")
    end_date = LazyAttribute(lambda o: o.start_date + timedelta(randint(1, 60)))
    name = UniqueFaker("word")

    create: ClassVar[Callable[..., Operation]]
    create_batch: ClassVar[Callable[..., list[Operation]]]
    build: ClassVar[Callable[..., Operation]]
    build_batch: ClassVar[Callable[..., list[Operation]]]


class PermanentCrewMemberFactory(
    DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass
):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = PermanentCrewMember
        skip_postgeneration_save = True

    create: ClassVar[Callable[..., PermanentCrewMember]]
    create_batch: ClassVar[Callable[..., list[PermanentCrewMember]]]
    build: ClassVar[Callable[..., PermanentCrewMember]]
    build_batch: ClassVar[Callable[..., list[PermanentCrewMember]]]

    class Params:
        # The seed is used so we only depend on the randomness from factory_boy
        name = Faker("name")
        languages_count = Faker("random_int", min=1, max=5)
        language_codes = Faker(
            "random_elements",
            unique=True,
            length=SelfAttribute("..languages_count"),
            elements=[lang["code"] for lang in data.languages],
        )
        language_levels = Faker(
            "random_choices",
            length=SelfAttribute("..languages_count"),
            elements=[0, 1, 2, 3],
        )

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")

    positions = RelatedFactoryList(
        PositionFactory,
        size=lambda: randint(1, 5),  # type: ignore
    )
    languages = LazyAttribute(
        lambda s: {
            code: level for (code, level) in zip(s.language_codes, s.language_levels)
        }
    )
    gender = Faker("random_element", elements=Gender.values)


class OperationLineFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = OperationLine
        skip_postgeneration_save = True

    create: ClassVar[Callable[..., OperationLine]]
    create_batch: ClassVar[Callable[..., list[OperationLine]]]
    build: ClassVar[Callable[..., OperationLine]]
    build_batch: ClassVar[Callable[..., list[OperationLine]]]

    operation = SubFactory(OperationFactory)
    position = SubFactory(PositionFactory)

    class Params:
        selected = Faker(
            "random_element", elements=["volunteer", "permanent_crew", "nobody"]
        )
        selected_is_volunteer = LazyAttribute(lambda self: self.selected == "volunteer")

    permanent_crew = LazyAttribute(lambda self: self.selected == "permanent_crew")
    crew_member = Maybe(
        "selected_is_volunteer",
        SubFactory(VolunteerFactory),  # type: ignore
    )
    permanent_crew_member = Maybe(
        "permanent_crew",
        SubFactory(PermanentCrewMemberFactory),  # type: ignore
    )

    status = Faker("random_element", elements=OperationLine.Status)

    @post_generation
    def ensure_crew_member_has_sar_profile(self, create, extracted, **kwargs):
        crew_member = cast(Volunteer, self.crew_member)
        if crew_member and not crew_member.sarprofile_set.exists():
            method = SarProfileFactory.create if create else SarProfileFactory.build
            method(volunteer=crew_member)


class AvailabilityFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Availability

    create: ClassVar[Callable[..., Availability]]
    create_batch: ClassVar[Callable[..., list[Availability]]]
    build: ClassVar[Callable[..., Availability]]
    build_batch: ClassVar[Callable[..., list[Availability]]]

    operation = SubFactory(OperationFactory)
    volunteer = SubFactory(VolunteerFactory)
    uid = LazyFunction(lambda: str(uuid4()))
    availability = Faker("random_element", elements=["yes", "no", "maybe", "unknown"])
    operation_version = LazyAttribute(lambda self: self.operation.version)
