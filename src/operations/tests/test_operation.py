import datetime
from http import HTTPStatus
from typing import assert_never

import bs4
from bs4.element import Tag
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from planner_common.optional import unwrap
from seawatch_registration.models import Position
from volunteers.models import Document, Volunteer
from volunteers.tests.factories import DocumentFactory, SarProfileFactory
from playwright.sync_api import BrowserContext, Locator, expect
from pytest import fixture, mark, skip
from pytest_subtests import SubTests

from ..models import Operation, OperationLine, PermanentCrewMember
from ..tests.factories import (
    OperationFactory,
    OperationLineFactory,
    PermanentCrewMemberFactory,
)
from .. import views
from ..views import (
    CurrentOperationsListView,
    OperationLineDeleteView,
)
from planner_common.models import Gender
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.tests.factories import ExperienceFactory, VolunteerFactory


@fixture
def user() -> User:
    return UserFactory.create(permission="operations.view_operation")


@fixture
def get_request(rf: RequestFactory, user: User) -> HttpRequest:
    request = rf.get("")
    request.user = user
    return request


def td_by_title(html: bs4.BeautifulSoup, title: str) -> Tag:
    column_idx = [th.text.strip() for th in html.select("thead th")].index(title)
    td = html.select_one(
        f"tbody tr:not(.rowgroup-header) td:nth-child({column_idx + 1})"
    )
    assert td
    return td


def select_crew_member(
    row: Locator, crew_member: None | Volunteer | PermanentCrewMember
):
    # If this fails, either we got something completely different or the class hierarchies have
    # changd enough for this to be adjusted
    assert crew_member is None or (
        isinstance(crew_member, Volunteer)
        != isinstance(crew_member, PermanentCrewMember)
    )
    row.get_by_role("button", name="Select crew member").click()
    dialog = row.page.get_by_role("dialog")
    match crew_member:
        case None:
            skip("TODO #276 implement deselecting")
        case PermanentCrewMember(name=name):
            dialog.get_by_role("checkbox", name="Permanent Crew").check()
            dialog.get_by_role("row", name=name).get_by_role(
                "button", name="Select"
            ).click()
        case Volunteer(name=name):
            dialog.get_by_role("checkbox", name="Permanent Crew").uncheck()
            dialog.get_by_role("row", name=name).get_by_role(
                "button", name="Select"
            ).click()
    expect(dialog).not_to_be_visible()


@mark.django_db
def test_list_links_to_operation_details(get_request: HttpRequest):
    operation: Operation = OperationFactory.create(name="sea")

    response = CurrentOperationsListView.as_view()(get_request)
    assert isinstance(response, TemplateResponse)

    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    entry = [th for th in html.find_all("th") if th.text.strip() == "sea"][0]
    assert isinstance(entry, Tag)

    link = entry.find("a")
    assert link["href"] == reverse("operation", kwargs={"pk": operation.id})  # type: ignore


@mark.django_db
def test_show_operation_details_page(get_request: HttpRequest):
    test_start_date = datetime.date(year=2024, month=4, day=20)
    test_end_date = datetime.date(year=2024, month=6, day=23)
    operation: Operation = OperationFactory.create(
        name="sea", start_date=test_start_date, end_date=test_end_date
    )

    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    assert str(operation.name) in response.rendered_content
    assert "20.04.2024 - 23.06.2024" in response.rendered_content


@mark.django_db
def test_shows_button_for_adding_position(get_request: HttpRequest):
    operation = OperationFactory.create()
    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")
    forms = html.find_all("form")

    assert any(form.text.strip() in ["Add Line", "Add line"] for form in forms)


@mark.django_db
def test_shows_empty_table_for_new_operation(get_request: HttpRequest):
    # Given a new operation
    operation = OperationFactory.create()

    # When I navigate to the operation details
    response = views.operation(get_request, pk=operation.id)

    # Then I expect the table to be empty
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    assert not html.select("table tbody tr")


@mark.django_db
def test_shows_table_with_headers(get_request: HttpRequest):
    # Given any operation
    operation = OperationFactory.create()

    # When I navigate to the operation details
    response = views.operation(get_request, pk=operation.id)

    # Then I expect the table to display the headers
    assert isinstance(response, TemplateResponse)
    assert "Name" in response.rendered_content
    assert "Position" in response.rendered_content
    assert "Status" in response.rendered_content
    assert "SAR Experience" in response.rendered_content
    assert "Languages" in response.rendered_content
    assert "Gender" in response.rendered_content
    assert "Certs" in response.rendered_content


@mark.django_db
def test_post_adds_line_and_redirects_back(
    rf: RequestFactory, user: User, subtests: SubTests
):
    operation = OperationFactory.create()
    request = rf.post("")
    request.user = user
    response = views.operation(request, pk=operation.id)

    with subtests.test("redirects back"):
        assert (
            isinstance(response, HttpResponseRedirect)
            and response.url == operation.get_absolute_url()
        )

    with subtests.test("creates line"):
        assert operation.lines.count() == 1


@mark.django_db
def test_clicking_button_adds_new_line_to_table(
    logged_in_context: BrowserContext, backoffice_url
):
    # Given any operation
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    page.goto(f"{backoffice_url}{reverse('operation', kwargs={'pk': operation.id})}")

    # When I click on the 'add position for operation' button
    page.locator("form").get_by_role("button").get_by_text("Add Line").click()

    # Then I see that a new line was added to the table
    row_list = page.locator("table tbody tr:not(.rowgroup-header)")
    expect(row_list).to_have_count(1)


@mark.django_db
@mark.parametrize(
    ["column", "expected"],
    [
        ("SAR Experience", "Yes"),
        ("Languages", "German(N)/Italian(B)"),
        ("Gender", "Male"),
    ],
)
def test_shows_operation_line_details(
    column: str, expected: str, get_request: HttpRequest
):
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create(
        languages={"deu": 3, "ita": 1},
        gender=Gender.MALE,
    )
    SarProfileFactory.create(volunteer=crew_member)
    ExperienceFactory.create(volunteer=crew_member)
    operation.lines.create(crew_member=crew_member)

    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    assert td_by_title(html, column).text.strip() == expected


@mark.django_db
def test_shows_dropdown_menu_for_volunteer_position(get_request: HttpRequest):
    operation = OperationFactory.create()
    PositionFactory.create_batch(3)
    operation.lines.create()
    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    td = td_by_title(html, "Position")
    values = [option.text for option in td.select("option")]
    selectedOptions = [option.text for option in td.select("option[selected]")]
    assert set(selectedOptions) == {"Choose Position"}
    assert set(values) == {
        "Choose Position",
        *{position.name for position in Position.objects.all()},
    }


@mark.django_db
def test_positions_dropdown_still_works_after_row_is_changed(
    logged_in_context: BrowserContext,
):
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    PositionFactory.create_batch(3)
    operation.lines.create()
    new_volunteer = SarProfileFactory.create(volunteer__gender=Gender.FEMALE).volunteer
    page.goto(reverse("operation", kwargs={"pk": operation.id}))

    select_crew_member(page.locator("tbody tr"), new_volunteer)

    # wait until it's refreshed
    expect(page.locator("tbody tr:not(.rowgroup-header)")).to_contain_text(
        str(Gender.FEMALE.label)
    )

    assert {
        t.strip()
        for t in page.locator("select[name='position'] option").all_text_contents()
    } == {"Choose Position", *{position.name for position in Position.objects.all()}}


@mark.django_db
def test_shows_saved_volunteer_position(get_request: HttpRequest):
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create()
    position = PositionFactory.create()
    operation.lines.create(crew_member=crew_member, position=position)
    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    td = td_by_title(html, "Position")
    values = [option.text for option in td.select("option[selected]")]
    assert set(values) == {position.name}


@mark.django_db
def test_changing_volunteer_position_dropdown_changes_get_saved(
    logged_in_context: BrowserContext, backoffice_url
):
    # Given any operation
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create()
    line = operation.lines.create(
        crew_member=crew_member, position=PositionFactory.create()
    )
    new_position = PositionFactory.create()
    page.goto(f"{backoffice_url}{reverse('operation', kwargs={'pk': operation.id})}")

    # When I click on the dropdown menu for the volunteer rank and choose "Master"
    with page.expect_response(reverse("operation_line", kwargs={"pk": line.pk})):
        page.locator("select[name='position']").select_option(new_position.name)

    # Then I see that "Master" is in the database
    line.refresh_from_db()
    assert line.position == new_position


@mark.django_db
def test_shows_dropdown_menu_for_volunteer_status(get_request: HttpRequest):
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create()
    operation.lines.create(crew_member=crew_member)
    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    td = td_by_title(html, "Status")
    values = [option.text for option in td.select("option")]
    assert set(values) == {"Confirmed", "Contacted", "Considered"}


@mark.django_db
def test_shows_saved_volunteer_status(get_request: HttpRequest):
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create()
    operation.lines.create(
        crew_member=crew_member, status=OperationLine.Status.CONFIRMED
    )
    response = views.operation(get_request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    td = td_by_title(html, "Status")
    values = [option.text for option in unwrap(td).select("option[selected]")]
    assert set(values) == {"Confirmed"}


@mark.django_db
def test_changing_volunteer_status_dropdown_changes_get_saved(
    logged_in_context: BrowserContext, backoffice_url
):
    # Given any operation
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    crew_member = VolunteerFactory.create()
    line = operation.lines.create(crew_member=crew_member)
    page.goto(f"{backoffice_url}{reverse('operation', kwargs={'pk': operation.id})}")

    # When I click on the dropdown menu for the volunteer status and choose "confirmed"
    with page.expect_response(reverse("operation_line", kwargs={"pk": line.pk})):
        page.locator("select[name='status']").select_option("confirmed")

    # Then I see that "confirmed" is in the database
    line.refresh_from_db()
    assert line.status == OperationLine.Status.CONFIRMED


@mark.django_db
def test_changing_volunteer_updates_row(logged_in_context: BrowserContext):
    # Given any operation
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    operation.lines.create(
        crew_member=SarProfileFactory.create(
            volunteer__gender=Gender.NONE_OTHER
        ).volunteer
    )
    new_crewie = SarProfileFactory.create(
        volunteer__gender=Gender.PREFER_NOT_SAY
    ).volunteer

    page.goto(reverse("operation", kwargs={"pk": operation.id}))

    # When I click the select volunteer button
    page.get_by_role("button", name="Select crew member").click()

    # And select a new volunteer from the dialog
    dialog = page.get_by_role("dialog")
    expect(dialog).to_be_visible()
    dialog.get_by_role("row", name=new_crewie.name).click()

    # Wait for reload, check that the new crewie is in the row
    expect(page.locator("tbody", has_text=new_crewie.name)).to_be_visible()


@mark.xfail
def test_sorts_by_position_order():
    # TODO: #276 todo
    # after changing positions, on the next reload, the lines should be in order
    # (not immediately, moving stuff directly is more confusing than helpful)
    raise NotImplementedError("todo")


@mark.django_db
def test_post_removes_line_and_redirects_back(
    rf: RequestFactory, user: User, subtests: SubTests
):
    # GIVEN an operation with one line
    operation = OperationFactory.create()
    operation_line = operation.lines.create()

    # WHEN I delete one line
    request = rf.post("", {"confirm_delete": "1"})
    request.user = user
    response = OperationLineDeleteView.as_view()(
        request, operation_id=operation.id, line_pk=operation_line.pk
    )

    # THEN I will be redirected to the same operation
    with subtests.test("redirects back"):
        assert (
            isinstance(response, HttpResponseRedirect)
            and response.url == operation.get_absolute_url()
        )
    # AND the operation will have no lines
    with subtests.test("deletes line"):
        assert operation.lines.count() == 0


@mark.django_db
def test_clicking_on_remove_line_button_opens_delete_confirmation_dialog(
    logged_in_context: BrowserContext, backoffice_url
):
    # Given any operation with one line
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    page.goto(f"{backoffice_url}{reverse('operation', kwargs={'pk': operation.id})}")
    page.locator("form").get_by_role("button").get_by_text("Add Line").click()

    # When I click on the 'remove' button of the first line
    page.locator(':nth-match(:text("Delete"), 1)').click()

    # Then I see a modal that asks me to confirm the delete request
    expect(page.get_by_text("Do you really want to delete this line?")).to_be_visible()


@mark.django_db
def test_confirming_delete_deletes_the_respective_line(
    logged_in_context: BrowserContext, backoffice_url
):
    # Given any operation with three lines
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    page.goto(f"{backoffice_url}{reverse('operation', kwargs={'pk': operation.id})}")
    for _ in range(3):
        page.locator("form").get_by_role("button").get_by_text("Add Line").click()

    # When I click on the 'remove' button
    page.locator(':nth-match(:text("Delete"), 2)').click()

    # And confirm that I really want to remove the line
    page.locator("form").get_by_role("button").get_by_text("Confirm").click()

    # # Then there are only non-header lines
    row_list = page.locator("table tbody tr:not(.rowgroup-header)")
    expect(row_list).to_have_count(2)


@mark.django_db
def test_OperationLineDeleteView_bad_request_response_on_empty_post(
    rf: RequestFactory, user: User
):
    # GIVEN an operation with one line
    operation = OperationFactory.create()
    operation_line = operation.lines.create()

    # WHEN I start the delete process without confirming or canceling
    request = rf.post("")
    request.user = user
    response = OperationLineDeleteView.as_view()(
        request, operation_id=operation.id, line_pk=operation_line.pk
    )

    # THEN I will get a bad request response
    assert response.status_code == HTTPStatus.BAD_REQUEST


@mark.parametrize("new_crewie", ["volunteer", "permanent_crew", "nobody"])
def test_change_crew_member(
    new_crewie: str,
    logged_in_context: BrowserContext,
    backoffice_url: str,
    subtests: SubTests,
):
    page = logged_in_context.new_page()

    # create operation with a line that has a crew_member
    operation = OperationFactory.create()
    operation_line = operation.lines.create(
        crew_member=SarProfileFactory.create().volunteer
    )
    assert operation_line.crew_member

    page.goto(f"{backoffice_url}{operation.get_absolute_url()}")

    match new_crewie:
        case "volunteer":
            new_crew_member = SarProfileFactory.create().volunteer
        case "permanent_crew":
            new_crew_member = PermanentCrewMemberFactory.create()
        case "nobody":
            new_crew_member = None
        case x:
            assert_never(x)  # type: ignore

    select_crew_member(
        page.get_by_role("row").filter(has_text=operation_line.crew_member.name),
        new_crew_member,
    )

    with subtests.test("ui"):
        table = page.locator("table")
        name_column = table.locator("thead th").all_inner_texts().index("Name")
        # expect column to have empty inner html
        row = table.locator(f"tbody tr td:nth-child({name_column + 1})")
        if new_crew_member:
            expect(row).to_contain_text(new_crew_member.name)
        else:
            expect(row).to_be_empty()

    # This is after UI so we don't get race conditions
    with subtests.test("database"):
        # Then the line should have no crew member
        operation_line.refresh_from_db()
        match new_crewie:
            case "volunteer":
                assert operation_line.crew_member == new_crew_member
                assert operation_line.permanent_crew is False
                assert operation_line.permanent_crew_member is None
            case "permanent_crew":
                assert operation_line.crew_member is None
                assert operation_line.permanent_crew is True
                assert operation_line.permanent_crew_member == new_crew_member
            case "nobody":
                assert operation_line.crew_member is None
                assert operation_line.permanent_crew_member is None
                assert operation_line.permanent_crew is False
            case x:
                assert_never(x)  # type: ignore


@mark.django_db
def test_permanent_crew_line(rf: RequestFactory, user: User):
    line = OperationLineFactory.create(permanent_crew=True)

    request = rf.get("")
    request.user = user
    response = views.operation(request, pk=line.operation.id)

    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    td = td_by_title(html, "Name")
    assert f"{unwrap(line.permanent_crew_member).name} (PC)" in td.text


@mark.django_db
def test_lists_genders(rf: RequestFactory, user: User):
    operation = OperationFactory.create()
    for sar_profile in SarProfileFactory.create_batch(
        size=2, volunteer__gender=Gender.MALE
    ):
        operation.lines.create(crew_member=sar_profile.volunteer)
    for sar_profile in SarProfileFactory.create_batch(
        size=3, volunteer__gender=Gender.FEMALE
    ):
        operation.lines.create(crew_member=sar_profile.volunteer)
    for sar_profile in SarProfileFactory.create_batch(
        size=4, volunteer__gender=Gender.NONE_OTHER
    ):
        operation.lines.create(crew_member=sar_profile.volunteer)
    for sar_profile in SarProfileFactory.create_batch(
        size=5, volunteer__gender=Gender.PREFER_NOT_SAY
    ):
        operation.lines.create(crew_member=sar_profile.volunteer)

    request = rf.get("")
    request.user = user

    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    assert "2♂/ 3♀/ 4○/ 5?" in response.rendered_content


@mark.django_db
def test_lists_zero_count_for_all_genders_by_default(rf: RequestFactory, user: User):
    operation = OperationFactory.create()

    request = rf.get("")
    request.user = user

    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")
    keyinfo = html.select_one("#operation-keyinfo")
    assert "0♂/ 0♀/ 0○/ 0?" in unwrap(keyinfo).text


@mark.django_db
def test_lists_ranks(rf: RequestFactory, user: User):
    T = Document.Type
    operation = OperationFactory.create()
    pax = [
        [],  # no certificates
        [T.medical_fitness],  # only medical fitness
        [T.stcw_basic_safety],
        [T.stcw_watchkeeping],
        [T.stcw_basic_safety, T.stcw_watchkeeping],
        [T.medical_fitness, T.stcw_watchkeeping],
    ]
    crew = [
        [T.stcw_basic_safety, T.medical_fitness],
    ]
    watch = [
        [T.stcw_basic_safety, T.medical_fitness, T.stcw_watchkeeping],
    ]
    for doc_types in [*pax, *crew, *watch]:
        volunteer = SarProfileFactory.create().volunteer
        for t in doc_types:
            DocumentFactory.create(volunteer=volunteer, type=t, expired=False)
        DocumentFactory.create_batch(10, volunteer=volunteer, expired=True)
        operation.lines.create(crew_member=volunteer)

    request = rf.get("")
    request.user = user

    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")
    keyinfo = html.select_one("#operation-keyinfo")
    assert f"{len(pax)} PAX / {len(watch)} WATCH" in unwrap(keyinfo).text


@mark.django_db
def test_lists_confimed_count(rf: RequestFactory, user: User):
    operation = OperationFactory.create()
    OperationLineFactory.create_batch(
        5,
        operation=operation,
        permanent_crew=False,
        status=OperationLine.Status.CONFIRMED,
    )
    OperationLineFactory.create_batch(
        3,
        operation=operation,
        permanent_crew=False,
        status=OperationLine.Status.CONSIDERED,
    )
    OperationLineFactory.create_batch(7, operation=operation, permanent_crew=True)

    request = rf.get("")
    request.user = user

    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")
    keyinfo = html.select_one("#operation-keyinfo")
    assert "12/15 confirmed" in unwrap(keyinfo).text


@mark.django_db
@mark.parametrize("with_permanent_crew", [True, False])
def test_keyinfo_includes_permanent_crew_notice_iff_permanent_crew_lines_exist(
    with_permanent_crew: bool,
    rf: RequestFactory,
    user: User,
):
    operation = OperationFactory.create()
    if with_permanent_crew:
        operation.lines.create(permanent_crew=True)
    request = rf.get("")
    request.user = user
    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")
    keyinfo = html.select_one("#operation-keyinfo")
    assert ("(+ permanent crew)" in unwrap(keyinfo).text) == with_permanent_crew


@mark.django_db
@mark.parametrize("what_changes", ["volunteer", "permanent_crew"])
def test_updates_keyinfo_when_changing_volunteers(
    what_changes: str, logged_in_context: BrowserContext
):
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    operation.lines.create(
        crew_member=SarProfileFactory.create(
            volunteer__gender=Gender.FEMALE, volunteer__first_name="Anastasia"
        ).volunteer
    )
    match what_changes:
        case "volunteer":
            crew_member = SarProfileFactory.create(
                volunteer__gender=Gender.MALE, volunteer__first_name="Nicolas"
            ).volunteer
        case "permanent_crew":
            crew_member = PermanentCrewMemberFactory.create(
                gender=Gender.MALE, first_name="Patrick"
            )
        case x:
            assert_never(x)  # type: ignore

    page.goto(operation.get_absolute_url())

    expect(page.locator("#operation-keyinfo")).to_contain_text("0♂/ 1♀")

    tr = page.locator("tbody tr")
    select_crew_member(tr, crew_member)
    expect(page.locator("#operation-keyinfo")).to_contain_text("1♂/ 0♀")


@mark.django_db
def test_groups_by_opsgroup(rf: RequestFactory, user: User):
    operation = OperationFactory.create()
    line1_1 = OperationLineFactory.create(
        operation=operation,
        selected="volunteer",
        position__with_ops_group=True,
        position__ops_group__order=5,
        position__ops_group_order=3,
    )
    assert line1_1.position
    assert line1_1.position.ops_group
    assert line1_1.crew_member
    line1_2 = OperationLineFactory.create(
        operation=operation,
        selected="volunteer",
        position__ops_group=line1_1.position.ops_group,
        position__ops_group_order=12,
    )
    assert line1_2.crew_member
    line2 = OperationLineFactory.create(
        operation=operation,
        selected="volunteer",
        position__with_ops_group=True,
        position__ops_group__order=7,
        position__ops_group_order=6,
    )
    assert line2.position
    assert line2.position.ops_group
    assert line2.crew_member

    request = rf.get("")
    request.user = user
    response = views.operation(request, pk=operation.id)
    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    rows = html.select("tbody tr")
    name_index = [e.text.strip() for e in html.select("thead tr > *")].index("Name")
    assert line1_1.position.ops_group.title in rows[0].text
    assert line1_1.crew_member.name in rows[1].select("tr > *")[name_index].text.strip()
    assert line1_2.crew_member.name in rows[2].select("tr > *")[name_index].text.strip()
    assert line2.position.ops_group.title in rows[3].text
    assert line2.crew_member.name in rows[4].select("tr > *")[name_index].text.strip()


@mark.django_db
def test_reorders_lines_when_changing_positions(logged_in_context: BrowserContext):
    page = logged_in_context.new_page()
    operation = OperationFactory.create()
    line1 = OperationLineFactory.create(
        operation=operation,
        position__with_ops_group=True,
        position__ops_group__order=1,
    )
    assert line1.position
    new_position = PositionFactory.create(with_ops_group=True, ops_group__order=2)
    line2 = OperationLineFactory.create(
        operation=operation,
        position__with_ops_group=True,
        position__ops_group__order=3,
    )
    assert line2.position
    line3 = OperationLineFactory.create(
        operation=operation,
        position__with_ops_group=True,
        position__ops_group__order=4,
    )
    assert line3.position

    page.goto(operation.get_absolute_url())

    # Verify the old order real quick
    assert page.locator(
        "select[name='position'] option[selected]"
    ).all_text_contents() == [
        line1.position.name,
        line2.position.name,
        line3.position.name,
    ], "hm? initial order is wrong?"
    # When I change the position of the first line
    page.locator(
        f"select[name='position']:has(option[selected][value='{line3.position.key}'])"
    ).select_option(new_position.name)
    page.wait_for_load_state("networkidle")

    line3.refresh_from_db()
    assert line3.position == new_position, "hm? position wasn't saved?"

    # Then the lines are reordered
    assert page.locator(
        "select[name='position'] option[selected]"
    ).all_text_contents() == [
        line1.position.name,
        line3.position.name,
        line2.position.name,
    ]


@mark.django_db
def test_all_positions_are_grouped():
    ps = Position.objects.filter(ops_group=None)
    assert [p.key for p in ps] == []


@mark.django_db
def test_dialog_shows_permanent_crew_if_selected(logged_in_context: BrowserContext):
    permanent_crew = PermanentCrewMemberFactory.create(first_name="Penelope")
    volunteer = SarProfileFactory.create(volunteer__first_name="Vladimir").volunteer

    op_line = OperationLineFactory.create()

    page = logged_in_context.new_page()
    page.goto(op_line.operation.get_absolute_url())

    page.get_by_role("button", name="Select crew member").click()
    dialog = page.get_by_role("dialog")
    table = dialog.get_by_role("table")
    expect(table).to_be_visible()

    toggle = dialog.get_by_role("checkbox", name="Permanent Crew")
    toggle.check()
    expect(table).to_contain_text(permanent_crew.name)
    expect(table).not_to_contain_text(volunteer.name)
    toggle.uncheck()
    expect(table).not_to_contain_text(permanent_crew.name)
    expect(table).to_contain_text(volunteer.name)
    toggle.check()
    expect(table).to_contain_text(permanent_crew.name)
    expect(table).not_to_contain_text(volunteer.name)
