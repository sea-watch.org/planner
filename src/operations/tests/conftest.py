from pytest import fixture

from seawatch_planner.tests.site import site


@fixture(autouse=True)
def backoffice_site():
    with site("backoffice"):
        yield
