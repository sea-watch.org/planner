from pytest import mark
from django.urls import reverse
from playwright.sync_api import BrowserContext
from operations.tests.factories import OperationLineFactory
from volunteers.tests.factories import SarProfileFactory


@mark.django_db
def test_simple_select(logged_in_context: BrowserContext):
    page = logged_in_context.new_page()
    operationLine = OperationLineFactory.create(crew_member=None, permanent_crew=False)

    page.goto(reverse("operation", kwargs={"pk": operationLine.operation.pk}))

    volunteer = SarProfileFactory.create().volunteer

    # Click the button that opens the modal
    page.get_by_role("button", name="Select Crew Member").click()

    # Click the select button in the row containing the volunteer's name
    row = page.get_by_role("row").filter(
        has=page.get_by_role("columnheader", name=volunteer.name)
    )
    row.get_by_role("button").click()

    # Wait for navigation after form submission
    page.wait_for_load_state("networkidle")

    # Verify modal is closed by checking its visibility
    modal = page.get_by_role("dialog")
    assert not modal.is_visible()

    # Refresh the operation line from DB to check the volunteer was saved
    operationLine.refresh_from_db()
    assert operationLine.crew_member is not None

    # Verify the crew member name is visible on the page
    crew_member_name = operationLine.crew_member.name
    assert page.get_by_text(crew_member_name).is_visible()
