import datetime

from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark

from operations.tests.factories import OperationFactory
from operations.views import CurrentOperationsListView, PastOperationsListView
from seawatch_registration.tests.factories import UserFactory


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(
            permissions=[
                "applications.view_sarapplication",
                "volunteers.view_volunteer",
                "operations.view_operation",
            ]
        )
    )
    response = backoffice_client.get(
        reverse("operation_list"),
    )
    assert response.status_code == 200


@mark.django_db
def test_shows_empty_table_if_there_are_no_planned_operations(rf: RequestFactory):
    # Given there are no future or ongoing operations

    # When I navigate to the operation list
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = CurrentOperationsListView.as_view()(request)

    # Then I expect the operation to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert "Planned Operations" in response.rendered_content
    assert "Past Operations" in response.rendered_content
    assert "There are no planned operations." in response.rendered_content


@mark.django_db
def test_shows_empty_table_if_there_are_no_past_operations(rf: RequestFactory):
    # Given there are no past operations

    # When I navigate to the operation list
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = PastOperationsListView.as_view()(request)

    # Then I expect the operation to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert "Planned Operations" in response.rendered_content
    assert "Past Operations" in response.rendered_content
    assert "There are no past operations." in response.rendered_content


@mark.django_db
def test_shows_future_or_ongoing_operations(rf: RequestFactory):
    # Given there is one future or ongoing operation
    operation = OperationFactory.create()

    # When I navigate to the operation list
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = CurrentOperationsListView.as_view()(request)

    # Then I expect the operation to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert "Planned Operations" in response.rendered_content
    assert "Past Operations" in response.rendered_content
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    links = {a.text.strip(): a["href"] for a in html.select("a")}
    assert links["Planned Operations"] == reverse("operation_list")
    assert links["Past Operations"] == reverse("past_operation_list")
    assert str(operation.name) in response.rendered_content


@mark.django_db
def test_shows_past_operations(rf: RequestFactory):
    # Given there is a past operation
    operation = OperationFactory.create(
        end_date=datetime.date.today() - datetime.timedelta(5)
    )

    # When I navigate to the operation list for past operations
    request = rf.get("past_operation_list")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = PastOperationsListView.as_view()(request)

    # Then I expect the operation to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert "Planned Operations" in response.rendered_content
    assert "Past Operations" in response.rendered_content
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    links = {a.text.strip(): a["href"] for a in html.select("a")}
    assert links["Past Operations"] == reverse("past_operation_list")
    assert links["Planned Operations"] == reverse("operation_list")
    assert str(operation.name) in response.rendered_content


@mark.django_db
def test_shows_columns_for_future_or_ongoing_operations(rf: RequestFactory):
    # When I navigate to the operation list
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = CurrentOperationsListView.as_view()(request)

    # Then I expect the columns "operation name", "start/end date" and "#confirmed / #total"
    assert isinstance(response, TemplateResponse)
    assert "Operation Name" in response.rendered_content
    assert "Start Date" in response.rendered_content
    assert "End Date" in response.rendered_content
    assert "Confirmed/Total" in response.rendered_content


@mark.django_db
def test_shows_columns_for_past_operations(rf: RequestFactory):
    # When I navigate to the past operation list
    request = rf.get("")
    request.user = UserFactory.create(
        permissions=[
            "applications.view_sarapplication",
            "volunteers.view_volunteer",
            "operations.view_operation",
        ]
    )
    response = PastOperationsListView.as_view()(request)

    # Then I expect the columns "operation name", "start/end date" and "#confirmed / #total"
    assert isinstance(response, TemplateResponse)
    assert "Operation Name" in response.rendered_content
    assert "Start Date" in response.rendered_content
    assert "End Date" in response.rendered_content
