import htpy as h

from volunteers.models import Document, Volunteer


def operation_line_cell_rank(crew_member: Volunteer | None):
    match crew_member:
        case None:
            html = h.ul
        case Volunteer():
            html = h.ul[
                crew_member.document_set.filter(
                    type=Document.Type.stcw_basic_safety
                ).exists()
                and h.li[
                    h.abbr(
                        title="STCW I/9 Medical Fitness and Eyesight Requirements for Seafarers"
                    )["BS"]
                ],
                crew_member.document_set.filter(
                    type=Document.Type.stcw_watchkeeping
                ).exists()
                and h.li[h.abbr(title="Watchkeeping")["WK"]],
                crew_member.document_set.filter(
                    type=Document.Type.medical_fitness
                ).exists()
                and h.li[h.abbr(title="Medical Fitness")["MF"]],
            ]
    return html
