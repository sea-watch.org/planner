# Generated by Django 5.0.4 on 2024-04-08 16:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("operations", "0007_alter_operationline_rank"),
    ]

    operations = [
        migrations.AlterField(
            model_name="operationline",
            name="id",
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
