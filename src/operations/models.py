from __future__ import annotations
from itertools import groupby
from collections.abc import Iterable
import os

from typing import TYPE_CHECKING, Tuple
from uuid import uuid4

from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from planner_common.models import Gender
from seawatch_registration.models import Position, PositionOpsGroup, ShortTextField
from volunteers.models import Volunteer, validate_languages_dict

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager


def uuid4_str():
    return str(uuid4())


class Operation(models.Model):
    id = models.AutoField(primary_key=True)
    name = ShortTextField(unique=True)
    start_date = models.DateField()
    end_date = models.DateField()
    version = models.IntegerField(default=0)
    available = models.ManyToManyField(Volunteer, through="Availability")

    if TYPE_CHECKING:
        lines: RelatedManager[OperationLine]

    def __str__(self):
        return f"Operation {self.name} ({self.start_date} - {self.end_date})"

    def lines_by_position_group(
        self,
    ) -> Iterable[Tuple[PositionOpsGroup | None, Iterable[OperationLine]]]:
        lines = (
            self.lines.select_related("position__ops_group")
            .annotate(
                group_order=models.F("position__ops_group__order"),
                order_in_group=models.F("position__ops_group_order"),
            )
            .order_by("group_order", "order_in_group")
        )
        # https://stackoverflow.com/questions/6906593/itertools-groupby-in-a-django-template
        # tldr; django templates break generators
        return [
            (ops_group, list(group_lines))
            for ops_group, group_lines in groupby(
                lines,
                key=lambda line: line.position.ops_group if line.position else None,
            )
        ]

    def save(self, *args, **kwargs):
        """On save, increment version"""
        self.version += 1
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("operation", kwargs={"pk": self.id})

    def prefill_positions(self, position_keys: Iterable[str]):
        """Convenience function until we build a ui for it, this will be called via django shell"""
        if self.lines.exists():
            raise ValueError("Operation already has lines")

        position_by_key = {
            position.key: position for position in Position.objects.all()
        }
        OperationLine.objects.bulk_create(
            OperationLine(operation=self, position=position_by_key[key])
            for key in position_keys
        )

    def prefill_positions_from_environ(self, env_var: str):
        """Convenience function until we build a ui for it, this will be called via django shell"""
        position_keys = [k for k in os.environ[env_var].split(" ") if k]
        self.prefill_positions(position_keys)


class PermanentCrewMember(models.Model):
    id = models.AutoField(primary_key=True)

    first_name = ShortTextField()
    last_name = ShortTextField()
    email = models.EmailField()

    positions = models.ManyToManyField(Position)
    languages = models.JSONField(
        validators=[validate_languages_dict], default=dict
    )  # {"deu": 3, "eng": 2}

    gender = ShortTextField(choices=Gender.choices)

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}".strip()

    def __str__(self) -> str:
        return self.name


class OperationLine(models.Model):
    operation = models.ForeignKey(
        Operation,
        on_delete=models.CASCADE,
        related_name="lines",
    )
    id = models.AutoField(primary_key=True)

    crew_member = models.ForeignKey(
        Volunteer, on_delete=models.PROTECT, null=True, blank=True
    )

    permanent_crew = models.BooleanField(default=False)
    permanent_crew_member = models.ForeignKey(
        PermanentCrewMember,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    position = models.ForeignKey(Position, null=True, on_delete=models.PROTECT)

    class Status(models.TextChoices):
        CONSIDERED = "considered", _("Considered")
        CONTACTED = "contacted", _("Contacted")
        CONFIRMED = "confirmed", _("Confirmed")

    status = ShortTextField(default=Status.CONSIDERED, choices=Status.choices)


class Availability(models.Model):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        verbose_name_plural = "availabilities"

    class Choices(models.TextChoices):
        YES = "yes", _("yes")
        NO = "no", _("no")
        MAYBE = "maybe", _("maybe")
        UNKNOWN = "unknown", _("unknown")
        "UNKNOWN means that the volunteer hasn't responded yet"

    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
    operation_version = models.IntegerField()
    uid = ShortTextField(default=uuid4_str)
    availability = ShortTextField(default=Choices.UNKNOWN, choices=Choices.choices)

    def save(self, *args, **kwargs):
        """On save, increment version"""
        if self.operation_version is None:
            self.operation_version = self.operation.version
        return super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.volunteer} for {self.operation} -> {self.availability}"
