from django import template
from django.utils.safestring import mark_safe

from volunteers.models import Volunteer
from .. import html


register = template.Library()


@register.simple_tag
def operation_line_cell_rank(crew_member: Volunteer | None):
    return mark_safe(str(html.operation_line_cell_rank(crew_member)))  # nosec b308 b703 htpy does escaping
