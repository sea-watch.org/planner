from django.urls import path

from operations import views

urlpatterns = [
    path("", views.CurrentOperationsListView.as_view(), name="operation_list"),
    path(
        "past_operations/",
        views.PastOperationsListView.as_view(),
        name="past_operation_list",
    ),
    path(
        "<int:pk>/",
        views.operation,
        name="operation",
    ),
    path(
        "line/<int:line_pk>/delete-dialog/",
        views.OperationLineDeleteView.as_view(),
        name="operationline_confirm_delete",
    ),
    path(
        "operation_line/<int:pk>/",
        views.operation_line,
        name="operation_line",
    ),
    path(
        "operation_line/<int:pk>/select_volunteer_dialog/",
        views.operationline_select_volunteer_dialog,
        name="operationline_select_volunteer_dialog",
    ),
]
