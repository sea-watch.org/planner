from datetime import date
import itertools

from django.utils.decorators import method_decorator
from planner_common.feature_flags import features
from planner_common.models import Gender
from http import HTTPStatus
from typing import Any, Dict, assert_never

from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q, Count, F, Exists, OuterRef
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
    QueryDict,
)
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse_lazy
from django.views.decorators.http import require_http_methods
from django.views.generic import ListView
from django.views.generic.edit import DeleteView
from django.views.decorators.cache import cache_control

from operations.models import Operation, OperationLine, PermanentCrewMember
from seawatch_registration.models import Position
from volunteers.models import Document, Volunteer


@method_decorator(cache_control(no_cache=True), name="dispatch")
class CurrentOperationsListView(PermissionRequiredMixin, ListView):
    model = Operation
    permission_required = "operations.view_operation"
    if features["daisy_ui_migration"]:
        template_name = "operations/operation_list.html"
    else:
        template_name = "operations/operation_list_original.html"

    def get_queryset(self):
        qs = Operation.objects.all()
        qs = qs.order_by("start_date").filter(end_date__gte=date.today())
        return qs

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        stats_by_operation = {
            row["id"]: {"confirmed": row["confirmed"], "total": row["total"]}
            for row in self.get_queryset()
            .annotate(
                total=Count("lines"),
                confirmed=Count(
                    "lines",
                    filter=(
                        Q(lines__status=OperationLine.Status.CONFIRMED)
                        | Q(lines__permanent_crew=True)
                    ),
                ),
            )
            .values("id", "total", "confirmed")
        }
        ctx = super().get_context_data(**kwargs)
        ctx["operations_with_data"] = [
            (
                operation,
                stats_by_operation[operation.id]["confirmed"],
                stats_by_operation[operation.id]["total"],
            )
            for operation in self.get_queryset()
        ]
        return ctx


class PastOperationsListView(PermissionRequiredMixin, ListView):
    model = Operation
    permission_required = "operations.view_operation"
    if features["daisy_ui_migration"]:
        template_name = "operations/past_operation_list.html"
    else:
        template_name = "operations/past_operation_list_original.html"

    def get_queryset(self):
        qs = Operation.objects.all()
        qs = qs.order_by("-start_date").filter(end_date__lt=date.today())
        return qs


@permission_required("operations.view_operation")
@require_http_methods(["GET", "POST"])
def operation(request: HttpRequest, pk: int) -> HttpResponse:
    if features["daisy_ui_migration"]:
        template = "operations/operation_details.html"
    else:
        template = "operations/operation_details_original.html"

    operation = Operation.objects.get(pk=pk)
    match request.method:
        case "GET":
            return TemplateResponse(
                request,
                template,
                {
                    "operation": operation,
                    "volunteer_options": operation__volunteer_options(),
                    "available_positions": Position.objects.all(),
                    "keyinfo": operation__keyinfo_lines(operation),
                },
            )
        case "POST":
            operation.lines.create()
            return HttpResponseRedirect(operation.get_absolute_url())
        case m:
            assert_never(m)  # type: ignore


def operation__volunteer_options():
    return [
        {"value": volunteer.id, "label": volunteer.name}
        for volunteer in Volunteer.objects.exclude(sarprofile=None)
    ]


def operation__keyinfo_lines(operation: Operation):
    volunteer_lines = operation.lines.exclude(crew_member=None)
    permanent_crew_lines = operation.lines.exclude(permanent_crew_member=None)

    gender_counts = {
        row["gender"]: row["count"]
        for row in itertools.chain(
            volunteer_lines.values(gender=F("crew_member__gender")).annotate(
                count=Count("gender")
            ),
            permanent_crew_lines.values(
                gender=F("permanent_crew_member__gender")
            ).annotate(count=Count("gender")),
        )
    }
    gender_symbols = {
        Gender.MALE: "♂",
        Gender.FEMALE: "♀",
        Gender.NONE_OTHER: "○",
        Gender.PREFER_NOT_SAY: "?",
    }
    gender_line = "/ ".join(
        f"{gender_counts.get(gender, 0)}{gender_symbols[gender]}" for gender in Gender
    )

    # TODO: Include permanent crew in rank count #276
    valid_document = Document.objects.filter(
        archived=None,
        # TODO match mission time, not today #276
        valid_until__gt=date.today(),
        volunteer=OuterRef("crew_member"),
    )
    ranks = {
        "pax": (
            volunteer_lines.exclude(
                Exists(valid_document.filter(type=Document.Type.stcw_basic_safety))
                & Exists(
                    valid_document.filter(
                        type=Document.Type.medical_fitness,
                    )
                )
            ).count()
        ),
        "watchkeeping": (
            volunteer_lines.filter(
                Exists(valid_document.filter(type=Document.Type.stcw_watchkeeping)),
                Exists(valid_document.filter(type=Document.Type.stcw_basic_safety)),
                Exists(valid_document.filter(type=Document.Type.medical_fitness)),
            )
        ).count(),
    }
    rank_line = f"{ranks['pax']} PAX / {ranks['watchkeeping']} WATCH"

    has_permanent_crew = operation.lines.filter(permanent_crew=True).exists()
    keyinfo_suffix = " (+ permanent crew)" if has_permanent_crew else ""

    confirmed = operation.lines.filter(
        Q(status=OperationLine.Status.CONFIRMED) | Q(permanent_crew=True)
    ).count()
    confirmed_line = f"{confirmed}/{operation.lines.count()} confirmed"

    return {
        "gender_line": gender_line + keyinfo_suffix,
        "rank_line": rank_line + keyinfo_suffix,
        "confirmed_line": confirmed_line,
    }


@require_http_methods(["PATCH"])
def operation_line(request: HttpRequest, pk: int) -> HttpResponse:
    line = get_object_or_404(OperationLine, pk=pk)
    ctx = {}
    match QueryDict(request.body):
        case {"volunteer": volunteer_id}:
            if volunteer_id:
                line.crew_member = get_object_or_404(Volunteer, pk=volunteer_id)
            else:
                line.crew_member = None
            line.save()
            ctx["keyinfo"] = operation__keyinfo_lines(line.operation)
        case {"status": status}:
            line.status = OperationLine.Status(status)
            line.save()
            return TemplateResponse(
                request, "operations/operation_details_line_status.html", {"line": line}
            )
        case {"position": key}:
            line = get_object_or_404(OperationLine, pk=pk)
            try:
                line.position = Position.objects.get(key=key)
            except Position.DoesNotExist:
                return HttpResponseBadRequest("Position doesn't exist")
            line.save()
            # In this case, we actually need to re-render the whole table since the position
            # ordering might have changed
            return TemplateResponse(
                request,
                "operations/operation_details_tbody.html",
                {
                    "operation": line.operation,
                    "volunteer_options": operation__volunteer_options(),
                    "available_positions": Position.objects.all(),
                },
            )
        case _:
            return HttpResponseBadRequest("POST must be")

    return TemplateResponse(
        request,
        "operations/operation_line_patch_response.html",
        {"line": line, "available_positions": Position.objects.all(), **ctx},
    )


class OperationLineDeleteView(DeleteView):
    template = "operations/operationline_confirm_delete.html"
    model = OperationLine
    object: OperationLine | None
    permission_required = "operations.view_operation"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        ctx = super().get_context_data(**kwargs)

        line_pk = self.kwargs.get("line_pk", None)
        line = get_object_or_404(OperationLine, pk=line_pk)

        ctx["line"] = line
        ctx["operation"] = line.operation
        ctx["show_modal_immediately"] = True
        return ctx

    def get_object(self, queryset=None):
        line_pk = self.kwargs.get("line_pk", None)
        return get_object_or_404(OperationLine, pk=line_pk)

    def get_success_url(self) -> str:
        assert self.object is not None  # otherwise we would get http status "not found"
        return reverse_lazy("operation", kwargs={"pk": self.object.operation.id})

    def post(self, request, *args, **kwargs):
        try:
            line = self.get_object()
        except OperationLine.DoesNotExist:
            return HttpResponse(status=HTTPStatus.NOT_FOUND)
        operation = get_object_or_404(Operation, pk=line.operation.id)
        if self.request.POST.get("confirm_delete"):
            line.delete()
            return HttpResponseRedirect(operation.get_absolute_url())
        elif self.request.POST.get("cancel"):
            return HttpResponseRedirect(operation.get_absolute_url())
        else:
            return HttpResponseBadRequest()


@require_http_methods(["GET", "POST"])
def operationline_select_volunteer_dialog(request: HttpRequest, pk: int):
    line = get_object_or_404(OperationLine, pk=pk)
    match request.method:
        case "GET":
            positions_by_group = Position.objects.by_group()
            position_key = request.GET.get("position")
            permanent_crew = request.GET.get("permanent_crew") == "on"
            if permanent_crew:
                permanent_crew = PermanentCrewMember.objects.all()
                if position_key:
                    permanent_crew = permanent_crew.filter(positions__key=position_key)
                potential_crew_members = permanent_crew
            else:
                volunteers = Volunteer.objects.exclude(sarprofile=None)
                if position_key:
                    volunteers = volunteers.filter(
                        sarprofile__position__key=position_key
                    )
                potential_crew_members = volunteers

            if search_term := request.GET.get("search"):
                for word in search_term.split(" "):
                    potential_crew_members = potential_crew_members.filter(
                        Q(first_name__icontains=word) | Q(last_name__icontains=word)
                    )

            return TemplateResponse(
                request,
                "operations/operation_details_line_crew_member_select_dialog.html",
                {
                    "line": line,
                    "show_modal_immediately": True,
                    "positions_without_group": positions_by_group.pop("", []),
                    "positions_with_group": positions_by_group,
                    "permanent_crew": permanent_crew,
                    "potential_crew_members": potential_crew_members,
                },
            )
        case "POST":
            try:
                permanent_crew = request.POST.get("permanent_crew") == "permanent_crew"
            except KeyError:
                return HttpResponseBadRequest("Missing permanent_crew parameter")
            try:
                crew_member_id = request.POST["crew_member_id"]
            except KeyError:
                return HttpResponseBadRequest("Missing crew_member_id parameter")

            if permanent_crew:
                line.crew_member = None
                line.permanent_crew_member = get_object_or_404(
                    PermanentCrewMember, pk=crew_member_id
                )
                line.permanent_crew = True
            else:
                line.crew_member = get_object_or_404(
                    Volunteer.objects.exclude(sarprofile=None), pk=crew_member_id
                )
                line.permanent_crew_member = None
                line.permanent_crew = False
            line.save()
            return redirect(line.operation.get_absolute_url())
        case method:
            assert_never(method)  # type: ignore
