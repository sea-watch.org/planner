from django import forms


class SelectDateWidget(forms.SelectDateWidget):
    template_name = "public/forms/widgets/select_date.html"
