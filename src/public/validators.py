import datetime
import os
import platform

from django import forms
from django.core.files import File

if platform.system() == "Darwin" and os.environ.get("LD_LIBRARY_PATH"):
    if os.environ.get("DYLD_LIBRARY_PATH") is not None:
        raise Exception(
            "DYLD_LIBRARY_PATH is already defined but we need to set it here!"
        )
    os.environ["DYLD_LIBRARY_PATH"] = os.environ["LD_LIBRARY_PATH"]
    import magic

    del os.environ["DYLD_LIBRARY_PATH"]
else:
    import magic


def validate_date_in_future(value):
    if value is None:
        return
    if value < datetime.date.today():
        raise forms.ValidationError(
            "The date you entered is in the past. Please only upload certificates that are still valid."
        )


max_file_size_bytes = 10 * 1024 * 1024
max_file_size_bytes_message = "Please upload a file below 10MB in size."


def validate_file_size(value: File | None):
    if value is None:
        return
    if value.size > max_file_size_bytes:
        raise forms.ValidationError(max_file_size_bytes_message)


required_mime_type = "application/pdf"
required_mime_type_message = "Please upload a file in PDF format."


def validate_file_type(value: File | None):
    if value is None:
        return
    value.seek(0)
    mimetype = magic.from_buffer(value.read(), mime=True)
    if mimetype != required_mime_type:
        raise forms.ValidationError(required_mime_type_message)


invalid_phone_number_message = "Please enter a valid phone number."
