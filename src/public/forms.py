from collections.abc import Mapping
from planner_common.feature_flags import features
from typing import Any

from django import forms

from volunteers.models import Document

from planner_common.forms.widgets import LanguagesWidget
from planner_common.models import Gender

from .validators import validate_date_in_future, validate_file_size, validate_file_type
from .widgets import SelectDateWidget


class SecurityError(Exception):
    pass


class ClamdUnexpectedOutput(Exception):
    pass


class RankRelatedDocumentsForm(forms.Form):
    basic_safety_certificate = forms.FileField(
        widget=forms.FileInput(attrs={"accept": "application/pdf"}),
        validators=[validate_file_size, validate_file_type],
    )
    basic_safety_certificate_validity_date = forms.DateField(
        widget=SelectDateWidget(),
        validators=[validate_date_in_future],
    )

    medical_fitness_certificate = forms.FileField(
        widget=forms.FileInput(attrs={"accept": "application/pdf"}),
        validators=[validate_file_size, validate_file_type],
    )
    medical_fitness_certificate_validity_date = forms.DateField(
        widget=SelectDateWidget(),
        validators=[validate_date_in_future],
    )

    watchkeeping_license_certificate = forms.FileField(
        widget=forms.FileInput(attrs={"accept": "application/pdf"}),
        required=False,
        validators=[validate_file_size, validate_file_type],
    )
    watchkeeping_license_certificate_validity_date = forms.DateField(
        widget=SelectDateWidget(),
        required=False,
        validators=[validate_date_in_future],
    )

    required_css_class = "required"

    @property
    def template_name(self):
        if features["check_if_document_is_present"]:
            return "public/inquiries/forms/rank_related_template_new.html"
        else:
            return "public/inquiries/forms/rank_related_template.html"

    def __init__(
        self,
        *args,
        basic_cert: Document | None,
        medic_cert: Document | None,
        watchkeeping_cert: Document | None,
        **kwargs,
    ):
        self.basic_cert = basic_cert
        self.medic_cert = medic_cert
        self.watchkeeping_cert = watchkeeping_cert
        super().__init__(*args, **kwargs)

    def get_context(self) -> Mapping[str, Any]:
        # This works but the type definitions disagree
        ctx = super().get_context()  # type: ignore
        return {
            **ctx,
            "basic_cert": self.basic_cert,
            "medic_cert": self.medic_cert,
            "watchkeeping_cert": self.watchkeeping_cert,
        }

    def clean(self) -> dict[str, Any]:
        result = super().clean()
        self._check_watchkeping_both_or_none(result)
        return result

    def _check_watchkeping_both_or_none(self, result):
        # sentinel value to distinguish between error in validation and field not provided
        invalid = object()

        file = result.get("watchkeeping_license_certificate", invalid)
        date = result.get("watchkeeping_license_certificate_validity_date", invalid)

        match (file, date):
            case (None, None):
                pass  # OK this is fine
            case (None, _):
                raise forms.ValidationError(
                    {
                        "watchkeeping_license_certificate": [
                            "Please upload your watchkeeping certificate."
                        ]
                    }
                )
            case (_, None):
                raise forms.ValidationError(
                    {
                        "watchkeeping_license_certificate_validity_date": [
                            "Please fill the validity date for your watchkeeping certificate."
                        ]
                    }
                )
            case (_, _):
                pass  # OK nothing to do here


class NationalitiesWidget(forms.SelectMultiple):
    pass


class Select(forms.Select):
    template_name = "public.forms.html.select"


class ApplicationForm(forms.Form):
    template_name = "public/applications/application_form.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.label_suffix = ""

    first_name = forms.CharField(
        required=True,
        label="First Name",
        widget=forms.TextInput(attrs={"placeholder": "First Name"}),
    )
    last_name = forms.CharField(
        required=True,
        label="Last Name",
        widget=forms.TextInput(attrs={"placeholder": "Last Name"}),
    )
    phone_number = forms.CharField(
        required=True,
        label="Phone Number",
        widget=forms.TextInput(attrs={"placeholder": "+49 30 23125 555"}),
        help_text="Please include the international prefix (e.g. +49)",
    )
    email = forms.EmailField(
        required=True,
        label="Email Address",
        widget=forms.TextInput(
            attrs={"placeholder": "you_are_awesome@example.com", "type": "email"}
        ),
    )
    date_of_birth = forms.DateField(
        required=True,
        label="Date of Birth",
        widget=forms.TextInput(attrs={"type": "date"}),
    )
    gender = forms.ChoiceField(
        required=True,
        label="Gender",
        widget=Select,
        choices=[("", "Please select"), *Gender.choices],
    )
    nationalities = forms.MultipleChoiceField(
        required=True,
        label="Nationality",
        help_text="Please select all your nationalities",
        widget=NationalitiesWidget,
    )
    spoken_languages = forms.JSONField(
        required=True, label="Spoken Languages", widget=LanguagesWidget
    )
