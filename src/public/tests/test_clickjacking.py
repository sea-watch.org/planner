from django.http import HttpResponse
from django.test import RequestFactory

from ..clickjacking import ContentSecurityPolicyMiddleware

###################################
# ContentSecurityPolicyMiddleware #
###################################


def test_ContentSecurityPolicyMiddleware_sets_header(rf: RequestFactory):
    r = HttpResponse()
    response = ContentSecurityPolicyMiddleware(lambda _: r)(rf.get(""))
    assert response == r
    assert response.get("Content-Security-Policy") == "frame-ancestors 'none';"


def test_ContentSecurityPolicyMiddleware_raises_on_existing_header(rf: RequestFactory):
    r = HttpResponse()
    r["Content-Security-Policy"] = "asdf"
    response = ContentSecurityPolicyMiddleware(lambda _: r)(rf.get(""))
    assert response["Content-Security-Policy"] == "asdf"
