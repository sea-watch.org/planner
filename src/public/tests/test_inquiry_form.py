import json
from datetime import date

import pytest
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.test import RequestFactory
from freezegun import freeze_time
from operations.tests.factories import OperationFactory
from pytest_django.asserts import assertContains
from seawatch_registration.tests.util import htmlnormalize_text
from volunteers.tests.factories import VolunteerFactory

from public.models import Inquiry

from ..views import inquiry_form
from .factories import AvailabilityInquiryFactory


@pytest.mark.django_db
def test_renders_availability_form(rf: RequestFactory):
    operation = OperationFactory.create(
        start_date=date(2021, 8, 9),
        end_date=date(2021, 12, 3),
    )
    inquiry = Inquiry.objects.create(
        type=Inquiry.Type.AVAILABILITY,
        volunteer=VolunteerFactory.create(first_name="Cooper"),
    )
    with freeze_time(date(2020, 1, 1)):
        response = inquiry_form(rf.get(""), uid=inquiry.uid)
    html = BeautifulSoup(response.content, features="html.parser")
    form = html.find("availability-form")
    assert isinstance(form, Tag)
    operations_json = form["operations"]
    assert isinstance(operations_json, str)
    assert json.loads(operations_json) == [
        {
            "previous_answer": None,
            "start_date": "2021-08-09",
            "end_date": "2021-12-03",
            "operation_id": operation.id,
        }
    ]
    assert form["name"] == "Cooper"
    assert form["token"] == str(inquiry.uid)


@pytest.mark.django_db
def test_does_not_render_past_operations(rf: RequestFactory):
    OperationFactory.create(start_date=date(2010, 1, 1))
    OperationFactory.create(start_date=date(2030, 1, 1))
    with freeze_time(date(2020, 1, 1)):
        response = inquiry_form(rf.get(""), uid=AvailabilityInquiryFactory.create().uid)
    html = BeautifulSoup(response.content, features="html.parser")
    operations = json.loads(html.find("availability-form")["operations"])  # type: ignore
    assert [o["start_date"] for o in operations] == [
        # does _not_ include the earlier one
        "2030-01-01",
    ]


@pytest.mark.django_db
def test_renders_availability_form_with_correct_title(rf: RequestFactory):
    inquiry = AvailabilityInquiryFactory.create()
    response = inquiry_form(rf.get(""), uid=inquiry.uid)
    html = BeautifulSoup(response.content, features="html.parser")
    title = html.find("title")
    assert title
    assert htmlnormalize_text(title.getText()).startswith("Update Availabilities")


@pytest.mark.django_db
def test_shows_data_protection_note(rf: RequestFactory):
    inquiry = AvailabilityInquiryFactory.create()
    response = inquiry_form(rf.get(".."), uid=inquiry.uid)
    assertContains(response, "gemäß Art. 13 DSGVO")


@pytest.mark.django_db
def test_throws_error_when_wrong_inquiry_type(rf: RequestFactory):
    inquiry = AvailabilityInquiryFactory.create()
    inquiry.type = "wrong"
    inquiry.save()
    with pytest.raises(ValueError):
        inquiry_form(rf.get(".."), uid=inquiry.uid)
