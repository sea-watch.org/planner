from datetime import timezone
from typing import Callable, ClassVar

from factory import Faker, SubFactory  # type: ignore
from factory.django import DjangoModelFactory

from planner_common.test.factories import (
    DeprecateDirectCallMetaclass,
)
from volunteers.tests.factories import VolunteerFactory

from ..models import Inquiry


class UnassociatedUploadedFileFactory:
    pass


class InquiryFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Inquiry
        skip_postgeneration_save = True

    create: ClassVar[Callable[..., Inquiry]]
    create_batch: ClassVar[Callable[..., list[Inquiry]]]
    build: ClassVar[Callable[..., Inquiry]]
    build_batch: ClassVar[Callable[..., list[Inquiry]]]

    uid = Faker("uuid4", cast_to=None)
    created = Faker("past_datetime", tzinfo=timezone.utc)

    volunteer = SubFactory(VolunteerFactory)

    type = Faker("random_element", elements=Inquiry.Type)


class RankRelatedDocumentsInquiryFactory(InquiryFactory):
    type = Inquiry.Type.RANK_RELATED_DOCUMENTS


class AvailabilityInquiryFactory(InquiryFactory):
    type = Inquiry.Type.AVAILABILITY
