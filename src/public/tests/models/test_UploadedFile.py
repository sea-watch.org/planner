from datetime import datetime, timezone

from freezegun import freeze_time

from ...models import UnassociatedUploadedFile


def creates_with_created_at_current_time():
    a_summer_day = datetime(2284, 6, 10, 15, 45, tzinfo=timezone.utc)
    with freeze_time(a_summer_day):
        f = UnassociatedUploadedFile.objects.create()

    assert f.created_at == a_summer_day
