from planner_common import schema


def test_schema_defines_denial_of_all_possible_addition_properties():
    assert schema.initial_application["additionalProperties"] is False

    # same check recursively
    def recursively_check(node):
        if node["type"] == "array" and "items" in node:
            recursively_check(node["items"])
        elif node["type"] == "object":
            assert node["additionalProperties"] is False
            for prop in node["properties"].values():
                recursively_check(prop)

    recursively_check(schema.initial_application)
