from bs4 import BeautifulSoup

from ..widgets import SelectDateWidget


def test_select_date_widget():
    widget = SelectDateWidget()
    html_str = widget.render(name="foo", value=None)
    html = BeautifulSoup(html_str, features="html.parser")
    assert html.find(class_="planner-date-select")
