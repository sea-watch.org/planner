from urllib.parse import urlparse, urlunparse

from pytest import fixture

from seawatch_planner.tests.site import site


@fixture(autouse=True)
def public_site():
    with site("public"):
        yield


@fixture
def public_url(live_server):
    parts = urlparse(live_server.url)
    assert (
        parts.netloc.split(":")[0] == "localhost"
    ), "this code expects the live_server to run on localhost!"
    return urlunparse(
        parts._replace(netloc=f"public.{parts.netloc}"),
    )
