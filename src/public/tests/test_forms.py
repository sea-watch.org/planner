import datetime

from django import forms
from django.core.files.uploadedfile import SimpleUploadedFile
from freezegun import freeze_time
from pytest import mark

from ..forms import RankRelatedDocumentsForm


def helper_create_RankRelatedDocumentsForm(
    minimal_pdf: bytes,
    basic_safety_file=None,
    basic_safety_validity_date=None,
    medical_fitness_file=None,
    medical_fitness_validity_date=None,
    with_watchkeeping=False,
    watchkeeping_license_file=None,
    watchkeeping_license_validity_date=None,
):
    if basic_safety_file is None:
        basic_safety_file = SimpleUploadedFile("test_basic_safety.pdf", minimal_pdf)
    if basic_safety_validity_date is None:
        basic_safety_validity_date = datetime.datetime(2323, 1, 1)
    if medical_fitness_file is None:
        medical_fitness_file = SimpleUploadedFile(
            "test_medical_fitness.pdf", minimal_pdf
        )
    if medical_fitness_validity_date is None:
        medical_fitness_validity_date = datetime.datetime(2334, 2, 2)
    if with_watchkeeping:
        if watchkeeping_license_file is None:
            watchkeeping_license_file = SimpleUploadedFile(
                "test_watchkeeping.pdf", minimal_pdf
            )
        if watchkeeping_license_validity_date is None:
            watchkeeping_license_validity_date = datetime.datetime(2325, 3, 3)

    form = RankRelatedDocumentsForm(
        basic_cert=None,
        medic_cert=None,
        watchkeeping_cert=None,
        data={
            "basic_safety_certificate_validity_date_day": basic_safety_validity_date.day,
            "basic_safety_certificate_validity_date_month": basic_safety_validity_date.month,
            "basic_safety_certificate_validity_date_year": basic_safety_validity_date.year,
            "medical_fitness_certificate_validity_date_day": medical_fitness_validity_date.day,
            "medical_fitness_certificate_validity_date_month": medical_fitness_validity_date.month,
            "medical_fitness_certificate_validity_date_year": medical_fitness_validity_date.year,
            **(
                {
                    "watchkeeping_license_certificate_validity_date_day": watchkeeping_license_validity_date.day,
                    "watchkeeping_license_certificate_validity_date_month": watchkeeping_license_validity_date.month,
                    "watchkeeping_license_certificate_validity_date_year": watchkeeping_license_validity_date.year,
                }
                if watchkeeping_license_validity_date
                else {}
            ),
        },
        files={
            "basic_safety_certificate": basic_safety_file,
            "medical_fitness_certificate": medical_fitness_file,
            **(
                {"watchkeeping_license_certificate": watchkeeping_license_file}
                if watchkeeping_license_file
                else {}
            ),
        },
    )
    return form


#######################################################
### VALIDATE WATCHKEEPING AND VALIDITY DATE OR NONE ###
#######################################################


@mark.parametrize(
    ["with_file", "with_validity_date", "expected_errors"],
    [
        (False, False, {}),
        (True, True, {}),
        (
            True,
            False,
            {
                "watchkeeping_license_certificate_validity_date": [
                    "Please fill the validity date for your watchkeeping certificate."
                ],
            },
        ),
        (
            False,
            True,
            {
                "watchkeeping_license_certificate": [
                    "Please upload your watchkeeping certificate."
                ],
            },
        ),
    ],
)
@freeze_time("2023-01-01")
@mark.django_db
def test_RankRelatedDocumentsForm_shows_error_if_watchkeeping_file_xor_validity_date(
    with_file: bool, with_validity_date: bool, expected_errors, minimal_pdf: bytes
):
    watchkeeping_license_file = SimpleUploadedFile("test_watchkeeping.pdf", minimal_pdf)
    watchkeeping_license_validity_date = datetime.datetime(2325, 3, 3)

    args = {
        **(
            {"watchkeeping_license_file": watchkeeping_license_file}
            if with_file
            else {}
        ),
        **(
            {"watchkeeping_license_validity_date": watchkeeping_license_validity_date}
            if with_validity_date
            else {}
        ),
    }
    form = helper_create_RankRelatedDocumentsForm(minimal_pdf=minimal_pdf, **args)

    assert form.errors == expected_errors


test_data = b"." * 12 * 1024 * 1024


@mark.django_db
def test_RankRelatedDocumentsForm_validates_files(minimal_pdf: bytes):
    form = helper_create_RankRelatedDocumentsForm(
        minimal_pdf=minimal_pdf,
        basic_safety_file=SimpleUploadedFile("test_watchkeeping.pdf", test_data),
        medical_fitness_file=SimpleUploadedFile("test_watchkeeping.pdf", test_data),
        watchkeeping_license_file=SimpleUploadedFile(
            "test_watchkeeping.pdf", test_data
        ),
        watchkeeping_license_validity_date=datetime.date(2300, 1, 1),
    )

    expected_errors = {
        "Please upload a file in PDF format.",
        "Please upload a file below 10MB in size.",
    }
    assert {field: set(errs) for field, errs in form.errors.items()} == {
        "basic_safety_certificate": expected_errors,
        "medical_fitness_certificate": expected_errors,
        "watchkeeping_license_certificate": expected_errors,
    }


@mark.django_db
def test_RankRelatedDocumentsForm_validates_dates(minimal_pdf: bytes):
    def always_fail(_):
        raise forms.ValidationError("test failure")

    form = helper_create_RankRelatedDocumentsForm(
        minimal_pdf=minimal_pdf,
        basic_safety_validity_date=datetime.datetime(2002, 3, 3),
        medical_fitness_validity_date=datetime.datetime(2003, 3, 3),
        with_watchkeeping=True,
        watchkeeping_license_validity_date=datetime.datetime(2004, 3, 3),
    )

    expected_errors = [
        "The date you entered is in the past. Please only upload certificates that are still valid."
    ]
    assert form.errors == {
        "basic_safety_certificate_validity_date": expected_errors,
        "medical_fitness_certificate_validity_date": expected_errors,
        "watchkeeping_license_certificate_validity_date": expected_errors,
    }
