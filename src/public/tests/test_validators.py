import datetime
import io
from pathlib import Path

import pytest
from django.core.files.uploadedfile import SimpleUploadedFile
from django.forms import forms
from freezegun import freeze_time

from ..validators import validate_date_in_future, validate_file_size, validate_file_type

#######################################################
### VALIDATE FILE SECURITY (SIZE, EXTENSION, VIRUS) ###
#######################################################


def test_validate_file_size_rejects_files_over_10MB():
    big_file = SimpleUploadedFile("11mb_basic_safety.pdf", 11 * 1024 * 1024 * b"x")
    with pytest.raises(forms.ValidationError) as exc_info:
        validate_file_size(big_file)
    assert exc_info.value.args[0] == "Please upload a file below 10MB in size."


def test_validate_file_size_accepts_files_under_10MB():
    small_file = SimpleUploadedFile("11mb_medical_fitness.pdf", 1 * 1024 * 1024 * b"x")
    try:
        validate_file_size(small_file)
    except forms.ValidationError as error:
        assert not error


def test_validate_file_type_accepts_a_pdf():
    pdf_bytes = (
        Path(__file__).parent.parent.parent.parent / "test-data/minimal.pdf"
    ).read_bytes()
    pdf_file = SimpleUploadedFile("example_BS.pdf", pdf_bytes)
    try:
        validate_file_type(pdf_file)
    except forms.ValidationError as error:
        assert not error


def test_validate_file_type_rejects_files_not_pdf():
    not_pdf_file = SimpleUploadedFile("medical_fitness_NOT_PDF.pdf", b"...")
    with pytest.raises(forms.ValidationError) as exc_info:
        validate_file_type(not_pdf_file)
    assert exc_info.value.args[0] == "Please upload a file in PDF format."


def test_validate_file_type_can_handle_already_read_file():
    pdf_bytes = (
        Path(__file__).parent.parent.parent.parent / "test-data/minimal.pdf"
    ).read_bytes()
    read_pdf_file = SimpleUploadedFile("example_BS.pdf", pdf_bytes)
    read_pdf_file.seek(0, io.SEEK_END)
    try:
        validate_file_type(read_pdf_file)
    except forms.ValidationError as error:
        assert not error


#################################################
### VALIDATE VALIDITY DATES ARE IN THE FUTURE ###
#################################################


@freeze_time("2023-01-01")
def test_validate_date_in_future_throws_for_past_date():
    date = datetime.date(2001, 2, 2)

    with pytest.raises(forms.ValidationError) as exc_info:
        validate_date_in_future(date)
    assert (
        exc_info.value.args[0]
        == "The date you entered is in the past. Please only upload certificates that are still valid."
    )


@freeze_time("2023-01-01")
def test_RankRelatedDocumentsForm_doesnt_throw_for_future_date():
    date = datetime.date(2023, 2, 2)

    try:
        validate_date_in_future(date)
    except forms.ValidationError as e:
        assert e is None
