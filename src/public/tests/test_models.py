from pytest import mark

from .factories import InquiryFactory


@mark.django_db
def test_Inquiry_form_url():
    uid = "c7a8c71f-83f9-4551-a0c3-53421fb7c3ee"
    assert (
        InquiryFactory.create(uid=uid).form_url() == f"/inquiries/inquiry_form/{uid}/"
    )
