import pytest
from datetime import date, timedelta
from playwright.sync_api import Page, expect
from planner_common import feature_flags
from public.tests.factories import RankRelatedDocumentsInquiryFactory
from volunteers.models import Document
from volunteers.tests.factories import DocumentFactory, VolunteerFactory

CERT_ALREADY_PRESENT = "Cert already present"
UPLOAD_ANOTHER_CERT = "Upload another Certificate"
KEEP_CURRENT_VERSION = "Keep Current Version"


@pytest.fixture
def volunteer():
    return VolunteerFactory.create()


@pytest.fixture
def inquiry(volunteer):
    return RankRelatedDocumentsInquiryFactory.create(volunteer=volunteer)


@pytest.mark.django_db
@pytest.mark.skipif(
    not feature_flags.features["check_if_document_is_present"],
    reason="needs feature enabled",
)
@pytest.mark.parametrize("cert_exists", [True, False])
@pytest.mark.parametrize(
    "cert_info",
    [
        (
            Document.Type.stcw_watchkeeping,
            "Watchkeeping License",
            "Watchkeeping license certificate:",
        ),
        (
            Document.Type.stcw_basic_safety,
            "Basic Safety Certificate",
            "Basic safety certificate:",
        ),
        (
            Document.Type.medical_fitness,
            "Medical Fitness Certificate",
            "Medical fitness certificate:",
        ),
    ],
)
def test_document_upload_toggle(
    cert_exists, cert_info, volunteer, inquiry, page: Page, public_url: str
):
    cert_type, group_name, input_label = cert_info

    if cert_exists:
        DocumentFactory.create(
            volunteer=volunteer,
            type=cert_type,
            valid_until=date.today() + timedelta(days=10),
        )

    page.goto(f"{public_url}{inquiry.form_url()}")
    group = page.get_by_role("group", name=group_name)

    def check_visibility(selector, should_be_visible):
        element = group.get_by_text(selector)
        expect(element).to_be_visible() if should_be_visible else expect(
            element
        ).not_to_be_visible()

    def check_cert_upload_panel_visibility(should_be_visible: bool):
        should_be_visible = not should_be_visible
        check_visibility(CERT_ALREADY_PRESENT, should_be_visible)
        check_visibility(UPLOAD_ANOTHER_CERT, should_be_visible)
        check_visibility(input_label, not should_be_visible)
        if cert_exists:
            check_visibility(KEEP_CURRENT_VERSION, not should_be_visible)

    check_cert_upload_panel_visibility(not cert_exists)
    if cert_exists:
        group.get_by_text(UPLOAD_ANOTHER_CERT).click()
        check_cert_upload_panel_visibility(True)
        group.get_by_text(KEEP_CURRENT_VERSION).click()
        check_cert_upload_panel_visibility(False)


@pytest.mark.skipif(
    not feature_flags.features["check_if_document_is_present"],
    reason="needs feature enabled",
)
@pytest.mark.django_db
@pytest.mark.parametrize(
    "date_offset",
    [
        timedelta(days=10),  # Date in the future
        timedelta(days=-10),  # Date in the past
    ],
)
def test_multiple_cert_validity_dates_shown(
    date_offset: timedelta, volunteer, inquiry, page: Page, public_url: str
):
    current_date = date.today()

    valid_until = current_date + date_offset

    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.stcw_watchkeeping,
        valid_until=valid_until,
    )
    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.stcw_watchkeeping,
        valid_until=valid_until - timedelta(days=1),
    )
    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.stcw_basic_safety,
        valid_until=valid_until,
    )
    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.stcw_basic_safety,
        valid_until=valid_until - timedelta(days=1),
    )
    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.medical_fitness,
        valid_until=valid_until,
    )
    DocumentFactory.create(
        volunteer=volunteer,
        type=Document.Type.medical_fitness,
        valid_until=valid_until - timedelta(days=1),
    )
    page.goto(f"{public_url}{inquiry.form_url()}")

    watchkeeping_group = page.get_by_role("group", name="Watchkeeping License")
    print(watchkeeping_group.text_content())

    def check_date_visibility(group_name, should_be_visible):
        element = page.get_by_role("group", name=group_name)
        if should_be_visible:
            expect(element).to_contain_text(
                f"valid until {valid_until.strftime('%Y-%m-%d')}"
            )
        else:
            expect(element.get_by_text("valid until")).not_to_be_visible()

    if date_offset.days < 0:
        check_date_visibility("Watchkeeping License", False)
        check_date_visibility("Basic Safety Certificate", False)
        check_date_visibility("Medical Fitness Certificate", False)
    else:
        check_date_visibility("Watchkeeping License", True)
        check_date_visibility("Basic Safety Certificate", True)
        check_date_visibility("Medical Fitness Certificate", True)
