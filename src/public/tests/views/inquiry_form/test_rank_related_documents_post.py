import io
import os
from datetime import date
from http import HTTPStatus
from pathlib import Path
from typing import Literal
from unittest import mock

from django.http import HttpResponseRedirect
import pytest
from bs4 import BeautifulSoup
from django.conf import settings
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.handlers.wsgi import WSGIRequest
from django.test import RequestFactory, override_settings
from django.urls import reverse
from freezegun import freeze_time
from pytest import mark
from pytest_subtests import SubTests

from public.forms import ClamdUnexpectedOutput, SecurityError
from volunteers.models import Document
from volunteers.tests.factories import VolunteerFactory

from .... import views
from ....models import Inquiry
from ....views import inquiry_form
from ...factories import RankRelatedDocumentsInquiryFactory


@pytest.mark.django_db
def test_renders_rank_related_documents_form_with_correct_name(rf: RequestFactory):
    inquiry = Inquiry.objects.create(
        type=Inquiry.Type.RANK_RELATED_DOCUMENTS,
        volunteer=VolunteerFactory.create(first_name="Bob"),
    )

    response = inquiry_form(rf.get(""), uid=inquiry.uid)
    html = BeautifulSoup(response.content, features="html.parser")

    assert "Bob" in html.get_text()


def build_valid_rank_related_document_form_POST(
    rf: RequestFactory,
    *,
    volunteer=None,
    basic_safety=None,
    medical_fitness=None,
    watchkeeping_license=None,
    with_watchkeeping: bool = False,
) -> tuple[Inquiry, WSGIRequest]:
    if volunteer is None:
        volunteer = VolunteerFactory.create()
    inquiry = RankRelatedDocumentsInquiryFactory.create(volunteer=volunteer)

    # create form data fake request
    form_data_to_submit = {
        "basic_safety_certificate_validity_date_day": "1",
        "basic_safety_certificate_validity_date_month": "1",
        "basic_safety_certificate_validity_date_year": "2073",
        "medical_fitness_certificate_validity_date_day": "2",
        "medical_fitness_certificate_validity_date_month": "2",
        "medical_fitness_certificate_validity_date_year": "2084",
        **(
            {
                "watchkeeping_license_certificate_validity_date_day": "3",
                "watchkeeping_license_certificate_validity_date_month": "3",
                "watchkeeping_license_certificate_validity_date_year": "2095",
            }
            if with_watchkeeping
            else {}
        ),
    }

    request = rf.post(
        "",
        form_data_to_submit,
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )

    if not basic_safety:
        basic_safety_bytes = (
            Path(__file__).parent.parent.parent / "example_BS.pdf"
        ).read_bytes()
        basic_safety = SimpleUploadedFile("example_BS.pdf", basic_safety_bytes)
    if not medical_fitness:
        medical_fitness_bytes = (
            Path(__file__).parent.parent.parent / "example_MF.pdf"
        ).read_bytes()
        medical_fitness = SimpleUploadedFile("example_MF.pdf", medical_fitness_bytes)
    request.FILES["basic_safety_certificate"] = basic_safety
    request.FILES["medical_fitness_certificate"] = medical_fitness
    if with_watchkeeping:
        if not watchkeeping_license:
            watchkeeping_license_bytes = (
                Path(__file__).parent.parent.parent / "example_WK.pdf"
            ).read_bytes()
            watchkeeping_license = SimpleUploadedFile(
                "example_WK.pdf", watchkeeping_license_bytes
            )
        request.FILES["watchkeeping_license_certificate"] = watchkeeping_license

    return (inquiry, request)


@freeze_time("2020-01-01")
@mark.django_db
@override_settings(CLAMD_URL="testing-nullclamd")
def test_submitting_rank_related_documents_redirects_to_thank_you_page(
    rf: RequestFactory,
):
    inquiry, request = build_valid_rank_related_document_form_POST(rf)
    response = views.inquiry_form(request, inquiry.uid)

    assert response.status_code // 100 == 3, "expected redirect"
    assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
        "inquiry_success_rank_related_documents"
    )


@freeze_time("2020-01-01")
@mark.django_db
@override_settings(CLAMD_URL="testing-nullclamd")
@mark.parametrize(
    "file_with_malware", ["basic_safety", "medical_fitness", "watchkeeping_license"]
)
def test_submitting_form_with_malware_file_returns_thank_you_page(
    file_with_malware: Literal[
        "basic_safety", "medical_fitness", "watchkeeping_license"
    ],
    rf: RequestFactory,
    subtests: SubTests,
    mailoutbox: list[mail.EmailMessage],
):
    # TODO: This test tests a lot of things, if we refactor the production code we can tease
    # this apart as well
    clamd_mock = mock.Mock()

    def instream(f):
        assert (
            f.tell() == 0
        ), "expected file to be at the start! If this is missed, malware detection might not work"
        return {
            "stream": (
                ("FOUND", "unittest-malware")
                if b"malware" in f.read()
                else ("OK", None)
            )
        }

    clamd_mock.instream = instream

    volunteer = VolunteerFactory.create(id=1298319)
    file = SimpleUploadedFile(
        "malware.pdf",
        Path(settings.PROJECT_DIR + "test-data/minimal.pdf").read_bytes() + b"malware",
    )
    file.seek(0, io.SEEK_END)
    inquiry, request = build_valid_rank_related_document_form_POST(
        rf,
        volunteer=volunteer,
        **{file_with_malware: file},
        with_watchkeeping="watchkeeping" in file_with_malware,
    )
    try:
        with override_settings(ADMINS=[("Admin", "admin@example.com")]):
            response = views.inquiry_form(
                request,
                inquiry.uid,
                get_clamd=lambda: clamd_mock,
            )
    except SecurityError:
        pytest.fail("expected security error to be handled")

    # Same as for a successful save, the user shouldn't see any difference
    with subtests.test("redirects as normal"):
        assert response.status_code // 100 == 3, "expected redirect"
        assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
            "inquiry_success_rank_related_documents"
        )

    with subtests.test("sends a notification by email"):
        (mail,) = [m for m in mailoutbox if m.recipients() == ["admin@example.com"]]
        assert "unittest-malware" in mail.body
        assert "Rank Related Documents" in mail.body

        assert (
            date(
                int(
                    request.POST[f"{file_with_malware}_certificate_validity_date_year"]
                ),
                int(
                    request.POST[f"{file_with_malware}_certificate_validity_date_month"]
                ),
                int(request.POST[f"{file_with_malware}_certificate_validity_date_day"]),
            ).isoformat()
            in mail.body
        )
        assert str(volunteer.id) in mail.body
        assert f"in {file_with_malware}" in mail.body

    with subtests.test("deletes the inquiry"):
        assert not Inquiry.objects.filter(id=inquiry.id).exists()

    with subtests.test("does not create any documents"):
        assert not volunteer.document_set.exists()


@mark.django_db
@mark.parametrize("instream_result", [None, "yo"])
def test_raises_on_unexpected_clamd_output(instream_result, rf: RequestFactory):
    clamd_mock = mock.Mock()
    clamd_mock.instream = lambda f: instream_result

    volunteer = VolunteerFactory.create(id=1298319)
    inquiry, request = build_valid_rank_related_document_form_POST(
        rf, volunteer=volunteer
    )
    with pytest.raises(ClamdUnexpectedOutput):
        views.inquiry_form(
            request,
            inquiry.uid,
            get_clamd=lambda: clamd_mock,
        )


@mark.django_db
@mark.parametrize("with_watchkeeping", [True, False])
def test_submitting(with_watchkeeping: bool, rf: RequestFactory, subtests: SubTests):
    # Prepare data
    volunteer = VolunteerFactory.create()
    inquiry = RankRelatedDocumentsInquiryFactory.create(volunteer=volunteer)

    # Prepare request
    request = rf.post(
        "",
        {
            "basic_safety_certificate_validity_date_day": "1",
            "basic_safety_certificate_validity_date_month": "1",
            "basic_safety_certificate_validity_date_year": "2023",
            "medical_fitness_certificate_validity_date_day": "2",
            "medical_fitness_certificate_validity_date_month": "2",
            "medical_fitness_certificate_validity_date_year": "2024",
            **(
                {
                    "watchkeeping_license_certificate_validity_date_day": "3",
                    "watchkeeping_license_certificate_validity_date_month": "3",
                    "watchkeeping_license_certificate_validity_date_year": "2025",
                }
                if with_watchkeeping
                else {}
            ),
        },
    )
    pdfdir = Path(__file__).parent.parent.parent
    file_bytes = {
        "basic_safety_certificate": (pdfdir / "example_BS.pdf").read_bytes(),
        "medical_fitness_certificate": (pdfdir / "example_MF.pdf").read_bytes(),
        **(
            {
                "watchkeeping_license_certificate": (
                    pdfdir / "example_WK.pdf"
                ).read_bytes()
            }
            if with_watchkeeping
            else {}
        ),
    }
    files = {
        name: SimpleUploadedFile(f"{name}.pdf", content)
        for name, content in file_bytes.items()
    }
    for name, file in files.items():
        request.FILES[name] = file

    # Call the view
    with freeze_time(date(2020, 1, 1)):
        response = views.inquiry_form(request, inquiry.uid)

    with subtests.test("redirects to success page"):
        assert response.status_code // 100 == 3, "expected redirect"
        assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
            "inquiry_success_rank_related_documents"
        )

    with subtests.test("saves the documents"):

        def doc_content(doc: Document):
            with doc.file.open("rb") as f:
                return f.read()

        assert {
            (Document.Type(doc.type), doc_content(doc), doc.valid_until)
            for doc in volunteer.document_set.all()
        } == {
            (
                Document.Type.stcw_basic_safety,
                file_bytes["basic_safety_certificate"],
                date(2023, 1, 1),
            ),
            (
                Document.Type.medical_fitness,
                file_bytes["medical_fitness_certificate"],
                date(2024, 2, 2),
            ),
            *(
                {
                    (
                        Document.Type.stcw_watchkeeping,
                        file_bytes["watchkeeping_license_certificate"],
                        date(2025, 3, 3),
                    )
                }
                if with_watchkeeping
                else set()
            ),
        }

    with subtests.test("saves the documents under the correct filename"):
        for type, filename_part in [
            (Document.Type.stcw_basic_safety, "stcw_basic_safety"),
            (Document.Type.medical_fitness, "medical_fitness"),
            *(
                [(Document.Type.stcw_watchkeeping, "stcw_watchkeeping")]
                if with_watchkeeping
                else []
            ),
        ]:
            basename = os.path.basename(volunteer.document_set.get(type=type).file.name)
            expected = f"{volunteer.first_name}_{volunteer.last_name}_{filename_part}_".replace(
                " ", "_"
            )
            assert basename.startswith(expected)


# test email is sent to volunteer after we save his response for the RRD form
@freeze_time("2023-01-01")
@mark.django_db
def test_inquiry_form_view_sends_email_after_submitting_rank_related_documents(
    rf: RequestFactory, tmp_path: Path
):
    inquiry, request = build_valid_rank_related_document_form_POST(
        rf, with_watchkeeping=True
    )

    with override_settings(MEDIA_URL="http://test.media", MEDIA_ROOT=str(tmp_path)):
        response = views.inquiry_form(request, inquiry.uid)
    assert (
        response.status_code == HTTPStatus.FOUND
    ), "expected form to be valid, test is probably broken"

    assert response.status_code == 302

    assert len(mail.outbox) == 1
    email = mail.outbox[0]
    assert email.to == [inquiry.volunteer.email]
    assert email.subject == "Document upload confirmed"
    assert "thank you" in email.body
    assert "http://" not in email.body

    # rrd specific asserts: documents just by file name? and validity date
    assert "Basic Safety Certificate" in email.body
    assert "2073-01-01" in email.body
    assert "Medical Fitness Certificate" in email.body
    assert "2084-02-02" in email.body
    assert "Watchkeeping License" in email.body
    assert "2095-03-03" in email.body
