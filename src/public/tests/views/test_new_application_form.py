# Commented out because it has not been adjusted to the latest refactors


# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_renders_form(rf: RequestFactory):
#     response = new_application_form(rf.get(""))
#     assert response.status_code == HTTPStatus.OK
#     html = BeautifulSoup(
#         cast(TemplateResponse, response).rendered_content, features="html.parser"
#     )
#     assert html.select_one("input[name=first_name]")
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_rerenders_page_on_form_error(rf: RequestFactory):
#     response = new_application_form(rf.post("", {"first_name": "yooo"}))
#     assert response.status_code == HTTPStatus.OK
#     html = BeautifulSoup(
#         cast(TemplateResponse, response).rendered_content, features="html.parser"
#     )
#     last_name_field = html.select_one("div.field:has(input[name=last_name])")
#     assert last_name_field
#     errorlist = last_name_field.select_one(".errorlist")
#     assert errorlist
#     assert "required" in errorlist.text
#
#
# with (Path(settings.PROJECT_DIR) / "test-data/minimal.pdf").open("rb") as f:
#     pdf_data = f.read()
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# @mark.django_db
# @mark.parametrize("with_documents", [[True, False]])
# def test_post_saves_application(with_documents, rf: RequestFactory):
#     # This test case duplicates ~half of the playwright test below, but it's easier for
#     # debugging (playwright tests run the server in a background thread so breakpoint() won't
#     # work)
#     easy_to_validate_fields = {
#         "first_name": "fff",
#         "last_name": "lll",
#         "phone_number": "+441134960000",
#         "email": "text@example.com",
#         "date_of_birth": "1999-09-12",
#         "gender": "female",
#         "nationalities": ["AL", "CW"],
#         "positions": sorted(["hom", "cook"]),
#         "certificates": ["STCW-VI/4-2", "other: advanced mischief"],
#         "motivation": "yooo",
#         "qualification": "hmmm",
#         # "availability": "letsgooo",  # TODO: Implement #261
#         # "experience": "b",  # TODO implement, values #277
#     }
#     cv = UnassociatedUploadedFileFactory.create(filename="test-cv.pdf")
#     stcw = UnassociatedUploadedFileFactory.create(filename="test-stcw.pdf")
#     documents = [
#         {
#             "id": "9cdec541-7091-431b-a84c-e3834bd56f88",
#             "unassociated-file": cv,
#             "type": "cv",
#             "metadata": {},
#         },
#         {
#             "id": "a0a80629-3c84-480a-805b-554700334741",
#             "unassociated-file": stcw,
#             "type": "stcw",
#             "metadata": {"valid-until": "2074-03-01"},
#         },
#     ]
#     hard_to_validate_fields = {
#         "spoken_languages": json.dumps({"deu": 3, "ara-levantine": 1}),
#         **(
#             {
#                 "documents": [d["id"] for d in documents],
#                 **{f"document-{d['id']}-type": d["type"] for d in documents},
#                 **{
#                     f"document-{d['id']}-file-uid": str(d["unassociated-file"].uid)
#                     for d in documents
#                 },
#                 **(
#                     {
#                         f"document-{d['id']}-{k}": v
#                         for d in documents
#                         for k, v in d["metadata"].items()
#                     }
#                 ),
#             }
#             if with_documents
#             else {
#                 "documents": [],
#             }
#         ),
#         # "documents"
#     }
#     fake_now = datetime(2500, 3, 20, 13, 15, 30, tzinfo=timezone.utc)
#     with freeze_time(fake_now):
#         response = new_application_form(
#             rf.post("", {**easy_to_validate_fields, **hard_to_validate_fields})
#         )
#
#     assert isinstance(response, HttpResponseRedirect), (
#         cast(TemplateResponse, response).context_data["form"].errors  # type: ignore
#     )
#     assert response.url == reverse("application_success")
#
#     application = Application.objects.get()
#
#     assert application.uid
#     assert application.created == fake_now
#
#     for field, expected in easy_to_validate_fields.items():
#         assert application.data[field] == expected
#
#     assert {
#         l["language"]: l["points"] for l in application.data["spoken_languages"]
#     } == json.loads(hard_to_validate_fields.pop("spoken_languages"))
#
#     if with_documents:
#
#         def read(f):
#             with f.open("rb") as opened:
#                 return opened.read()
#
#         assert {
#             (read(d.file), d.type, tuple(sorted(d.metadata.items())))
#             for d in application.documents.all()
#         } == {
#             (
#                 read(d["unassociated-file"].file),
#                 d["type"],
#                 tuple(sorted(d["metadata"].items())),
#             )
#             for d in documents
#         }
#     else:
#         assert not application.documents.exists()
#     hard_to_validate_fields = {
#         k: v for k, v in hard_to_validate_fields.items() if not k.startswith("document")
#     }
#
#     assert not hard_to_validate_fields, "forgot to assert on some fields?"
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_marks_fields_as_required(rf: RequestFactory):
#     # Ideally, we would check this with playwright to see the css actually work, but I can't find
#     # a way to get the computed text content our of playwright, so we have to settle for this.
#     response = new_application_form(rf.get(""))
#     html = BeautifulSoup(
#         cast(TemplateResponse, response).rendered_content, features="html.parser"
#     )
#     label = html.select_one(".field:has(input[name=first_name]) label")
#     assert label
#     assert "required" in label["class"]
#
#
# # documents = {
# #     Document.Type.CV: {"last_byte": b"c", "metadata": {}},
# #     Document.Type.STCW: {
# #         "last_byte": b"s",
# #         "metadata": {"valid-until": date(2080, 1, 4).isoformat()},
# #     },
# # }
# #
# # page1 = [
# #     ("First name", "Ahab", "text"),
# #     ("Last name", "Airbender", "text"),
# #     ("Phone number", "+441134960000", "text"),
# #     ("Email address", "fish_pity_me@example.com", "text"),
# #     ("Date of birth", date(1988, 2, 20), "date"),
# #     ("Gender", "None/Other", "select"),
# #     ("Nationalities", ["Curaçao"], "choicesjs"),
# #     ("Spoken Languages", {"eng": 2, "ara-levantine": 1}, "languages"),
# #     ("Select positions", ["Cook", "Nurse"], "choicesjs"),
# #     ("Homepage", "https://foo.example.com", "text"),
# # ]
# #
# # page2 = [
# #     ("Select certificates", ["STCW I/9"], "choicesjs"),
# #     ("Documents", documents, "documents"),
# # ]
# #
# # page3 = [
# #     ("Why do you want to work with Sea-Watch?", "woof", "text"),
# #     ("What qualifies you for the chosen position(s)?", "wazam", "text"),
# #     # (
# #     #     "What is your general availability for an operation?",
# #     #     "everyday I'm juggeling",
# #     #     "text",
# #     # ), # TODO: implement #261
# #     # (
# #     #     "Do you have any experience with search-and-rescue NGOs or Sea-Watch?",
# #     #     "B",
# #     #     "select",
# #     # ),  # TODO: implement, values #277
# # ]
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# @mark.parametrize("page_nav_method", ["next_button", "nav", "enter"])
# def test_allows_applying(
#     page_nav_method: Literal["next_button", "nav", "enter"], page: Page, tmp_path: Path
# ):
#     page.goto(reverse("application_form"))
#
#     ########
#     # Page 1
#     expect(page.locator("nav li.form-wizard_current >> visible=true")).to_contain_text(
#         "Personal Data"
#     )
#     for entry in page1:
#         _fill_field(tmp_path, page, documents, *entry)
#     match page_nav_method:
#         case "next_button":
#             page.get_by_role("button").get_by_text("Next step").click()
#         case "nav":
#             page.locator("nav.form-wizard_nav li:nth-child(2) button").click()
#         case "enter":
#             page.focus("input, select, textarea")
#             page.keyboard.press("Enter")
#         case other:
#             assert_never(other)
#
#     ########
#     # Page 2
#     expect(page.locator("nav li.form-wizard_current >> visible=true")).to_contain_text(
#         "Qualifications & certificates"
#     )
#     for entry in page2:
#         _fill_field(tmp_path, page, documents, *entry)
#     match page_nav_method:
#         case "next_button":
#             page.get_by_role("button").get_by_text("Next step").click()
#         case "nav":
#             page.locator("nav.form-wizard_nav li:nth-child(3) button").click()
#         case "enter":
#             page.focus("input, select, textarea")
#             page.keyboard.press("Enter")
#         case other:
#             assert_never(other)
#
#     ########
#     # Page 3
#     expect(page.locator("nav li.form-wizard_current >> visible=true")).to_contain_text(
#         "Letter of motivation"
#     )
#     for entry in page3:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     with page.expect_response(lambda r: r.status // 100 == 3):
#         if page_nav_method == "enter":
#             page.get_by_role("button").get_by_text("Submit application").focus()
#             page.keyboard.press("Enter")
#         else:
#             page.get_by_role("button").get_by_text("Submit application").click()
#
#     ############
#     # Assertions
#     application = Application.objects.get()
#     assert application.data == {
#         "first_name": "Ahab",
#         "last_name": "Airbender",
#         "phone_number": "+441134960000",
#         "email": "fish_pity_me@example.com",
#         "date_of_birth": date(1988, 2, 20).isoformat(),
#         "gender": Gender.NONE_OTHER.value,
#         "nationalities": ["CW"],
#         "spoken_languages": [
#             {"language": "eng", "points": 2},
#             {"language": "ara-levantine", "points": 1},
#         ],
#         "positions": sorted(["cook", "nurse"]),
#         "certificates": ["STCW-I/9"],
#         "homepage": "https://foo.example.com",
#         "motivation": "woof",
#         "qualification": "wazam",
#         # (
#         #     "What is your general availability for an operation?",
#         #     "everyday I'm juggeling",
#         #     "text",
#         # ), # TODO: implement #261
#         # (
#         #     "Do you have any experience with search-and-rescue NGOs or Sea-Watch?",
#         #     "B",
#         #     "select",
#         # ),  # TODO: implement, values #277
#     }
#
#     def read(file: FieldFile):
#         with file.open("rb") as f:
#             return f.read()
#
#     assert {
#         Document.Type(doc.type): {
#             "last_byte": read(doc.file)[-1:],
#             "metadata": doc.metadata,
#         }
#         for doc in application.documents.all()
#     } == documents
#
#
# def _fill_field(tmp_path, page, documents, label, value, kind):
#     match kind:
#         case "text":
#             page.get_by_label(label).fill(value)
#         case "date":
#             page.get_by_label(label).fill(value.isoformat())
#         case "select":
#             page.get_by_label(label).select_option(label=value)
#         case "choicesjs":
#             choices = ChoicesJS(page.get_by_label(label))
#             for v in value:
#                 choices.select(v)
#         case "languages":
#             choices = ChoicesJS(page.get_by_label(label))
#             for v in value:
#                 choices.select_value(v)
#             for code, points in value.items():
#                 page.locator(
#                     f".language-mapper-level[data-language='{code}'] select"
#                 ).select_option(str(points))
#         case "documents":
#             cv_subform = page.locator(
#                 ".document:has(input.mandatory-document-select-replacement)"
#             )
#             stcw_subform = page.locator(
#                 ".document:not(:has(input.mandatory-document-select-replacement))"
#             )
#             expect(cv_subform).to_be_visible()
#             expect(stcw_subform).to_be_visible()
#
#             docs = documents.copy()
#
#             label_for = {
#                 "valid-until": "Valid until",
#             }
#
#             cv_data = docs.pop(Document.Type.CV)
#             cv_path = tmp_path / "cv.pdf"
#             with cv_path.open("wb") as f:
#                 f.write(pdf_data + cv_data["last_byte"])
#             with page.expect_file_chooser() as fc_info:
#                 cv_subform.get_by_role("button", name="Select file").click()
#             fc_info.value.set_files(cv_path)
#             for name, v in cv_data["metadata"].items():
#                 cv_subform.get_by_label(label_for[name]).fill(v)
#
#             stcw_subform.get_by_label("type").select_option(stcw_option)
#             stcw_data = docs.pop(Document.Type.STCW)
#             stcw_path = tmp_path / "stcw.pdf"
#             stcw_path.write_bytes(pdf_data + stcw_data["last_byte"])
#             with page.expect_file_chooser() as fc_info:
#                 stcw_subform.get_by_role("button", name="Select file").click()
#             fc_info.value.set_files(stcw_path)
#             for name, v in stcw_data["metadata"].items():
#                 stcw_subform.get_by_label(label_for[name]).fill(v)
#
#             assert not docs
#         case x:
#             assert_never(x)
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_shows_only_first_page_initially(page: Page):
#     page.goto(reverse("application_form"))
#     expect(page.locator("label[for$=certificates]")).not_to_be_visible()
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_disallows_skipping_pages(page: Page, tmp_path: Path):
#     page.goto(reverse("application_form"))
#     expect(page.locator(".form-wizard_nav li:nth-child(3) button")).to_be_disabled()
#     for entry in page1:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     page.locator(".form-wizard_nav li:nth-child(2) button").click()
#     for entry in page2:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     page.locator(".form-wizard_nav li:nth-child(3) button").click()
#     expect(page.get_by_label("Why do you want to work with Sea-Watch?")).to_be_visible()
#
#     page.locator(".form-wizard_nav li:nth-child(1) button").click()
#     expect(
#         page.get_by_label("Why do you want to work with Sea-Watch?")
#     ).not_to_be_visible()
#     expect(page.get_by_label("First name")).to_be_visible()
#
#     expect(page.locator(".form-wizard_nav li:nth-child(3) button")).to_be_disabled()
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_links_to_seawatch_page(rf: RequestFactory):
#     response = new_application_form(rf.get(""))
#     assert isinstance(response, TemplateResponse)
#     html = BeautifulSoup(response.rendered_content, features="html.parser")
#     link = html.find("a", string="overview of positions on the ship")
#     assert isinstance(link, Tag)
#     join_us = requests.get(link.attrs["href"])
#     assert "The following positions are available on " in join_us.text
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# @mark.parametrize("navigation_method", ["nav", "next-button"])
# @mark.parametrize(
#     ["invalid_field", "input_type"],
#     [
#         ("First name", "text"),
#         ("Nationalities", "choicesjs"),
#         ("Language", "choicesjs"),
#         ("English", "select"),
#     ],
# )
# def test_prevents_navigation_to_next_page_on_invalid(
#     navigation_method: Literal["nav", "next-button"],
#     invalid_field: str,
#     input_type: str,
#     # ↑ parameters
#     # ↓ fixtures
#     page: Page,
#     tmp_path: Path,
# ):
#     page.goto(reverse("application_form"))
#     for entry in page1:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     input = page.get_by_label(invalid_field).locator("visible=true")
#     match input_type:
#         case "text":
#             input.fill("")
#         case "choicesjs":
#             choices = ChoicesJS(input)
#             choices.deselect_all()
#         case "select":
#             input.select_option(index=0)
#         case _:
#             fail(f"unexpected input_type: {input_type}")
#
#     if navigation_method == "next_button":
#         page.get_by_role("button").get_by_text("Next step").click()
#     else:
#         page.locator("nav.form-wizard_nav li:nth-child(2) button").click()
#
#     expect(input).to_be_visible()
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_rejects_invalid_phone_numbers(page: Page, tmp_path: Path):
#     page.goto(reverse("application_form"))
#     for entry in page1:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     input = page.get_by_label("Phone number")
#     input.fill("+4912")
#     input.blur()
#     assert (
#         input.evaluate("e => e.validationMessage")
#         == "Please enter a valid phone number."
#     )
#
#
# @mark.skipif(not features["application_wizard"], reason="needs feature enabled")
# def test_rerenders_and_opens_first_error_page_on_errors(page: Page, tmp_path: Path):
#     page.goto(reverse("application_form"))
#     for entry in page1:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     page.locator(".form-wizard_nav li:nth-child(2) button").click()
#     for entry in page2:
#         _fill_field(tmp_path, page, documents, *entry)
#
#     # We're adding a file that has the ending pdf but isn't actually a pdf file here. This cannot be
#     # validated in the client and needs to be handled by a rerendering
#     invalid_pdf = tmp_path / "not-a-pdf.pdf"
#     with invalid_pdf.open("wb") as f:
#         f.write(b"I'm not a pdf!")
#     page.locator("[type=file]").first.set_input_files([invalid_pdf])
#
#     page.locator(".form-wizard_nav li:nth-child(3) button").click()
#     for entry in page3:
#         _fill_field(tmp_path, page, documents, *entry)
#     page.get_by_role("button").get_by_text("Submit application").click()
#
#     homepage = page.get_by_label("homepage")
#     assert homepage.is_visible()
#     assert homepage.input_value() == next(
#         value for label, value, _ in page1 if label == "Homepage"
#     )
#
#     # TODO Document error messages aren't great and they have to be re-filled #262
#
#
# # TODO: #262 show uploaded file name after uploading
