from typing import cast
from urllib.parse import unquote

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.conf import settings
from django.contrib.staticfiles import finders
from django.template.response import TemplateResponse
from django.test import RequestFactory


from ... import views


def test_HomeView_contains_application_form(rf: RequestFactory):
    response = views.HomeView.as_view()(rf.get(".."))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    assert len(html.find_all("application-form")) == 1


def test_HomeView_data_protection_document_links_works(rf: RequestFactory):
    response = views.HomeView.as_view()(rf.get(""))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    data_protection_info = html.find(id="data-protection-info")
    assert isinstance(data_protection_info, Tag)
    links = [
        link
        for link in data_protection_info.find_all("a")
        if cast(str, cast(Tag, link).attrs["href"]).startswith(settings.STATIC_URL)
    ]
    assert (
        links
    ), "no staticfiles link found in data protection, either bug or useless test"

    for link in links:
        assert isinstance(link, Tag)
        attr = link.attrs["href"]
        assert isinstance(attr, str)
        file = unquote(attr.removeprefix(settings.STATIC_URL))
        assert finders.find(file)
