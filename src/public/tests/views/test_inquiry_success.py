from django.test import RequestFactory

from ...views import inquiry_success


def test_renders(rf: RequestFactory):
    assert "Thank you" in inquiry_success(rf.get("")).rendered_content
