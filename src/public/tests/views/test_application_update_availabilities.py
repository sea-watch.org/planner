import json
from datetime import date
from http import HTTPStatus
from uuid import uuid4

import pytest
from django.core import mail
from django.http import HttpResponseNotAllowed
from django.test import Client, RequestFactory
from django.urls import reverse
from operations.models import Availability
from operations.tests.factories import AvailabilityFactory, OperationFactory
from volunteers.tests.factories import VolunteerFactory

from ... import views
from ...models import Inquiry
from ..factories import AvailabilityInquiryFactory


def test_is_reachable(public_client: Client):
    response = public_client.get(
        reverse("application_update_availabilities"),
    )
    assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED


def test_get_not_allowed(rf: RequestFactory):
    request = rf.get("application_update_availabilities")
    response = views.application_update_availabilities(request)
    assert isinstance(response, HttpResponseNotAllowed)


def test_raises_when_vars_missing(rf: RequestFactory):
    request = rf.post(
        "application_update_availabilities",
        json.dumps({}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_raises_when_token_is_invalid(
    rf: RequestFactory,
):
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": "fake_token"}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_raises_when_inquiry_vars_are_missing(
    rf: RequestFactory,
):
    inquiry = AvailabilityInquiryFactory.create()
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(inquiry.uid)}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_updates_availabilities(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    operations = OperationFactory.create_batch(size=3)
    Availability.objects.create(
        volunteer=volunteer,
        operation=operations[0],
        availability=Availability.Choices.YES,
    )
    Availability.objects.create(
        volunteer=volunteer,
        operation=operations[1],
        availability=Availability.Choices.NO,
    )
    # Third one has no response yet

    inquiry = Inquiry.objects.create(
        volunteer=volunteer, type=Inquiry.Type.AVAILABILITY
    )
    availability = [
        {"operation_id": operation.id, "answer": "maybe"} for operation in operations
    ]
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(inquiry.uid), "answers": availability}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 204, response.content
    assert not Inquiry.objects.filter(id=inquiry.id).exists()
    assert [
        Availability.objects.get(volunteer=volunteer, operation=operation).availability
        for operation in operations
    ] == [Availability.Choices.MAYBE] * 3


@pytest.mark.django_db
def test_rejects_unknown_operation_ids(
    rf: RequestFactory,
):
    forged = [{"operation_id": 92329, "answer": "yes"}]
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(uuid4()), "answers": forged}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_sends_mail_after_submitting_availabilities(
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create(
        first_name="Brandon", email="volunteer@example.com"
    )
    availabilities = [
        AvailabilityFactory.create(
            operation__start_date=date(2020, 1, 1),
            operation__end_date=date(2020, 2, 2),
        ),
        AvailabilityFactory.create(
            operation__start_date=date(2020, 3, 1),
            operation__end_date=date(2020, 4, 2),
        ),
        AvailabilityFactory.create(
            operation__start_date=date(2020, 5, 1),
            operation__end_date=date(2020, 6, 2),
        ),
    ]
    inquiry = Inquiry.objects.create(
        type=Inquiry.Type.AVAILABILITY, volunteer=volunteer
    )
    answers = [
        {"operation_id": availabilities[0].operation.id, "answer": "yes"},
        {"operation_id": availabilities[1].operation.id, "answer": "no"},
        {"operation_id": availabilities[2].operation.id, "answer": "maybe"},
    ]
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(inquiry.uid), "answers": answers}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)

    assert response.status_code == 204, response.content

    assert len(mail.outbox) == 1
    email = mail.outbox[0]
    assert email.to == ["volunteer@example.com"]
    assert email.subject == "Operation availability confirmed"
    assert "Brandon" in email.body
    assert "thank you" in email.body
    assert "2020-01-01 to 2020-02-02: Yes" in email.body
    assert "2020-03-01 to 2020-04-02: No" in email.body
    assert "2020-05-01 to 2020-06-02: Maybe" in email.body
    assert "http://" not in email.body


@pytest.mark.django_db
def test_rejects_availabilities_when_selection_has_invalid_value(
    rf: RequestFactory,
):
    inquiry = AvailabilityInquiryFactory.create()
    availability = [{"operation_id": OperationFactory.create().id, "answer": "wtf"}]
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(inquiry.uid), "operations": availability}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422


@pytest.mark.django_db
def test_rejects_availabilities_when_selection_is_None(
    rf: RequestFactory,
):
    inquiry = AvailabilityInquiryFactory.create()
    availability = [{"operation_id": OperationFactory.create().id, "answer": None}]
    request = rf.post(
        "application_update_availabilities",
        json.dumps({"token": str(inquiry.uid), "operations": availability}),
        content_type="application/json",
    )
    response = views.application_update_availabilities(request)
    assert response.status_code == 422
