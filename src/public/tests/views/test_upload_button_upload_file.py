import json
import re
from datetime import datetime, timezone
from http import HTTPStatus
from pathlib import Path
from uuid import UUID

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import render
from django.test.client import RequestFactory
from django.urls import reverse
from freezegun import freeze_time
from playwright.sync_api import Page, expect
from pytest import mark, skip
from pytest_subtests import SubTests

from ...models import UnassociatedUploadedFile
from ...views import upload_button_upload_file


def test_returns_method_not_allowed_for_GET(rf: RequestFactory):
    assert (
        HTTPStatus(upload_button_upload_file(rf.get("")).status_code)
        == HTTPStatus.METHOD_NOT_ALLOWED
    )


minimal_pdf_path = Path(settings.PROJECT_DIR + "test-data/minimal.pdf")
minimal_pdf = Path(settings.PROJECT_DIR + "test-data/minimal.pdf").read_bytes()


@mark.django_db
def test_saves_file(rf: RequestFactory, subtests: SubTests):
    name = "certificate"
    id = "the-button"

    request = rf.post("", {"_name": name, "_id": id})
    request.FILES["file"] = SimpleUploadedFile("test_file.pdf", minimal_pdf)

    now = datetime(2023, 7, 7, 20, 32, tzinfo=timezone.utc)

    with freeze_time(now):
        response = upload_button_upload_file(request)

    uploaded_file = None
    with subtests.test("saved document"):
        uploaded_file = UnassociatedUploadedFile.objects.last()
        assert uploaded_file
        with uploaded_file.file.open("rb") as f:
            assert f.read() == minimal_pdf
        assert isinstance(uploaded_file.uid, UUID)
        assert uploaded_file.created_at == now

    with subtests.test("response"):
        if not uploaded_file:
            skip("dependent test failed")
        assert HTTPStatus(response.status_code) == HTTPStatus.OK
        assert json.loads(response.content) == {
            "valid": True,
            "uid": str(uploaded_file.uid),
        }


@mark.django_db  # not needed if everything works but improves failure messages
def test_validates_file(rf: RequestFactory):
    big_file_not_pdf = SimpleUploadedFile(
        "11mb_basic_safety.pdf", 11 * 1024 * 1024 * b"x"
    )
    request = rf.post("", {"_name": "foo", "_id": "bar"})
    request.FILES["file"] = big_file_not_pdf

    response = upload_button_upload_file(request)
    assert HTTPStatus(response.status_code) == HTTPStatus.OK
    assert json.loads(response.content) == {
        "valid": False,
        "errors": [
            "Please upload a file below 10MB in size.",
            "Please upload a file in PDF format.",
        ],
    }
    assert not UnassociatedUploadedFile.objects.exists()


def test_uploads_file(rf, page: Page, public_url):
    render(rf.get(""), "testing_api/upload_button.html")
    page.goto(
        f"{public_url}{reverse('test-upload-button', urlconf=settings.PUBLIC_URLCONF)}"
    )
    button = page.get_by_label("Cool label")
    with page.expect_file_chooser() as fc_info:
        button.click()
    fc_info.value.set_files(minimal_pdf_path)
    hidden_input = page.locator("[name=certificate]")
    expect(hidden_input).to_have_value(re.compile(r".+"))

    assert UnassociatedUploadedFile.objects.filter(
        uid=hidden_input.input_value()
    ).exists()


def test_displays_validation_errors(page: Page, tmp_path: Path, public_url):
    not_a_pdf = tmp_path / "test.pdf"
    not_a_pdf.write_bytes(b"not a pdf file")
    page.goto(
        f"{public_url}{reverse('test-upload-button', urlconf=settings.PUBLIC_URLCONF)}"
    )
    button = page.get_by_label("Cool label")
    with page.expect_file_chooser() as fc_info:
        button.click()
    fc_info.value.set_files(not_a_pdf)
    hidden_input = page.locator("[name=certificate]")
    expect(page.locator(".errorlist li")).to_have_text(
        "Please upload a file in PDF format."
    )
    expect(hidden_input).to_have_value("")
