import pytest
from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import Client
from django.test.client import RequestFactory
from pytest_django.asserts import assertContains

from seawatch_registration.tests.util import htmlnormalize_text

from ...views import HomeView


def test_renders_application_form_with_correct_title(rf: RequestFactory):
    response = HomeView.as_view()(rf.get(""))
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    title = html.find("title")
    assert title
    assert htmlnormalize_text(title.getText()) == "Join the Crew • Sea-Watch"


def test_renders_default_title(public_client: Client):
    response = public_client.get(
        "/applications/application_success/",
    )
    html = BeautifulSoup(response.content, features="html.parser")
    title = html.find("title")
    assert (
        title and htmlnormalize_text(title.getText()) == "Crew Management • Sea-Watch"
    )


@pytest.mark.django_db
def test_shows_data_protection_note(rf: RequestFactory):
    response = HomeView.as_view()(rf.get(".."))
    assertContains(response, "gemäß Art. 13 DSGVO")
