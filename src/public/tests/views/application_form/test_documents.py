from pathlib import Path
from typing import Literal

from django.conf import settings
from django.urls import reverse
from playwright.sync_api import Page, expect
from pytest import mark

from planner_common.feature_flags import features
from planner_common.playwright_utils import MultiChoicesJS

pytestmark = mark.skipif(
    not features["application_wizard"], reason="needs feature enabled"
)

stcw_option = "STCW (Nautical Certificates)"


def skip_personal_data_page(page: Page):
    page.locator("[form-wizard_page=personal-data]").evaluate(
        "(e) => e.dataset.testAssumeValid = true;"
    )
    page.locator("button[form-wizard_goto=qualifications]").click()


def test_can_add_and_remove_subforms(page: Page):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)

    field = page.locator('fieldset:has(legend:has-text("documents"))')
    documents = field.locator("#documents")
    assert documents.count() == 1

    assert documents.get_by_label("type").count() == 1

    add_button = field.get_by_role("button").get_by_text("Add a document")
    add_button.click()
    add_button.click()

    assert documents.get_by_label("type").count() == 3

    assert documents.locator("input[name=documents]").count() == 3

    documents.locator("button").nth(2).click()
    documents.locator("button").nth(1).click()

    assert documents.get_by_label("type").count() == 1


def test_shows_valid_until_depending_on_document_type(page: Page):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)

    documents = page.locator('fieldset:has(legend:has-text("documents"))').locator(
        "#documents"
    )
    metadata = documents.locator("[data-container-for=metadata]")

    expect(metadata).to_be_empty()

    documents.get_by_label("type").select_option("CV")
    expect(metadata).to_be_empty()

    documents.get_by_label("type").select_option(stcw_option)
    expect(metadata.get_by_label("Valid until")).to_be_visible()


def test_file_input_is_disabled_as_long_as_no_type_is_selected(page: Page):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)

    documents = page.locator('fieldset:has(legend:has-text("documents"))').locator(
        "#documents"
    )

    type_select = documents.locator("select[data-name=type]")
    file_input = documents.locator('input[type=file][name$="-file"]')

    expect(file_input).to_be_disabled()

    type_select.select_option("CV")
    expect(file_input).not_to_be_disabled()


def test_adds_mandatory_cv_for_positions_that_require_it(page: Page):
    page.goto(reverse("application_form"))

    positions_select = MultiChoicesJS(page.get_by_label("Positions"))
    positions_select.select("Nurse")

    skip_personal_data_page(page)
    documents = page.locator('fieldset:has(legend:has-text("documents"))').locator(
        "#documents"
    )

    cv_subform = documents.locator(
        ".document:has(input.mandatory-document-select-replacement)"
    )
    expect(cv_subform).to_be_visible()
    expect(cv_subform.locator("select[data-name=type]")).to_be_hidden()
    expect(cv_subform.locator("select[data-name=type]")).to_have_value("cv")

    replacement_input = cv_subform.get_by_label("type")
    expect(replacement_input).to_be_visible()
    expect(replacement_input).to_have_value("CV")

    expect(cv_subform.locator("[data-container-for=metadata]")).to_be_empty()
    expect(cv_subform.locator("button")).to_be_disabled()


def test_removes_mandatory_cv_again(page: Page):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)

    documents = page.locator('fieldset:has(legend:has-text("documents"))').locator(
        "#documents"
    )
    documents.locator(
        "button"
    ).click()  # Remove the existing subform for easier handling

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions_select = MultiChoicesJS(page.get_by_label("Positions"))
    positions_select.select("Nurse")

    page.locator("button[form-wizard_goto=qualifications]").click()
    cv_subform = documents.locator(".document")
    expect(cv_subform).to_be_visible()

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions_select.deselect("Nurse")

    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(
        cv_subform.locator(".mandatory-document-select-replacement")
    ).not_to_be_visible()
    expect(cv_subform.get_by_label("type")).to_be_visible()
    expect(cv_subform.locator("select[data-name=type]")).to_be_visible()
    expect(cv_subform.locator("select[data-name=type]")).to_have_value("cv")
    expect(cv_subform.get_by_role("button")).not_to_be_disabled()


def test_uses_existing_subform_for_mandatory_document(page: Page):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)

    documents = page.locator('fieldset:has(legend:has-text("documents"))').locator(
        "#documents"
    )
    documents.locator("select[data-name=type]").select_option("CV")

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions_select = MultiChoicesJS(page.get_by_label("Positions"))
    positions_select.select("Nurse")

    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(documents.locator(".document")).to_have_count(1)
    expect(documents.locator("button")).to_be_disabled()


def test_prints_why_documents_are_mandatory(page: Page):
    page.goto(reverse("application_form"))
    positions = MultiChoicesJS(page.get_by_label("Positions"))

    skip_personal_data_page(page)
    note = page.locator("#mandatory-documents-note")

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions.select("Cook")
    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(note).to_contain_text("no documents are required at this time")

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions.select("Nurse")
    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(note).to_have_text(
        "The following documents are mandatory because of your selected positions: CV (Nurse)"
    )

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions.select("Media Coordinator")
    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(note).to_have_text(
        "The following documents are mandatory because of your selected positions: CV (Media Coordinator, Nurse)"
    )

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions.deselect("Nurse")
    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(note).to_have_text(
        "The following documents are mandatory because of your selected positions: CV (Media Coordinator)"
    )

    page.locator("button[form-wizard_goto=personal-data]").click()
    positions.deselect("Media Coordinator")
    page.locator("button[form-wizard_goto=qualifications]").click()
    expect(note).to_contain_text("no documents are required at this time")


@mark.parametrize(
    "problems", [["too_large"], ["wrong_type"], ["too_large", "wrong_type"]]
)
def test_shows_document_file_errors(
    problems: list[Literal["too_large", "wrong_type"]], page: Page
):
    page.goto(reverse("application_form"))
    skip_personal_data_page(page)
    page.select_option("select[data-name=type]", "CV")
    input = page.locator("input[type=file]")

    # Prep depending on problem(s)
    file = (
        Path(__file__)
        if "wrong_type" in problems
        else Path(settings.PROJECT_DIR) / "test-data/minimal.pdf"
    )

    if "too_large" in problems:
        with file.open("rb") as f:
            size = len(f.read())
        input.evaluate(f"(e) => e.dataset.maxSize = {size - 1}")

    input.set_input_files(file)
    validation_message = input.evaluate("e => e.validationMessage")

    if "too_large" in problems:
        assert "a file below" in validation_message
    if "wrong_type" in problems:
        assert "a file in PDF format" in validation_message

    input.evaluate("e => e.dataset.maxSize = 999999999999999")
    input.set_input_files(Path(settings.PROJECT_DIR) / "test-data/minimal.pdf")
    assert input.evaluate("e => e.validationMessage") == ""
