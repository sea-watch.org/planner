import dataclasses
import json
from datetime import date
from http import HTTPStatus
from logging import LogRecord
from pathlib import Path
from typing import List
from unittest import mock

import pytest
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, RequestFactory, override_settings
from django.urls import reverse
from freezegun import freeze_time
from pytest_subtests import SubTests

from planner_common.clamav import NullClamd
from planner_common.test.factories import ApplicationData, ApplicationDataFactory
from seawatch_registration.tests.util import ensure_positions_exists
from volunteers.models import Document, Volunteer

from ... import views

expected_status = HTTPStatus.NO_CONTENT


@pytest.mark.django_db
def test_is_reachable(public_client: Client):
    response = public_client.get(
        reverse("application_create"),
    )
    assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED


def test_method_not_allowed_on_GET(rf: RequestFactory):
    assert (
        views.application_create(rf.get("")).status_code
        == HTTPStatus.METHOD_NOT_ALLOWED
    )


@pytest.mark.django_db
def test_saves_data_as_volunteer_and_sarapplications(rf: RequestFactory):
    alice = ApplicationDataFactory.create(
        homepage="https://wowzer.yeehaw",
        positions=[
            # some positions are special-cased so we avoid them here
            "bosun",
            "hom",
            "media-coord",
        ],
    )
    ensure_positions_exists(alice.positions)

    request = rf.post(
        "application_create",
        {"data": alice.as_json()},
    )
    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert HTTPStatus(response.status_code) == expected_status, response.content
    assert len(response.content) == 0

    volunteer = Volunteer.objects.get()

    assert volunteer.first_name == alice.first_name
    assert volunteer.last_name == alice.last_name
    assert volunteer.email == alice.email
    assert volunteer.date_of_birth == date.fromisoformat(alice.date_of_birth)
    assert volunteer.gender == alice.gender
    assert volunteer.motivation == alice.motivation
    assert volunteer.qualification == alice.qualification
    assert volunteer.phone_number == alice.phone_number
    assert volunteer.homepage == alice.homepage
    assert volunteer.languages == {
        lang["language"]: lang["points"] for lang in alice.spoken_languages
    }
    assert volunteer.nationalities == [code.lower() for code in alice.nationalities]

    assert {sa.position.key for sa in volunteer.sarapplication_set.all()} == set(
        alice.positions
    )


@pytest.mark.django_db
def test_rejects_invalid_positions(rf: RequestFactory):
    alice = ApplicationDataFactory.create(positions=["invalid"])

    request = rf.post(
        "application_create",
        {"data": alice.as_json()},
    )
    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    assert not Volunteer.objects.exists()


@pytest.mark.django_db
def test_rejects_duplicate_positions(rf: RequestFactory):
    alice = ApplicationDataFactory.create(
        positions=[
            ApplicationData.valid_positions()[0],
            ApplicationData.valid_positions()[0],
        ]
    )

    request = rf.post(
        "application_create",
        {"data": alice.as_json()},
    )
    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY

    assert not Volunteer.objects.exists()


@pytest.mark.django_db
@pytest.mark.parametrize(
    "missing_field",
    [
        field.name
        for field in dataclasses.fields(ApplicationData)
        if field.name not in ["other_certificate", "homepage"]
    ],
)
def test_raises_without_mandatory_fields(missing_field: str, rf: RequestFactory):
    application_data_dto = ApplicationDataFactory.create()
    application_data = application_data_dto.to_dict()
    application_data.pop(missing_field)
    request = rf.post(
        "application_create",
        {"data": json.dumps(application_data)},
    )
    assert (
        views.application_create(request, get_clamd=lambda: NullClamd()).status_code
        == HTTPStatus.UNPROCESSABLE_ENTITY
    ), f"Application was created with missing field {missing_field}"


@pytest.mark.django_db
def test_raises_with_additional_properties(rf: RequestFactory):
    application_data = ApplicationDataFactory.create()
    application = application_data.to_dict()
    application["x"] = 666
    request = rf.post(
        "application_create",
        {"data": json.dumps(application)},
    )
    assert (
        views.application_create(request, get_clamd=lambda: NullClamd()).status_code
        == 422
    )
    assert (
        json.loads(views.application_create(request).content)["message"]
        == "Additional properties are not allowed ('x' was unexpected)"
    )


@pytest.mark.django_db
def test_raises_with_additional_property_in_language(
    rf: RequestFactory,
):
    application_data = ApplicationDataFactory.create()
    application = application_data.to_dict()
    application["spoken_languages"] = [{"language": "deu", "points": 5, "y": 6}]
    request = rf.post(
        "application_create",
        {"data": json.dumps(application)},
    )
    assert (
        views.application_create(request, get_clamd=lambda: NullClamd()).status_code
        == 422
    )
    assert (
        json.loads(
            views.application_create(request, get_clamd=lambda: NullClamd()).content
        )["message"]
        == "Additional properties are not allowed ('y' was unexpected)"
    )


@pytest.mark.django_db
def test_raises_with_future_date_of_birth(rf: RequestFactory):
    application_data = ApplicationDataFactory.create()
    application = application_data.to_dict()
    application["date_of_birth"] = "2020-04-08"

    request = rf.post("application_create", {"data": json.dumps(application)})
    with freeze_time("2020-04-07"):
        assert (
            views.application_create(request, get_clamd=lambda: NullClamd()).status_code
            == 422
        )


@pytest.mark.django_db
def test_raises_with_invalid_date_of_birth(rf: RequestFactory):
    application_data = ApplicationDataFactory.create()
    application = application_data.to_dict()
    application["date_of_birth"] = "abcd-ef-gh"

    request = rf.post(
        "application_create",
        {"data": json.dumps(application)},
    )
    assert (
        views.application_create(request, get_clamd=lambda: NullClamd()).status_code
        == 422
    )


@pytest.mark.django_db
def test_doesnt_require_csrf_token(public_client_with_csrf: Client):
    application_data = ApplicationDataFactory.create()
    ensure_positions_exists(application_data.positions)
    response = public_client_with_csrf.post(
        reverse("application_create"),
        {"data": application_data.as_json()},
    )
    assert response.status_code == HTTPStatus.NO_CONTENT


@pytest.mark.django_db
def test_sends_email(rf: RequestFactory):
    alice = ApplicationDataFactory.create(first_name="Alice", email="alice@example.com")
    ensure_positions_exists(alice.positions)

    request = rf.post("application_create", {"data": alice.as_json()})
    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert HTTPStatus(response.status_code) == expected_status

    assert len(mail.outbox) == 1
    confirmation_email = mail.outbox[0]
    assert confirmation_email.to == ["alice@example.com"]
    assert "Alice" in confirmation_email.body
    assert "Thank you for signing up" in confirmation_email.body
    assert "http://" not in confirmation_email.body


@pytest.mark.django_db
def test_logs_and_returns_200_if_sending_email_fails(rf: RequestFactory, caplog):
    exception = Exception("marvellous mr watson")
    data = ApplicationDataFactory.create(email="major.tonb@example.com")
    ensure_positions_exists(data.positions)
    with mock.patch.object(views.mail, "send_mail") as mail_mock:
        mail_mock.side_effect = exception

        response = views.application_create(
            rf.post("", {"data": data.as_json()}),
            get_clamd=lambda: NullClamd(),
        )
    assert HTTPStatus(response.status_code) == expected_status
    records = caplog.records
    assert len(records) == 1
    r: LogRecord = records[0]
    assert r.name == "public.views"
    assert r.levelname == "ERROR"
    assert r.message == "Error sending application confirmation mail to applicant"
    assert r.__dict__.get("applicant_email") == "major.tonb@example.com"
    assert r.exc_info and r.exc_info[1] == exception


@pytest.mark.django_db
def test_creates_application_with_cv_file(rf: RequestFactory):
    alice = ApplicationDataFactory.create()
    ensure_positions_exists(alice.positions)

    request = rf.post(
        "application_create",
        {"data": alice.as_json()},
    )
    cv_file = SimpleUploadedFile("alice_cv.pdf", minimal_pdf)
    request.FILES["cv_file"] = cv_file

    response = views.application_create(request, get_clamd=lambda: NullClamd())

    # check that application gets created
    assert response.status_code == 204
    volunteer = Volunteer.objects.get()

    cv = volunteer.document_set.get()
    with cv.file.open("rb") as cv_file:
        assert cv_file.read() == minimal_pdf


@pytest.mark.django_db
def test_on_virus_scanner_detection(
    rf: RequestFactory, mailoutbox: List[mail.EmailMessage], subtests: SubTests
):
    data = ApplicationDataFactory.create(motivation="letsgooooooo")

    request = rf.post("", {"data": data.as_json()})
    request.FILES["cv_file"] = SimpleUploadedFile("eicar_cv.pdf", minimal_pdf)

    clamd_mock = mock.Mock()
    clamd_mock.instream.return_value = {"stream": ("FOUND", "unittest-malware")}

    with override_settings(ADMINS=[("foo", "foo@example.com")]):
        response = views.application_create(request, get_clamd=lambda: clamd_mock)

    with subtests.test("response is normal"):
        assert response.status_code == HTTPStatus.NO_CONTENT

    with subtests.test("does not save the data"):
        assert not Volunteer.objects.exists()

    with subtests.test("sends email normally"):
        (confirmation_email,) = [mail for mail in mailoutbox if mail.to == [data.email]]
        assert data.first_name in confirmation_email.body
        assert "Thank you for signing up" in confirmation_email.body

    with subtests.test("Sends malware notification mail"):
        (malware_mail,) = [
            mail for mail in mailoutbox if mail.to == ["foo@example.com"]
        ]
        assert "unittest-malware" in malware_mail.body
        assert "Application" in malware_mail.body
        assert "letsgooooooo" in malware_mail.body


@pytest.mark.django_db
def test_rejects_attachments_over_10MB(rf: RequestFactory):
    data = ApplicationDataFactory.create()

    request = rf.post("", {"data": data.as_json()})
    request.FILES["cv_file"] = SimpleUploadedFile("11mb.pdf", 11 * 1024 * 1024 * b"x")

    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert response.status_code == HTTPStatus.REQUEST_ENTITY_TOO_LARGE


@pytest.mark.django_db
def test_rejects_non_pdf_files(rf: RequestFactory):
    data = ApplicationDataFactory.create()

    request = rf.post("", {"data": data.as_json()})
    request.FILES["cv_file"] = SimpleUploadedFile("not-a-pdf.pdf", b"asdf")

    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert response.status_code == HTTPStatus.BAD_REQUEST


@pytest.mark.django_db
def test_sets_filename(rf: RequestFactory):
    application_data = ApplicationDataFactory.create()
    ensure_positions_exists(application_data.positions)

    request = rf.post("", {"data": application_data.as_json()})
    request.FILES["cv_file"] = SimpleUploadedFile("cool-cv.pdf", minimal_pdf)

    response = views.application_create(
        request,
        get_clamd=lambda: NullClamd(),
    )
    assert response.status_code == expected_status

    document = Document.objects.get()
    assert document.file.name.startswith(f"volunteer_{document.volunteer.id}_cv")
    assert document.file.name.endswith(".pdf")


minimal_pdf = (
    Path(__file__).parent.parent.parent.parent.parent / "test-data/minimal.pdf"
).read_bytes()


@pytest.mark.django_db
def test_seeks_file_to_zero_before_passing_it_to_clamav(rf: RequestFactory):
    """If this is missed, the malware detection will not work!"""
    request = rf.post("", {"data": ApplicationDataFactory.create().as_json()})
    request.FILES["cv_file"] = SimpleUploadedFile("foo.pdf", minimal_pdf)

    clamd = mock.Mock()
    clamd.instream.return_value = {"stream": ("FOUND", "pytest")}

    views.application_create(request, get_clamd=lambda: clamd)

    assert clamd.instream.call_args[0][0].tell() == 0


@pytest.mark.django_db
def test_doesnt_crash_with_other_but_no_other_cert_field(rf: RequestFactory):
    # I'm not even sure how this would happen but it did 🤷
    data = ApplicationDataFactory.create(certificates=["other"]).to_dict()
    ensure_positions_exists(data["positions"])
    del data["other_certificate"]
    request = rf.post("", {"data": json.dumps(data)})

    response = views.application_create(request, get_clamd=lambda: NullClamd())
    assert HTTPStatus(response.status_code).is_success
    assert Volunteer.objects.exists()
    volunteer = Volunteer.objects.get()
    assert volunteer.certificates == ["other: <not specified>"]
