import json
import logging
import os
from collections.abc import Callable
from dataclasses import dataclass
from datetime import date, datetime, timezone
from http import HTTPStatus
from os.path import splitext
from typing import assert_never, cast
from uuid import UUID

import magic
from django.conf import settings
from django.core import mail
from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.files import File
from django.db.transaction import atomic
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_http_methods
from django.views.generic.base import TemplateView
from jsonschema import validate
from jsonschema.exceptions import ValidationError as SchemaValidationError

import volunteers.models
from applications.models import SarApplication
from operations.models import Availability, Operation
from planner_common import clamav, feature_flags, schema
from planner_common.models import Gender
from public.clamd import get_clamd, send_malware_notification
from seawatch_registration.models import Position
from volunteers.models import Document, Volunteer

from .forms import ApplicationForm, ClamdUnexpectedOutput, RankRelatedDocumentsForm
from .models import Inquiry, UnassociatedUploadedFile

logger = logging.getLogger(__name__)


@dataclass
class FetchedFile:
    content: bytes
    extension: str  # includes .


@csrf_exempt
@require_POST
def upload_button_upload_file(request: HttpRequest) -> HttpResponse:
    file = UnassociatedUploadedFile(file=request.FILES["file"])
    try:
        file.full_clean()
    except DjangoValidationError as e:
        response_data = {"valid": False, "errors": e.messages}
    else:
        file.save()
        response_data = {"valid": True, "uid": file.uid}

    return JsonResponse(response_data)


@require_http_methods(["GET", "POST"])
def new_application_form(request: HttpRequest):
    match request.method:
        case "POST":
            raise NotImplementedError()
        case "GET":
            form = ApplicationForm()
            return TemplateResponse(
                request, "public/applications/application_page.html", {"form": form}
            )
        case x:
            assert_never(x)  # type: ignore


class HomeView(TemplateView):
    template_name = "public/applications/index.html"


@require_POST
@csrf_exempt
def application_create(
    request: HttpRequest,
    get_clamd: Callable[[], clamav.ClamdOrNull] = get_clamd,
):
    # we were here, I just added TRY/CATCH for applications without a CV
    data = json.loads(request.POST["data"])
    cv_file = None

    try:
        validate(instance=data, schema=schema.initial_application)
    except SchemaValidationError as e:
        return JsonResponse(
            status=HTTPStatus.UNPROCESSABLE_ENTITY,
            data={"ok": False, "message": e.args[0]},
        )

    if date.fromisoformat(data["date_of_birth"]) > date.today():
        return JsonResponse(
            status=HTTPStatus.UNPROCESSABLE_ENTITY,
            data={"ok": False, "message": "date_of_birth must not be in the future"},
        )

    if cv_file := request.FILES.get("cv_file"):
        # if cv_file, check for security
        # check size
        if cv_file.size > 10 * 1024 * 1024:
            return JsonResponse(
                status=HTTPStatus.REQUEST_ENTITY_TOO_LARGE,
                data={"message": "Maximum file size is 10 megabytes"},
            )

        # check extensions (py magic)
        mimetype = magic.from_buffer(cv_file.read(), mime=True)
        if mimetype != "application/pdf":
            return HttpResponseBadRequest("Only pdf allowed")
        # virus scanner (clam anti virus)
        cd = get_clamd()
        # django gives us the file with the cursor at the end, so this without this seek we will get false negatives!
        cv_file.seek(0)
        scan_result = cd.instream(cv_file)

        if scan_result is None:
            raise Exception("unexpected clamav None")

        match scan_result["stream"]:
            case ("FOUND", malware_type):
                _application_create_send_confirmation_mail(data)
                send_malware_notification("Application", data, malware_type)
                return HttpResponse(status=HTTPStatus.NO_CONTENT)
            case ("OK", _):
                pass
            case _:
                raise Exception("unexpected clamav result")

    with atomic(durable=True):
        # We want the data to be saved even if email sending fails
        now = datetime.now(timezone.utc)
        volunteer = Volunteer.objects.create(
            **{
                field: data[field]
                for field in [
                    "first_name",
                    "last_name",
                    "phone_number",
                    "email",
                    "nationalities",
                    "motivation",
                    "qualification",
                ]
            },
            gender=Gender(data["gender"]),
            date_of_birth=date.fromisoformat(data["date_of_birth"]),
            # TODO: If we allow applications for new positions from existing volunteers, we need to
            # update this probably or the UI will show the date they *first* applied, which might be
            # confusing.
            applied_at=now,
            languages={
                language["language"]: language["points"]
                for language in data["spoken_languages"]
            },
            homepage=data.get("homepage", ""),
            certificates=[
                f"other: {data.get('other_certificate', '<not specified>')}"
                if cert == "other"
                else cert
                for cert in data["certificates"]
            ],
        )
        if cv_file:
            cv = Document(
                volunteer=volunteer,
                type=Document.Type.cv,
                file=cv_file,
                metadata={},
            )
            cv.file.name = f"volunteer_{volunteer.id}_cv{splitext(cv.file.name)[1]}"
            cv.save()

        def fixup_position_key(key):
            if key == "rib-driver":
                # Spelling has been changed
                return "rhib-driver"
            else:
                return key

        SarApplication.objects.bulk_create(
            [
                SarApplication(
                    volunteer=volunteer,
                    created=now,
                    position=Position.objects.get(key=fixup_position_key(position_key)),
                )
                for position_key in data["positions"]
            ]
        )

    try:
        _application_create_send_confirmation_mail(data)
    except:  # noqa: E722
        logger.exception(
            "Error sending application confirmation mail to applicant",
            extra={"applicant_email": data.get("email", "<unknown>")},
        )

    return HttpResponse(status=HTTPStatus.NO_CONTENT)


def _application_create_send_confirmation_mail(data):
    mail.send_mail(
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[data["email"]],
        subject="Thanks for signing up",
        message=render_to_string(
            "public/applications/email/application_submitted.txt",
            {"first_name": data["first_name"]},
        ),
        fail_silently=False,
    )


def inquiry_form(
    request: HttpRequest,
    uid: UUID,
    get_clamd=get_clamd,
) -> HttpResponse:
    inquiry = get_object_or_404(Inquiry, uid=uid)
    match Inquiry.Type(inquiry.type):
        case Inquiry.Type.AVAILABILITY:
            previous_answers = {
                a.operation.id: a.availability
                for a in Availability.objects.filter(
                    volunteer=inquiry.volunteer, operation__start_date__gt=date.today()
                )
            }
            operations = Operation.objects.filter(start_date__gt=date.today())
            return render(
                request,
                "public/inquiries/availability.html",
                context={
                    "inquiry": inquiry,
                    "form_data": [
                        {
                            "operation_id": operation.id,
                            "start_date": operation.start_date.isoformat(),
                            "end_date": operation.end_date.isoformat(),
                            "previous_answer": previous_answers.get(operation.id),
                        }
                        for operation in operations
                    ],
                },
            )
        case Inquiry.Type.RANK_RELATED_DOCUMENTS:
            if request.method == "POST":
                redirect_or_form = _inquiry_form_RankRelatedDocuments_Post(
                    request,
                    inquiry,
                    get_clamd=get_clamd,
                )
                if isinstance(redirect_or_form, HttpResponse):
                    return redirect_or_form
                else:
                    form = redirect_or_form
            else:
                # Fetch all relevant documents
                basic_safety_docs = inquiry.volunteer.document_set.filter(
                    type=Document.Type.stcw_basic_safety
                )
                medical_fitness_docs = inquiry.volunteer.document_set.filter(
                    type=Document.Type.medical_fitness
                )
                watchkeeping_docs = inquiry.volunteer.document_set.filter(
                    type=Document.Type.stcw_watchkeeping
                )

                # Order documents in Python Order documents by valid_until

                current_date = date.today()

                basic_cert = max(
                    (
                        d
                        for d in basic_safety_docs
                        if (d.valid_until or date.min) >= current_date
                    ),
                    key=lambda d: d.valid_until or date.min,
                    default=None,
                )
                medic_cert = max(
                    (
                        d
                        for d in medical_fitness_docs
                        if (d.valid_until or date.min) >= current_date
                    ),
                    key=lambda d: d.valid_until or date.min,
                    default=None,
                )
                watchkeeping_cert = max(
                    (
                        d
                        for d in watchkeeping_docs
                        if (d.valid_until or date.min) >= current_date
                    ),
                    key=lambda d: d.valid_until or date.min,
                    default=None,
                )

                form = RankRelatedDocumentsForm(
                    basic_cert=basic_cert,
                    medic_cert=medic_cert,
                    watchkeeping_cert=watchkeeping_cert,
                )
            return render(
                request,
                "public/inquiries/rank_related_documents.html",
                {"inquiry": inquiry, "form": form},
            )
        case _x:
            assert_never(_x)


def _inquiry_form_RankRelatedDocuments_Post(
    request: HttpRequest,
    inquiry: Inquiry,
    get_clamd=get_clamd,
) -> HttpResponseRedirect | RankRelatedDocumentsForm:
    success_url_name = "inquiry_success_rank_related_documents"
    if feature_flags.features["check_if_document_is_present"]:
        raise NotImplementedError()
    form = RankRelatedDocumentsForm(
        request.POST,
        request.FILES,
        basic_cert=None,
        medic_cert=None,
        watchkeeping_cert=None,
    )
    if form.is_valid():
        clamd = get_clamd()
        malware_found = False
        for file_field in [
            "basic_safety_certificate",
            "medical_fitness_certificate",
            "watchkeeping_license_certificate",
        ]:
            # This is very similar to what happens in applications.views.application_create, we
            # should probably factor this into a helper function
            f: File = form.cleaned_data[file_field]
            if not (f):
                continue
            f.seek(0)

            match clamd.instream(f):
                case {"stream": ("FOUND", malware_type)}:
                    send_malware_notification(
                        context="Rank Related Documents",
                        data={
                            "volunteer_id": inquiry.volunteer.id,
                            "which file?": f"in {file_field}",
                            **form.cleaned_data,
                        },
                        malware_type=malware_type,
                    )
                    malware_found = True
                case {"stream": ("OK", None)}:
                    pass
                case scan_result:
                    raise ClamdUnexpectedOutput(
                        "Clamd returned unexpected value", scan_result
                    )
        if not malware_found:
            # TODO: Add a transaction back into the loop if we need it
            volunteer = inquiry.volunteer

            # TODO: Make this even leaner
            def mk_name(file: File, type: volunteers.models.Document.Type) -> str:
                # TODO: This is using a specific value but it shouldnt I think?
                return f"{volunteer.first_name}_{volunteer.last_name}_{type.replace(':', '_')}_{form.cleaned_data['basic_safety_certificate_validity_date']}{os.path.splitext(file.name)[1].lower()}"

            form.cleaned_data["basic_safety_certificate"].name = mk_name(
                form.cleaned_data["basic_safety_certificate"],
                volunteers.models.Document.Type.stcw_basic_safety,
            )
            form.cleaned_data["medical_fitness_certificate"].name = mk_name(
                form.cleaned_data["medical_fitness_certificate"],
                volunteers.models.Document.Type.medical_fitness,
            )
            if form.cleaned_data["watchkeeping_license_certificate"]:
                form.cleaned_data["watchkeeping_license_certificate"].name = mk_name(
                    form.cleaned_data["watchkeeping_license_certificate"],
                    volunteers.models.Document.Type.stcw_watchkeeping,
                )
            files = [
                (
                    volunteers.models.Document.Type.stcw_basic_safety,
                    form.cleaned_data["basic_safety_certificate_validity_date"],
                    form.cleaned_data["basic_safety_certificate"],
                ),
                (
                    volunteers.models.Document.Type.medical_fitness,
                    form.cleaned_data["medical_fitness_certificate_validity_date"],
                    form.cleaned_data["medical_fitness_certificate"],
                ),
                *(
                    [
                        (
                            volunteers.models.Document.Type.stcw_watchkeeping,
                            form.cleaned_data[
                                "watchkeeping_license_certificate_validity_date"
                            ],
                            form.cleaned_data["watchkeeping_license_certificate"],
                        ),
                    ]
                    if (form.cleaned_data["watchkeeping_license_certificate"])
                    else []
                ),
            ]
            for type, valid_until, file in files:
                volunteer.document_set.create(
                    type=type,
                    file=file,
                    metadata={"valid_until": valid_until.isoformat()},
                    valid_until=valid_until,
                    created=inquiry.created,
                )

        inquiry.delete()

        # send confirmation email to volunteer with what documents where uploaded
        # (no actual files) and the validity dates
        mail.send_mail(
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[inquiry.volunteer.email],
            subject="Document upload confirmed",
            message=render_to_string(
                "public/inquiries/email/rank_related_documents_confirmed.txt",
                {
                    "inquiry": inquiry,
                    "rank_documents": {
                        "basic_safety_validity_date": form.cleaned_data[
                            "basic_safety_certificate_validity_date"
                        ],
                        "medical_fitness_validity_date": form.cleaned_data[
                            "medical_fitness_certificate_validity_date"
                        ],
                        "watchkeeping_license_validity_date": form.cleaned_data[
                            "watchkeeping_license_certificate_validity_date"
                        ],
                    },
                },
            ),
            fail_silently=False,
        )
        return HttpResponseRedirect(reverse(success_url_name))
    return form


inquiry_success = cast(
    Callable[[HttpRequest], TemplateResponse],
    TemplateView.as_view(template_name="public/inquiries/inquiry_success.html"),
)


@require_POST
@csrf_exempt
def application_update_availabilities(request):
    # Validate input
    try:
        data = json.loads(request.body)
        token = UUID(data["token"])
    except (json.JSONDecodeError, KeyError, ValueError):
        return JsonResponse(
            status=422, data={"ok": False, "message": "Token is missing or invalid"}
        )

    try:
        validate(instance=data, schema=schema.availability)
    except SchemaValidationError as e:
        return JsonResponse(status=422, data={"ok": False, "message": e.args[0]})

    with atomic(durable=True):
        try:
            inquiry = Inquiry.objects.get(uid=token, type=Inquiry.Type.AVAILABILITY)
        except Inquiry.DoesNotExist:
            return JsonResponse(
                status=422, data={"ok": False, "message": "Token is missing or invalid"}
            )

        for answer in data["answers"]:
            Availability.objects.update_or_create(
                volunteer=inquiry.volunteer,
                operation=Operation.objects.get(id=answer["operation_id"]),
                defaults={"availability": Availability.Choices(answer["answer"])},
            )

        inquiry.delete()

        mail.send_mail(
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[inquiry.volunteer.email],
            subject="Operation availability confirmed",
            message=render_to_string(
                "public/inquiries/email/availability_confirmed.txt",
                {
                    "inquiry": inquiry,
                    "operations": [
                        {
                            "start_date": a.operation.start_date,
                            "end_date": a.operation.end_date,
                            "answer": a.availability,
                        }
                        for a in Availability.objects.filter(
                            volunteer=inquiry.volunteer
                        )
                    ],
                },
            ),
            fail_silently=False,
        )

    return HttpResponse(status=204)
