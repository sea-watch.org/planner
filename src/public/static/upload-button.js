const template = document.createElement("template");
// TODO: required hidden input validation
template.innerHTML = `
  <input type="hidden" data-replace="name"></input>
  <input type="file" accept="application/pdf" style="display: none;"></input>
  <button type="button" data-replace="id">Select file (PDF only, max. 10MB)</button>
`;

class UploadButton extends HTMLElement {
  constructor() {
    super();

    this.initialRenderContent();

    /** @type {HTMLInputElement} */
    this.hiddenInput = this.querySelector("input[type='hidden']");

    /** @type {HTMLInputElement} */
    this.fileInput = this.querySelector("input[type='file']");

    /** @type {HTMLButtonElement} */
    this.button = this.querySelector("button");

    this.initialConnectEventHandlers();
  }

  initialRenderContent() {
    const fragment = template.content.cloneNode(true);

    /** @type {HTMLElement[]} */
    const elementsWithAttributeChanges = fragment.querySelectorAll("[data-replace]");
    for (const element of elementsWithAttributeChanges) {
      const attribute = element.dataset.replace;
      element.setAttribute(attribute, this.getAttribute(attribute));
      this.removeAttribute(attribute);
    }

    this.innerHTML = "";
    this.appendChild(fragment);
  }

  initialConnectEventHandlers() {
    this.button.addEventListener("click", () => this.fileInput.click());
    this.fileInput.addEventListener("change", async () => {
      // TODO #262 network error
      // TODO #262 500 return code
      const data = new FormData();
      data.append("file", this.fileInput.files[0]);
      const response = await fetch("/applications/upload-button-upload-file/", {
        method: "POST",
        body: data,
      });
      const responseData = await response.json();

      this.querySelector(".errorlist")?.remove();

      if (responseData.valid) {
        this.hiddenInput.value = responseData.uid;
        this.button.setCustomValidity("");
      } else {
        this.hiddenInput.value = "";

        //TODO #262 make sure validation error if type is selected but a file is never selected
        this.button.setCustomValidity("Please select a valid file");

        const errorlist = document.createElement("ul");
        errorlist.classList.add("errorlist");
        errorlist.append(
          ...responseData.errors.map((error) => {
            const li = document.createElement("li");
            li.textContent = error;
            return li;
          }),
        );
        this.append(errorlist);
      }
    });
  }
}
customElements.define("upload-button", UploadButton);
