from inspect import cleandoc
from django.conf import settings
from django.conf.urls.static import static
from django.http import HttpResponse
from django.urls import include, path
from django.views.generic.base import TemplateView

from planner_common.feature_flags import features

from . import views

urlpatterns = (
    [
        path(
            "",
            (
                views.new_application_form
                if features["application_wizard"]
                else views.HomeView.as_view()
            ),
            name="application_form",
        ),
        path(
            "applications/create/", views.application_create, name="application_create"
        ),
        path(
            "applications/application_success/",
            TemplateView.as_view(
                template_name="public/applications/application_success.html"
            ),
            name="application_success",
        ),
        path(
            "applications/upload-button-upload-file/",  # Careful when changing, this url is referenced in javascript
            views.upload_button_upload_file,
        ),
        path(
            "inquiries/inquiry_form/<uuid:uid>/",
            views.inquiry_form,
            name="inquiry_form",
        ),
        path(
            "inquiries/inquiry_success/",
            views.inquiry_success,
            name="inquiry_success",
        ),
        path(
            "inquiries/inquiry_success/rank_related_documents/",
            TemplateView.as_view(
                template_name="public/inquiries/rank_related_documents_success.html"
            ),
            name="inquiry_success_rank_related_documents",
        ),
        path(
            "inquiries/update/",
            views.application_update_availabilities,
            name="application_update_availabilities",
        ),
        *(
            [path("_testing/", include("testing_api.urls"))]
            if (
                settings.ENABLE_TESTING_API
                == "I 100% know what I'm doing by enabling this"
            )
            else []
        ),
        path("robots.txt", lambda _: HttpResponse("User-agent: *\nDisallow: /\n")),
        path(
            ".well-known/traffic-advice",
            lambda _: HttpResponse(
                cleandoc(
                    """
                    [{
                        "user_agent": "prefetch-proxy",
                        "google_prefetch_proxy_eap": {
                            "fraction": 1.0
                        }
                    }]
                    """
                ),
                content_type="application/trafficadvice+json",
            ),
        ),
    ]
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # The `static` function actually doesn't do anything if not in
    # DEBUG, see https://docs.djangoproject.com/en/4.0/howto/static-files/#serving-files-uploaded-by-a-user-during-development
    # This is fine since we're using S3 comppatible storage in prod and staging
)

if settings.DEBUG:
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
        path("__reload__/", include("django_browser_reload.urls")),
    ]
