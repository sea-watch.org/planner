from __future__ import annotations

from datetime import datetime, timezone
from uuid import uuid4

from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from volunteers.models import Volunteer

from .validators import validate_file_size, validate_file_type


def now():
    return datetime.now(tz=timezone.utc)


class UnassociatedUploadedFile(models.Model):
    """A file we don't know anything about yet because the enclosing form is yet to be submitted"""

    file = models.FileField(validators=[validate_file_size, validate_file_type])
    uid = models.UUIDField(default=uuid4, editable=False)

    created_at = models.DateTimeField(default=now)


class Inquiry(models.Model):
    """An inquiry to a volunteer. Will be deleted when the volunteer fills the form."""

    id = models.AutoField(primary_key=True)
    uid = models.UUIDField(default=uuid4, editable=False)
    created = models.DateTimeField(editable=False)

    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)

    class Type(models.TextChoices):
        AVAILABILITY = "AVAILABILITY", _("availability")
        RANK_RELATED_DOCUMENTS = "RANK_RELATED_DOCUMENTS", _("rank_related_documents")

    type = models.CharField(max_length=255, choices=Type.choices)

    def save(self, *args, **kwargs):
        """On save, create timestamp"""
        if not self.id and self.created is None:
            self.created = datetime.now(timezone.utc)
        return super().save(*args, **kwargs)

    def form_url(self):
        return reverse(
            "inquiry_form", kwargs={"uid": self.uid}, urlconf=settings.PUBLIC_URLCONF
        )
