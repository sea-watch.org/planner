import htpy as h


def select(context, request):
    # from django/forms/templates/django/forms/widgets/select.html
    widget = context["widget"]

    def mk_attrs(attrs):
        # stringify the value if it's not bool to match default django template
        return {k: v if isinstance(v, bool) else str(v) for k, v in attrs.items()}

    optgroups = (
        {
            "name": group_name,
            "options": [
                h.option(
                    value=str(option.value),
                    **mk_attrs(widget["attrs"]),
                )[option.label]
                for option in group_choices
            ],
        }
        for group_name, group_choices, group_index in widget["optgroups"]
    )

    # If there's a name, we wrap it in an <optgroup>
    optgroups = (
        h.optgroup(label=group["name"])[group["options"]]
        if group["name"]
        else group["options"]
        for group in optgroups
    )

    return h.select(
        name=widget["name"],
        **mk_attrs(widget["attrs"]),
    )[optgroups]
