from collections.abc import Callable

from django.http import HttpRequest, HttpResponse

_csp_key = "Content-Security-Policy"


class ContentSecurityPolicyMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        response = self.get_response(request)

        if not response.has_header(_csp_key):
            response[_csp_key] = "frame-ancestors 'none';"

        return response
