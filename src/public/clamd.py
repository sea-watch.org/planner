from django.conf import settings
from django.core import mail

from planner_common import clamav


def get_clamd() -> clamav.ClamdOrNull:
    if settings.CLAMD_URL == "testing-nullclamd":
        return clamav.NullClamd()
    else:
        return clamav.clamd_from_url(settings.CLAMD_URL)


def send_malware_notification(context: str, data: dict, malware_type: str | None):
    mail.mail_admins(
        "Suspicious attachment",
        "\n".join(
            [
                f"Signature: {malware_type or '<Unknown>'}",
                "",
                context,
                *[f"  {k}:\n    {v}" for k, v in data.items()],
            ]
        ),
    )
