"""Theses settings are used for build steps in the dockerfile"""

import os

from .base import *  # noqa: F403
from .base import env

STATIC_ROOT = env("PLANNER_STATIC_ROOT")

#  Add configuration for static files storage using whitenoise
STATICFILES_DIRS = ["/tmp/public-client/dist"]  # nosec hardcoded_tmp_directory

# XXX: This is an ugly hack. If we have the staticfiles backend defined, the checks which django
# automatically runs at the start of ./manage.py error out since some files (actually all of them)
# aren't found in the manifest (which doesn't exist).
# We need to run `./manage.py tailwind install` and `build` _before_ collectstatic though, so we
# make this overridable.
# An alternative could be to run collectstatic twice, but I decided for this.
if "PLANNER_BUILD_WORKAROUND_SKIP_STORAGES" not in os.environ:
    STORAGES = {
        "default": {
            # This is not really a valid storage, since it will error on use, which is what we want here
            # because otherwise we might lose stuff in the build process.
            "BACKEND": "django.core.files.storage.Storage",
        },
        "staticfiles": {
            "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
        },
    }
