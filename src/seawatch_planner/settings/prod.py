import email.utils
import logging
import os

import ldap
from django_auth_ldap.config import LDAPGroupQuery, LDAPSearch, PosixGroupType

from .build import *  # noqa: F403
from .build import (
    env,
    MIDDLEWARE,
    BACKOFFICE_URLCONF,
    PUBLIC_URLCONF,
    WEBPACK_LOADER_DEFAULT_DEFAULTS,
)


def group_query_from_env(varname):
    val = env(varname)
    assert isinstance(val, str)
    groups = val.split(":")
    query = LDAPGroupQuery(groups.pop(0))
    for group in groups:
        query |= LDAPGroupQuery(group)

    return query


DEBUG = False

if not MIDDLEWARE[0].endswith(".SecurityMiddleware"):
    raise Exception(f"MIDDLEWARE setup isn't as expected ({repr(MIDDLEWARE)})")

MIDDLEWARE = [
    MIDDLEWARE[0],
    "whitenoise.middleware.WhiteNoiseMiddleware",
    *MIDDLEWARE[1:],
]


# Must contain a list of all public hostnames for the service as a comma-separated list.
# See https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts

BACKOFFICE_HOSTS = env.list("PLANNER_BACKOFFICE_HOSTS")
PUBLIC_HOSTS = env.list("PLANNER_PUBLIC_HOSTS")
assert isinstance(BACKOFFICE_HOSTS, list)
assert isinstance(PUBLIC_HOSTS, list)
ALLOWED_HOSTS = [*BACKOFFICE_HOSTS, *PUBLIC_HOSTS]
CSRF_TRUSTED_ORIGINS = [f"https://{h}" for h in [*BACKOFFICE_HOSTS, *PUBLIC_HOSTS]]
VHOSTS = {
    **{host: BACKOFFICE_URLCONF for host in BACKOFFICE_HOSTS},
    **{host: PUBLIC_URLCONF for host in PUBLIC_HOSTS},
}

SECRET_KEY = env.str("PLANNER_SECRET_KEY")

# EMAIL_URL="smtp://user:password@hostname:25". See EMAIL_SCHEMES at
# https://github.com/joke2k/django-environ/blob/master/environ/environ.py for more
# details,
vars().update(env.email_url("PLANNER_EMAIL_URL"))
EMAIL_URL_HOST = env.str("PLANNER_EMAIL_URL_HOST")

STORAGES = {
    "default": {
        "BACKEND": "storages.backends.s3boto3.S3Boto3Storage",
        "OPTIONS": {
            "bucket_name": env("PLANNER_AWS_STORAGE_BUCKET_NAME"),
            "endpoint_url": env("PLANNER_AWS_S3_ENDPOINT_URL"),
            "access_key": env("PLANNER_AWS_S3_ACCESS_KEY_ID"),
            "secret_key": env("PLANNER_AWS_S3_SECRET_ACCESS_KEY"),
            "file_overwrite": False,
        },
    },
    "staticfiles": {
        "BACKEND": "whitenoise.storage.CompressedManifestStaticFilesStorage",
    },
}


# PLANNER_ADMINS=Full Name <email-with-name@example.com>,anotheremailwithoutname@example.com
_planner_admins = env("PLANNER_ADMINS")
assert isinstance(_planner_admins, str)
ADMINS = email.utils.getaddresses([_planner_admins])

DEFAULT_FROM_EMAIL = env("PLANNER_DEFAULT_FROM_EMAIL")
SERVER_EMAIL = env("PLANNER_ERROR_EMAIL_FROM")
# INTERNAL_EMAIL_FROM = env("PLANNER_INTERNAL_EMAIL_FROM")
EXTERNAL_EMAIL_FROM = env("PLANNER_EXTERNAL_EMAIL_FROM")

DATABASES = {"default": env.db("PLANNER_DATABASE_URL")}

if os.environ.get("PLANNER_AUTH_LDAP_SERVER_URI"):
    AUTH_LDAP_SERVER_URI = env("PLANNER_AUTH_LDAP_SERVER_URI")
    AUTH_LDAP_BIND_DN = env("PLANNER_AUTH_LDAP_BIND_DN")
    AUTH_LDAP_BIND_PASSWORD = env("PLANNER_AUTH_LDAP_BIND_PASSWORD")
    AUTH_LDAP_USER_SEARCH = LDAPSearch(
        env("PLANNER_AUTH_LDAP_USER_SEARCH"),
        ldap.SCOPE_SUBTREE,  # type: ignore
        "(uid=%(user)s)",
    )
    AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
        env("PLANNER_AUTH_LDAP_GROUP_SEARCH"),
        ldap.SCOPE_SUBTREE,  # type: ignore
        "(objectClass=posixGroup)",
    )
    AUTH_LDAP_GROUP_TYPE = PosixGroupType()
    AUTH_LDAP_REQUIRE_GROUP = group_query_from_env("PLANNER_AUTH_LDAP_REQUIRE_GROUP")
    AUTH_LDAP_CONNECTION_OPTIONS = {
        ldap.OPT_X_TLS_CACERTFILE: env("PLANNER_AUTH_LDAP_CA_CERT_PATH"),  # type: ignore
        ldap.OPT_X_TLS_REQUIRE_CERT: ldap.OPT_X_TLS_DEMAND,  # type: ignore
        ldap.OPT_X_TLS_NEWCTX: 0,  # type: ignore
    }

    AUTHENTICATION_BACKENDS = ["django_auth_ldap.backend.LDAPBackend"]
    AUTH_LDAP_USER_ATTR_MAP = {
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mailPrimaryAddress",
    }
    AUTH_LDAP_FIND_GROUP_PERMS = True
    AUTH_LDAP_USER_FLAGS_BY_GROUP = {
        "is_staff": group_query_from_env("PLANNER_AUTH_LDAP_REQUIRE_GROUP")
    }


class SkipWaitressQueueDepthWarning(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        if record.name == "waitress.queue" and record.levelno == logging.WARNING:
            return False
        else:
            return True


class Skip404Errors(logging.Filter):
    def filter(self, record: logging.LogRecord) -> bool:
        if (
            record.name == "django.request"
            and isinstance(record.args, tuple)
            and record.args[0] == "Not Found"
        ):
            return False
        else:
            return True


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "Skip404Errors": {"()": "seawatch_planner.settings.prod.Skip404Errors"},
        "SkipWaitressQueueDepthWarning": {
            "()": "seawatch_planner.settings.prod.SkipWaitressQueueDepthWarning"
        },
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
        },
        "mail_admins": {
            "level": "WARNING",
            "filters": ["Skip404Errors", "SkipWaitressQueueDepthWarning"],
            "class": "django.utils.log.AdminEmailHandler",
        },
    },
    "root": {
        "handlers": ["console", "mail_admins"],
        "level": "WARNING",
    },
}

SHOW_ENV_WARNING = env(
    "PLANNER_SHOW_ENV_WARNING",
    default=None,  # type: ignore
)

BASE_URL = env("PLANNER_BASE_URL")
WEBPACK_LOADER = {
    "DEFAULT": {
        **WEBPACK_LOADER_DEFAULT_DEFAULTS,
        "CACHE": True,
        "STATS_FILE": "/tmp/public-client/webpack-stats.json",  # nosec hardcoded_tmp_directory
    }
}
CLAMD_URL = env("PLANNER_CLAMD_URL")
