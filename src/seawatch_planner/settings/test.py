import atexit
from shutil import rmtree
from tempfile import mkdtemp

from .base import *

SKIP_JS = True

SECRET_KEY = "insecure"

DATABASES = {"default": env.db("PLANNER_DATABASE_URL")}

BACKOFFICE_HOSTS = ["backoffice.localhost"]
PUBLIC_HOSTS = ["public.localhost"]
VHOSTS = {
    **{host: BACKOFFICE_URLCONF for host in BACKOFFICE_HOSTS},
    **{host: PUBLIC_URLCONF for host in PUBLIC_HOSTS},
}
ALLOWED_HOSTS = [*BACKOFFICE_HOSTS, *PUBLIC_HOSTS]

MEDIA_ROOT = mkdtemp()
atexit.register(lambda: rmtree(MEDIA_ROOT))

ENABLE_TESTING_API = "I 100% know what I'm doing by enabling this"
INSTALLED_APPS = [*INSTALLED_APPS[:-1], "testing_api", INSTALLED_APPS[-1]]
EXTERNAL_EMAIL_FROM = "test-backoffice-external@example.com"
EMAIL_URL_HOST = "random-hostname-for-testing.example.com"

BASE_URL = ""
CLAMD_URL = env("PLANNER_CLAMD_URL", default="testing-nullclamd")  # type: ignore
