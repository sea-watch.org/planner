from .base import *

PUBLIC_CLIENT_DIR = PROJECT_DIR + "public-client"

DEBUG = True

INSTALLED_APPS = [
    *INSTALLED_APPS,
    "debug_toolbar",
    "django_extensions",
    "django_browser_reload",
]

MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    *MIDDLEWARE,
    "django_browser_reload.middleware.BrowserReloadMiddleware",
]

INTERNAL_IPS = ["127.0.0.1"]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "insecure secret key"

ALLOWED_HOSTS = ["*"]
BACKOFFICE_HOSTS = ["backoffice.localhost"]
PUBLIC_HOSTS = ["public.localhost"]
VHOSTS = {
    **{host: BACKOFFICE_URLCONF for host in BACKOFFICE_HOSTS},
    **{host: PUBLIC_URLCONF for host in PUBLIC_HOSTS},
}

# Email Config
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
# EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"
EMAIL_FILE_PATH = PROJECT_DIR("sent_emails")
DEFAULT_FROM_EMAIL = "dev@planner.sea-watch.org"
EXTERNAL_EMAIL_FROM = "dev-backoffice-external@example.com"
EMAIL_URL_HOST = "example.com"

LOGIN_REQUIRED_URLS_EXCEPTIONS = [*LOGIN_REQUIRED_URLS_EXCEPTIONS, r"^/__debug__/"]

MEDIA_URL = "/media/"
MEDIA_ROOT = "media/"

DATABASES = {"default": env.db("PLANNER_DATABASE_URL")}

SHOW_ENV_WARNING = "dev"

BASE_URL = "http://localhost:8000"

ADMINS = [("Test", "foo@bar.de")]

# Left here to debug admin mails:
# LOGGING = {
#     "version": 1,
#     "disable_existing_loggers": False,
#     "handlers": {
#         "console": {
#             "level": "INFO",
#             "class": "logging.StreamHandler",
#         },
#         "mail_admins": {
#             "level": "WARNING",
#             "class": "django.utils.log.AdminEmailHandler",
#         },
#     },
#     "root": {
#         "handlers": ["console", "mail_admins"],
#         "level": "WARNING",
#     },
# }

WEBPACK_LOADER = {
    "DEFAULT": {
        **WEBPACK_LOADER_DEFAULT_DEFAULTS,
        "CACHE": False,
        "STATS_FILE": PUBLIC_CLIENT_DIR("webpack-stats.json"),
    }
}

STATICFILES_DIRS = [PUBLIC_CLIENT_DIR("dist")]
CLAMD_URL = env("PLANNER_CLAMD_URL", default="testing-nullclamd")  # type: ignore
