from django.conf import settings

import planner_common.feature_flags

colors = {
    "dev": "green",
    "staging": "hotpink",
}


def skip_js(request):
    try:
        skip_js = settings.SKIP_JS
    except AttributeError:
        skip_js = False

    return {"SKIP_JS": skip_js}


def env_warning(request):
    env = settings.SHOW_ENV_WARNING
    if not env:
        return {}
    return {"env_warning": {"color": colors[env], "message": " ".join([env] * 100)}}


def features(request=None):
    return {
        "features": {
            k: v
            for k, v in planner_common.feature_flags.features.items()
            if isinstance(v, bool)
        }
    }
