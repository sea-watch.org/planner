from typing import get_args

from django.test import override_settings
from django.urls import get_urlconf, set_urlconf
from pytest import mark

from .site import Site, site


@mark.parametrize("the_site", get_args(Site))
def test_it_changes_the_current_urlconf(the_site: Site):
    with override_settings(BACKOFFICE_URLCONF="bo", PUBLIC_URLCONF="pub"):
        with site(the_site):
            assert get_urlconf() == {"backoffice": "bo", "public": "pub"}[the_site]


def test_it_changes_it_back_afterwards():
    try:
        set_urlconf("yeah")
        with site("backoffice"):
            pass
        assert get_urlconf() == "yeah"
    finally:
        set_urlconf(None)


def test_it_changes_it_back_afterwards_on_exception():
    exc = Exception()
    try:
        set_urlconf("yeah")
        try:
            with site("backoffice"):
                raise exc
        except Exception as e:
            assert e == exc, "got a *different* exception somehow?!?"

        assert get_urlconf() == "yeah"
    finally:
        set_urlconf(None)
