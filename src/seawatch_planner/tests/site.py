from contextlib import contextmanager
from typing import Literal

from django.conf import settings
from django.urls import get_urlconf, set_urlconf

Site = Literal["backoffice", "public"]


@contextmanager
def site(site: Site):
    old = get_urlconf()
    try:
        set_urlconf(
            {
                "backoffice": settings.BACKOFFICE_URLCONF,
                "public": settings.PUBLIC_URLCONF,
            }[site]
        )
        yield
    finally:
        set_urlconf(old)
