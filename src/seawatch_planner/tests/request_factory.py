from __future__ import annotations
from typing import Literal, cast

from django.contrib.messages.storage.base import BaseStorage
from django.core.handlers.wsgi import WSGIRequest
import django.test


Site = Literal["backoffice", "public"]


class WSGIRequestWithStubMessages(WSGIRequest):
    _messages: StubMessageStorage


class RequestFactory(django.test.RequestFactory):
    def get(self, *args, **kwargs) -> WSGIRequestWithStubMessages:
        return super().get(*args, **kwargs)  # type: ignore

    def post(self, *args, **kwargs) -> WSGIRequestWithStubMessages:
        return super().post(*args, **kwargs)  # type: ignore

    def request(self, **request):
        r = super().request(**request)

        if not hasattr(r, "session"):
            # This should theoretically be a subclass of SessionBase but I'm pretty sure that this
            # is fine for our purposes.
            r.session = {}  # type: ignore

        r = cast(WSGIRequestWithStubMessages, r)
        r._messages = StubMessageStorage(r)

        return r


class StubMessageStorage(BaseStorage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.messages = []
        self.__stored_messages = []

    def _get(self, *args, **kwargs):
        return (self.__stored_messages, True)

    def _store(self, messages, response, *args, **kwargs):
        self.__stored_messages = self.__stored_messages + messages
        return []

    def add(self, level, message, extra_tags=""):
        super().add(level, message, extra_tags)
        self.messages.append((level, message, extra_tags))
