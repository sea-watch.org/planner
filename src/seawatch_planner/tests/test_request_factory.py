from .request_factory import RequestFactory


def test_requests_have_a_session():
    assert (
        getattr(
            RequestFactory().get("", site="backoffice"),
            "session",
            None,
        )
        is not None
    )
