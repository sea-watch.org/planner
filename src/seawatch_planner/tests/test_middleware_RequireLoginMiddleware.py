from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, override_settings
from django.urls import reverse
from pytest import mark
from pytest_subtests import SubTests

from seawatch_registration.tests.factories import UserFactory

from ..middleware import RequireLoginMiddleware

dummyResponse = object()


def respondStub(request):
    return dummyResponse


def test_denies_by_default(rf: RequestFactory, subtests: SubTests):
    request = rf.get(
        "/any/random/path",
        headers={"Host": settings.BACKOFFICE_HOSTS[0]},  # type: ignore
    )
    request.user = AnonymousUser()

    view_with_middleware = RequireLoginMiddleware(respondStub, exceptions=[])
    response = view_with_middleware(request)

    assert response.status_code == 302
    assert (
        response.url
        == f"{reverse('login', urlconf=settings.BACKOFFICE_URLCONF)}?next=/any/random/path"
    )

    with subtests.test("but allows public urls always"):
        public_urlconf = "whatever it just needs to match"
        with override_settings(PUBLIC_URLCONF=public_urlconf):
            request = rf.get("")
            request.urlconf = public_urlconf  # type: ignore
            response = view_with_middleware(request)
        assert response == dummyResponse


@mark.django_db
def test_allow_when_logged_in(rf):
    request = rf.get("/any/random/path")
    request.user = UserFactory.create()

    response = RequireLoginMiddleware(respondStub, exceptions=[])(request)

    assert response is dummyResponse


@mark.django_db
def test_allows_exceptions(rf):
    request = rf.get("/special/path")
    request.user = AnonymousUser()

    response = RequireLoginMiddleware(respondStub, exceptions=[r"/special"])(request)

    assert response is dummyResponse
