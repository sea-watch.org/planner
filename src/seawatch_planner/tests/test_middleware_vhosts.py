from unittest.mock import Mock

from django.http import Http404, HttpRequest
from django.test import RequestFactory, override_settings
from pytest import mark, raises

from seawatch_planner.middleware import vhosts


@mark.parametrize("scenario", ["without_port", "with_port"])
def test_sets_urlconf_based_on_host(scenario, rf: RequestFactory):
    mock_response = object()
    mock_view = Mock(return_value=mock_response)
    mock_urlconf = "mock.urlconf"

    with override_settings(
        ALLOWED_HOSTS=["cool.domain.example"],
        VHOSTS={"cool.domain.example": mock_urlconf},
    ):
        response = vhosts(mock_view)(
            rf.get(
                "",
                headers={
                    "Host": {
                        "without_port": "cool.domain.example",
                        "with_port": "cool.domain.example:1234",
                    }[scenario]
                },  # type: ignore
            )
        )

    assert response == mock_response

    (request,) = mock_view.call_args_list[0][0]
    assert isinstance(request, HttpRequest)
    assert getattr(request, "urlconf") == mock_urlconf


def test_returns_404_if_vhost_not_found(rf: RequestFactory):
    mock_view = Mock()

    with override_settings(
        ALLOWED_HOSTS=["cool.domain.example"],
        VHOSTS={},
    ):
        with raises(Http404):
            vhosts(mock_view)(
                rf.get(
                    "",
                    headers={"Host": "cool.domain.example"},  # type: ignore
                )
            )

    assert mock_view.call_count == 0


def test_ignores_port(rf: RequestFactory):
    mock_response = object()
    mock_view = Mock(return_value=mock_response)
    mock_urlconf = "mock.urlconf"

    with override_settings(
        ALLOWED_HOSTS=["cool.domain.example"],
        VHOSTS={"cool.domain.example": mock_urlconf},
    ):
        response = vhosts(mock_view)(
            rf.get(
                "",
                headers={"Host": "cool.domain.example:9000"},  # type: ignore
            )
        )

    assert response == mock_response

    (request,) = mock_view.call_args_list[0][0]
    assert isinstance(request, HttpRequest)
    assert getattr(request, "urlconf") == mock_urlconf
