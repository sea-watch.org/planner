from http import HTTPStatus
from typing import cast

from django.http import HttpResponse
from django.test import RequestFactory
from pytest import mark

from seawatch_planner.tests.site import site

from ..middleware import HttpResponseNotAllowedMiddleware


@mark.parametrize(
    "args",
    [
        (status, status in [HTTPStatus.BAD_REQUEST, HTTPStatus.METHOD_NOT_ALLOWED])
        for status in HTTPStatus
    ],
)
@mark.parametrize("site_", ["public", "backoffice"])
def test_adds_content_if_missing(args, site_, rf: RequestFactory):
    # When there's an error that a user shouldn't usually see, we don't want to return an empty
    # page but return a helpful error message inside a html template instead.
    (status, should_replace) = args
    with site(site_):
        response = HttpResponseNotAllowedMiddleware(
            lambda _: HttpResponse(status=status)
        )(rf.get(""))
    if should_replace:
        assert b"this page is not supposed to be accessed directly" in response.content
    else:
        assert not response.content


@mark.parametrize("status", HTTPStatus)
def test_preserves_content_if_present(status, rf: RequestFactory):
    # when response does have a content we need to keep it for functionality
    response = HttpResponseNotAllowedMiddleware(
        lambda _: HttpResponse("foo412345", status=status)
    )(rf.get(""))
    assert response.content == b"foo412345"


def test_preserves_response_for_FileResponse(
    rf: RequestFactory,
):
    # our own middleware accesses the content attribute of the response (which is good),
    # but now we send a FileResponse to get documents from public-backend and this middleware
    # would fail without our fix because FileResponse.content raises AttributeError
    class MockFileResponse:
        status_code = HTTPStatus.OK

        @property
        def content(self):
            raise AttributeError()

    mock_Response = cast(HttpResponse, MockFileResponse())
    response = HttpResponseNotAllowedMiddleware(lambda _: mock_Response)(rf.get(""))

    assert response == mock_Response
