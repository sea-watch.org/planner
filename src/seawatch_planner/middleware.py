import re
from http import HTTPStatus
from typing import Callable

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpRequest, HttpResponse
from django.template import loader


class RequireLoginMiddleware(object):
    """
    Middleware component that wraps the login_required decorator around
    all views except matching url patterns. To use, add the class to MIDDLEWARE_CLASSES
    and define LOGIN_REQUIRED_URLS_EXCEPTIONS in your settings.py.
    For example:
    ------
    LOGIN_REQUIRED_URLS_EXCEPTIONS = [
        r'/topsecret/login(.*)$',
        r'/topsecret/logout(.*)$',
    ]
    ------
    LOGIN_REQUIRED_URLS_EXCEPTIONS is where you define any exceptions
    (like login and logout URLs).
    """

    def __init__(
        self,
        get_response,
        exceptions: (
            list[str] | Callable[[], list[str]]
        ) = lambda: settings.LOGIN_REQUIRED_URLS_EXCEPTIONS,
    ):
        self.get_response = get_response
        if callable(exceptions):
            exceptions = exceptions()

        self.exceptions = list(re.compile(url) for url in exceptions)

    def __call__(self, request):
        if getattr(request, "urlconf", object()) == settings.PUBLIC_URLCONF or any(
            exception.match(request.path) for exception in self.exceptions
        ):
            return self.get_response(request)
        else:
            return login_required(self.get_response)(request)


class HttpResponseNotAllowedMiddleware:
    def __init__(self, get_response: Callable[[HttpRequest], HttpResponse]):
        self.get_response = get_response

    def __call__(self, request: HttpRequest) -> HttpResponse:
        response = self.get_response(request)

        status_code = response.status_code

        if (
            status_code
            in [
                HTTPStatus.BAD_REQUEST,
                HTTPStatus.METHOD_NOT_ALLOWED,
            ]
            and not response.content
        ):
            context = {}
            template = loader.get_template("400_405_error.html")
            return HttpResponse(template.render(context, request), status=status_code)

        return response


def ensurePasswordIsntInErrorMailMiddleware(get_response):
    def middleware(request):
        request.sensitive_post_parameters = ["password"]
        return get_response(request)

    return middleware


def vhosts(get_response: Callable[[HttpRequest], HttpResponse]):
    def middleware(request: HttpRequest):
        match settings.VHOSTS.get(request.get_host().split(":")[0]):
            case None:
                raise Http404()
            case urlconf:
                request.urlconf = urlconf  # type: ignore
                return get_response(request)

    return middleware
