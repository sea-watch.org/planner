from inspect import cleandoc
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponse
from django.urls import include, path
from django.views.generic.base import RedirectView, TemplateView

urlpatterns = [
    path("operations/", include("operations.urls")),
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("applications/", include("applications.urls")),
    path("volunteers/", include("volunteers.urls")),
    path("", RedirectView.as_view(pattern_name="application_list"), name="index"),
    *(
        [path("_testing/", include("testing_api.urls"))]
        if settings.ENABLE_TESTING_API == "I 100% know what I'm doing by enabling this"
        else []
    ),
    path("robots.txt", lambda _: HttpResponse("User-agent: *\nDisallow: /\n")),
    path(
        ".well-known/traffic-advice",
        lambda _: HttpResponse(
            cleandoc(
                """
                [{
                    "user_agent": "prefetch-proxy",
                    "google_prefetch_proxy_eap": {
                        "fraction": 1.0
                    }
                }]
                """
            ),
            content_type="application/trafficadvice+json",
        ),
    ),
    path("styleguide", TemplateView.as_view(template_name="styleguide.html")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# The `static` function actually doesn't do anything if not in
# DEBUG, see https://docs.djangoproject.com/en/4.0/howto/static-files/#serving-files-uploaded-by-a-user-during-development
# This is fine since we're using S3 comppatible storage in prod and staging

if settings.DEBUG:
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
        path("__reload__/", include("django_browser_reload.urls")),
    ]
