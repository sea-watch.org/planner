from django.urls import path

from . import views

urlpatterns = [
    path("", views.VolunteerWithSarProfileListView.as_view(), name="volunteer_list"),
    ##########
    ## Details
    path(
        "<int:pk>/",
        views.VolunteerPersonalDetailsView.as_view(),
        name="volunteer_detail",
    ),
    path("<int:pk>/autosave/", views.volunteer_update_autosave),
    path("<int:pk>/archive/", views.volunteer_archive, name="volunteer_archive"),
    ###########
    ## Subpages
    path(
        "<int:volunteer_id>/comments/",
        views.volunteer_comments_view,
        name="volunteer_comments",
    ),
    path(
        "<int:volunteer_id>/qualification/",
        views.volunteer_qualifications_view,
        name="volunteer_qualification",
    ),
    path(
        "<int:volunteer_id>/documents/",
        views.volunteer_documents_view,
        name="volunteer_documents",
    ),
    path(
        "<int:volunteer_id>/documents/<int:document_id>/edit",
        views.volunteer_document_edit,
        name="volunteer_document_edit",
    ),
    path(
        "<int:volunteer_id>/documents/create",
        views.volunteer_document_edit,
        name="volunteer_document_create",
    ),
    ############
    ## Inquiries
    path(
        "<int:volunteer_id>/request_rank_related_documents",
        views.request_rank_related_documents,
        name="volunteer_request_rank_related_documents",
    ),
    path(
        "<int:pk>/request_availabilites_dialog/",
        views.volunteer_request_availabilities_dialog,
        name="volunteer_request_availabilities_dialog",
    ),
    path(
        "request_availabilities/<int:volunteer_id>/",
        views.request_availabilities_view,
        name="volunteer_request_availabilities",
    ),
    ##############
    ## Experiences
    path(
        "<int:volunteer_id>/experience/",
        views.volunteer_experience_view,
        name="volunteer_experience",
    ),
    path(
        "<int:pk>/experience/add_experience/",
        views.ExperienceCreateView.as_view(),
        name="add_experience",
    ),
    path(
        "<int:pk>/experience/<int:exp_id>/update",
        views.ExperienceUpdateView.as_view(),
        name="update_experience",
    ),
    path(
        "<int:vol_id>/experience/<int:pk>/delete-dialog/",
        views.delete_experience_dialog,
        name="delete_experience_dialog",
    ),
    path(
        "<int:vol_id>/experience/<int:pk>/delete/",
        views.delete_experience,
        name="delete_experience",
    ),
    path("<int:pk>/experience/autosave/", views.experience_update_autosave),
]
