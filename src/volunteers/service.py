from datetime import date

from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string

from operations.models import Availability, Operation
from public.models import Inquiry
from volunteers.models import Volunteer

TIMEOUT_SECONDS = 10


def request_availabilities(volunteer: Volunteer) -> None:
    inquiry, _created = volunteer.inquiry_set.get_or_create(
        type=Inquiry.Type.AVAILABILITY
    )

    previous_answers = {
        a.operation.id: a.availability
        for a in Availability.objects.filter(
            volunteer=inquiry.volunteer, operation__start_date__gt=date.today()
        )
    }
    operations = Operation.objects.filter(start_date__gt=date.today())

    mail.send_mail(
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[volunteer.email],
        subject="Operation availability request",
        message=render_to_string(
            "public/inquiries/email/availability_requested.txt",
            {
                "inquiry": inquiry,
                "operations": [
                    {
                        "operation_id": operation.id,
                        "start_date": operation.start_date,
                        "end_date": operation.end_date,
                        "previous_answer": previous_answers.get(operation.id),
                    }
                    for operation in operations
                ],
                "form_url": f"https://{settings.EMAIL_URL_HOST}{inquiry.form_url()}",
            },
        ),
        fail_silently=False,
    )


def request_rank_related_documents(
    volunteer: Volunteer,
):
    inquiry, _created = volunteer.inquiry_set.get_or_create(
        type=Inquiry.Type.RANK_RELATED_DOCUMENTS
    )

    mail.send_mail(
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[inquiry.volunteer.email],
        subject="Please upload your documents",
        message=render_to_string(
            "public/inquiries/email/rank_related_documents_requested.txt",
            {
                "inquiry": inquiry,
                "form_url": f"https://{settings.EMAIL_URL_HOST}{inquiry.form_url()}",
            },
        ),
        fail_silently=False,
    )
