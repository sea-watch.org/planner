# Generated by Django 4.1.3 on 2022-11-22 11:21

import django.db.models.deletion
from django.apps.registry import Apps
from django.db import migrations, models


def sarprofiles_from_positions(apps: Apps, schema_editor):
    Volunteer = apps.get_model("volunteers", "Volunteer")
    SarProfile = apps.get_model("volunteers", "SarProfile")
    for volunteer in Volunteer.objects.all():
        SarProfile.objects.bulk_create(
            [
                SarProfile(volunteer=volunteer, position=p)
                for p in volunteer.positions.all()
            ]
        )


def positions_from_sarprofiles(apps, schema_editor):
    Volunteer = apps.get_model("volunteers", "Volunteer")
    apps.get_model("volunteers", "SarProfile")
    for volunteer in Volunteer.objects.all():
        volunteer.positions.set(
            [sarprofile.position for sarprofile in volunteer.sarprofile_set.all()]
        )


class Migration(migrations.Migration):
    dependencies = [
        ("volunteers", "0032_delete_malformedinquiry"),
    ]

    operations = [
        migrations.CreateModel(
            name="SarProfile",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "position",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="seawatch_registration.position",
                    ),
                ),
                (
                    "volunteer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="volunteers.volunteer",
                    ),
                ),
            ],
        ),
        migrations.RunPython(sarprofiles_from_positions, positions_from_sarprofiles),
        migrations.RemoveField(
            model_name="volunteer",
            name="positions",
        ),
    ]
