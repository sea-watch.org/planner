# Generated by Django 4.0.3 on 2022-03-24 16:54

from datetime import date, datetime, timezone

import django.db.models.deletion
from django.db import migrations, models
from django.db.migrations.operations.special import RunPython


def mk_refill_application(Application, Language, Comment, User):
    def fun(volunteer):
        data = volunteer.application_data
        volunteer.application = Application.objects.create(
            created=datetime(1, 1, 1, tzinfo=timezone.utc),
            accepted=datetime.now(timezone.utc),
            first_name=data.pop("first_name"),
            last_name=data.pop("last_name"),
            phone_number=data.pop("phone_number"),
            email=data.pop("email"),
            date_of_birth=date.fromisoformat(data.pop("date_of_birth")),
            gender=data.pop("gender"),
            certificates_csv=data.pop("certificates_csv"),
            motivation=data.pop("motivation"),
            qualification=data.pop("qualification"),
            nationalities=data.pop("nationalities"),
        )
        volunteer.save()

        # The positions saved in application_data are broken, but since we cannot edit
        # volunteer profiles yet, they must be still the same
        del data["positions"]
        volunteer.application.positions.set(volunteer.positions.all())

        Language.objects.bulk_create(
            [
                Language(application=volunteer.application, code=code, points=points)
                for (code, points) in data.pop("languages").items()
            ]
        )

        if "comments" in data:
            Comment.objects.bulk_create(
                [
                    Comment(
                        application=volunteer.application,
                        author=User.objects.get(username=c["author"]),
                        date=c["date"],
                        text=c["text"],
                    )
                    for c in data.pop("comments")
                ]
            )

        if data:
            warnings.warn(f"didn't process all data: {', '.join(data.keys())}")

    return fun


def recreate_applications(apps, schema_editor):
    Volunteer = apps.get_model("volunteers", "Volunteer")
    refill_application = mk_refill_application(
        Application=apps.get_model("applications", "Application"),
        Language=apps.get_model("applications", "Language"),
        Comment=apps.get_model("applications", "Comment"),
        User=apps.get_model("auth", "User"),
    )
    for volunteer in Volunteer.objects.filter(application=None):
        refill_application(volunteer)


class Migration(migrations.Migration):
    dependencies = [
        ("applications", "0031_comment"),
        (
            "volunteers",
            "0009_alter_malformedinquiry_uid_alter_volunteer_email_and_more",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="volunteer",
            name="application",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="applications.application",
            ),
        ),
        RunPython(recreate_applications),
    ]
