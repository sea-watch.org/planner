# Generated by Django 4.1.5 on 2023-01-13 15:20

import django.db
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("volunteers", "0032_delete_and_recreate_rankrelateddocuments"),
    ]

    operations = [
        migrations.DeleteModel(
            name="RankRelatedDocuments",
        ),
        migrations.CreateModel(
            name="RankRelatedDocuments",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "basic_safety",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="+",
                        to="volunteers.documentwithexpiry",
                    ),
                ),
                (
                    "medical_fitness",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="+",
                        to="volunteers.documentwithexpiry",
                    ),
                ),
                (
                    "volunteer",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="rank_related_documents",
                        to="volunteers.volunteer",
                    ),
                ),
                (
                    "watchkeeping_license",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="+",
                        to="volunteers.documentwithexpiry",
                    ),
                ),
            ],
        ),
    ]
