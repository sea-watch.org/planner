class AutosaveChoicesJS extends HTMLSelectElement {
  constructor() {
    super();

    const label = this.labels[0];
    if (!label.id) {
      // Generate an id real quick
      var arr = new Uint8Array(20);
      window.crypto.getRandomValues(arr);
      label.id = Array.from(arr, (d) => d.toString(16).padStart(2, "0")).join("");
    }
    this._choices = new Choices(this, {
      removeItemButton: true,
      duplicateItemsAllowed: false,
      allowHTML: false,
      shouldSort: false,
      labelId: label.id,
    });
    this.dispatchEvent(new Event("initialized"));

    this.addEventListener("htmx:responseError", (e) => {
      this.closest(".input-container").classList.add("autosave-error");
    });
  }
}
customElements.define("autosave-choices-js", AutosaveChoicesJS, { extends: "select" });
