var update_stcw_disabled = () => {
  const type_select = document.querySelector("[name=type]");
  const type = type_select.value;
  const form = type_select.form;
  for (const e of form.querySelectorAll("[data-metadata-for-type]")) {
    e.disabled = true;
  }
  if (type == "medical_fitness" || type.startsWith("stcw:")) {
    form.querySelector("[data-metadata-for-type=stcw]").disabled = false;
  }
};
addEventListener("DOMContentLoaded", () => {
  document
    .querySelector("[name=type]")
    .addEventListener("change", update_stcw_disabled);
  update_stcw_disabled();
});
