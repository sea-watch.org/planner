from __future__ import annotations

from datetime import datetime, timezone
from typing import TYPE_CHECKING, Any, ClassVar, assert_never, cast

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.query import QuerySet
from django.urls import reverse
from django.utils.translation import gettext as _
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.phonenumber import PhoneNumber

from planner_common import data
from planner_common.models import Gender
from seawatch_registration.models import Position, ShortTextField, URLField

if TYPE_CHECKING:
    from django.db.models.manager import RelatedManager

    from applications.models import SarApplicationManager
    from history.models import HistoryItemManager
    from operations.models import Availability
    from public.models import Inquiry

valid_language_codes = {lang["code"] for lang in data.languages}


def validate_languages_dict(value):
    if not isinstance(value, dict):
        raise ValidationError("languages must be a dict")
    for code, level in value.items():
        if code not in valid_language_codes:
            raise ValidationError(
                _("%(code)r is not a valid language code"), params={"code": code}
            )
        if level not in [0, 1, 2, 3]:
            raise ValidationError(
                _("%(level)r is not a valid language level"), params={"level": level}
            )


class VolunteerManager(models.Manager["Volunteer"]):
    def get_queryset(self):
        return super().get_queryset().filter(archived_at=None)

    def archived(self) -> QuerySet["Volunteer"]:
        return super().get_queryset().filter(archived_at__isnull=False)


class ExperienceLevels(models.TextChoices):
    UNKNOWN = "unknown", _("Unknown")
    NO_EXPERIENCE = "no_experience", _("No Experience")
    HAS_EXPERIENCE = "has_experience", _("Has Experience")

    def display(self):
        match self:
            case ExperienceLevels.UNKNOWN:
                return "Unknown"
            case ExperienceLevels.NO_EXPERIENCE:
                return "No"
            case ExperienceLevels.HAS_EXPERIENCE:
                return "Yes"
            case other:
                assert_never(other)


class Volunteer(models.Model):
    objects: ClassVar[VolunteerManager] = VolunteerManager()  # pyright: ignore [reportIncompatibleVariableOverride]

    ### fields

    id = models.AutoField(primary_key=True)
    first_name = ShortTextField()
    last_name = ShortTextField()
    phone_number = PhoneNumberField(blank=True)
    # TODO: Propper Email field
    email = ShortTextField()

    motivation = models.TextField(blank=True)
    qualification = models.TextField(blank=True)

    homepage = URLField(blank=True)

    date_of_birth = models.DateField()
    gender = ShortTextField(choices=Gender.choices)
    private_has_experience = ShortTextField(
        choices=ExperienceLevels.choices, default=ExperienceLevels.UNKNOWN
    )

    certificates = models.JSONField(
        blank=True
    )  # ["drivers-license", "more-things", "other: foo"]
    languages = models.JSONField(
        validators=[validate_languages_dict]
    )  # {"deu": 3, "eng": 2}
    nationalities = models.JSONField()  # ["jp", "de"]

    archived_at = models.DateTimeField(null=True, blank=True)

    notes = models.TextField(blank=True)

    applied_at = models.DateTimeField()
    joined_at = models.DateTimeField(null=True)

    if TYPE_CHECKING:
        history: HistoryItemManager
        availability_set: RelatedManager[Availability]
        experiences: RelatedManager[Experience]
        sarapplication_set: SarApplicationManager
        sarprofile_set: RelatedManager[SarProfile]
        document_set: RelatedManager[Document]
        comments: RelatedManager[Comment]
        inquiry_set: RelatedManager[Inquiry]

    @property
    def name(self):
        return f"{self.first_name} {self.last_name}".strip()

    @property
    def name_reversed(self):
        return f"{self.last_name}, {self.first_name}"

    @property
    def is_archived(self):
        return self.archived_at is not None

    @property
    def has_experience(self) -> ExperienceLevels:
        if self.experiences.exists():
            return ExperienceLevels.HAS_EXPERIENCE
        else:
            return ExperienceLevels(self.private_has_experience)

    def __str__(self):
        return f"<Volunteer: {self.name}>"

    def save(self, *args, **kwargs):
        self.nationalities = [n.lower() for n in self.nationalities]
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("volunteer_detail", kwargs={"pk": self.pk})

    def to_history_dict(self):
        return {
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": cast(PhoneNumber, self.phone_number).as_international,
            "email": self.email,
            "date_of_birth": self.date_of_birth.isoformat(),
            "gender": self.gender,
            "positions": ",".join(
                p.position.key for p in self.sarprofile_set.order_by("position__key")
            ),
            "certificates": sorted(self.certificates),
            "languages": self.languages,
            "nationalities": sorted([n.lower() for n in self.nationalities]),
        }

    def language_short_display(self):
        language_names = {entry["code"]: entry["name"] for entry in data.languages}
        language_levels = {0: "A", 1: "B", 2: "C", 3: "N"}
        return "/".join(
            f"{language_names[lang]}({language_levels[level]})"
            for lang, level in self.languages.items()
        )


class SarProfile(models.Model):
    """A Volunteer's search-and-rescue profile

    If a volunteer has one of these, they are considered "in our volunteer pool" for operations.

    """

    id = models.AutoField(primary_key=True)

    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)
    position = models.ForeignKey(Position, on_delete=models.PROTECT)

    def __repr__(self) -> str:
        return f"<SarProfile: {self.volunteer.name} as {self.position.name}>"


class NGO(models.Model):
    name = ShortTextField()

    def __str__(self) -> str:
        return self.name


class Ship(models.Model):
    name = ShortTextField()

    def __str__(self) -> str:
        return self.name


class Experience(models.Model):
    id = models.AutoField(primary_key=True)

    ngo = models.ForeignKey(
        NGO, null=True, blank=True, on_delete=models.PROTECT, related_name="+"
    )
    ship = models.ForeignKey(
        Ship, null=True, blank=True, on_delete=models.PROTECT, related_name="+"
    )
    date_of_experience = models.DateField(default=datetime.now)
    operation_name = ShortTextField(blank=True)
    position = models.ForeignKey(
        Position, on_delete=models.CASCADE, null=True, blank=True
    )
    notes = models.TextField(null=True, blank=True)

    volunteer = models.ForeignKey(
        Volunteer, on_delete=models.CASCADE, related_name="experiences"
    )
    volunteer_id: int | None

    def to_history_dict(self):
        return {
            "date_of_experience": self.date_of_experience,
            "operation_name": self.operation_name,
            "position": self.position,
            "notes": self.notes,
        }

    def get_absolute_url(self):
        return reverse(
            "volunteer_experience", kwargs={"volunteer_id": self.volunteer.pk}
        )


def now():
    return datetime.now(timezone.utc)


class Comment(models.Model):
    text = models.TextField()
    date = models.DateTimeField(default=now)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, related_name="+"
    )
    volunteer = models.ForeignKey(
        Volunteer, on_delete=models.CASCADE, related_name="comments"
    )


class Document(models.Model):
    class Type(models.TextChoices):
        stcw_basic_safety = "stcw:basic_safety", "STCW Basic Safety"
        stcw_watchkeeping = "stcw:watchkeeping", "STCW Watchkeeping"
        stcw_other = "stcw:other", "STCW Other"
        medical_fitness = "medical_fitness", "Medical Fitness"
        cv = "cv", "CV"

    id = models.AutoField(primary_key=True, auto_created=True, verbose_name="ID")

    created = models.DateTimeField(default=now)
    archived = models.DateTimeField(default=None, null=True)

    type = ShortTextField(choices=Type.choices)
    file = models.FileField()
    valid_until = models.DateField(null=True)
    # TODO: The metadata for each field is currently not specified anywhere.
    # We cannot assume it is always present, but we should somehow encode (or at least document) our
    # assumptions. Currently, cv has {} and all others have {'valid_until': date.isoformat()}
    metadata: models.JSONField[dict[str, Any]] = models.JSONField()

    volunteer = models.ForeignKey(Volunteer, on_delete=models.CASCADE)

    def archive(self):
        self.archived = datetime.now(tz=timezone.utc)
        self.save()
