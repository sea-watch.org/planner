from django.contrib import admin

from . import models


@admin.register(models.Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    list_display = ["name"]
    exclude = ["application"]


@admin.register(models.NGO)
class NGOAdmin(admin.ModelAdmin):
    list_display = ["name"]


@admin.register(models.Ship)
class ShipAdmin(admin.ModelAdmin):
    list_display = ["name"]
