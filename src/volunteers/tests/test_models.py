from datetime import datetime, timedelta, timezone

from freezegun import freeze_time
from pytest import mark

from volunteers.models import NGO, ExperienceLevels, Ship, Volunteer
from volunteers.tests.factories import (
    DocumentFactory,
    ExperienceFactory,
    VolunteerFactory,
)


@mark.django_db
def test_archived_volunteers_are_hidden_by_default():
    volunteer = VolunteerFactory.create(
        archived_at=datetime.now(timezone.utc) - timedelta(days=1)
    )
    assert not Volunteer.objects.filter(pk=volunteer.id).exists()


@mark.django_db
def test_archived_volunteers_are_accessible():
    VolunteerFactory.create()
    archived_volunteer = VolunteerFactory.create(
        archived_at=datetime.now(timezone.utc) - timedelta(days=1)
    )
    assert [volunteer.id for volunteer in Volunteer.objects.archived()] == [
        archived_volunteer.id
    ]


@mark.django_db
def test_Volunteer_has_experience_returns_private_field_value_if_no_experiences_exist():
    v = VolunteerFactory.create(private_has_experience=ExperienceLevels.UNKNOWN)
    assert v.has_experience == ExperienceLevels.UNKNOWN


@mark.django_db
def test_Volunteer_has_experience_returns_has_experience_if_experiences_exist():
    v = VolunteerFactory.create(private_has_experience=ExperienceLevels.UNKNOWN)
    ExperienceFactory.create(volunteer=v)
    assert v.has_experience == ExperienceLevels.HAS_EXPERIENCE


def test_ngo_string_uses_name():
    assert str(NGO(name="hello")) == "hello"


def test_ship_string_uses_name():
    assert str(Ship(name="hello")) == "hello"


@mark.django_db
def test_Document_archive_sets_archived():
    d = DocumentFactory.create(archived=None)
    fake_now = datetime(1984, 4, 1, 20, 15, tzinfo=timezone.utc)
    with freeze_time(fake_now):
        d.archive()
    d.refresh_from_db()
    assert d.archived == fake_now
