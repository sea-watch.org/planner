import bs4
from bs4.element import Tag
from django.template.response import TemplateResponse
from django.test.client import RequestFactory
from django.urls import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import (
    VolunteerPersonalDetailsView,
    volunteer_comments_view,
    volunteer_documents_view,
    volunteer_experience_view,
    volunteer_qualifications_view,
)

subpages = [
    [VolunteerPersonalDetailsView.as_view(), "volunteer_detail", "pk"],
    [volunteer_comments_view, "volunteer_comments", "volunteer_id"],
    [volunteer_qualifications_view, "volunteer_qualification", "volunteer_id"],
    [volunteer_experience_view, "volunteer_experience", "volunteer_id"],
    [volunteer_documents_view, "volunteer_documents", "volunteer_id"],
]


def content(response):
    return (
        response.rendered_content
        if isinstance(response, TemplateResponse)
        else response.content
    )


@mark.django_db()
@mark.parametrize(["view", "key"], [[p[0], p[2]] for p in subpages])
def test_title(view, key, rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = view(request, **{key: volunteer.id})
    html = bs4.BeautifulSoup(content(response), features="html.parser")
    assert (
        (title := html.select_one("head title"))
        and (title.string)
        and title.string.strip().startswith(volunteer.name)
    )


@mark.django_db
@mark.parametrize(
    ["view", "expected_link_view_name", "volunteer_id_key"],
    subpages,
)
def test_sidebar(view, expected_link_view_name, volunteer_id_key, rf: RequestFactory):
    # GIVEN a profile page
    volunteer = VolunteerFactory.create()

    # WHEN I open it
    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = view(request, **{volunteer_id_key: volunteer.id})

    # THEN There's a sidebar containing the links to the sections
    html = bs4.BeautifulSoup(content(response), features="html.parser")
    sidebar = html.find("aside")
    assert isinstance(sidebar, Tag)

    assert len(sidebar.find_all("a")) == len(subpages)

    activeLinks = sidebar.find_all("a", class_="active")
    assert len(activeLinks) == 1
    activeLink = activeLinks[0]
    assert isinstance(activeLink, Tag)

    assert activeLink.attrs["href"] == reverse(
        expected_link_view_name, kwargs={volunteer_id_key: volunteer.pk}
    )
