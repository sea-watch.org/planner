from datetime import datetime, timedelta, timezone

from django.test import RequestFactory
import freezegun
from django.test.client import Client
from django.urls.base import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import VolunteerPersonalDetailsView, volunteer_archive


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    volunteer = VolunteerFactory.create()
    backoffice_client.force_login(
        UserFactory.create(view_permissions=VolunteerPersonalDetailsView)
    )
    response = backoffice_client.get(
        reverse("volunteer_archive", kwargs={"pk": volunteer.pk}),
    )
    assert response.status_code < 400


@mark.django_db
def test_redirects(
    rf: RequestFactory,
):
    # Given there is one volunteer
    volunteer = VolunteerFactory.create()

    request = rf.get("")
    request.user = UserFactory.create()
    response = volunteer_archive(request, volunteer.id)

    assert 300 <= response.status_code < 400
    assert response.headers["Location"] == reverse("volunteer_list")  # type: ignore


@mark.django_db
def test_archive_volunteers(
    rf: RequestFactory,
):
    # Given there is one volunteer
    volunteer = VolunteerFactory.create()

    # When I navigate to the volunteer list
    request = rf.get("")
    request.user = UserFactory.create()
    with freezegun.freeze_time(datetime.now(timezone.utc) - timedelta(days=1)):
        volunteer_archive(request, volunteer.id)

    # assert volunteer is archived
    volunteer.refresh_from_db()
    assert volunteer.is_archived
