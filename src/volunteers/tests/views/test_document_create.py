from http import HTTPStatus

from bs4 import BeautifulSoup
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from pytest import mark

from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import volunteer_document_edit, volunteer_documents_view


@mark.django_db
def test_upload_form_is_linked_from_documents_page(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    assert [
        link["href"] for link in html.select("a") if link.text.strip() == "Upload"
    ] == [reverse("volunteer_document_create", kwargs={"volunteer_id": volunteer.id})]


@mark.django_db
def test_view_works_without_document_id(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_document_edit(request, volunteer_id=volunteer.id)
    assert response.status_code == HTTPStatus.OK
