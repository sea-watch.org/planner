from django.urls import reverse
from playwright.sync_api import BrowserContext, expect

from volunteers.tests.factories import VolunteerFactory


def test_can_add_comments_from_volunteer_page(
    logged_in_context: BrowserContext, backoffice_url
):
    # GIVEN a volunteer and we are on the volunteer comments page
    volunteer = VolunteerFactory.create()

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("volunteer_comments", kwargs={"volunteer_id": volunteer.pk})}'
    )

    # WHEN I fill the form with a new comment and submit
    text_input = page.get_by_role("textbox")
    text_input.fill("This is my new comment!")

    submit_button = page.get_by_role("button").get_by_text("Submit")
    submit_button.click()

    # The new comment is visible on the page
    comment = page.locator(".text")
    expect(comment).to_have_text("This is my new comment!")
