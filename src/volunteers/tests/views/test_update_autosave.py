import json
from datetime import date
from http import HTTPStatus

from django.core.exceptions import ValidationError
from django.test.client import RequestFactory
from pytest import fail, mark

from seawatch_registration.tests.factories import PositionFactory
from volunteers.tests.factories import SarProfileFactory, VolunteerFactory
from volunteers.views import volunteer_update_autosave


@mark.parametrize(
    "data",
    [
        {},
        {"foo": 5},
        {"first_name": "x", "last_name": "y"},
        {"first_name": "y", "foo": 5},
    ],
)
def test_bad_request(data, rf: RequestFactory):
    request = rf.post("", data=data)
    assert (
        HTTPStatus(volunteer_update_autosave(request, 42).status_code)
        == HTTPStatus.BAD_REQUEST
    )


@mark.django_db
def test_saves_regular_field(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    request = rf.post("", data={"first_name": "foo"})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert 200 <= response.status_code < 300

    volunteer.refresh_from_db()
    assert volunteer.first_name == "foo"


@mark.django_db
def test_saves_languages(rf: RequestFactory):
    new_languages = {"deu": 2, "eng": 3}
    volunteer = VolunteerFactory.create()
    request = rf.post("", data={"languages": json.dumps(new_languages)})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert 200 <= response.status_code < 300

    volunteer.refresh_from_db()
    assert volunteer.languages == new_languages


@mark.django_db
def test_validates_languages(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    request = rf.post("", data={"languages": json.dumps({"asdf": 42})})
    response = volunteer_update_autosave(request, pk=volunteer.id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content) == {
        "errors": {"languages": ["'asdf' is not a valid language code"]}
    }


@mark.django_db
def test_returns_no_content(rf: RequestFactory):
    request = rf.post("", data={"first_name": "x"})
    response = volunteer_update_autosave(request, pk=VolunteerFactory.create().id)

    assert HTTPStatus(response.status_code) == HTTPStatus.NO_CONTENT


@mark.django_db
def test_renders_errors_in_json(rf: RequestFactory):
    request = rf.post("", data={"first_name": ""})
    response = volunteer_update_autosave(request, pk=VolunteerFactory.create().id)
    assert HTTPStatus(response.status_code) == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content) == {
        "errors": {"first_name": ["This field is required."]}
    }


@mark.parametrize(
    ["field", "value", "expected"],
    [
        ("first_name", "bar", None),
        ("phone_number", "+81 60-259-3211", None),
        ("date_of_birth", "2020-02-20", None),
        (
            "positions",
            lambda: [PositionFactory.create(key=k).id for k in ["p1", "p2"]],
            "p1,p2",
        ),
        ("nationalities", ["DE", "AS"], sorted(["de", "as"])),
    ],
)
@mark.django_db
def test_saves_history(field, value, expected, rf: RequestFactory):
    if callable(value):
        value = value()

    volunteer = VolunteerFactory.create(
        first_name="foo", date_of_birth=date(2020, 2, 10)
    )
    old = volunteer.to_history_dict()[field]
    request = rf.post("", data={field: value})
    response = volunteer_update_autosave(request, pk=volunteer.id)

    assert (response.status_code, response.content) == (HTTPStatus.NO_CONTENT, b"")
    last = volunteer.history.last()
    assert last
    assert last.change_json == [{"field": field, "old": old, "new": expected or value}]


@mark.django_db
def test_allows_saving_fields_even_if_phone_number_is_invalid(rf: RequestFactory):
    invalid_phone_number = "999nowwith50%morebeast"
    volunteer = VolunteerFactory.create(
        phone_number=invalid_phone_number,
        first_name="Anakin",
    )
    for position in PositionFactory.create_batch(size=2):
        SarProfileFactory.create(volunteer=volunteer, position=position)

    try:
        volunteer.full_clean()
    except ValidationError as e:
        assert e.error_dict and set(e.error_dict.keys()) == {"phone_number"}
    else:
        fail("the volunteer should not validate at this time")

    request = rf.post("", data={"first_name": "Darth"})
    response = volunteer_update_autosave(request, pk=volunteer.pk)

    assert response.status_code == HTTPStatus.NO_CONTENT, response.content
    volunteer.refresh_from_db()
    assert volunteer.first_name == "Darth"
    assert volunteer.phone_number == invalid_phone_number
    assert volunteer.sarprofile_set.count() == 2
