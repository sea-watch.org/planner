from datetime import date
from http import HTTPStatus
from typing import Tuple, cast

from bs4 import BeautifulSoup
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http.response import HttpResponseRedirectBase
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark
from pytest_subtests import SubTests

from seawatch_registration.tests.factories import UserFactory
from volunteers.models import Document
from volunteers.tests.factories import DocumentFactory
from volunteers.views import volunteer_document_edit


@mark.django_db
@mark.parametrize(
    ["type", "expected_to_show"],
    [
        (Document.Type.stcw_basic_safety, True),
        (Document.Type.medical_fitness, True),
        (Document.Type.cv, False),
    ],
)
def test_initially_sets_hidden_on_metadata(
    type, expected_to_show, logged_in_context: BrowserContext, backoffice_url
):
    document = DocumentFactory.create(type=type)

    page = logged_in_context.new_page()
    path = reverse(
        "volunteer_document_edit",
        kwargs={
            "volunteer_id": document.volunteer.id,
            "document_id": document.id,
        },
    )
    page.goto(f"{backoffice_url}{path}")

    ex = expect(page.get_by_label("Valid until:"))
    if expected_to_show:
        ex.to_be_visible()
    else:
        ex.not_to_be_visible()


@mark.django_db
def test_changes_metadata_visibility_when_type_changes(
    logged_in_context: BrowserContext, backoffice_url
):
    document = DocumentFactory.create(type=Document.Type.cv)

    page = logged_in_context.new_page()
    path = reverse(
        "volunteer_document_edit",
        kwargs={
            "volunteer_id": document.volunteer.id,
            "document_id": document.id,
        },
    )
    page.goto(f"{backoffice_url}{path}")
    type_select = page.get_by_label("Type:")
    valid_until = page.get_by_label("Valid until:")

    expect(valid_until).not_to_be_visible()
    steps: list[Tuple[Document.Type, bool]] = [
        (Document.Type.medical_fitness, True),
        (Document.Type.stcw_basic_safety, True),
        (Document.Type.cv, False),
        (Document.Type.stcw_basic_safety, True),
    ]
    for t, expected_visible in steps:
        type_select.select_option(t.value)
        if expected_visible:
            expect(valid_until).to_be_visible()
        else:
            expect(valid_until).not_to_be_visible()


@mark.django_db
def def_test_shows_correct_initial_valid_until(rf: RequestFactory):
    valid_until = "2049-12-13"
    document = DocumentFactory.create(
        type=Document.Type.stcw_basic_safety, valid_until=valid_until
    )
    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = volunteer_document_edit(request, document.volunteer.id, document.id)
    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    valid_until = html.select_one("[name=stcw_valid_until]")
    assert valid_until and valid_until.attrs["value"] == valid_until


@mark.django_db
@mark.parametrize(
    ["new_type", "valid_until", "expected_error"],
    [
        (Document.Type.stcw_watchkeeping, "2024-06-20", False),
        (Document.Type.stcw_watchkeeping, None, True),
        (Document.Type.cv, None, False),
    ],
)
def test_accepts_stcw_with_valid_until(
    new_type, valid_until, expected_error, rf: RequestFactory, subtests: SubTests
):
    document = DocumentFactory.create(type=Document.Type.stcw_basic_safety)
    request = rf.post(
        "",
        {
            "type": new_type,
            "file": "",
            **({"stcw_valid_until": valid_until} if valid_until else {}),
        },
    )
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = volunteer_document_edit(request, document.volunteer.id, document.id)

    if expected_error:
        assert response.status_code == HTTPStatus.OK
        html = BeautifulSoup(
            cast(TemplateResponse, response).rendered_content, features="html.parser"
        )
        errorlist = html.select_one("[name=stcw_valid_until] + .errorlist")
        assert errorlist and "This field is required" in errorlist.text
    else:
        with subtests.test("response"):
            assert isinstance(response, HttpResponseRedirectBase)
            assert response.url == reverse(
                "volunteer_documents", kwargs={"volunteer_id": document.volunteer.id}
            )
        with subtests.test("changes"):
            document.refresh_from_db()
            assert (document.type, document.valid_until) == (
                new_type.value,
                date.fromisoformat(valid_until) if valid_until else None,
            )


@mark.django_db
def test_saves_updated_file(rf: RequestFactory, subtests: SubTests, minimal_pdf: bytes):
    document = DocumentFactory.create(type=Document.Type.stcw_basic_safety)
    request = rf.post(
        "",
        {
            "type": Document.Type.stcw_basic_safety,
            "stcw_valid_until": "2024-04-01",
        },
    )
    request.FILES["file"] = SimpleUploadedFile("new and shiny.pdf", minimal_pdf)
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])

    response = volunteer_document_edit(request, document.volunteer.id, document.id)
    assert isinstance(response, HttpResponseRedirectBase)

    document.refresh_from_db()
    with subtests.test("valid_until"):
        assert document.valid_until and document.valid_until.isoformat() == "2024-04-01"

    with subtests.test("file contents"):
        assert document.file.read() == minimal_pdf


@mark.django_db
def test_form_has_enctype(rf: RequestFactory):
    document = DocumentFactory.create()
    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = volunteer_document_edit(request, document.volunteer.id, document.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")
    assert [form["enctype"] for form in html.select("#main-content form")] == [
        "multipart/form-data"
    ]
