import dataclasses
from dataclasses import dataclass
from datetime import date
from typing import Any, Callable, Literal

from django.db.models import Manager
from django.urls import reverse
from playwright.sync_api import BrowserContext, Page, expect
from pytest import fixture, mark
from typing_extensions import assert_never

from applications.tests.factories import SarApplicationFactory
from planner_common.models import Gender
from seawatch_registration.models import Position
from seawatch_registration.tests.factories import PositionFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import SarProfileFactory, VolunteerFactory


@dataclass
class Data:
    old_position: Position
    new_position: Position
    volunteer: Volunteer


@fixture()
def data(db) -> Data:
    old_p = PositionFactory.create(name="Foo")
    new_p = PositionFactory.create(
        name="Bar"
    )  # needs to be in the DB when we call the page
    volunteer = VolunteerFactory.create(
        gender=Gender.MALE,
        nationalities=["de"],
        certificates=["STCW-VI/6"],
        languages={"deu": 3},
    )
    SarApplicationFactory.create(
        volunteer=volunteer,
        position=old_p,
        is_closed=True,
    )
    SarProfileFactory.create(volunteer=volunteer, position=old_p)
    return Data(old_p, new_p, volunteer)


@fixture()
def page(logged_in_context: BrowserContext, data: Data, backoffice_url) -> Page:
    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("volunteer_detail", kwargs={"pk": data.volunteer.id})}'
    )
    return page


@dataclass
class Testcase:
    __test__ = False  # prevent pytest from picking this up
    type: Literal["text", "select", "choicesjs"]
    field: str
    label: str
    new: str
    _: dataclasses.KW_ONLY
    expected: Any = None
    _custom_getter: Callable[[Volunteer], Any] | None = None

    def value_from_volunteer(self, volunteer):
        return (
            self._custom_getter(volunteer)
            if self._custom_getter
            else getattr(volunteer, self.field)
        )


# Sentinel value because we cannot write the positions in the parametrize call
_both_positions = object()


@mark.parametrize(
    "t",
    [
        Testcase("text", "first_name", "first name", "ffff"),
        Testcase("text", "last_name", "last name", "lllll"),
        Testcase("text", "phone_number", "phone number", "+81602593219"),
        Testcase("text", "email", "email address", "foo@example.com"),
        Testcase(
            "text",
            "date_of_birth",
            "date of birth",
            "2041-01-02",
            expected=date(2041, 1, 2),
        ),
        Testcase("select", "gender", "gender", Gender.PREFER_NOT_SAY),
        Testcase(
            "choicesjs",
            "nationalities",
            "nationalities",
            "Afghanistan",
            expected={"de", "af"},
        ),
        Testcase("text", "homepage", "homepage", "https://lets.goooooo.com"),
        Testcase(
            "choicesjs",
            "positions",
            "positions",
            "Bar",
            expected=_both_positions,
            _custom_getter=lambda v: {p.position for p in v.sarprofile_set.all()},
        ),
        Testcase(
            "choicesjs",
            "certificates",
            "certificates",
            "STCW-II/1",
            expected={"STCW-VI/6", "STCW-II/1"},
        ),
    ],
)
def test_autosaves_field(t: Testcase, data: Data, page: Page):
    assert (
        t.value_from_volunteer(data.volunteer) != t.new
    ), "need the new value to be different from the old one"
    field = page.locator(".field", has=page.get_by_label(t.label))
    element = field.get_by_label(t.label)
    match t.type:
        case "text":
            element.fill(t.new)
            page.keyboard.press("Enter")
        case "select":
            element.select_option(t.new)
        case "choicesjs":
            input = field.locator(".choices__input:visible")
            input.click()
            input.type(t.new)
            page.keyboard.press("Enter")
            page.keyboard.press("Escape")
        case _:
            assert_never(t.type)
    expect(field.locator(".autosave-indicator-saved:visible")).to_be_visible()

    data.volunteer.refresh_from_db()
    actual = t.value_from_volunteer(data.volunteer)
    expected = t.expected or t.new
    if isinstance(actual, Manager):
        # If we are looking at a relation (positions) we need to call .all()
        actual = actual.all()
    if expected == _both_positions:
        # sentinel value because we couldn't access data in the setup
        expected = {data.old_position, data.new_position}
    if isinstance(expected, set):
        # if we expect a set, we set(..) so the comparison is order-independent
        actual = set(actual)
    assert actual == expected
