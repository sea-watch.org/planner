import random

from django.http import HttpResponseRedirect
from planner_common.optional import unwrap
from datetime import UTC, datetime, timezone
from http import HTTPStatus
from os.path import basename
from typing import cast

from bs4 import BeautifulSoup
from bs4.element import Tag
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark

from seawatch_registration.tests.factories import UserFactory
from volunteers.models import Document
from volunteers.tests.factories import (
    DocumentFactory,
    SarProfileFactory,
    VolunteerFactory,
)
from volunteers.views import volunteer_documents_view


def test_request_rank_related_documents(
    logged_in_context: BrowserContext, backoffice_url
):
    volunteer = VolunteerFactory.create()
    page = logged_in_context.new_page()
    page.goto(
        f"{backoffice_url}{reverse('volunteer_documents', kwargs={'volunteer_id': volunteer.id})}"
    )

    page.get_by_role("button", name="Request documents").click()
    dialog_form = page.locator("css=.planner-modal >> form")
    expect(dialog_form.get_by_role("button", name="Send inquiry")).to_be_visible()
    expect(dialog_form).to_have_attribute(
        "action",
        reverse(
            "volunteer_request_rank_related_documents",
            kwargs={"volunteer_id": volunteer.id},
        ),
    )


@mark.django_db
def test_rank_related_documents_and_validity_dates_are_visible_in_qualification_page(
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create()
    documents = [
        DocumentFactory.create(volunteer=volunteer, type=type)
        for type in [
            Document.Type.medical_fitness,
            Document.Type.stcw_basic_safety,
            Document.Type.stcw_watchkeeping,
        ]
    ]

    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    for document in documents:
        [dd] = [
            e
            for e in html.find_all("dd")
            if unwrap(cast(Tag, e).select_one("a")).text.strip()
            == basename(document.file.name)
        ]
        assert isinstance(dd, Tag)
        a = dd.find("a")
        assert isinstance(a, Tag)
        assert a.attrs["href"] == document.file.url
        assert f"{document.valid_until:%d.%m.%Y}" in dd.text.strip()


@mark.django_db
def test_shows_document(rf: RequestFactory):
    filename = "jipjupjea.pdf"
    document = DocumentFactory.create(filename=filename)

    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_documents_view(request, volunteer_id=document.volunteer.id)
    assert isinstance(response, TemplateResponse)

    html = BeautifulSoup(response.rendered_content, features="html.parser")

    link = html.find("a", string=lambda s: s is not None and filename in s)
    assert isinstance(link, Tag)
    assert link["href"] == document.file.url


@mark.django_db
def test_archive_document(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    NUM_DOCS = random.randint(1, 5)

    docs = [DocumentFactory.create(volunteer=volunteer) for _ in range(NUM_DOCS)]

    request = rf.post("", {"document": [d.id for d in docs], "action": "archive"})
    request.user = UserFactory.create(
        permissions=["volunteers.view_volunteer", "volunteers.change_volunteer"]
    )
    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert 300 <= response.status_code < 400
    assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
        "volunteer_documents", kwargs={"volunteer_id": volunteer.id}
    )
    for d in docs:
        d.refresh_from_db()
        assert d.archived


@mark.django_db
def test_unarchive_document(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    NUM_DOCS = random.randint(1, 5)

    docs = [
        DocumentFactory.create(volunteer=volunteer, archived=datetime.now(timezone.utc))
        for _ in range(NUM_DOCS)
    ]

    request = rf.post("", {"document": [d.id for d in docs], "action": "unarchive"})
    request.user = UserFactory.create(
        permissions=["volunteers.view_volunteer", "volunteers.change_volunteer"]
    )
    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert 300 <= response.status_code < 400
    assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
        "volunteer_documents", kwargs={"volunteer_id": volunteer.id}
    )
    for d in docs:
        d.refresh_from_db()
        assert d.archived is None


@mark.django_db
def test_delete_document(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    NUM_DOCS = random.randint(1, 5)

    docs = [
        DocumentFactory.create(volunteer=volunteer, archived=datetime.now(timezone.utc))
        for _ in range(NUM_DOCS)
    ]

    request = rf.post("", {"document": [d.id for d in docs], "action": "delete"})
    request.user = UserFactory.create(
        permissions=["volunteers.view_volunteer", "volunteers.change_volunteer"]
    )
    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert 300 <= response.status_code < 400
    assert isinstance(response, HttpResponseRedirect) and response.url == reverse(
        "volunteer_documents", kwargs={"volunteer_id": volunteer.id}
    )
    assert all(not Document.objects.filter(id=doc.id).exists() for doc in docs)


@mark.django_db
def test_delete_document_via_browser(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create()
    doc = DocumentFactory.create(
        volunteer=volunteer, archived=datetime.now(timezone.utc)
    )

    page = logged_in_context.new_page()
    page.goto(
        f"{backoffice_url}{reverse('volunteer_documents', kwargs={'volunteer_id': volunteer.id})}"
    )

    doc_line = page.get_by_role("definition").filter(has_text=doc.file.name)
    doc_line.get_by_role("checkbox").check()
    page.get_by_role("button").get_by_text("Delete").click()
    page.get_by_role("button").get_by_text("Confirm delete").click()
    expect(doc_line).not_to_be_visible()

    assert not Document.objects.filter(id=doc.id).exists()


@mark.django_db
def test_edit_button_only_enabled_for_one_selected_doc(
    logged_in_context: BrowserContext, backoffice_url
):
    volunteer = VolunteerFactory.create()
    SarProfileFactory.create(volunteer=volunteer)
    documents_to_archive = [
        DocumentFactory.create(volunteer=volunteer),
        DocumentFactory.create(volunteer=volunteer),
        DocumentFactory.create(volunteer=volunteer),
    ]
    # create some more to make sure we're selecting the right ones
    DocumentFactory.create(volunteer=volunteer, type=documents_to_archive[0].type)
    DocumentFactory.create(volunteer=volunteer)

    page = logged_in_context.new_page()
    page.goto(
        f"{backoffice_url}{reverse('volunteer_documents', kwargs={'volunteer_id': volunteer.id})}"
    )

    button_edit = page.get_by_role("button").get_by_text("Edit")
    expect(button_edit).to_be_disabled()

    d = documents_to_archive[0]
    page.get_by_role("definition").filter(has_text=d.file.name).get_by_role(
        "checkbox"
    ).check()
    expect(button_edit).to_be_enabled()

    d = documents_to_archive[1]
    page.get_by_role("definition").filter(has_text=d.file.name).get_by_role(
        "checkbox"
    ).check()
    expect(button_edit).to_be_disabled()


@mark.django_db
def test_delete_button_toggle(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create()

    DocumentFactory.create(volunteer=volunteer, archived=datetime.now(timezone.utc))
    d = DocumentFactory.create(volunteer=volunteer, archived=datetime.now(timezone.utc))

    page = logged_in_context.new_page()
    page.goto(
        f"{backoffice_url}{reverse('volunteer_documents', kwargs={'volunteer_id': volunteer.id})}"
    )

    page.get_by_role("definition").filter(has_text=d.file.name).get_by_role(
        "checkbox"
    ).check()

    button_delete = page.get_by_role("button").get_by_text("Delete")
    button_delete.click()
    page.wait_for_selector("text=Cancel")

    button_confirm = page.get_by_role("button").get_by_text("Confirm delete")
    expect(button_confirm).to_be_visible()

    page.get_by_role("button").get_by_text("Cancel").click()
    page.wait_for_selector("text=Delete")

    expect(button_confirm).not_to_be_visible()


@mark.django_db
def test_BadRequest_on_unknown_action(rf: RequestFactory):
    doc = DocumentFactory.create()
    request = rf.post(
        "",
        {
            "document": [doc.id],
            "action": "some_imagined_action_chatgpt_told_me_to_use",
        },
    )
    request.user = UserFactory.create(
        permissions=["volunteers.view_volunteer", "volunteers.change_volunteer"]
    )
    response = volunteer_documents_view(request, volunteer_id=doc.volunteer.id)

    assert response.status_code == HTTPStatus.BAD_REQUEST


@mark.django_db
def test_archived_documents_are_sorted_by_archival_time(rf: RequestFactory):
    volunteer = VolunteerFactory.create()
    earlier = DocumentFactory.create(
        volunteer=volunteer,
        archived=datetime(1999, 3, 2, 1, tzinfo=UTC),
    )
    later = DocumentFactory.create(
        volunteer=volunteer,
        archived=datetime(2024, 3, 2, 1, tzinfo=UTC),
    )

    request = rf.get("")
    request.user = UserFactory.create(
        permissions=["volunteers.view_volunteer", "volunteers.change_volunteer"]
    )

    response = volunteer_documents_view(request, volunteer_id=volunteer.id)
    assert isinstance(response, TemplateResponse)

    html = BeautifulSoup(response.rendered_content, features="html.parser")
    filenames_in_html = [e.text for e in html.select("dd > a")]
    assert filenames_in_html == [later.file.name, earlier.file.name]
