from http import HTTPStatus
from typing import Literal

from django.test import RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_subtests import SubTests

from volunteers.models import Experience
from volunteers.tests.factories import ExperienceFactory
from volunteers.views import delete_experience


@mark.django_db
@mark.parametrize("missing", ["experience", "volunteer", "both"])
def test_returns_404_on_nonexisting_ids(
    missing: Literal["experience", "volunteer", "both"], rf: RequestFactory
):
    experience = ExperienceFactory.create()
    match missing:
        case "experience":
            volunteer_id = experience.volunteer.id
            experience_id = 204202
        case "volunteer":
            volunteer_id = 402404
            experience_id = experience.id
        case "both":
            experience_id = 204202
            volunteer_id = 402404

    response = delete_experience(rf.post(""), vol_id=volunteer_id, pk=experience_id)
    assert response.status_code == HTTPStatus.NOT_FOUND


@mark.django_db
def test_happy_path(rf: RequestFactory, subtests: SubTests):
    experience = ExperienceFactory.create()

    response = delete_experience(
        rf.post(""),
        vol_id=experience.volunteer.id,
        pk=experience.id,
    )

    with subtests.test("deletes the experience"):
        assert not Experience.objects.filter(id=experience.id).exists()

    with subtests.test("redirects to experience page for the volunteer"):
        assert (
            response.status_code,
            getattr(response, "url", None),
        ) == (
            HTTPStatus.FOUND,
            reverse(
                "volunteer_experience",
                kwargs={"volunteer_id": experience.volunteer.id},
            ),
        )
