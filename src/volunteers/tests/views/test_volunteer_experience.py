from datetime import date

import bs4
from django.template.response import TemplateResponse
from django.test import RequestFactory
from django.urls import reverse
from playwright.sync_api import BrowserContext, expect
from pytest import mark
from pytest_django.asserts import assertContains

from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers import views
from volunteers.models import ExperienceLevels
from volunteers.tests.factories import ExperienceFactory, VolunteerFactory


@mark.django_db
def test_selecting_position_offers_not_applicable_as_default(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create()

    response = views.ExperienceCreateView.as_view()(
        request, pk=VolunteerFactory.create().id
    )

    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    select = html.find("select", attrs={"name": "position"})
    assert isinstance(select, bs4.Tag)
    option = select.find("option", selected=True)
    assert isinstance(option, bs4.Tag)
    assert option.string == "- Not Applicable -"


@mark.django_db
def test_all_experience_fields_are_shown_in_the_list(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")

    experience = ExperienceFactory.create(
        ship__name="amazing ship",
        ngo__name="amazing ngo",
        position=PositionFactory.create(name="amazing position"),
        notes="this was amazing",
        operation_name="amazing operation",
        date_of_experience=date(2020, 11, 1),
    )
    response = views.volunteer_experience_view(request, experience.volunteer.id)

    assertContains(response, "amazing ship")
    assertContains(response, "amazing ngo")
    assertContains(response, "amazing position")
    assertContains(response, "this was amazing")
    assertContains(response, "amazing operation")
    assertContains(response, "November 2020")


@mark.django_db
def test_volunteer_experience(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create(first_name="Awesome", last_name="Protagonist")

    page = logged_in_context.new_page()

    page.goto(
        f'{backoffice_url}{reverse("volunteer_detail", kwargs={"pk": volunteer.pk})}'
    )

    sidebar = page.query_selector(
        f"aside:has(a[href='{volunteer.get_absolute_url()}'])"
    )
    assert sidebar
    [link] = filter(
        lambda b: b.text_content() == "SAR Experience",
        sidebar.query_selector_all("p"),
    )
    assert link
    link.click()

    title = page.locator('h1:has-text("SAR Experience")')
    expect(title).to_have_text("SAR Experience")


def test_volunteer_without_experience(
    logged_in_context: BrowserContext, backoffice_url
):
    volunteer = VolunteerFactory.create(first_name="Awesome", last_name="Protagonist")

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("volunteer_experience", kwargs={"volunteer_id": volunteer.pk})}'
    )

    # check whether there is a dropdown
    exp_status = page.locator("id=id_private_has_experience")
    expect(exp_status).to_have_value("unknown")
    exp_status.select_option("no_experience")
    expect(exp_status).to_have_value("no_experience")

    saved_indicator = page.locator(
        ".field:has(#id_private_has_experience) .autosave-indicator-saved"
    )
    expect(saved_indicator).to_be_visible()

    # confirm no experiences are recorded (div with class "experience-details")
    volunteer.refresh_from_db()
    assert volunteer.has_experience == ExperienceLevels.NO_EXPERIENCE


def test_whole_experience_workflow_should_work(
    logged_in_context: BrowserContext, backoffice_url
):
    volunteer = VolunteerFactory.create(first_name="Awesome", last_name="Protagonist")

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("volunteer_experience", kwargs={"volunteer_id": volunteer.pk})}'
    )

    # check that initial experience is set to "unknown"
    exp_status = page.locator("id=id_private_has_experience")
    expect(exp_status).to_have_value("unknown")
    assert volunteer.private_has_experience == "unknown"
    assert volunteer.experiences.all().count() == 0

    # Create experience,
    add_experience_button = page.locator('button:has-text("Add experience")')
    expect(add_experience_button).to_be_visible()

    add_experience_button.click()
    title = page.locator('h1:has-text("ADD SAR EXPERIENCE")')
    expect(title).to_be_visible()

    page.locator("id=id_operation_name").fill("Operation One")
    save = page.get_by_role("button").get_by_text("Save")
    expect(save).to_be_visible()
    save.click()

    title = page.locator('h1:has-text("SAR EXPERIENCE")')
    expect(title).to_be_visible()

    # verify it exists in list
    assert volunteer.experiences.all().count() == 1
    exp_status = page.locator("id=id_private_has_experience")
    expect(exp_status).not_to_be_visible()
    sar_experience = page.locator(
        "id=experiences",
        has=page.locator('p:has-text("Operation Name: Operation One")'),
    )
    expect(sar_experience).to_be_visible()

    # update
    edit_button = page.locator('button:has-text("Edit")')
    expect(edit_button).to_be_visible()
    edit_button.click()
    title = page.locator('h1:has-text("ADD SAR EXPERIENCE")')
    expect(title).to_be_visible()
    operation = page.locator("id=id_operation_name")
    expect(operation).to_be_visible()
    operation.fill("Operation Op1")
    save = page.get_by_role("button").get_by_text("Save")
    expect(save).to_be_visible()
    save.click()

    # verify
    assert volunteer.experiences.all().count() == 1
    exp_status = page.locator("id=id_private_has_experience")
    expect(exp_status).not_to_be_visible()
    sar_experience = page.locator(
        "id=experiences",
        has=page.locator('p:has-text("Operation Name: Operation Op1")'),
    )
    expect(sar_experience).to_be_visible()

    # and delete)
    delete_button = page.get_by_role("button").get_by_text("Delete")
    expect(delete_button).to_be_visible()
    delete_button.click()

    modal = page.locator('"Delete experience?"')
    expect(modal).to_be_visible()
    confirm_button = (
        page.locator(".planner-modal.visible")
        .get_by_role("button")
        .get_by_text("DELETE")
    )
    expect(confirm_button).to_be_visible()
    confirm_button.click()
    assert volunteer.experiences.all().count() == 0
