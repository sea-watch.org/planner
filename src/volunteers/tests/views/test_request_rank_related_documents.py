from http import HTTPStatus
from unittest.mock import Mock, call

from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.urls import reverse
from pytest import mark, raises

from seawatch_planner.tests.request_factory import RequestFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import request_rank_related_documents


@mark.django_db
def test_request_documents(rf: RequestFactory):
    # GIVEN volunteer
    volunteer = VolunteerFactory.create()

    # WHEN requesting rank-related documents
    rank_rel_doc_mock = Mock()
    request = rf.post("")
    request_rank_related_documents(
        request,
        volunteer_id=volunteer.id,
        request_rank_related_documents=rank_rel_doc_mock,
    )

    # THEN data is serialized and forwarded to public-backend
    assert rank_rel_doc_mock.call_args_list == [call(volunteer)]


@mark.django_db
def test_redirects_to_qualification_and_shows_success_message(
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create(first_name="Hulk", email="hulk@superheroes.com")
    request = rf.post("")
    response: HttpResponse = request_rank_related_documents(
        request,
        volunteer_id=volunteer.id,
        request_rank_related_documents=lambda _: None,
    )
    assert isinstance(response, HttpResponseRedirect)
    assert response.url == reverse(
        "volunteer_qualification", kwargs={"volunteer_id": volunteer.id}
    )
    assert request._messages.messages == [
        (
            25,
            "Sent request for documents to Hulk at hulk@superheroes.com",
            "",
        )
    ]


@mark.django_db
def test_requires_post(rf: RequestFactory):
    response = request_rank_related_documents(rf.get(""), volunteer_id=42)
    assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED


@mark.django_db
def test_requires_responds_with_not_found_if_volunteer_doesnt_exist(rf: RequestFactory):
    with raises(Http404):
        request_rank_related_documents(rf.post(""), volunteer_id=42)
