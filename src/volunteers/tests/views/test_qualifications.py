from django.test import RequestFactory
from pytest import mark
from pytest_django.asserts import assertContains

from seawatch_registration.tests.factories import UserFactory
from volunteers.tests.factories import VolunteerFactory
from volunteers.views import volunteer_qualifications_view


@mark.django_db
def test_shows_qualificatioin(rf: RequestFactory):
    volunteer = VolunteerFactory.create(qualification="wow, much qualified")
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_qualifications_view(request, volunteer_id=volunteer.id)
    assertContains(response, "wow, much qualified")


@mark.django_db
def test_shows_motivation(rf: RequestFactory):
    volunteer = VolunteerFactory.create(motivation="wow, much motivated")
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = volunteer_qualifications_view(request, volunteer_id=volunteer.id)
    assertContains(response, "wow, much motivated")
