import json
from datetime import date
from typing import cast

import bs4
from bs4.element import Tag
from django.template.response import TemplateResponse
from django.test.client import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains

from planner_common.models import Gender
from seawatch_registration.tests.factories import PositionFactory, UserFactory
from volunteers.models import Volunteer
from volunteers.tests.factories import SarProfileFactory, VolunteerFactory
from volunteers.views import (
    VolunteerPersonalDetailsView,
    VolunteerWithSarProfileListView,
)


@mark.django_db
def test_list_links_to_profile(rf: RequestFactory):
    volunteer: Volunteer = SarProfileFactory.create(
        volunteer__first_name="Super",
        volunteer__last_name="Mario",
    ).volunteer

    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerWithSarProfileListView.as_view()(request)
    assert isinstance(response, TemplateResponse)

    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    entry: Tag = [
        th
        for th in html.find_all("th")
        if th.text.strip() == "Super Mario"  # type: ignore
    ][0]
    assert entry

    link = entry.find("a")
    assert link
    assert link["href"] == reverse("volunteer_detail", kwargs={"pk": volunteer.pk})  # type: ignore


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    volunteer: Volunteer = VolunteerFactory.create()
    backoffice_client.force_login(
        UserFactory.create(view_permissions=VolunteerPersonalDetailsView)
    )
    response = backoffice_client.get(
        reverse("volunteer_detail", kwargs={"pk": volunteer.pk}),
    )
    assert response.status_code == 200


@mark.django_db
def test_shows_fields(rf: RequestFactory):
    simple_to_verify_fields = {
        "first_name": "Marceline",
        "last_name": "the Vampire Queen",
        "phone_number": "+414204242",
        "email": "this-is-fine+foo@example.com",
    }
    homepage = "https://beep.boop.ding.dong"
    hard_to_verify_fields = {
        "gender": Gender.PREFER_NOT_SAY,
        "date_of_birth": date(2132, 3, 15),
        "certificates": ["STCW-VI/3", "other: general awesomeness"],
        "languages": {"deu": 1, "eng": 3},
        "nationalities": ["jp", "kz"],
    }
    positions = [
        PositionFactory.create(name="Left pos"),
        PositionFactory.create(name="Right pos"),
    ]
    notes = "some test notes"
    volunteer: Volunteer = VolunteerFactory.create(
        **simple_to_verify_fields,
        **hard_to_verify_fields,
        homepage=homepage,
        notes=notes,
    )
    for position in positions:
        SarProfileFactory.create(volunteer=volunteer, position=position)

    request = rf.get("")
    request.user = UserFactory.create(view_permissions=VolunteerPersonalDetailsView)

    response = VolunteerPersonalDetailsView.as_view()(request, pk=volunteer.pk)

    assert isinstance(response, TemplateResponse)
    html = bs4.BeautifulSoup(response.rendered_content, features="html.parser")

    for value in simple_to_verify_fields.values():
        assertContains(response, value)

    assertContains(response, homepage)

    assertContains(response, notes)

    assert str(hard_to_verify_fields["gender"].label) in response.rendered_content

    assert "2132-03-15" in response.rendered_content

    for position in positions:
        assert position.name in response.rendered_content

    assert html.find("option", value="STCW-VI/3", selected=True)
    assert html.find("option", value="other", selected=True)
    assert html.find(
        "input", attrs={"name": "certificates__other", "value": "general awesomeness"}
    )

    assert (
        json.loads(cast(Tag, html.find("script", id="language-mapper--value")).text)
        == hard_to_verify_fields["languages"]
    )

    nationalities = html.find("select", attrs={"name": "nationalities"})
    assert isinstance(nationalities, Tag)

    def attr_as_string(tag: Tag, attr: str) -> str:
        v = tag.attrs[attr]
        assert isinstance(v, str)
        return v

    assert {
        (attr_as_string(option, "value").lower(), option.text)
        for option in nationalities.select("option[selected]")
    } == {("jp", "Japan"), ("kz", "Kazakhstan")}


@mark.django_db
def test_archive_button(rf: RequestFactory):
    # GIVEN a profile page
    volunteer = VolunteerFactory.create()

    # WHEN I open it
    request = rf.get("")
    request.user = UserFactory.create(view_permissions=VolunteerPersonalDetailsView)
    response = VolunteerPersonalDetailsView.as_view()(request, pk=volunteer.id)

    # THEN There's a delete button in a form with the right url and method
    html = bs4.BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    assert html.find(
        "form",
        attrs={"action": reverse("volunteer_archive", kwargs={"pk": volunteer.pk})},
    )
    assert html.find("input", attrs={"type": "submit", "value": "Archive Profile"})
