from datetime import date, timedelta
from http import HTTPStatus
from typing import cast
from unittest.mock import Mock, call

import pytest
from bs4 import BeautifulSoup
from bs4.element import Tag
from django.http.response import Http404
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from freezegun import freeze_time
from pytest import mark

from operations.models import Availability
from operations.tests.factories import AvailabilityFactory, OperationFactory
from seawatch_planner.tests.request_factory import RequestFactory
from seawatch_registration.tests.factories import UserFactory
from seawatch_registration.tests.util import htmlnormalize_text

from ...views import (
    VolunteerWithSarProfileListView,
    request_availabilities_view,
    volunteer_request_availabilities_dialog,
)
from ..factories import SarProfileFactory, VolunteerFactory


def noop(*args, **kwargs):
    return None


@mark.django_db
def test_volunteer_list_items_show_button_to_request_availability(
    rf: RequestFactory,
):
    # Given there is one volunteer and one operation
    SarProfileFactory.create(
        volunteer__first_name="Mail", volunteer__last_name="Gebson"
    )
    OperationFactory.create()

    # When I navigate to the volunteer list
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerWithSarProfileListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    html = BeautifulSoup(response.rendered_content, features="html.parser")

    # I expect to see a button with which i can request availabilities from the vol
    button = html.find("button", string="Request Availabilities")
    assert button is not None, "Could not find button to request availabilities"
    assert button.text == "Request Availabilities"


@mark.django_db
def test_request_availabilities_shows_success_message(rf: RequestFactory):
    vol = VolunteerFactory.create(first_name="asdf", email="a@b.de")

    request = rf.post("", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_view(request, vol.pk, request_availabilities=Mock())

    assert request._messages.messages == [
        (25, "Sent availabilities request to asdf at a@b.de", "")
    ]


@mark.django_db
def test_request_availabilities_calls_request_availabilities(
    rf: RequestFactory,
):
    # Given there is one volunteer and an operation
    volunteer = VolunteerFactory.create(
        first_name="Emailuel", email="cant@reine-vernunft.de"
    )
    # When I call the availability request function
    request = rf.post("", {"confirm": "Confirm"})
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    request_availabilities_mock = Mock()
    request_availabilities_view(
        request, volunteer.pk, request_availabilities=request_availabilities_mock
    )

    # And i expect that request availabilites was called with the correct object
    assert request_availabilities_mock.call_args_list == [call(volunteer)]


@mark.django_db
def test_page_produces_404_when_no_valid_volunteer_id_is_passed(client: Client):
    user = UserFactory.create(permission="volunteers.view_volunteer")
    client.force_login(user)
    response = client.post(
        reverse("volunteer_request_availabilities", kwargs={"volunteer_id": 1})
    )
    assert response.status_code == 404


@mark.django_db
def test_get_endpoint_returns_404_on_missing_user(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    with pytest.raises(Http404):
        volunteer_request_availabilities_dialog(request, pk=42)


@mark.django_db
def test_post_endpoint_returns_404_on_missing_user(rf: RequestFactory):
    request = rf.post("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    with pytest.raises(Http404):
        request_availabilities_view(request, volunteer_id=42)


@mark.django_db
def test_request_availabilities_page_shows_volunteer_details(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    name = "Carola"
    email = "carola@rackete.org"
    volunteer = VolunteerFactory.create(first_name=name, email=email)
    OperationFactory.create()
    response = volunteer_request_availabilities_dialog(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert name in html.text
    assert email in html.text


@mark.django_db
@freeze_time(date(2020, 1, 21))
def test_request_availabilities_page_shows_operation_details_and_answer(
    rf: RequestFactory,
):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    name = "Pia"
    volunteer = VolunteerFactory.create(first_name=name)
    OperationFactory.create(start_date=date.fromisoformat("2022-01-12"))
    operation2 = OperationFactory.create(start_date=date.fromisoformat("2022-02-12"))
    Availability.objects.create(
        volunteer=volunteer, operation=operation2, availability=Availability.Choices.YES
    )
    response = volunteer_request_availabilities_dialog(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert "12.01.2022 - " in html.text
    assert "Yes" in html.text


@mark.django_db
def test_request_availabilities_page_has_form_submit(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer = VolunteerFactory.create()
    OperationFactory.create()
    response = volunteer_request_availabilities_dialog(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    form = html.find("form")
    assert isinstance(form, Tag)
    assert form.attrs.get("action") in [
        None,  # This is fine since we're on that page currently
        reverse(
            "volunteer_request_availabilities", kwargs={"volunteer_id": volunteer.pk}
        ),
    ]


@mark.django_db
@freeze_time(date(2022, 1, 21))
def test_view_does_not_include_operations_that_are_over(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    volunteer = VolunteerFactory.create()
    OperationFactory.create(start_date=date(2010, 12, 21))
    OperationFactory.create(start_date=date(2024, 12, 21))
    response = volunteer_request_availabilities_dialog(request, volunteer.pk)
    html = BeautifulSoup(response.content, features="html.parser")
    assert "21.12.2010 - " not in html.text
    assert "21.12.2024 - " in html.text


@mark.django_db
def test_shows_newest_availabilities_if_there_are_multiple_versions(
    rf: RequestFactory,
):
    volunteer = VolunteerFactory.create()
    SarProfileFactory.create(volunteer=volunteer)

    operation = OperationFactory.create()
    Availability.objects.create(
        volunteer=volunteer, operation=operation, availability=Availability.Choices.YES
    )

    operation.end_date += timedelta(days=7)
    # This will increase the operations version number, which implies that existing answers are "outdated"
    operation.save()
    Availability.objects.create(
        volunteer=volunteer,
        operation=operation,
        availability=Availability.Choices.MAYBE,
    )

    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = volunteer_request_availabilities_dialog(request, volunteer.id)
    html = BeautifulSoup(response.content, features="html.parser")

    def matcher(e: Tag) -> bool:
        return e.name == "tr" and operation.name in e.text

    # Select tbody first, otherwise we might select the header tr if the random operation name is
    # contained in it
    tbody = html.select_one("tbody")
    assert tbody
    row = tbody.find(matcher)
    assert isinstance(row, Tag)
    ## This failed for reasons I don't understand, so adding the html to the error message might help
    assert row.find_all("td")[-1].text.strip() == "Maybe", str(html)


@mark.django_db
def test_shows_previous_responses_as_outdated_if_operation_changed(
    rf: RequestFactory,
):
    outdated = AvailabilityFactory.create(availability=Availability.Choices.MAYBE)
    operation = outdated.operation
    operation.end_date += timedelta(days=1)
    operation.save()
    assert (
        outdated.operation_version < operation.version
    ), "the operation version should have been updated"

    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = volunteer_request_availabilities_dialog(request, outdated.volunteer.id)

    html = BeautifulSoup(response.content, features="html.parser")

    def matcher(e: Tag) -> bool:
        return (
            e.name == "tr"
            and cast(Tag, e.parent).name == "tbody"
            and operation.name in e.text
        )

    row = html.find(matcher)
    assert isinstance(row, Tag)
    assert htmlnormalize_text(row.find_all("td")[-1].text) == "Maybe (outdated)"


@mark.django_db
def test_post_endpoint_requires_post(rf: RequestFactory):
    request = rf.get("")
    request.user = UserFactory.create(permissions=["volunteers.view_volunteer"])
    response = request_availabilities_view(request, 5123)
    assert response.status_code == HTTPStatus.METHOD_NOT_ALLOWED
