from typing import cast

from bs4 import BeautifulSoup, Tag
from django.http.response import HttpResponse
from django.template.response import TemplateResponse
from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertNotContains

from operations.tests.factories import OperationFactory
from seawatch_planner.tests.asserts import assert_requires_permissions
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ...views import VolunteerWithSarProfileListView
from ..factories import SarProfileFactory, VolunteerFactory


@mark.django_db
def test_is_reachable(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(permission="volunteers.view_volunteer")
    )
    response = backoffice_client.get(
        reverse("volunteer_list"),
    )
    assert response.status_code == 200


@mark.django_db
def test_has_volunteers(rf: RequestFactory):
    SarProfileFactory.create(
        volunteer__first_name="Harry", volunteer__last_name="Potter"
    )
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")

    response = VolunteerWithSarProfileListView.as_view()(request)
    assert isinstance(response, TemplateResponse)
    assert "Harry Potter" in response.rendered_content


@mark.django_db
def test_does_not_list_volunteers_without_sarprofile(rf: RequestFactory):
    volunteer = VolunteerFactory.create(first_name="Alfred")
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")

    response = VolunteerWithSarProfileListView.as_view()(request)
    assertNotContains(response, volunteer.first_name, html=True)


@mark.django_db
def test_does_not_repeat_volunteers_with_multiple_positions(rf: RequestFactory):
    volunteer = VolunteerFactory.create(first_name="Alfred")
    SarProfileFactory.create(volunteer=volunteer)
    SarProfileFactory.create(volunteer=volunteer)
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")

    response = VolunteerWithSarProfileListView.as_view()(request)

    html = BeautifulSoup(
        cast(TemplateResponse, response).rendered_content, features="html.parser"
    )
    tbody = html.find("tbody")
    assert isinstance(tbody, Tag)
    table_rows = tbody.find_all("tr")
    assert len(table_rows) == 1


@mark.django_db
def test_requires_permission(rf: RequestFactory):
    assert_requires_permissions(
        rf, VolunteerWithSarProfileListView, ["volunteers.view_volunteer"]
    )


@mark.django_db
def test_shows_operations(rf: RequestFactory):
    # Given there is one operation
    operation = OperationFactory.create()

    # When I navigate to the volunteer list
    request = rf.get("volunteer_list")
    request.user = UserFactory.create(permission="volunteers.view_volunteer")
    response = VolunteerWithSarProfileListView.as_view()(request)

    # Then I expect the operation to be shown on the page
    assert isinstance(response, TemplateResponse)
    assert str(operation) in response.rendered_content


@mark.django_db
def test_filters_by_position_and_search(rf: RequestFactory):
    pos_a = PositionFactory.create()
    pos_b = PositionFactory.create()
    SarProfileFactory.create(
        volunteer__first_name="Mary", volunteer__last_name="Poppins", position=pos_a
    )
    SarProfileFactory.create(
        volunteer__first_name="Paul", volunteer__last_name="Paulsen", position=pos_a
    )
    SarProfileFactory.create(
        volunteer__first_name="Captain", volunteer__last_name="Hook", position=pos_b
    )

    request = rf.get("", {"position": pos_a.key, "search": "Mary"})
    request.user = UserFactory.create(view_permissions=VolunteerWithSarProfileListView)
    response = VolunteerWithSarProfileListView.as_view()(request)
    assert isinstance(response, HttpResponse)

    assertContains(response, "Mary Poppins")
    assertNotContains(response, "Captain Hook")
    assertNotContains(response, "Paul Paulsen")
