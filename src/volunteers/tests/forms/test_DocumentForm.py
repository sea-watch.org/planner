from datetime import date

from django.core.files.uploadedfile import SimpleUploadedFile
from pytest import mark

from volunteers.forms import DocumentForm
from volunteers.models import Document
from volunteers.tests.factories import DocumentFactory, VolunteerFactory


@mark.django_db
def test_uses_valid_until_as_default():
    valid_until = date(2029, 4, 1)
    document = DocumentFactory.create(
        type=Document.Type.stcw_other,
        valid_until=valid_until,
    )
    form = DocumentForm(volunteer=document.volunteer, instance=document)
    assert form.initial["stcw_valid_until"] == valid_until


@mark.django_db
@mark.parametrize("type", Document.Type)
def test_overrides_filename(type: Document.Type, minimal_pdf: bytes):
    form = DocumentForm(
        volunteer=VolunteerFactory.create(first_name="Peter", last_name="Porker"),
        data={"type": type, "stcw_valid_until": "1979-9-11"},
        files={"file": SimpleUploadedFile("foo.pdf", minimal_pdf)},
    )
    assert not form.errors, "test needs to be adjusted"
    document = form.save()
    if type == Document.Type.cv:
        assert document.file.name == (
            f"Peter_Porker-{type.label.replace(' ', '_')}.pdf"
        )
    else:
        assert document.file.name == (
            f"Peter_Porker-{type.label.replace(' ', '_')}_1979-09-11.pdf"
        )
