from django.conf import settings
from django.core.mail import EmailMessage
from django.test import RequestFactory, override_settings
from django.urls import reverse
from pytest import mark, skip
from pytest_subtests import SubTests

from operations.models import Availability, Operation
from operations.tests.factories import AvailabilityFactory
from public.models import Inquiry
from public.tests.factories import (
    AvailabilityInquiryFactory,
    RankRelatedDocumentsInquiryFactory,
)
from volunteers.tests.factories import VolunteerFactory

from ..service import request_availabilities, request_rank_related_documents


@mark.django_db
def test_request_availabilities(subtests: SubTests, mailoutbox: list[EmailMessage]):
    volunteer = VolunteerFactory.create()
    AvailabilityFactory.create(
        volunteer=volunteer, availability=Availability.Choices.YES
    )
    AvailabilityFactory.create(
        volunteer=volunteer, availability=Availability.Choices.NO
    )
    request_availabilities(volunteer)

    inquiry = None
    with subtests.test("saves the inquiry"):
        inquiry = volunteer.inquiry_set.get()
        assert inquiry
        assert inquiry.type == Inquiry.Type.AVAILABILITY

    with subtests.test("email contains operation dates"):
        if not inquiry:
            skip("prerequisite failed")
        operations = Operation.objects.all()
        email = mailoutbox[0]
        assert (
            operations[0].start_date.isoformat() in email.body
        ), "Operation starting date for operation 1 is missing"
        assert (
            operations[0].end_date.isoformat() in email.body
        ), "Operation end date for operation 1 is missing"
        assert (
            operations[1].start_date.isoformat() in email.body
        ), "Operation starting date for operation 2 is missing"
        assert (
            operations[1].end_date.isoformat() in email.body
        ), "Operation end date for operation 2 is missing"
        assert "Yes" in email.body, "Does not contain pre-selected availability"
        assert (
            "unknown" not in email.body
        ), "Email contains pre-selected avail even though it is set to unknown"

    with subtests.test("email contains correct link"):
        if not inquiry:
            skip("prerequisite failed")
        app_request_email = mailoutbox[0]
        assert "http://" not in app_request_email.body
        form_url = f"https://{settings.EMAIL_URL_HOST}" + reverse(
            "inquiry_form", kwargs={"uid": inquiry.uid}, urlconf=settings.PUBLIC_URLCONF
        )
        assert form_url in app_request_email.body, "No url found in email"


@mark.django_db
def test_request_availabilities_resends_link_on_existing_inquiry(
    subtests: SubTests, mailoutbox: list[EmailMessage]
):
    inquiry = AvailabilityInquiryFactory.create()

    request_availabilities(inquiry.volunteer)

    with subtests.test("does not create an additional inquiry"):
        assert list(Inquiry.objects.all()) == [inquiry]

    with subtests.test("re-sends email"):
        assert len(mailoutbox) == 1
        assert inquiry.form_url() in mailoutbox[0].body


@mark.django_db
def test_request_rank_related_documents(
    subtests: SubTests, rf: RequestFactory, mailoutbox: list[EmailMessage]
):
    volunteer = VolunteerFactory.create()
    with override_settings(EMAIL_URL_HOST="public.test"):
        request_rank_related_documents(volunteer=volunteer)

    inquiry = None
    with subtests.test("saves request data"):
        inquiry = volunteer.inquiry_set.get()
        assert inquiry
        assert inquiry.type == Inquiry.Type.RANK_RELATED_DOCUMENTS

    with subtests.test("sends email with right name"):
        assert len(mailoutbox) == 1, "No email has been sent"
        app_request_email = mailoutbox[0]
        assert volunteer.first_name in app_request_email.body
        assert app_request_email.recipients() == [volunteer.email]
        assert (
            "Please upload the following documents to our internal database:"
            in app_request_email.body
        )

    with subtests.test("email contains correct link"):
        if not inquiry:
            skip("prerequisite failed")
        app_request_email = mailoutbox[0]
        assert "http://" not in app_request_email.body
        form_url = "https://public.test" + reverse(
            "inquiry_form",
            kwargs={"uid": inquiry.uid},
            urlconf=settings.PUBLIC_URLCONF,
        )
        assert form_url in app_request_email.body, "No url found in email"


@mark.django_db
def test_request_rank_related_documents_resends_link_on_existing_inquiry(
    subtests: SubTests, mailoutbox: list[EmailMessage]
):
    inquiry = RankRelatedDocumentsInquiryFactory.create()

    request_rank_related_documents(inquiry.volunteer)

    with subtests.test("does not create an additional inquiry"):
        assert list(Inquiry.objects.all()) == [inquiry]

    with subtests.test("re-sends email"):
        assert len(mailoutbox) == 1
        assert inquiry.form_url() in mailoutbox[0].body
