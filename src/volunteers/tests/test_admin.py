from bs4 import BeautifulSoup
from django.conf import settings
from django.template.response import TemplateResponse
from django.test import Client
from django.urls import reverse
from pytest import mark

from seawatch_planner.tests.site import site
from seawatch_registration.tests.factories import UserFactory

from .factories import VolunteerFactory


@mark.django_db
def test_volunteer_admin(backoffice_client: Client):
    backoffice_client.force_login(
        UserFactory.create(is_staff=True, permission="view_volunteer")
    )
    assert (
        backoffice_client.get(
            reverse("admin:volunteers_volunteer_changelist")
        ).status_code
        == 200
    )


@mark.django_db
def test_volunteer_name_in_list(backoffice_client: Client):
    first_name = "Reason"
    last_name = "Davis"
    VolunteerFactory.create(id=94, first_name=first_name, last_name=last_name)
    backoffice_client.force_login(
        UserFactory.create(is_staff=True, permission="view_volunteer")
    )

    response = backoffice_client.get(
        reverse("admin:volunteers_volunteer_changelist"),
    )
    assert isinstance(response, TemplateResponse)
    with site("backoffice"):  # rendering and thus url resolving is delayed
        html = BeautifulSoup(response.rendered_content, features="html.parser")

    detailsUrl = reverse(
        "admin:volunteers_volunteer_change",
        kwargs={"object_id": 94},
        urlconf=settings.BACKOFFICE_URLCONF,
    )

    links = html.find_all("a", href=detailsUrl)
    assert len(links) == 1
    assert links[0].text == f"{first_name} {last_name}"
