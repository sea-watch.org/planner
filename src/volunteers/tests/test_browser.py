from typing import cast
from planner_common.feature_flags import features

from django.urls import reverse
from playwright.sync_api import BrowserContext
from pytest import mark

from volunteers.tests.factories import SarProfileFactory, VolunteerFactory

pytestmark = mark.browser


# NOTE: Please don't use this file anymore, put tests in the test file that corresponds to the
# features you are testing. This file was created when we were experimenting with browser tests.


def test_volunteer_request_availability_popup(
    logged_in_context: BrowserContext, backoffice_url
):
    VolunteerFactory.create_batch(3)
    volunteer = SarProfileFactory.create(
        volunteer__first_name="Hiero", volunteer__last_name="Protagonist"
    ).volunteer

    page = logged_in_context.new_page()
    page.goto(f"{backoffice_url}{reverse('volunteer_list')}")

    tr = page.query_selector(f"tr:has(a[href='{volunteer.get_absolute_url()}'])")
    assert tr
    [button] = filter(
        lambda b: b.text_content() == "Request Availabilities",
        tr.query_selector_all("button"),
    )
    assert button
    button.click()

    if features["daisy_ui_migration"]:
        assert (modal := page.wait_for_selector("dialog[open]"))
        assert "Hiero Protagonist will receive" in cast(str, modal.text_content())
        assert (form := modal.query_selector("form[method='post']"))
        assert form.get_attribute("action") in [
            None,
            reverse(
                "volunteer_request_availabilities",
                kwargs={"volunteer_id": volunteer.id},
            ),
        ]
        assert (button := form.query_selector("button[type=submit]"))
        assert button.text_content() == "Send inquiry"
    else:
        assert (modal := page.wait_for_selector(".planner-modal.visible"))
        assert "Hiero Protagonist will receive" in cast(str, modal.text_content())
        assert (form := modal.query_selector("form"))
        assert form.get_attribute("action") == reverse(
            "volunteer_request_availabilities", kwargs={"volunteer_id": volunteer.id}
        )
        assert (submit := form.query_selector("input[type=submit]"))
        assert submit.input_value() == "Send inquiry"


# NOTE: Please don't use this file anymore, put tests in the test file that corresponds to the
# features you are testing. This file was created when we were experimenting with browser tests.


def test_volunteer_qualification(logged_in_context: BrowserContext, backoffice_url):
    volunteer = VolunteerFactory.create(first_name="Awesome", last_name="Protagonist")

    page = logged_in_context.new_page()
    page.goto(
        f'{backoffice_url}{reverse("volunteer_detail", kwargs={"pk": volunteer.pk})}'
    )

    sidebar = page.query_selector(
        f"aside:has(a[href='{volunteer.get_absolute_url()}'])"
    )
    assert sidebar
    [link] = filter(
        lambda b: b.text_content() == "Qualification",
        sidebar.query_selector_all("p"),
    )

    assert link
    link.click()

    title = page.locator('h1:has-text("APPLICATION MOTIVATION & QUALIFICATION")')
    assert title


# NOTE: Please don't use this file anymore, put tests in the test file that corresponds to the
# features you are testing. This file was created when we were experimenting with browser tests.
