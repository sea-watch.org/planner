from datetime import date, timezone
from typing import Callable, ClassVar

from django.core.files.base import ContentFile
from factory import Faker, LazyFunction, Maybe, SubFactory, post_generation  # type: ignore
from factory.declarations import LazyAttribute, SelfAttribute
from factory.django import DjangoModelFactory
from phonenumber_field.phonenumber import PhoneNumber

from planner_common import data
from planner_common.models import Gender
from planner_common.test.factories import (
    CertificatesListFactory,
    DeprecateDirectCallMetaclass,
)
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ..models import NGO, Comment, Document, Experience, SarProfile, Ship, Volunteer


class VolunteerFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Volunteer

    create: ClassVar[Callable[..., Volunteer]]
    create_batch: ClassVar[Callable[..., list[Volunteer]]]
    build: ClassVar[Callable[..., Volunteer]]
    build_batch: ClassVar[Callable[..., list[Volunteer]]]

    class Params:
        # The seed is used so we only depend on the randomness from factory_boy
        languages_count = Faker("random_int", min=1, max=5)
        language_codes = Faker(
            "random_elements",
            unique=True,
            length=SelfAttribute("..languages_count"),
            elements=[lang["code"] for lang in data.languages],
        )
        language_levels = Faker(
            "random_choices",
            length=SelfAttribute("..languages_count"),
            elements=[0, 1, 2, 3],
        )
        phone_number_tail = Faker("numerify", text="#######")

        homepage_ = Faker("uri")
        has_homepage = Faker("boolean")

        has_joined_at = Faker("boolean")
        """We generate this sometimes even though there is no position because in theory,
        a volunteer could join sea-watch and then have his SarProfile be removed
        """

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    phone_number = LazyAttribute(
        lambda s: PhoneNumber.from_string(f"+49 170 {s.phone_number_tail}")
    )
    email = Faker("ascii_email")
    date_of_birth = Faker("date_of_birth")
    gender = Faker("random_element", elements=Gender.values)
    certificates = SubFactory(CertificatesListFactory)
    languages = LazyAttribute(
        lambda s: {
            code: level for (code, level) in zip(s.language_codes, s.language_levels)
        }
    )
    nationalities = ["gb", "it", "au"]

    homepage = LazyAttribute(lambda s: s.homepage_ if s.has_homepage else "")

    notes = Faker("paragraph")

    applied_at = Faker(
        "past_datetime", tzinfo=timezone.utc
    )  # NOTE: We cannot guarantee that this is earlier than joined_at
    joined_at = Maybe(
        "has_joined_at",
        yes_declaration=Faker("past_datetime", tzinfo=timezone.utc),  # type: ignore
    )
    motivation = Faker("paragraph")
    qualification = Faker("paragraph")


class SarProfileFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = SarProfile
        rename = {"volunteer_fixed": "volunteer"}
        skip_postgeneration_save = True

    class Params:
        volunteer_fixed__v = SelfAttribute("..volunteer")
        volunteer_fixed__joined_at = Faker("past_datetime", tzinfo=timezone.utc)

    create: ClassVar[Callable[..., SarProfile]]
    create_batch: ClassVar[Callable[..., list[SarProfile]]]
    build: ClassVar[Callable[..., SarProfile]]
    build_batch: ClassVar[Callable[..., list[SarProfile]]]

    volunteer = SubFactory(VolunteerFactory)
    position = SubFactory(PositionFactory)

    @post_generation
    def volunteer_fixed(
        obj: SarProfile,  # type: ignore
        create: bool,  # True if .create() was used, false for .build(). True => saved in DB
        extracted,
        **kwargs,
    ):
        """This exists so we can make sure that the volunteer has a joined_at date, even if it was
        generated before calling this factory
        """
        v = kwargs["v"]
        if not v.joined_at:
            v.joined_at = kwargs["joined_at"]
        if create:
            v.save()


class NGOFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = NGO

    create: ClassVar[Callable[..., NGO]]
    create_batch: ClassVar[Callable[..., list[NGO]]]
    build: ClassVar[Callable[..., NGO]]
    build_batch: ClassVar[Callable[..., list[NGO]]]

    name = Faker("company")


class ShipFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Ship

    create: ClassVar[Callable[..., Ship]]
    create_batch: ClassVar[Callable[..., list[Ship]]]
    build: ClassVar[Callable[..., Ship]]
    build_batch: ClassVar[Callable[..., list[Ship]]]

    name = Faker("color_name")


class ExperienceFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Experience

    create: ClassVar[Callable[..., Experience]]
    create_batch: ClassVar[Callable[..., list[Experience]]]
    build: ClassVar[Callable[..., Experience]]
    build_batch: ClassVar[Callable[..., list[Experience]]]

    class Params:
        has_position = Faker("boolean")
        position_ = SubFactory(PositionFactory)
        date_of_experience_ = Faker("past_date", start_date="-10y")

    date_of_experience = LazyAttribute(
        lambda self: date(
            self.date_of_experience_.year, self.date_of_experience_.month, 1
        )
    )
    operation_name = Faker("word")
    notes = Faker("paragraph")
    volunteer = SubFactory(VolunteerFactory)
    position = LazyAttribute(lambda self: self.position_ if self.has_position else None)
    ngo = SubFactory(NGOFactory)
    ship = SubFactory(ShipFactory)


class CommentFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Comment

    create: ClassVar[Callable[..., Comment]]
    create_batch: ClassVar[Callable[..., list[Comment]]]
    build: ClassVar[Callable[..., Comment]]
    build_batch: ClassVar[Callable[..., list[Comment]]]

    volunteer = SubFactory(VolunteerFactory)
    author = SubFactory(UserFactory)
    date = Faker("past_datetime", tzinfo=timezone.utc)
    text = Faker("paragraph")


class DocumentFactory(DjangoModelFactory, metaclass=DeprecateDirectCallMetaclass):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Document

    create: ClassVar[Callable[..., Document]]
    create_batch: ClassVar[Callable[..., list[Document]]]
    build: ClassVar[Callable[..., Document]]
    build_batch: ClassVar[Callable[..., list[Document]]]

    class Params:
        expired = Faker("boolean")
        valid_until_ = Maybe(
            "expired",
            Faker("past_date"),  # type: ignore
            Faker("future_date"),  # type: ignore
        )
        filename_ = Faker("word")
        filename = LazyAttribute(lambda obj: f"{obj.filename_}.pdf")

    volunteer = SubFactory(VolunteerFactory)
    type = Faker("random_element", elements=Document.Type.values)
    file = LazyAttribute(
        lambda obj: ContentFile(
            b"content of " + obj.filename.encode(), name=obj.filename
        )
    )
    created = Faker("past_datetime", tzinfo=timezone.utc)
    valid_until = LazyAttribute(
        lambda obj: (None if obj.type == Document.Type.cv else obj.valid_until_)
    )
    metadata = LazyFunction(dict)
