import datetime
import re
from os.path import splitext
from typing import Any, assert_never, cast
from xml.dom.minicompat import StringTypes

import django_countries
import phonenumber_field.widgets
from django import forms
from django.forms.fields import MultipleChoiceField
from django.forms.widgets import Select, Widget
from django.utils.dates import MONTHS
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from planner_common.forms.fields import CertificatesField
from planner_common.forms.widgets import LanguagesWidget
from seawatch_registration.forms import PositionsField
from seawatch_registration.models import Position
from volunteers.models import Document, Experience, SarProfile, Volunteer

RE_DATE = re.compile(r"(\d{4})-(\d\d?)-(\d\d?)$")


class AutosavingLanguagesWidget(LanguagesWidget):
    template_name = "volunteers/forms/widgets/languages.html"


class AutosavingCertificatesWidget(forms.SelectMultiple):
    template_name = "volunteers/forms/widgets/autosaving-certificates.html"

    def get_context(self, name, value, attrs):
        other = None
        for idx, cert in enumerate(value):
            if cert.startswith("other: "):
                other = cert
                value[idx] = "other"
        ctx = super().get_context(name, value, attrs)
        if other:
            ctx["other"] = other.removeprefix("other: ")
        return ctx


class MonthYearWidget(Widget):
    none_value = (0, "---")
    month_field = "%s_month"
    year_field = "%s_year"

    def __init__(self, attrs=None, years=None, months=None, required=True):
        self.attrs = attrs or {}
        self.required = required

        # Optional list or tuple of years to use in the "year" select box.
        if years:
            self.years = years
        else:
            this_year = datetime.date.today().year
            self.years = range(2010, this_year + 1)

    def render(self, name, value, attrs=None, renderer=None):
        try:
            year_val, month_val = value.year, value.month
        except AttributeError:
            year_val = month_val = None
            if isinstance(value, StringTypes):
                match = RE_DATE.match(value)
                if match:
                    year_val, month_val, day_val = [int(v) for v in match.groups()]

        output = []

        if "id" in self.attrs:
            id_ = self.attrs["id"]
        else:
            id_ = "id_%s" % name

        month_choices = list(MONTHS.items())
        if not (self.required and value):
            month_choices.append(self.none_value)
        month_choices.sort()
        local_attrs = self.build_attrs(base_attrs=self.attrs)
        local_attrs["style"] = "grid-row-start: 2; grid-column-start: 1"
        s = Select(choices=month_choices)
        select_html = s.render(self.month_field % name, month_val, local_attrs)
        output.append(select_html)

        year_choices = [(i, i) for i in self.years]
        if not (self.required and value):
            year_choices.insert(0, self.none_value)
        local_attrs["id"] = self.year_field % id_
        local_attrs["style"] = "width:30%;  grid-row-start: 2; grid-column-start: 2;"
        s = Select(choices=year_choices)
        select_html = s.render(self.year_field % name, year_val, local_attrs)
        output.append(select_html)

        return mark_safe("\n".join(output))  # nosec B308 B703

    def id_for_label(self, id_):
        return "%s_month" % id_

    def value_from_datadict(self, data, files, name):
        y = data.get(self.year_field % name)
        m = data.get(self.month_field % name)
        if y == m == "0":
            return None
        if y and m:
            return "%s-%s-%s" % (y, m, 1)
        return data.get(name, None)


class VolunteerForm(forms.ModelForm):
    positions = PositionsField(queryset=Position.objects.all(), required=False)

    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Volunteer
        fields = [
            "first_name",
            "last_name",
            "phone_number",
            "email",
            "date_of_birth",
            "gender",
            "nationalities",
            "languages",
            "positions",
            "certificates",
            "homepage",
            "notes",
        ]
        field_classes = {
            "certificates": CertificatesField,
            # ModelForm passes de/encoder because the model has a json field
            "nationalities": (
                lambda *a, encoder=None, decoder=None, **k: MultipleChoiceField(
                    *a,
                    choices=django_countries.countries,
                    **k,
                )
            ),
        }
        widgets = {
            "languages": AutosavingLanguagesWidget,
            "certificates": AutosavingCertificatesWidget,
            "nationalities": forms.widgets.SelectMultiple,
        }
        labels = {
            "first_name": _("First Name"),
            "last_name": _("Last Name"),
            "phone_number": _("Phone Number"),
            "email": _("Email Address"),
            "date_of_birth": _("Date of Birth"),
            "gender": _("Gender"),
            "nationalities": _("Nationalities"),
            "languages": _("Spoken Languages"),
            "positions": _("Positions"),
            "certificates": _("Certificates"),
        }

    class Media:
        js = ["volunteers/autosave-input.js", "volunteers/autosave-select-choicesjs.js"]

    _custom_elements_by_widget = {
        forms.widgets.TextInput: "autosave-input",
        forms.widgets.DateInput: "autosave-input",
        forms.widgets.Select: "autosave-select",
        forms.widgets.SelectMultiple: "autosave-choices-js",
        AutosavingLanguagesWidget: None,
        AutosavingCertificatesWidget: None,
        forms.widgets.Textarea: None,
        phonenumber_field.widgets.RegionalPhoneNumberWidget: "autosave-input",
    }

    def __init__(self, instance: Volunteer, initial=None, **kwargs):
        if instance:
            instance.nationalities = [n.upper() for n in instance.nationalities]

        if instance is not None and (initial is None or "positions" not in initial):
            initial = {
                "positions": [p.position for p in instance.sarprofile_set.all()],
                **(initial or {}),
            }

        super().__init__(instance=instance, initial=initial, **kwargs)
        for field_name, field in self.fields.items():
            if field_name not in ["certificates", "languages"]:
                field.widget.attrs = {
                    **(field.widget.attrs or {}),
                    "hx-post": "autosave/",
                    "hx-indicator": "closest .input-container",
                    "hx-target": "closest .field",
                    "hx-swap": "outerHTML",
                }
                if isinstance(field.widget, forms.widgets.SelectMultiple):
                    field.widget.attrs["hx-ext"] = "json-enc"

            # We don't use get() here because we want to know when we forgot to add one.
            custom_element = self._custom_elements_by_widget[type(field.widget)]
            if custom_element:
                field.widget.attrs["is"] = custom_element

        nationalities_field = self.fields.get("nationalities")
        if nationalities_field:
            nationalities_field.choices = django_countries.countries

    def save(self, commit=True):
        if not commit:
            raise NotImplementedError(
                "commit=False requires us to do a little additional work regarding save_m2m"
            )
        volunteer: Volunteer = super().save(commit=commit)
        current = SarProfile.objects.filter(volunteer=volunteer)
        to_create = [
            SarProfile(volunteer=volunteer, position=p)
            for p in (
                set(self.cleaned_data["positions"])
                - set(profile.position for profile in current)
            )
        ]
        to_delete = current.exclude(position__in=self.cleaned_data["positions"])

        SarProfile.objects.bulk_create(to_create)
        to_delete.delete()


class ExperienceCreateForm(forms.ModelForm):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        cast(
            forms.ModelChoiceField, self.fields["position"]
        ).empty_label = "- Not Applicable -"
        cast(forms.ModelChoiceField, self.fields["ngo"]).empty_label = "- Unknown -"
        cast(forms.ModelChoiceField, self.fields["ship"]).empty_label = "- Unknown -"

    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Experience
        fields = [
            "date_of_experience",
            "operation_name",
            "position",
            "notes",
            "ngo",
            "ship",
        ]
        widgets = {
            "date_of_experience": MonthYearWidget,
        }
        help_texts = {
            "position": (
                "If the position is not listed, choose the one where the "
                "responsiblities are the most similar"
            )
        }

    template_name = "volunteers/forms/experience_form.html"


class VolunteerHasExperienceForm(forms.ModelForm):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Volunteer
        fields = ["private_has_experience"]
        labels = {"private_has_experience": "Prior SAR experience"}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields["private_has_experience"].widget.attrs = {
            **(self.fields["private_has_experience"].widget.attrs or {}),
            "hx-post": "autosave/",
            "hx-indicator": "closest .input-container",
            "hx-target": "closest .field",
            "hx-swap": "outerHTML",
        }


class DateInput(forms.DateInput):
    input_type = "date"


class DocumentForm(forms.ModelForm):
    class Meta:  # pyright: ignore [reportIncompatibleVariableOverride]
        model = Document
        fields = ["type", "file"]

    class Media:
        js = ["volunteers/document-form.js"]

    template_name = "volunteers/forms/document_form.html"

    stcw_valid_until = forms.DateField(
        label="Valid until", widget=DateInput, required=False
    )

    def clean(self) -> dict[str, Any]:
        try:
            type = Document.Type(self.cleaned_data["type"])
        except ValueError:
            # If this doesn't work, the type validation will have failed
            pass
        else:
            match type:
                case (
                    Document.Type.stcw_watchkeeping
                    | Document.Type.stcw_basic_safety
                    | Document.Type.stcw_other
                    | Document.Type.medical_fitness
                ):
                    if not self.cleaned_data.get("stcw_valid_until"):
                        self.add_error(
                            "stcw_valid_until",
                            forms.ValidationError(
                                self.fields["stcw_valid_until"].error_messages[
                                    "required"
                                ],
                                code="required",
                            ),
                        )
                case Document.Type.cv:
                    pass
                case x:
                    assert_never(x)
        return self.cleaned_data

    def __init__(
        self,
        volunteer: Volunteer,
        *args,
        instance: Document | None = None,
        initial: dict[str, Any] | None = None,
        **kwargs,
    ) -> None:
        self.volunteer = volunteer

        if instance and instance.volunteer != volunteer:
            raise AssertionError("Trying to edit a document with the wrong volunteer")

        if instance and (valid_until := instance.valid_until):
            initial = initial or {}
            initial.setdefault("stcw_valid_until", valid_until)

        super().__init__(*args, instance=instance, initial=initial, **kwargs)

    def save(self, commit: bool = True) -> Any:
        self.instance.volunteer = self.volunteer

        type_label = Document.Type(self.instance.type).label
        base_filename = f"{self.volunteer.name}-{type_label}"
        extension = splitext(self.instance.file.name)[1]

        match Document.Type(self.instance.type):
            case (
                Document.Type.stcw_watchkeeping
                | Document.Type.stcw_basic_safety
                | Document.Type.stcw_other
                | Document.Type.medical_fitness
            ):
                self.instance.valid_until = self.cleaned_data["stcw_valid_until"]
                self.instance.metadata = {
                    "valid_until": self.cleaned_data["stcw_valid_until"].isoformat()
                }
                self.instance.file.name = f"{base_filename} ({self.instance.valid_until.isoformat()}){extension}"
            case Document.Type.cv:
                self.instance.valid_until = None
                self.instance.metadata = {}
                self.instance.file.name = f"{base_filename}{extension}"
            case x:
                assert_never(x)

        return super().save(commit=commit)
