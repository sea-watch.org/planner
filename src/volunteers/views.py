import json
from datetime import date, datetime, timezone
from http import HTTPStatus
from typing import Any, Callable, Dict, cast
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from django.forms.models import modelform_factory
from django.http.request import HttpRequest
from django.http.response import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    HttpResponseRedirect,
    HttpResponseRedirectBase,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.template import Template
from django.template.context import Context
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils.datastructures import MultiValueDict
from django.views.decorators.http import require_http_methods, require_POST
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView

from history.models import HistoryItem
from operations.models import Availability, Operation
from planner_common.feature_flags import features
from seawatch_registration.models import Position
from volunteers.forms import DocumentForm, VolunteerForm, VolunteerHasExperienceForm

from . import forms, service
from .models import Comment, Document, Experience, Volunteer


def get_position_or_none(key):
    try:
        return Position.objects.get(key=key)
    except Position.DoesNotExist:
        return None


class VolunteerWithSarProfileListView(PermissionRequiredMixin, ListView):
    model = Volunteer
    if features["daisy_ui_migration"]:
        template_name = "volunteers/volunteer_list.html"
    else:
        template_name = "volunteers/volunteer_list_original.html"

    permission_required = "volunteers.view_volunteer"

    def get_queryset(self):
        qs = Volunteer.objects.all()
        qs = qs.exclude(sarprofile__isnull=True).distinct()

        position = self.request.GET.get("position")
        if position:
            qs = qs.filter(sarprofile__position__key=position)

        self.search = self.request.GET.get("search")
        if self.search:
            search_without_spaces = self.search.split(" ")
            for word in search_without_spaces:
                qs = qs.filter(
                    Q(first_name__icontains=word) | Q(last_name__icontains=word)
                )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["operations"] = [str(o) for o in Operation.objects.all()]

        positions_by_group = Position.objects.by_group()
        context["positions_without_group"] = positions_by_group.pop("", [])
        context["positions_with_group"] = positions_by_group

        context["search"] = self.search or ""
        context["selected_position"] = get_position_or_none(
            self.request.GET.get("position")
        )
        return context


@require_POST
@permission_required("volunteers.view_volunteer", raise_exception=True)
def request_availabilities_view(
    request,
    volunteer_id: int,
    request_availabilities: Callable[
        [Volunteer], None
    ] = service.request_availabilities,
):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)

    request_availabilities(volunteer)

    messages.success(
        request,
        f"Sent availabilities request to {volunteer.first_name} at {volunteer.email}",
    )
    return HttpResponseRedirect(reverse("volunteer_list"))


# @permission_required("TBD", raise_exception=True)
@require_POST
def request_rank_related_documents(
    request,
    volunteer_id: int,
    request_rank_related_documents: Callable[
        [Volunteer], None
    ] = service.request_rank_related_documents,
):
    volunteer = get_object_or_404(Volunteer, id=volunteer_id)
    request_rank_related_documents(volunteer)
    # prepare response
    messages.success(
        request,
        f"Sent request for documents to {volunteer.first_name} at {volunteer.email}",
    )
    return HttpResponseRedirect(
        reverse("volunteer_qualification", kwargs={"volunteer_id": volunteer_id})
    )


@permission_required("volunteers.view_volunteer", raise_exception=True)
def volunteer_comments_view(request, volunteer_id: int):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    if request.method == "POST":
        text = request.POST.get("comment")
        if not text:
            return HttpResponseBadRequest()
        # create comment, then fetch all comments and render same page again with new comment
        volunteer.comments.create(
            author=request.user,
            text=text,
        )
        return redirect("volunteer_comments", volunteer_id=volunteer.pk)
    else:
        comments = Comment.objects.filter(volunteer_id=volunteer.pk)

        return render(
            request,
            "volunteers/volunteer_comments.html",
            {
                "volunteer": volunteer,
                "comments": comments,
                "current_active_subpage": "comments",
            },
        )


@permission_required("volunteers.view_volunteer", raise_exception=True)
def volunteer_experience_view(request, volunteer_id: int):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    experiences = Experience.objects.filter(volunteer_id=volunteer.id)
    return render(
        request,
        "volunteers/volunteer_experience.html",
        {
            "volunteer": volunteer,
            "experiences": experiences,
            "current_active_subpage": "experiences",
            "has_experience_form": VolunteerHasExperienceForm(instance=volunteer),
        },
    )


class ExperienceCreateView(CreateView):
    model = Experience
    form_class = forms.ExperienceCreateForm

    def setup(self, *args, **kwargs: Any) -> None:
        super().setup(*args, **kwargs)
        volunteer_id = self.kwargs.get("pk", None)
        self.volunteer = get_object_or_404(Volunteer, pk=volunteer_id)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        ctx = super().get_context_data(**kwargs)
        ctx["volunteer"] = self.volunteer
        return ctx

    def form_valid(self, form):
        form = cast(forms.ExperienceCreateForm, form)
        form.instance.volunteer = self.volunteer
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "volunteer_experience", kwargs={"volunteer_id": self.volunteer.id}
        )


class ExperienceUpdateView(UpdateView):
    model = Experience
    form_class = forms.ExperienceCreateForm

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        ctx = super().get_context_data(**kwargs)
        vol_id = self.kwargs.get("pk", None)
        ctx["volunteer"] = get_object_or_404(Volunteer, pk=vol_id)
        return ctx

    def get_object(self, queryset=None):
        exp_id = self.kwargs.get("exp_id", None)
        return get_object_or_404(Experience, pk=exp_id)

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


def delete_experience_dialog(request, vol_id: int, pk: int):
    volunteer = get_object_or_404(Volunteer, pk=vol_id)
    experience = get_object_or_404(Experience, pk=pk)

    return render(
        request,
        "volunteers/experience_delete_dialog.html",
        {
            "volunteer": volunteer,
            "experience": experience,
            "show_modal_immediately": True,
        },
    )


def delete_experience(
    request, vol_id: int, pk: int
) -> HttpResponseRedirect | HttpResponseNotFound:
    try:
        experience = Volunteer.objects.get(pk=vol_id).experiences.get(id=pk)
    except (Volunteer.DoesNotExist, Experience.DoesNotExist):
        return HttpResponseNotFound()

    experience.delete()
    return HttpResponseRedirect(
        reverse(
            "volunteer_experience",
            kwargs={"volunteer_id": experience.volunteer.id},
        )
    )


@permission_required("volunteers.view_volunteer", raise_exception=True)
def volunteer_qualifications_view(request, volunteer_id: int) -> TemplateResponse:
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    return TemplateResponse(
        request,
        "volunteers/volunteer_qualification.html",
        {
            "volunteer": volunteer,
            "current_active_subpage": "qualification",
        },
    )


class VolunteerPersonalDetailsView(UpdateView):
    model = Volunteer
    form_class = VolunteerForm

    def get_context_data(self, **kwargs) -> Dict[str, Any]:
        ctx = super().get_context_data(**kwargs)
        ctx["current_active_subpage"] = "personal details"
        return ctx


def volunteer_update_autosave(request: HttpRequest, pk: int):
    if request.content_type == "application/json":
        data = json.load(request)
    else:
        data = request.POST.copy()

    # Retrieve data from request
    try:
        [field] = data.keys()
    except ValueError:
        # too many values
        return HttpResponseBadRequest()
    if field not in VolunteerForm.Meta.fields:
        return HttpResponseBadRequest()

    # Save the data
    volunteer = get_object_or_404(Volunteer, pk=pk)
    old_data = volunteer.to_history_dict()
    for k, v in VolunteerForm(instance=volunteer).initial.items():
        if isinstance(data, MultiValueDict) and isinstance(v, list):
            data.setlistdefault(k, v)
        else:
            data.setdefault(k, v)
    # Note that the fields parameter is a bit of a trap. It can only be used to limit the fields
    # that are derived from the model, fields that are added explicitly to the form class can't
    # be skipped with this, which is why we're doing the dance to get all the defaults first.
    #
    # The reason why we're doing this is to avoid the save being blocked by validation
    # errors in unrelated fields. The positions not being excluded doesn't cause a problem here
    # since it can basically never be invalid. (It was mostly the phone number, since we can't
    # easily make sure that we applied the same rules because we're importing etc)
    form = modelform_factory(Volunteer, form=VolunteerForm, fields=[field])(
        data=data, instance=volunteer
    )

    if form.is_valid():
        form.save()
        HistoryItem.objects.create_from_dicts(
            volunteer, old=old_data, new=volunteer.to_history_dict()
        )

    # Render the response
    if "HX-Request" in request.headers:
        if set(form.errors.keys()) - {field} != set():
            raise AssertionError(
                "There should not be any errors in fields that we aren't saving"
            )
        ctx = {
            "autosaving": True,
            "field": form[field],
            "errors": form.errors.get(field, []),
        }

        if field in ["date_of_birth", "gender"]:
            ctx["cls"] = "narrow"
        content = render_to_string(
            "volunteers/forms/autosave_form_field.html", ctx, request=request
        )

        if field in ["first_name", "last_name", "positions"]:
            content = "\n".join(
                [
                    content,
                    Template(
                        "<span id='header-volunteer-name' hx-swap-oob='true'>{{ volunteer.name }}</span>"
                    ).render(Context({"volunteer": volunteer})),
                    render_to_string(
                        "base-volunteer-details--sidebar-header.html",
                        {"volunteer": volunteer},
                    ),
                ]
            )
        return HttpResponse(content=content)
    else:
        return (
            HttpResponse(status=HTTPStatus.NO_CONTENT)
            if form.is_valid()
            else HttpResponseBadRequest(content=json.dumps({"errors": form.errors}))
        )


def experience_update_autosave(request: HttpRequest, pk: int):
    if request.content_type == "application/json":
        data = json.load(request)
    else:
        data = request.POST

    # Retrieve data from request
    try:
        [field] = data.keys()
    except ValueError:
        # too many values
        return HttpResponseBadRequest()
    if field not in VolunteerHasExperienceForm.Meta.fields:
        return HttpResponseBadRequest()

    # Save the data
    volunteer = get_object_or_404(Volunteer, pk=pk)
    old_data = volunteer.to_history_dict()
    Form = modelform_factory(Volunteer, form=VolunteerHasExperienceForm, fields=[field])
    form = Form(data=data, instance=volunteer)

    if form.is_valid():
        form.save()
        HistoryItem.objects.create_from_dicts(
            volunteer, old=old_data, new=volunteer.to_history_dict()
        )

    # Render the response
    if "HX-Request" in request.headers:
        if set(form.errors.keys()) - {field} != set():
            raise AssertionError(
                "There should not be any errors in fields that we aren't saving"
            )
        ctx = {"field": form[field], "autosaving": True, "errors": form.errors}
        content = render_to_string(
            "volunteers/forms/autosave_form_field.html", ctx, request=request
        )
        return HttpResponse(content=content)
    else:
        return (
            HttpResponse(status=HTTPStatus.NO_CONTENT)
            if form.is_valid()
            else HttpResponseBadRequest(content=json.dumps({"errors": form.errors}))
        )


def volunteer_archive(request, pk):
    volunteer = get_object_or_404(Volunteer, pk=pk)
    volunteer.archived_at = datetime.now(timezone.utc)
    volunteer.save()
    return redirect("volunteer_list")


def volunteer_request_availabilities_dialog(request, pk):
    volunteer = get_object_or_404(Volunteer, pk=pk)
    if features["daisy_ui_migration"]:
        template_name = "volunteers/volunteer_request_availabilities_dialog.html"
    else:
        template_name = (
            "volunteers/volunteer_request_availabilities_dialog_original.html"
        )

    def fetch_last_response(operation: Operation):
        a = (
            volunteer.availability_set.filter(operation=operation)
            .exclude(availability=Availability.Choices.UNKNOWN)
            .order_by("-operation_version")
            .first()
        )
        if a is None:
            return None
        return {"availability": a, "outdated": a.operation_version != operation.version}

    operations = Operation.objects.filter(start_date__gte=date.today())
    entries = [
        {"operation": operation, "last_response": fetch_last_response(operation)}
        for operation in operations.order_by("start_date")
    ]

    return render(
        request,
        template_name,
        {
            "volunteer": volunteer,
            "entries": entries,
            "show_modal_immediately": True,
        },
    )


@permission_required("volunteers.view_volunteer", raise_exception=True)
def volunteer_documents_view(
    request, volunteer_id: int
) -> TemplateResponse | HttpResponseRedirect | HttpResponseBadRequest:
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    if request.method == "POST":
        match request.POST.get("action"):
            case "edit":
                return HttpResponseRedirect(
                    reverse(
                        "volunteer_document_edit",
                        kwargs={
                            "document_id": request.POST.getlist("document")[0],
                            "volunteer_id": volunteer.id,
                        },
                    )
                )
            case "delete":
                for doc_id in request.POST.getlist("document"):
                    Document.objects.filter(id=doc_id).delete()
            case "archive":
                Document.objects.filter(id__in=request.POST.getlist("document")).update(
                    archived=datetime.now(tz=timezone.utc)
                )
            case "unarchive":
                Document.objects.filter(id__in=request.POST.getlist("document")).update(
                    archived=None
                )
            case _:
                return HttpResponseBadRequest()
        return HttpResponseRedirect(
            reverse("volunteer_documents", kwargs={"volunteer_id": volunteer.id})
        )

    documents = volunteer.document_set.order_by("type")
    documents_by_type: dict[Document.Type, list[Document]] = {}
    archived: list[Document] = []
    for doc in documents:
        if doc.archived:
            archived += [doc]
            continue
        doc_type = Document.Type(doc.type)
        if doc_type in documents_by_type:
            documents_by_type[doc_type] += [doc]
        else:
            documents_by_type[doc_type] = [doc]
    documents_grouped = sorted(
        list(documents_by_type.items()), key=lambda x: x[0].label
    )

    def sortkey(doc: Document):  # Can't use lambda because lambdas don't support assert
        assert doc.archived
        return doc.archived

    return TemplateResponse(
        request,
        "volunteers/volunteer_documents.html",
        {
            "volunteer": volunteer,
            "documents": documents,
            "documents_grouped": documents_grouped,
            "archived": sorted(archived, key=sortkey, reverse=True),
            "current_active_subpage": "documents",
        },
    )


@require_http_methods(["GET", "POST"])
@permission_required("volunteers.view_volunteer", raise_exception=True)
def volunteer_document_edit(
    request: HttpRequest,
    volunteer_id: int,
    document_id: int | None = None,
) -> TemplateResponse | HttpResponseRedirectBase:
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    document = Document.objects.get(id=document_id) if document_id is not None else None
    match request.method:
        case "GET":
            form = DocumentForm(volunteer, instance=document)
        case "POST":
            form = DocumentForm(
                volunteer, instance=document, data=request.POST, files=request.FILES
            )
            if form.is_valid():
                form.save()
                return redirect("volunteer_documents", volunteer_id=volunteer.id)
        case _:
            raise AssertionError("shouldn't happen")
    return TemplateResponse(
        request,
        ("volunteers/document_edit.html"),
        {"volunteer": volunteer, "form": form, "current_active_subpage": "documents"},
    )
