import json
from datetime import date, datetime, timedelta, timezone

from django.http import HttpResponse
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from applications.models import SarApplication
from history.models import actor_context
from operations.models import Operation
from seawatch_registration.models import Position
from volunteers.models import SarProfile, Volunteer


@require_POST
@csrf_exempt
def create_application(request):
    data: dict = json.loads(request.body)
    positions = data.pop("positions", None)
    data.setdefault("nationalities", ["de"])
    data.setdefault("certificates", [])
    data.setdefault("languages", {"eng": 2, "deu": 1})
    data.setdefault("date_of_birth", date(1970, 1, 1))
    data.setdefault("applied_at", datetime.now(tz=timezone.utc).isoformat())

    with actor_context("testing_api"):
        volunteer = Volunteer.objects.create(**data)
        if positions:
            ps = {
                (
                    (p, SarApplication.Decision.NEW)
                    if isinstance(p, str)
                    else (p[0], SarApplication.Decision(p[1]))
                )
                for p in positions
            }
            SarApplication.objects.bulk_create(
                [
                    SarApplication(
                        volunteer=volunteer,
                        position=Position.objects.get(name=name),
                        decision=decision,
                    )
                    for name, decision in ps
                ]
            )

    return JsonResponse({"id": volunteer.id})


@require_POST
@csrf_exempt
def create_volunteer(request):
    with actor_context("testing_api"):
        data = json.loads(request.body)
        data.setdefault(
            "applied_at",
            (datetime.now(tz=timezone.utc) - timedelta(days=30)).isoformat(),
        )
        positions = data.pop("positions", [])
        volunteer = Volunteer.objects.create(
            **{
                "certificates": [],
                "languages": {},
                "date_of_birth": date(1000, 1, 1),
                "nationalities": [],
                **data,
            }
        )
        SarProfile.objects.bulk_create(
            [
                SarProfile(
                    volunteer=volunteer, position=Position.objects.get(name=name)
                )
                for name in positions
            ]
        )
    return JsonResponse(data={"id": volunteer.id})


@require_POST
@csrf_exempt
def delete_all_volunteers(request):
    Volunteer.objects.all().delete()
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def delete_all_operations(request):
    Operation.objects.all().delete()
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
def create_operation(request):
    Operation.objects.create(**json.loads(request.body))
    return HttpResponse(status=204)
