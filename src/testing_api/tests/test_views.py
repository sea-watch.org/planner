import json
from datetime import date
from http import HTTPStatus

from django.http import HttpResponseNotAllowed
from django.test import Client, RequestFactory
from pytest import mark

from applications.models import SarApplication
from seawatch_registration.tests.factories import PositionFactory
from testing_api import views
from volunteers.models import Volunteer
from volunteers.tests.factories import VolunteerFactory

#####################
## create_application


@mark.django_db
def test_create_application_from_partial_data(rf):
    response = views.create_application(
        rf.post("", content_type="application/json", data={"first_name": "yo"})
    )
    r = json.loads(response.content)
    assert Volunteer.objects.filter(pk=r["id"]).exists()


@mark.django_db
def test_create_application(rf):
    basic_data = {
        "first_name": "foo",
        "last_name": "bar",
        "email": "foo@bar.de",
        "phone_number": "1234",
        "motivation": "motivated!",
        "qualification": "qualified!",
        "gender": "none_other",
        "certificates": sorted(["STCW-VI/3", "STCW-VI/1", "other: tapdancing"]),
        "languages": {"deu": 1, "arz": 3},
    }
    position_1 = PositionFactory.create()
    position_2 = PositionFactory.create()
    position_3 = PositionFactory.create()

    complex_data = {
        "date_of_birth": "1984-02-14",  # isoformat parsing
        "nationalities": ["FR", "UK"],  # needs to be lower case
    }
    response = views.create_application(
        rf.post(
            "",
            content_type="application/json",
            data={
                **basic_data,
                **complex_data,
                "positions": [
                    [position_1.name, "accept"],
                    position_2.name,
                    [position_3.name, "in_progress"],
                ],
            },
        )
    )
    r = json.loads(response.content)

    volunteer = Volunteer.objects.get(pk=r["id"])
    for k, v in basic_data.items():
        assert getattr(volunteer, k) == v

    assert volunteer.date_of_birth == date.fromisoformat(
        complex_data.pop("date_of_birth")
    )
    assert volunteer.nationalities == [
        n.lower() for n in complex_data.pop("nationalities")
    ]
    assert {
        (sa.position, sa.decision) for sa in volunteer.sarapplication_set.open()
    } == {
        (position_1, SarApplication.Decision.ACCEPT),
        (position_2, SarApplication.Decision.NEW),
        (position_3, SarApplication.Decision.IN_PROGRESS),
    }
    assert not complex_data  # just to make sure we didn't forget anything


###################
## create_volunteer


def test_create_volunteer_requires_post(rf):
    response = views.create_volunteer(rf.get("/_testing/create_volunteer/"))
    assert isinstance(response, HttpResponseNotAllowed)


@mark.django_db
def test_creates_volunteer_creates_volunteer(rf):
    response = views.create_volunteer(
        rf.post(
            "not used",
            content_type="application/json",
            data={"first_name": "liselotte", "last_name": "meier"},
        )
    )
    assert response.status_code == HTTPStatus.OK

    assert Volunteer.objects.all().count() == 1
    volunteer = Volunteer.objects.first()
    assert volunteer
    assert volunteer.name == "liselotte meier"

    assert json.loads(response.content) == {"id": volunteer.id}


@mark.django_db
def test_create_volunteer_also_creates_SarProfiles_for_given_positions(
    rf: RequestFactory,
):
    positions = PositionFactory.create_batch(2)
    response = views.create_volunteer(
        rf.post(
            "",
            content_type="application/json",
            data={
                "first_name": "foo",
                "last_name": "bar",
                "positions": [p.name for p in positions],
            },
        )
    )
    assert response.status_code == HTTPStatus.OK
    volunteer = Volunteer.objects.get()
    assert {sp.position for sp in volunteer.sarprofile_set.all()} == set(positions)


########################
## delete_all_volunteers


def test_delete_all_volunteers_is_reachable(backoffice_client: Client):
    # The endpoint requires post, so it will return a 405, but because it doesn't return
    # a 404 we know it's there, and this way we avoid actually doing a lot of work in
    # this test.
    assert (
        backoffice_client.get(
            "/_testing/delete_all_volunteers/",
        ).status_code
        == 405
    )


@mark.django_db
def test_delete_all_volunteers_deletes_volunteers(rf):
    VolunteerFactory.create()
    VolunteerFactory.create()
    response = views.delete_all_volunteers(rf.post(""))
    assert response.status_code == 204
    assert Volunteer.objects.count() == 0


#################
## create_operation


def test_create_operation_requires_post(rf):
    response = views.create_operation(rf.get("/_testing/create_operation/"))
    assert isinstance(response, HttpResponseNotAllowed)
