from django.apps import AppConfig


class TestingApiConfig(AppConfig):
    name = "testing_api"
