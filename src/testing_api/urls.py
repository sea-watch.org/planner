from django.urls import path
from django.views.generic.base import TemplateView  # type: ignore

from . import views

urlpatterns = [
    path("create_application/", views.create_application),
    path("create_volunteer/", views.create_volunteer),
    path("delete_all_volunteers/", views.delete_all_volunteers),
    path("create_operation/", views.create_operation),
    path("delete_all_operations/", views.delete_all_operations),
    path(
        "test/upload-button/",
        TemplateView.as_view(template_name="testing_api/upload_button.html"),
        name="test-upload-button",
    ),
]
