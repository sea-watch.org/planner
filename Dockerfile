# vim: ft=dockerfile

####################################
## Bundle the public client js & css

FROM docker.io/library/node:22-alpine AS client_builder
COPY public-client /tmp/public-client
COPY src /tmp/src
WORKDIR /tmp/public-client/
# TODO: Production webpack config
RUN npm ci --omit=dev && npm run build

#####################
## Build the packages

FROM docker.io/library/python:3.13-alpine AS python_builder

ENV PYTHONFAULTHANDLER=1 \
  PYTHONHASHSEED=random \
  PYTHONUNBUFFERED=1 \
  PIP_DEFAULT_TIMEOUT=100 \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  PIP_NO_CACHE_DIR=1

RUN apk add build-base libffi-dev dasel openldap-dev postgresql-dev nodejs npm
RUN pip install poetry poetry-plugin-export

RUN echo 'INPUT ( libldap.so )' > /usr/lib/libldap_r.so

#####################
## Build the packages

# python packages that need compilation
WORKDIR /work/dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry export --without-hashes -o /requirements.txt
RUN sed -n '/^python-ldap/p' < /requirements.txt > ldap_constraint.txt
RUN pip download python-ldap -c ldap_constraint.txt --no-binary :all: \
  && tar -xzvf python-ldap-* \
  && rm *.tar.gz \
  && echo "INPUT ( libldap.so )" > /usr/lib/libldap_r.so \
  && (cd python-ldap-* && python -m build --wheel && mv dist/*.whl /)
RUN grep -qE '^brotli' /requirements.txt
RUN sed -n '/^brotli/p' < /requirements.txt > brotli_constraint.txt \
  && pip download brotli -c brotli_constraint.txt --no-binary :all: \
  && tar -xf Brotli-* \
  && rm *.tar.* \
  && (cd Brotli-* && python -m build --wheel && mv dist/*.whl /)

# planner
WORKDIR /work/planner
COPY poetry.lock pyproject.toml ./
COPY src ./src
ENV DJANGO_SETTINGS_MODULE=seawatch_planner.settings.build
ENV PLANNER_STATIC_ROOT=/planner-staticfiles
RUN poetry install --only main

RUN env PLANNER_BUILD_WORKAROUND_SKIP_STORAGES=1 poetry run django-admin tailwind install
RUN env PLANNER_BUILD_WORKAROUND_SKIP_STORAGES=1 poetry run django-admin tailwind build

COPY --from=client_builder /tmp/public-client/dist /tmp/public-client/dist/
RUN poetry run django-admin collectstatic
# TODO compilemessages and copy into prod image
RUN poetry build && mv dist/*.whl /

# #############################
# ## Build the production image
#
FROM docker.io/library/python:3.13-alpine

RUN apk add postgresql-libs curl openldap-dev libmagic

COPY --from=client_builder /tmp/public-client/webpack-stats.json /tmp/public-client/
# Empty directory to avoid warnings, maybe we can avoid this?
RUN mkdir /tmp/public-client/dist

COPY --from=python_builder /requirements.txt /*.whl /
RUN sed -i '1 {/lock file is not up to date/d}' /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt /*.whl

ENV DJANGO_SETTINGS_MODULE=seawatch_planner.settings.prod
ENV PLANNER_STATIC_ROOT=/planner-staticfiles
COPY --from=python_builder /planner-staticfiles /planner-staticfiles

EXPOSE 8080/tcp

CMD ["waitress-serve", "--port=8080", "seawatch_planner.wsgi:application"]
