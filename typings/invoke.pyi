from typing import Any, Callable, TypeVar

class Task: ...

F = TypeVar("F")

def task(*args: Any, **kwargs: Any) -> Callable[[F], F]: ...
