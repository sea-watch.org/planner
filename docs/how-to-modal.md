# How to add a modal dialog

We use modal, meaning exclusive, dialogs for some confirmations or small forms.

Examples are customizing the acceptance mail or requesting availabilities.

## Historic notes

We started an implementation using the then-new `<dialog>` element, but at that time it was not
supported in all relevant browsers yet, so we redid it using plain html/javascript.

## How it works

The page adds an `<div class="planner-modal">` to the document.
This should be done by creating a new template that extends `"dialog.html"`, which is then included
into the main page like this:

```htmldjango
{% block extra_body_content %}
  {% include "applications/application_form_reject_dialog.html" with show_modal_immediately=reject_form_visible %}
{% endblock %}
```

In the example above, `reject_form_visible` is a variable set by the view. If you don't need the
dialog to be open on page load, you can omit it.

This is taken from templates/applications/application_form.html.
The "dialog.html" template offers a few `{% block %}`s that can be overridden.

The dialog will be invisible by default (unless `show_modal_immediately` is given a truthy value),
so we need to add a button to show it.
That is done by adding the `visible` css-class to the `.planner-modal` element. For example, the
`application_form.html` does this:

```
<button class="button"
        onClick="document.getElementById('planner-modal-reject-application').classList.add('visible')">
  ...
</button>
```

While inline javascript like this can be confusing when there's more to do, for very simple cases
like this it's fine. (Or, to put it in other terms, this is the all-in-all simplest way I could
think of of doing this.)

The dialog template brings affordances to close it, so you don't need to worry about that.
