{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    git-pair = {
      url = "github:voidus/git-pair";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      git-pair,
    }:
    flake-utils.lib.eachSystem
      [
        "aarch64-darwin"
        "aarch64-linux"
        "x86_64-darwin"
        "x86_64-linux"
      ]
      (
        system:
        let
          # pythonOverlay = final: prev: {
          #   pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [
          #     (pyFinal: pyPrev: {
          #     })
          #   ];
          # };

          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
            # overlays = [ pythonOverlay ];
          };

          # The whole playwright-browsers thing can be
          playwright-browsers = pkgs.callPackage ./nix/playwright-browsers.nix { };

          inherit (pkgs.lib) makeLibraryPath concatStringsSep;

          python = pkgs.python313;

          # This interferes with the invoke installed in the virtualenvs, so we hide this one behind
          # the symlinks
          invoke = python.pkgs.toPythonApplication (
            python.pkgs.invoke.overridePythonAttrs (old: {
              name = "invoke-with-libraries";
              propagatedBuildInputs = (old.propagatedBuildInputs or [ ]) ++ [ python.pkgs.requests ];
            })
          );
        in
        {
          legacyPackages = pkgs;
          packages = {
            inherit python;
            inherit invoke;
            inherit (git-pair.packages.${system}) git-pair;
            inherit playwright-browsers;
          };
          devShells = {
            default = pkgs.mkShell (
              {
                shellHook = ''
                  # For some reason that I don't completely understand, a lot of python applications in
                  # nix put their dependency paths in $PTYHONPATH unneccessarily. (The wrappers explicitly
                  # set sys.path). This messes up our stuff though, because poetry won't install the
                  # dependencies if they are already there, but VSCode doesn't know about $PYTHONPATH. So
                  # some dependencies are then missing. This sucks, so we reset this here.
                  PYTHONPATH=

                  export PGDATABASE=planner
                  export PGDATA=$PWD/.postgres_data
                  export PGHOST=$PWD/.postgres
                  export PLANNER_DATABASE_URL=postgresql:///''${PGDATABASE}
                '';
                packages = [
                  pkgs.nix

                  # languages
                  python
                  pkgs.poetry
                  pkgs.nodePackages.eslint

                  pkgs.nodejs_22

                  # dev tooling
                  pkgs.pre-commit
                  pkgs.ruff
                  pkgs.ruff
                  pkgs.djlint
                  invoke
                  pkgs.bandit
                  pkgs.nodePackages.prettier
                  pkgs.nodePackages.eslint
                  pkgs.nodePackages.typescript # Needed for editor integration

                  pkgs.mprocs
                  pkgs.postgresql
                  (pkgs.writeShellScriptBin "db" ''
                    if [ ! -d $PGHOST ]; then
                      mkdir -p $PGHOST
                    fi
                    if [ ! -d $PGDATA ]; then
                      echo 'Initializing postgresql database...'
                      initdb $PGDATA --auth=trust >/dev/null
                      (
                        X=8; until pg_isready -q -d postgres || [ $X -eq 0 ]; do sleep 0.1; let X=X-1; done
                        pg_isready -q -d postgres && createdb && echo "created database $PGDATABASE" || echo "failed to init db because psql didnt start" >&2
                      ) &
                    fi
                    exec postgres -c listen_addresses= -c unix_socket_directories=$PGHOST
                  '')
                ];

                buildInputs = [
                  pkgs.openldap
                  pkgs.cyrus_sasl
                  pkgs.openssl
                  pkgs.postgresql
                  pkgs.stdenv.cc.cc.lib
                ];

                POETRY_INSTALLER_NO_BINARY = "greenlet";

                PYTHONDEVMODE = "1";
                PYTHONWARNINGS = concatStringsSep "," [
                  "i:pkg_resources is deprecated as an API" # Maybe the ecosystem has adjusted in like 20 years or so
                  "i::ResourceWarning" # We don't care for now, we can re-enable this once we have the first production issue with resource exhaustion.
                ];

                LD_LIBRARY_PATH = makeLibraryPath [
                  pkgs.file # python-magic needs this
                ];
                #
              }
              // (
                if system == "x86_64-linux" then
                  {
                    PLAYWRIGHT_BROWSERS_PATH = playwright-browsers;
                  }
                else
                  {
                    # This shouldn't be needed anymore now that we use the browsers from nixpkgs
                    # # non x86_64-linux have to use `playwright install` and hope that works
                    # "PLANNER_PLAYWRIGHT_USE_MANUAL_BROWSER_INSTALL" = "yes";

                    # greenlet fails because of missing <cstdlib>
                    CPPFLAGS = "-I${pkgs.libcxx.dev}/include/c++/v1/";
                  }
              )
            );
            deploy = pkgs.mkShell {
              packages = [
                # Deployment
                pkgs.ansible
                (pkgs.writeShellApplication {
                  name = "terraform";
                  runtimeInputs = [ pkgs.terraform ];
                  text = ''
                    P="seawatch-crewingdb-personal"
                    TF_HTTP_USERNAME="$(set -e; gopass show "$P/gitlab_username")"
                    export TF_HTTP_USERNAME
                    TF_HTTP_PASSWORD="$(set -e; gopass show "$P/gitlab_personal_access_token")"
                    export TF_HTTP_PASSWORD

                    TF_VAR_hcloud_token="$(set -e; gopass show "seawatch-crewingdb/staging/hcloud_token")"
                    export TF_VAR_hcloud_token
                    TF_VAR_deploy_ssh_key_pub="$(set -e; gopass show "seawatch-crewingdb/staging/deploy_ssh_key.pub")"
                    export TF_VAR_deploy_ssh_key_pub

                    exec terraform "$@"
                  '';
                })
              ];
            };
          };
        }
      );
}
