# Seawatch Crewing DB

[TODO Project description]

## Installing prerequisites

The project requires a few tools to be installed on your system:

- [nix](https://nixos.org/download.html)
  You can verify that it's working by running `nix-shell -p cowsay --run "cowsay howdy"`. That
  should give you some ascii art.
- [direnv](direnv.net)
  Note that direnv requires an additional step after installing, see the [this documentation
  page](https://direnv.net/docs/hook.html) in particular.

TODO: Add a section at the bottom that explains the interplay between direnv and nix and link
it here.

## Getting ready for development

1. In a new shell, navigate to the project directory.
2. `direnv allow`

**Note**: Once you have a shell with the project dependencies, please run `pre-commit install` to
set up the pre-commit hooks and `pre-commit run` to trigger the setup. After that, they will execute
automatically.

To work on the specific subprojects, you also need to install the libraries we use.

- For python subprojects (`backoffice`, `common` and `public-backend`), this is done by entering the
  directory and running `poetry install`.
- For the javascript subprojects (`public-client` and `test`), run `npm install` inside the
  respective folder.

Now you should be ready to get started :)

## Common confusion

This section lists some things that are unintuitive.

- "Application" as a domain term refers to the process of a a new person becoming a volunteer. Think
  "job application", not "software application".
