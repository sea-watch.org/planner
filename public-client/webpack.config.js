const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const path = require("path");
const BundleTracker = require("webpack-bundle-tracker");
const { DefinePlugin, IgnorePlugin } = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");

const postcssLoaderOptions = {
  sourceMap: true,
};

module.exports = (env) => {
  const API_BASEURL = process.env.API_BASEURL || "/";
  return {
    mode: "production", // overridden by npm commands
    resolve: {
      roots: [path.resolve(__dirname, "src"), path.resolve(__dirname, "src", "js")],
      alias: {
        common: path.resolve(__dirname, "..", "src", "planner_common"),
      },
      fallback: {
        crypto: require.resolve("crypto-browserify"),
        stream: require.resolve("stream-browserify"),
      },
    },
    entry: "./src/js/index.js",
    output: {
      path: __dirname + "/dist/compiled/",
      publicPath: "/static/compiled/",
      filename: "[name].[contenthash:5].js",
    },
    plugins: [
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: "[name].[contenthash:5].css",
      }),
      ...(env?.test
        ? []
        : [new BundleTracker({ path: ".", filename: "webpack-stats.json" })]),
      new DefinePlugin({ API_BASEURL: JSON.stringify(API_BASEURL) }),
      new ImageMinimizerPlugin({
        minimizer: {
          implementation: ImageMinimizerPlugin.imageminMinify,
          options: {
            plugins: ["svgo"],
          },
        },
      }),
      // These two are needed for recent @open-wc/testing to work with webpack
      new IgnorePlugin({ resourceRegExp: /^data:text/ }),
      new IgnorePlugin({ resourceRegExp: /^\/__web-dev-server__web-socket.js/ }),
    ],
    devtool: "inline-source-map",
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
        {
          test: /\.(jpe?g|gif|svg|ttf|png)$/i,
          type: "asset",
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          type: "asset/resource",
        },
        {
          test: /\.html$/,
          type: "asset/source",
        },
        {
          test: /\.lit.css$/,
          use: [
            /* Note that url(..)s don't work in these yet, see
             * https://github.com/bennypowers/lit-css-loader/issues/8
             */
            "lit-css-loader",
            { loader: "postcss-loader", options: postcssLoaderOptions },
          ],
        },
        {
          test: /\.cssfrag$/,
          type: "asset/source",
          use: [
            {
              loader: "postcss-loader",
              options: postcssLoaderOptions,
            },
          ],
        },
        {
          test: /\.s?css$/i,
          exclude: /\.lit.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: "css-loader",
              options: {
                url: true,
                importLoaders: 1,
                sourceMap: true,
              },
            },
            {
              loader: "postcss-loader",
              options: postcssLoaderOptions,
            },
          ],
        },
      ],
    },
    optimization: {
      splitChunks: {
        chunks: "async",
      },
    },
  };
};
