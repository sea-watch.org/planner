process.env.CHROME_BIN = require("puppeteer").executablePath();

const headless = process.env.HEADLESS
  ? process.env.HEADLESS.toLocaleLowerCase() === "true"
  : true;

module.exports = (config) => {
  config.set({
    captureConsole: true, // Required so console.log works in tests
    browsers: [headless ? "ChromeHeadlessNoSandbox" : "Chrome"],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: "ChromeHeadless",
        flags: ["--no-sandbox", "--disable-setuid-sandbox"],
      },
    },

    frameworks: ["mocha", "source-map-support"],
    files: [
      {
        pattern: "test/**/*.test.js",
        watched: false,
      },
      {
        pattern: "src/js/elements/*/test.js",
        watched: false,
      },
    ],
    reporters: ["verbose"],
    preprocessors: {
      "test/**/*.test.js": ["webpack"],
      "src/js/elements/*/test.js": ["webpack"],
    },
    coverageReporter: {
      // TODO: Remove this once we have proper test coverage
      // Notes: I (Simon) am personally sceptical of the usefulness of this. 80% (the default)
      // allows us to ignore the "hard to test" 20% (a.k.a where bugs will happen), while 100% is
      // usually not realistic.
      thresholds: {
        global: {
          statements: 0,
          branches: 0,
          functions: 0,
          lines: 0,
        },
      },
    },
    autoWatch: false,
    singleRun: true,
    webpack: require("./webpack.config")({ test: true }),
  });
  // Uncomment the next line to inspect the config
  // console.log(config);
  return config;
};
