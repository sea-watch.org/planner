import "@babel/polyfill/noConflict";

import {
  oneEvent,
  expect,
  fixture,
  nextFrame,
  triggerFocusFor,
} from "@open-wc/testing";

import { LanguageMapping } from "/elements/LanguageMapping";

function getContainer(shadowRoot) {
  const children = shadowRoot.children;
  for (var i = 0; i < children.length; i++) {
    const child = children[i];
    if (child.className == "container") {
      return child;
    }
  }
  throw "Container not found";
}

describe("LanguageMapping", () => {
  it("contains a select element", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const container = getContainer(el.shadowRoot);
    expect(container).to.contain("select");
  });
  it("contains some of the requested languages", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const container = getContainer(el.shadowRoot);
    [
      "English",
      "French",
      "Italian",
      "German",
      "Spanish",
      "Bengali",
      "Tigrinya",
      "Somali",
    ].forEach((language) => {
      expect(container).to.contain.text(language);
    });
  });
  it('supports "Standard Arabic" and "Colloquial Arabic" (TODO!)');
  it("is accessible (whatever that means)", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    expect(el).shadowDom.to.be.accessible;
  });
  it("sends a change event when a choice is added", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const listener = oneEvent(el, "change");
    el.choices.setChoiceByValue("deu");
    const { detail, bubbles } = await listener;
    expect(detail).to.deep.equal([{ language: "deu", points: 0 }]);
    expect(bubbles).to.be.true;
  });
  it("the language choices are opened when the component receives focus", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);

    const input = el.shadowRoot.querySelector("input");
    expect(input).to.be.accessible;

    await triggerFocusFor(el);

    await nextFrame();
    const choicesClassList = Array.from(
      el.shadowRoot.querySelector(".choices").classList,
    );
    expect(choicesClassList).to.contain("is-open");
    expect(el.shadowRoot.activeElement).to.equal(input);
  });

  describe("validation", () => {
    it("is valid if not set to required", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping></language-mapping>`)
      );
      expect(el.reportValidity()).to.be.true;
    });

    it("is valid if not marked required after adding and removing an entry", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping></language-mapping>`)
      );
      el.choices.setChoiceByValue("eng");
      el.choices.removeActiveItemsByValue("eng");
      expect(el.reportValidity()).to.be.true;
    });

    it("is invalid if required and no langauge is selected", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping required></language-mapping>`)
      );
      expect(el.reportValidity()).to.be.false;
    });

    it("is invalid even if the user typed something in the input", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping required></language-mapping>`)
      );
      el.choices.input.element.value = "asdf";
      expect(el.reportValidity()).to.be.false;
    });

    it("is valid after adding a language", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping required></language-mapping>`)
      );
      el.choices.setChoiceByValue(["eng", "deu"]);
      expect(el.reportValidity()).to.be.true;
    });

    it("is invalid after adding and removing languages", async () => {
      const el = /** @type {LanguageMapping} */ (
        await fixture(`<language-mapping required></language-mapping>`)
      );
      el.choices.setChoiceByValue("eng");
      el.choices.removeActiveItemsByValue("eng");
      expect(el.reportValidity()).to.be.false;
    });
  });
});
