import { fixture, expect, nextFrame, html } from "@open-wc/testing";
import { oneEvent } from "@open-wc/testing-helpers";

import "/elements/MultiSelect";

/**
 * @typedef {import("../../src/js/elements/MultiSelect").MultiSelect} MultiSelect
 * @typedef {import("../../src/js/elements/TextareaCounter").TextareaCounter} TextareaCounter
 */

describe("MultiSelect", () => {
  it("should have a option for given property", async () => {
    // given
    let options = [
      { label: "One", value: "one" },
      { label: "Two", value: "two" },
    ];

    let el = /** @type {MultiSelect} */ (
      await fixture(html`<multi-select .options=${options}></multi-select>`)
    );
    await nextFrame();

    const items = /** @type {NodeListOf<HTMLElement>} */ (
      el.shadowRoot.querySelectorAll(".choices__item")
    );
    expect(items).to.have.length(2);
    expect(items[0].innerText).to.equal(options[0].label);
    expect(items[1].dataset.value).to.equal(options[1].value);
  });

  it("should emit a bubbling change event", async () => {
    let el = /** @type {MultiSelect} */ (
      await fixture(`<multi-select name="nationalities"></multi-select>`)
    );
    let listener = oneEvent(el, "change");

    setTimeout(() => el.change());

    let event = await listener;

    expect(event).to.exist;
    expect(event.bubbles).to.be.true;
    expect(event.composed).to.be.true;
  });

  it("displays buttons", async () => {
    const el = /** @type {TextareaCounter} */ (
      await fixture(`<textarea-counter></textarea-counter>`)
    );
    const buttons = /** @type {HTMLElement} */ (
      el.shadowRoot.querySelector(".buttonlist")
    );

    el.buttons = [{ label: "test", url: "" }];

    await nextFrame();

    expect(buttons.innerText).to.equal("test");
    expect(buttons.children.length).to.equal(1);
  });

  // XXX: I verified that this works in the browser, but I cannot get it to work here...
  // it("sends an initial event after creation", async() => {
  //     const options = [{"value": "a", "label": "A"}, {"value": "b", "label": "B"}]
  //     let value = undefined;
  //     const el = await fixture(
  //         `<multi-select
  //             .options="${options}"
  //             @change="${(ev) => {value = ev.target.value}}" />`
  //     );
  //     expect(value).to.equal([]);
  // })
});
