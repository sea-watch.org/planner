module.exports = {
  plugins: [
    require("postcss-nesting")({}),
    require("autoprefixer")({}),
    require("postcss-selector-matches")({}),
    require("cssnano")({
      preset: ["default", { discardComments: { removeAll: true } }],
    }),
  ],
};
