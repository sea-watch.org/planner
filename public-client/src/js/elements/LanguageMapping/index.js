import { LitElement, css, html } from "lit";
import { ifDefined } from "lit/directives/if-defined.js";

import Choices from "/utils/ChoicesWrapper";
import choicesCss from "../choices.lit.css";
import "@polymer/paper-slider/paper-slider.js";

var json = require("common/data/languages.json");
const languages_obj = new Object();
json.forEach(function (lang) {
  if (lang.public == true) {
    languages_obj[lang.code] = lang.name;
  }
});

export class LanguageMapping extends LitElement {
  static languages = languages_obj;

  static levels = {
    0: "Basic Knowledge (A1-A2)",
    1: "Good (B1-B2)",
    2: "Fluent (C1-C2)",
    3: "Native Speaker",
  };

  constructor() {
    super();
    this._languageMapping = {};
    this.required = false;
    this.label = undefined;
  }

  focus() {
    this.renderRoot.querySelector("select").click();
  }

  static styles = [
    choicesCss,
    css`
      @keyframes slidein {
        from {
          height: 0;
        }
      }
      .container {
        margin-top: 20px;
      }
      .mapping {
        margin-bottom: 5px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .mapping label {
        width: 5.5em;
        line-height: 1.25;
      }
      .mapping label .value {
        color: #aaa;
      }
      .mapping .slider {
        display: block;
        vertical-align: middle;
        flex-grow: 1;
        margin: 0 15px;
      }
      .mapping .level {
        padding-left: 20px;
        width: 45%;
      }
      .mapping .slider paper-slider {
        display: inline;
        --paper-slider-active-color: var(--color-primary);
        --paper-slider-knob-color: var(--color-primary);
        --paper-slider-pin-color: var(--color-primary);
      }
    `,
  ];

  static properties = {
    _languageMapping: { state: true },
    label: { type: String },
    required: { type: Boolean },
  };

  get value() {
    return Object.entries(this._languageMapping).map(([language, points]) => {
      return { language, points };
    });
  }

  mappingValueChanged(e) {
    const { name, value } = e.target;

    this._languageMapping = {
      ...this._languageMapping,
      [name]: value,
    };

    this.emitChangeEvent();
  }

  emitChangeEvent() {
    this.dispatchEvent(
      new CustomEvent("change", { bubbles: true, detail: this.value }),
    );
  }

  itemAdded(/** @type {CustomEvent} */ e) {
    this._languageMapping = {
      ...this._languageMapping,
      [e.detail.value]: 0,
    };
    this.choices.input.element.removeAttribute("required");
    this.emitChangeEvent();
  }

  itemRemoved(/** @type {CustomEvent} */ e) {
    const clone = { ...this._languageMapping };
    delete clone[e.detail.value];
    this._languageMapping = clone;

    if (this.required && Object.keys(this._languageMapping).length === 0) {
      this.choices.input.element.setAttribute("required", "");
    }

    this.emitChangeEvent();
  }

  reportValidity() {
    // This is a bit of a hack to re-use the browser display validation logic. Once we display
    // validation errors ourselves, we don't need to mess with the input anymore.
    this.choices.input.element.value = "";
    return this.choices.input.element.reportValidity();
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <div class="container">
        <div class="fields">
          <div class="field">
            <select
              ?required=${this.required}
              multiple
              aria-label=${ifDefined(this.label)}
              @addItem="${this.itemAdded}"
              @removeItem="${this.itemRemoved}"
            >
              <option value="" disabled>Please select</option>
            </select>
          </div>

          <div class="field">
            ${Object.keys(this._languageMapping).map(
              (language) => html`
                <div class="mapping">
                  <label class="language">
                    ${LanguageMapping.languages[language]}
                  </label>
                  <div class="slider">
                    <paper-slider
                      @change="${this.mappingValueChanged}"
                      name="${language}"
                      value="${this._languageMapping[language]}"
                      max="3"
                      pin
                      snaps
                      max-markers="3"
                      steps="1"
                    />
                  </div>
                  <span class="level"
                    >${LanguageMapping.levels[this._languageMapping[language]]}</span
                  >
                </div>
              `,
            )}
          </div>
        </div>
      </div>
    `;
  }

  firstUpdated(_changedProperties) {
    const select = this.renderRoot.querySelector("select");
    this.choices = new Choices(
      select,
      {
        choices: [
          ...Object.entries(LanguageMapping.languages).map(([code, name]) => {
            return { value: code, label: name, selected: false, disabled: false };
          }),
        ],
        allowHTML: true,
        removeItemButton: true,
        duplicateItemsAllowed: false,
        callbackOnCreateTemplates: () => ({
          // Add required to input if configured
          input: (...args) =>
            Object.assign(Choices.defaults.templates.input.call(this, ...args), {
              required: this.required,
            }),
        }),
      },
      this.renderRoot,
    );
  }
}

if (document.seawatchElementIsDefined_languageMapping === undefined) {
  customElements.define("language-mapping", LanguageMapping);
  document.seawatchElementIsDefined_languageMapping = true;
}
