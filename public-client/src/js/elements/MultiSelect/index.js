import { LitElement, html } from "lit";
import { ifDefined } from "lit/directives/if-defined.js";

import Choices from "/utils/ChoicesWrapper";
import choicesCss from "../choices.lit.css";

export class MultiSelect extends LitElement {
  constructor() {
    super();
    this._values = [];
    this.options = [];
    this.label = undefined;
    this.description = undefined;
    this.required = false;
  }

  focus() {
    this.renderRoot.querySelector("select").click();
  }

  static styles = choicesCss;

  static properties = {
    options: { type: Array },
    label: { type: String },
    description: { type: String },
    required: { type: Boolean },
  };

  get value() {
    return [...this._values];
  }

  updated(changedProperties) {
    if (changedProperties.has("options")) {
      this.choices?.setChoices(changedProperties.options);
    }
  }

  change() {
    this._values = Array.from(this.renderRoot.querySelectorAll("option"))
      .filter((option) => option.selected)
      .map((option) => option.value);

    if (this.required && this._values.length == 0) {
      this.choices.input.element.setAttribute("required", "");
    } else {
      this.choices.input.element.removeAttribute("required");
    }

    const event = new CustomEvent("change", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }

  reportValidity() {
    // Choices doesn't handle required properly, as a workaround, we set the filter input to required if no option is selected.
    // We clear the filter input to make sure a validation error is properly shown, even when some text was left in the filter input.
    this.choices.input.element.value = "";
    return this.choices.input.element.reportValidity();
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <select
        multiple
        @change="${this.change}"
        ?required="${this.hasAttribute("required")}"
        aria-label=${ifDefined(this.label)}
        aria-description=${ifDefined(this.description)}
      >
        <option value="" disabled>Please select</option>
        ${this.options.map(
          (option) =>
            html`<option value="${option.value}" ?disabled=${option.disabled}>
              ${option.label}
            </option>`,
        )}
      </select>
    `;
  }

  firstUpdated(changedProperties) {
    super.firstUpdated(changedProperties);

    const select = this.renderRoot.querySelector("select");
    this.choices = new Choices(
      select,
      {
        allowHTML: true,
        removeItemButton: true,
        duplicateItemsAllowed: false,
        shouldSort: false,
        callbackOnCreateTemplates: () => ({
          // Add required too the input if configured
          input: (...args) =>
            Object.assign(Choices.defaults.templates.input.call(this, ...args), {
              required: this.required,
            }),
        }),
      },
      /** @type {HTMLElement} */ (this.renderRoot),
    );

    // Notify the surrounding component of the initial state
    const event = new CustomEvent("change", { bubbles: true, composed: true });
    this.dispatchEvent(event);
  }
}

customElements.define("multi-select", MultiSelect);
