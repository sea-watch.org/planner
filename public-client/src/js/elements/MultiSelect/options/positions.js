// This is not the authoritive list of positions!
// That is in common/schema/initialApplication.json
export default [
  {
    value: "hom",
    label: "Head of Mission",
    url: "https://sea-watch.org/en/join-us/crew/#1687274222727-b9d35663-2f48",
  },
  {
    value: "protection-officer",
    label: "Protection Officer",
    url: "https://sea-watch.org/en/join-us/crew/#1687274223451-009b4b48-230f",
    disabled: true,
  },
  { value: "_seawatch_5", label: "-- Sea-Watch 5 --", disabled: true },
  {
    value: "1st-officer",
    label: "1st Officer",
    url: "https://sea-watch.org/en/join-us/crew/#1687274215917-1f5cca1f-7bb5",
    disabled: true,
  },
  {
    value: "2nd-officer",
    label: "2nd Officer",
    url: "https://sea-watch.org/en/join-us/crew/#1687274216303-5906d88d-9d8e",
    disabled: true,
  },
  {
    value: "bosun",
    label: "Bosun",
    url: "https://sea-watch.org/en/join-us/crew/#1687274218004-60fa06cb-3bd4",
    disabled: true,
  },
  {
    value: "master",
    label: "Master (Sea-Watch 5)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274215458-8250d1f6-5157",
    disabled: true,
  },
  {
    value: "chief-eng",
    label: "Chief Engineer (Sea-Watch 5)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274216702-623b64ba-edc8",
  },
  {
    value: "2nd-eng",
    label: "2nd Engineer (Sea-Watch 5)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274217114-940bc7b9-4be7",
    disabled: true,
  },
  {
    value: "3rd-eng",
    label: "3rd Engineer (Sea-Watch 5)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274217114-940bc7b9-4be7",
    disabled: true,
  },
  {
    value: "ab-seaman",
    label: "Deck Rating (Sea-Watch 5)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274218471-6639c76a-13a7",
    disabled: true,
  },
  {
    value: "ship-elec-it",
    label: "Ship's Electrician/IT",
    url: "https://sea-watch.org/en/join-us/crew/#1687274217548-7eff149e-a56c",
  },
  {
    value: "guest-coord",
    label: "Guest Coordinator",
    url: "https://sea-watch.org/en/join-us/crew/#1687274218944-bd703aed-995c",
    disabled: true,
  },
  {
    value: "cook",
    label: "Cook",
    url: "https://sea-watch.org/en/join-us/crew/#1687274219449-a0ade36d-becd",
  },
  {
    value: "rhib-driver",
    label: "RHIB Driver",
    url: "https://sea-watch.org/en/join-us/crew/#1687274219957-d472d5ca-768e",
  },
  {
    value: "rhib-team-lead",
    label: "RHIB Team Lead",
    url: "https://sea-watch.org/en/join-us/crew/#1687274219957-d472d5ca-768e",
  },
  {
    value: "cult-coord",
    label: "Cultural Mediator",
    url: "https://sea-watch.org/en/join-us/crew/#1687274221306-de4a8af1-f051",
    disabled: true,
  },
  {
    value: "media-coord",
    label: "Media Coordinator",
    url: "https://sea-watch.org/en/join-us/crew/#1687274221920-8b915041-fecb",
  },
  { value: "_aurora", label: "-- Aurora --", disabled: true },
  {
    value: "skipper-aurora",
    label: "Skipper (Aurora)",
    url: "https://sea-watch.org/en/join-us/crew/#aurora-captain",
  },
  {
    value: "mechanic-aurora",
    label: "Mechanic (Aurora)",
    url: "https://sea-watch.org/en/join-us/crew/#aurora-engineer",
  },
  {
    value: "deck-aurora",
    label: "Deck/Driver (Aurora)",
    url: "https://sea-watch.org/en/join-us/crew/#aurora-deck-driver",
  },
  { value: "_medical_positions", label: "-- Medical Positions --", disabled: true },
  {
    value: "paramedic",
    label: "Paramedic",
    url: "https://sea-watch.org/en/join-us/crew/#1687274220533-7ee5e9b0-a023",
    disabled: true,
  },
  {
    value: "nurse",
    label: "Nurse",
    url: "https://sea-watch.org/en/join-us/crew/#1687274220533-7ee5e9b0-a023",
    disabled: true,
  },
  {
    value: "physician",
    label: "Physician (>2 years professional experience)",
    url: "https://sea-watch.org/en/join-us/crew/#1687274220533-7ee5e9b0-a023",
    disabled: true,
  },
];
