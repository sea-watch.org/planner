import { countries } from "countries-list";

export default Object.entries(countries).map(([key, value]) => ({
  value: key,
  label: value.name,
}));
