import certificates_data from "common/data/certificates.json";

const groups = certificates_data.map(({ group, group_label, certificates }) => [
  { value: group, label: group_label, disabled: true },
  ...certificates,
]);

groups[0].push({ value: "other", label: "Other (free text)" });

export default groups.flat();
