import { LitElement, html } from "lit";
import styles from "./styles.lit.css";
import { sendAvailabilityForm } from "/APIService";

const choices = [
  { value: "yes", label: "Yes" },
  { value: "no", label: "No" },
  { value: "maybe", label: "Maybe" },
];

/**
 * @typedef {import("../../APIService").UUID} UUID
 * @typedef {import("../../APIService").Availability} Availability
 * @typedef {import("../../APIService").AvailabilityOrUnknown} AvailabilityOrUnknown
 * @typedef {import("../../APIService").Operation} Operation
 * @typedef {import("../../APIService").OperationRequest} OperationRequest
 */

export class AvailabilityForm extends LitElement {
  sendAvailabilityForm = sendAvailabilityForm; // overridable for testing

  static properties = {
    name: { type: String },
    token: { type: String },

    // Note that we use this for the input but we also store our responses in there while
    // filling out the form
    operations: { type: Array },
    notification: { type: String },
  };

  static styles = styles;

  constructor() {
    super();
    /** @type OperationRequest[] */
    this.operations = [];

    this.name = "there"; // This will make us say "Hi there" until we have a name
    this.token = null;
    this.notification = null;
  }

  onSuccess() {
    window.location.assign("/inquiries/inquiry_success/");
  }

  onFailure(e) {
    console.error("AvailabilityForm.onFailure", e, e.stack);
    this.notification = e.toString();
  }

  async onSubmit(e) {
    e.preventDefault();
    if (allOperationsKnown(this.operations)) {
      const operationData = this.operations.map((o) => ({
        operation_id: o.operation_id,
        answer: o.previous_answer,
      }));

      try {
        await this.sendAvailabilityForm(this.token, operationData);
        this.onSuccess();
      } catch (e) {
        this.onFailure(e);
      }
    } else {
      this.notification = "UNKNOWN operation not caught by form validation";
    }
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <form class="main" @submit="${this.onSubmit}">
        <div class="header">
          <h1>Hi ${this.name}, nice to see you again!</h1>
          <h2>
            We need further information from you in order to plan our operations. Please
            fill out the following form.
          </h2>
        </div>

        ${this.renderMisconfiguredWarningIfMisconfigured()}
        ${this.notification &&
        renderNotification(this.notification, () => {
          this.notification = null;
        })}

        <fieldset>
          <legend>Availability</legend>
          <p>
            We are currently planing operations on the following dates. Which ones are
            you available for?
          </p>
          <small
            >This is not a committment and can be changed later, but it helps us plan
            the year.</small
          >
          ${renderOperationsFormControls(this.operations, ({ operation_id, value }) => {
            this.operations = this.operations.map((o) => {
              if (o.operation_id == operation_id) {
                return { ...o, previous_answer: value };
              } else {
                return o;
              }
            });
          })}
        </fieldset>
        <div class="buttons">
          <input type="submit" value="Submit" class="button" />
        </div>
      </form>
    `;
  }

  renderMisconfiguredWarningIfMisconfigured() {
    if (!this.token) {
      return renderNotification(
        "The component was misconfigured (token property missing). If you're not a developer, " +
          "there is nothing you can do.",
      );
    }
  }
}
customElements.define("availability-form", AvailabilityForm);

/**
 * @param {String} notification
 * @param {() => void} onDismiss
 *
 * @returns {import('lit').TemplateResult}
 */
function renderNotification(notification, onDismiss = null) {
  return html`
    <section role="alert">
      ${onDismiss && html`<button aria-label="Dismiss" @click=${onDismiss}>×</button>`}
      Something went wrong, sorry about that.<br />
      <small>${notification}</small>
    </section>
  `;
}

/**
 * @param {OperationRequest[]} operations
 * @param {(_: {operation_id: number, value: ?Availability }) => void} onChange
 */
function renderOperationsFormControls(operations, onChange) {
  return html`<div class="availabilities">
    ${operations.map((operation) => {
      const op_id = operation.operation_id;
      return html`<div
          class="operation"
        >
          <label for="operation-select-${op_id}"
                 >${formatIsoDate(operation.start_date)}<br/> to&nbsp;${formatIsoDate(
                   operation.end_date,
                 )}
          </label>
          <select
            class="small"
            name="${op_id}"
            id="operation-select-${op_id}"
            required
            @change="${(e) =>
              onChange({
                operation_id: op_id,
                value: e.target.value == "" ? null : e.target.value,
              })}"
            >
            ${renderOptions(operation.previous_answer)}
        </div>`;
    })}
  </div>`;
}

function renderOptions(selected) {
  return html`
    <option value="" ?selected="${selected == null}" disabled hidden>- -</option>
    ${choices.map(
      ({ value, label }) =>
        html`<option value="${value}" ?selected="${selected == value}">
          ${label}
        </option>`,
    )}
  `;
}

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
function dayString(day) {
  switch (day) {
    case 1:
      return "1st";
    case 2:
      return "2nd";
    case 3:
      return "3rd";
    default:
      return `${day}th`;
  }
}
function formatIsoDate(isodate) {
  const d = new Date(isodate);
  return html`${months[d.getMonth()]}&nbsp;${dayString(
    d.getDate(),
  )},&nbsp;${d.getFullYear()}`;
}

/**
 * @param {OperationRequest[]} operations
 * @return {operations is Operation[]}
 */
export function allOperationsKnown(operations) {
  return operations.find((m) => m.previous_answer == null) === undefined;
}
