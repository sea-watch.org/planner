import "@babel/polyfill/noConflict";

import { fixture, html, expect, nextFrame } from "@open-wc/testing";
import { AvailabilityForm, allOperationsKnown } from ".";

/**
 * @typedef {import("../../APIService").Availability} Availability
 * @typedef {import("../../APIService").OperationRequest} OperationRequest
 */

const yes = "yes";
const no = "no";
const maybe = "maybe";

describe("allOperationKnown", () => {
  /** @type {(a: Availability) => OperationRequest} */
  const mkM = (a) => ({
    operation_id: 42,
    start_date: "1923-01-21",
    end_date: "1259-01-04",
    previous_answer: a,
  });

  it("is true for operations with YES", () => {
    expect(allOperationsKnown([mkM(yes)])).to.be.true;
  });
  it("is not true if there is one operation with null", () => {
    expect(allOperationsKnown([mkM(yes), mkM(null)])).to.be.false;
  });
});

describe("AvailabilityForm", () => {
  it("follow basic accessibility guidelines", async () => {
    const form = /** @type AvailabilityForm */ await fixture(
      "<availability-form></availability-form>",
    );
    expect(form).to.be.accessible();
    expect(form).to.be.instanceof(AvailabilityForm);
  });
  it("greets by name", async () => {
    /** @type AvailabilityForm */
    const form = await fixture(
      "<availability-form name='Theodore'></availability-form>",
    );
    expect(form.renderRoot).to.contain.text("Hi Theodore,");
  });
  it("renders operations", async () => {
    const operation_id = 43;
    /** @type OperationRequest[] */
    const operations = [
      {
        operation_id,
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        previous_answer: null,
      },
    ];
    /** @type AvailabilityForm */
    const form = await fixture(
      html`<availability-form .operations=${operations}></availability-form>`,
    );
    const label = form.renderRoot.querySelector(
      `label[for="operation-select-${operation_id}"]`,
    );
    expect(label).lightDom.to.equal(
      "March&nbsp;1st,&nbsp;2021<br/>to&nbsp;April&nbsp;1st,&nbsp;2021",
    );
  });
  it("submits the data", async () => {
    const operation_id = 44;
    /** @type OperationRequest[] */
    const operations = [
      {
        operation_id,
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        previous_answer: null,
      },
    ];
    /** @type AvailabilityForm */
    const form = await fixture(
      html`<availability-form
        token="token"
        .operations=${operations}
      ></availability-form>`,
    );
    const calls = [];
    form.sendAvailabilityForm = async (...args) => {
      calls.push(args);
    };
    let success = null;
    form.onSuccess = () => {
      success = true;
    };

    const select = form.renderRoot.querySelector("select");
    select.value = yes;
    select.dispatchEvent(new Event("change"));

    /** @type HTMLButtonElement */
    const submitButton = form.renderRoot.querySelector("input[type=submit]");
    submitButton.click();

    await nextFrame();
    expect(calls).to.deep.equal([["token", [{ operation_id, answer: yes }]]]);
    expect(success).to.be.true;
  });
  it("doesn't submit UNNKNOWN values", async () => {
    /** @type OperationRequest[] */
    const operations = [
      {
        operation_id: 45,
        start_date: "2021-03-01",
        end_date: "2021-04-01",
        previous_answer: yes,
      },
    ];
    /** @type AvailabilityForm */
    const form = await fixture(
      html`<availability-form .operations=${operations}></availability-form>`,
    );
    let called = false;
    form.sendAvailabilityForm = async () => {
      called = true;
      throw "no";
    };

    const select = form.renderRoot.querySelector("select");
    select.value = ""; // fails here
    select.dispatchEvent(new Event("change"));

    /** @type HTMLButtonElement */
    const submitButton = form.renderRoot.querySelector("input[type=submit]");
    submitButton.click();

    expect(called).to.be.false;
  });
});
