import { LitElement, html, unsafeCSS } from "lit";
import styles from "./styles.cssfrag";
import { ifDefined } from "lit/directives/if-defined.js";

export class TextareaCounter extends LitElement {
  constructor() {
    super();
    this.val = "";
    this.maxLength = 100;
    this.required = false;
    this.buttons = [];
    this.label = undefined;
  }

  focus() {
    this.renderRoot.querySelector("textarea").focus();
  }

  static properties = {
    val: { type: String },
    maxLength: { type: Number },
    required: { type: Boolean },
    buttons: { type: Array },
    label: { type: String },
  };

  get value() {
    return this.val;
  }

  static styles = unsafeCSS(styles);

  change(ev) {
    this.val = ev.target.value;

    const event = new CustomEvent("change", { bubbles: true });
    this.dispatchEvent(event);
  }

  reportValidity() {
    return this.renderRoot.querySelector("textarea").reportValidity();
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <div class="textarea-with-counter">
        <aside>
          <div class="textarea-counter">${this.val.length}/${this.maxLength}</div>
          <div class="buttonlist">
            ${this.buttons.map((button) => {
              return html`<a href="${button.url}" target="_blank">${button.label}</a>`;
            })}
          </div>
        </aside>
        <textarea
          maxlength=${this.maxLength}
          placeholder="${this.getAttribute("placeholder")}"
          @input="${this.change}"
          ?required=${this.required}
          aria-label=${ifDefined(this.label)}
        ></textarea>
      </div>
    `;
  }
}

customElements.define("textarea-counter", TextareaCounter);
