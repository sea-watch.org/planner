import { LitElement, html } from "lit";
import { ifDefined } from "lit/directives/if-defined.js";

const days = [...Array(31)].map((_, i) => (i + 1).toString().padStart(2, "0"));
const months = [...Array(12)].map((_, i) => (i + 1).toString().padStart(2, "0"));
const currentYear = new Date().getFullYear();
const years = [...Array(100)].map((_, i) => (currentYear - i - 1).toString());

import styles from "./styles.lit.css";

function isLeapYear(year) {
  return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

export function getMaxDay(year, month) {
  if (year && month) {
    switch (parseInt(month)) {
      case 2:
        return isLeapYear(parseInt(year)) ? 29 : 28;
      case 4:
      case 6:
      case 9:
      case 11:
        return 30;
      default:
        return 31;
    }
  }
  return 31;
}

export class DatePicker extends LitElement {
  constructor() {
    super();

    this.date = { day: "", month: "", year: "" };
    this.maxDay = getMaxDay("", "");
    this.label = undefined;
    this.required = false;
  }

  static properties = {
    date: { type: Object },
    label: { type: String },
    required: { type: Boolean },
  };

  static styles = styles;

  get value() {
    const { year, month, day } = this.date;
    if (day && month && year) {
      return `${year}-${month}-${day}`;
    }
    return "";
  }

  focus() {
    if (!this.date.year) {
      this.renderRoot.querySelector('[name="year"]').focus();
    } else if (!this.date.month) {
      this.renderRoot.querySelector('[name="month"]').focus();
    } else if (!this.date.day) {
      this.renderRoot.querySelector('[name="day"]').focus();
    } else {
      this.renderRoot.querySelector('[name="year"]').focus();
    }
  }

  change(originalEvent) {
    const prop = originalEvent.target.getAttribute("name");
    const value = originalEvent.target.value;

    const newDate = { ...this.date, [prop]: value };
    this.maxDay = getMaxDay(newDate.year, newDate.month);
    if (newDate.day > this.maxDay) {
      if (prop === "day") {
        newDate.month = "";
      } else {
        newDate.day = "";
      }
    }

    this.date = newDate;
    const event = new CustomEvent("change", { bubbles: true });
    this.dispatchEvent(event);
  }

  reportValidity() {
    const selects = Array.from(this.renderRoot.querySelectorAll("select[required]"));
    return selects.every((s) => s.reportValidity());
  }

  render() {
    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />
      <select
        name="year"
        @change="${this.change}"
        ?required="${this.required}"
        aria-label=${ifDefined(this.label && `${this.label}: year`)}
      >
        <option value="" disabled .selected="${this.date.year === ""}">YYYY</option>
        ${years.map(
          (year) =>
            html`<option value="${year}" .selected="${this.date.year === year}">
              ${year}
            </option>`,
        )}
      </select>
      <span>|</span>
      <select
        name="month"
        @change="${this.change}"
        ?required="${this.required}"
        aria-label=${ifDefined(this.label && `${this.label}: month`)}
      >
        <option value="" disabled .selected="${this.date.month === ""}">MM</option>
        ${months.map(
          (month) =>
            html`<option .selected="${this.date.month === month}" value="${month}">
              ${month}
            </option>`,
        )}
      </select>
      <span>|</span>
      <select
        name="day"
        @change="${this.change}"
        ?required="${this.required}"
        aria-label=${ifDefined(this.label && `${this.label}: day`)}
      >
        <option value="" disabled .selected="${this.date.day === ""}">DD</option>
        ${days.map(
          (day) =>
            html`<option value="${day}" .selected="${this.date.day === day}">
              ${day}
            </option>`,
        )}
      </select>
    `;
  }
}

customElements.define("date-picker", DatePicker);
