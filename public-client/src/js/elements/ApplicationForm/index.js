import { LitElement, html } from "lit";
import { ifDefined } from "lit/directives/if-defined.js";
import country_options from "/elements/MultiSelect/options/countries";
import position_options from "/elements/MultiSelect/options/positions";
import certificates_options from "/elements/MultiSelect/options/certificates";
import styles from "./styles.lit.css";
import { isValidPhoneNumber } from "libphonenumber-js";

import * as APIService from "../../APIService";

import { Validator } from "jsonschema";
import applicationSchema from "common/schema/initialApplication.json";

const featureData = document.getElementById("features-data");

export default class ApplicationForm extends LitElement {
  apiService = APIService; // overridable in tests

  static properties = {
    genderTooltipVisible: { type: Boolean },
    notification: { type: String },
    submitInProgress: { type: Boolean },
    data: { type: Object },
    cv_file: { type: File },
    featureFlags: { type: Object },
  };

  static styles = styles;

  constructor() {
    super();
    this.featureFlags = featureData ? JSON.parse(featureData.textContent) : {};
    this.genderTooltipVisible = false;
    this.otherCertificateTextboxVisible = false;
    this.notification = null;
    this.submitInProgress = false;
    this.data = {
      // We need this to be set right at the start because we use it when rendering, the other ones
      // can be set when the user fills them.
      positions: [],
      homepage: "",
    };
    this.cv_file = null;
  }

  /***********
   * Utilities
   */

  requiresHomepageOrCV() {
    return ["media-coord", "paramedic", "nurse", "physician"].some((pos) =>
      this.data.positions.includes(pos),
    );
  }

  /******************************************************************
   * Event handlers
   */

  isValidEmail(/** @type string */ email) {
    const regEx =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regEx.test(email) || email === "") {
      return true;
    }
    return false;
  }

  redirectToSuccessPage() {
    window.location.assign("/applications/application_success/");
  }

  allFieldsAreValid() {
    const nodes = [
      ...this.renderRoot.querySelectorAll("input:not([aria-hidden]), [required]"),
    ];
    const invalidNode = Array.from(nodes).find((/** @type {HTMLInputElement} */ n) => {
      return !this._fieldIsValid(n);
    });
    return !invalidNode;
  }

  _fieldIsValid(/** @type {HTMLInputElement} */ n) {
    switch (n.id) {
      case "input_phone_number":
        if (n.value && !isValidPhoneNumber(n.value)) {
          n.setCustomValidity("This doesn't seem to be a valid phone number");
        } else {
          n.setCustomValidity("");
        }
        break;
      case "input_email":
        if (n.value && !this.isValidEmail(n.value)) {
          n.setCustomValidity("This doesn't seem to be a valid email adress");
        } else {
          n.setCustomValidity("");
        }
        break;
      case "cv_file":
        if (n.files && Array.from(n.files).some((f) => !f.name.endsWith(".pdf"))) {
          n.setCustomValidity("Please select a pdf file");
        } else {
          n.setCustomValidity("");
        }
        break;
    }
    return n.reportValidity();
  }

  async submitForm(/** @type Event */ event) {
    event.preventDefault();

    if (this.submitInProgress) {
      console.log("submit already in progress");
      return;
    }

    this.notification = null;

    if (this.allFieldsAreValid()) {
      this.validateData(this.data);
      this.submitInProgress = true;
      this.apiService
        .sendApplicationForm({ data: JSON.stringify(this.data), cv_file: this.cv_file })
        .then(this.redirectToSuccessPage)
        .catch((e) => {
          this.notification = e.toString();
          window.scrollTo(0, 0);
          this.submitInProgress = false;
        });
    }
  }

  validateData(data) {
    const validationResult = new Validator().validate(data, applicationSchema);
    if (!validationResult.valid) {
      console.error("ApplicationForm validation failed with data: ", data);
      throw validationResult;
    }
  }

  hideGenderTooltip() {
    this.genderTooltipVisible = false;
  }
  showGenderTooltip() {
    this.genderTooltipVisible = true;
  }

  certificatesChange(/** @type Event */ e) {
    const wasOtherTextboxDisplayed = this.otherCertificateTextboxVisible;
    this.data = {
      ...this.data,
      certificates: /** @type HTMLInputElement */ (e.currentTarget).value,
    };

    this.otherCertificateTextboxVisible = this.data.certificates.indexOf("other") > -1;
    if (this.otherCertificateTextboxVisible && !wasOtherTextboxDisplayed) {
      /** @type HTMLElement */ (e.currentTarget).blur();
    }
  }

  /******************************************************************
   * Rendering
   */

  render() {
    const personalDataFields = [
      renderLabeled({
        target: "input_first_name",
        label: "First Name",
        required: true,
        node: html`
          <input
            type="text"
            name="first_name"
            id="input_first_name"
            placeholder="First Name"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                first_name: e.target.value,
              })}"
            required
          />
        `,
      }),
      renderLabeled({
        target: "input_last_name",
        label: "Last Name",
        required: true,
        node: html`
          <input
            type="text"
            name="last_name"
            id="input_last_name"
            placeholder="Last Name"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                last_name: e.target.value,
              })}"
            required
          />
        `,
      }),
      renderLabeled({
        target: "input_phone_number",
        label: "Phone Number",
        subLabel: "Please include the international prefix (like +49)",
        subLabelId: "input_phone_number_description",
        required: true,
        node: html`
          <input
            type="text"
            name="phone_number"
            id="input_phone_number"
            aria-describedby="input_phone_number_description"
            placeholder="+49 30 23125 555"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                phone_number: e.target.value,
              })}"
            required
          />
        `,
      }),
      renderLabeled({
        target: "input_email",
        label: "Email Address",
        required: true,
        node: html`
          <input
            type="email"
            name="email"
            id="input_email"
            placeholder="you_are_awesome@example.com"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                email: e.target.value,
              })}"
            required
          />
        `,
      }),
      renderLabeled({
        label: "Date of Birth",
        size: "narrow",
        required: true,
        node: html`
          <date-picker
            name="date_of_birth"
            label="Date of Birth"
            @change="${(e) => {
              if (e.target.value) {
                this.data = {
                  ...this.data,
                  date_of_birth: e.target.value,
                };
              }
            }}"
            required
          ></date-picker>
        `,
      }),
      renderLabeled({
        label: "Gender",
        target: "input_gender",
        size: "narrow",
        required: true,
        node: html`
          <div>
            <select
              name="gender"
              id="input_gender"
              aria-describedby="gender_tooltip"
              required
              @change="${(e) => (this.data = { ...this.data, gender: e.target.value })}"
              @focus="${this.showGenderTooltip}"
              @mouseover="${this.showGenderTooltip}"
              @blur="${this.hideGenderTooltip}"
              @mouseleave="${this.hideGenderTooltip}"
              @keydown="${(e) => {
                if (e.key === "Escape") this.hideGenderTooltip();
              }}"
            >
              <option value="" disabled selected hidden>Please select</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
              <option value="none_other">None/Other</option>
              <option value="prefer_not_say">Prefer not to say</option>
            </select>
            <div class="tooltip-container">
              <div
                id="gender_tooltip"
                class="tooltip"
                role="tooltip"
                aria-hidden="${!this.genderTooltipVisible}"
              >
                We are asking because we want to make sure to have a good mix on board
                our ships.
              </div>
            </div>
          </div>
        `,
      }),
      renderLabeled({
        label: "Nationality",
        subLabel: "Please select all your nationalities",
        required: true,
        node: html`
          <multi-select
            label="Nationalities"
            name="nationalities"
            description="Please select all your nationalities"
            .options="${country_options}"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                nationalities: e.target.value,
              })}"
            required
          ></multi-select>
        `,
      }),
      renderLabeled({
        label: "Spoken Languages",
        size: "wide",
        required: true,
        node: html`
          <language-mapping
            label="Spoken Languages"
            name="spoken_languages"
            @change="${(e) =>
              (this.data = {
                ...this.data,
                spoken_languages: e.target.value,
              })}"
            required
          ></language-mapping>
        `,
      }),
    ];

    const cvField = renderLabeled({
      target: "cv_file",
      label: "PDF Upload",
      subLabel: "max. 10 MB",
      required: this.requiresHomepageOrCV() && !this.data.homepage,
      node: html`
        <input
          type="file"
          name="cv_file"
          id="cv_file"
          accept=".pdf,application/pdf"
          ?required=${this.requiresHomepageOrCV() && !this.data.homepage}
          @change="${(/** @type Event & { target: HTMLInputElement }*/ e) =>
            (this.cv_file = e.target.files[0])}"
        />
      `,
    });
    const homepageField = renderLabeled({
      label: "Personal Homepage",
      required: this.requiresHomepageOrCV() && !this.cv_file,
      node: html`
        <input
          type="url"
          name="homepage"
          id="input_homepage"
          ?required=${this.requiresHomepageOrCV() && !this.cv_file}
          @change="${(/** @type Event & { target: HTMLInputElement } */ e) =>
            (this.data = {
              ...this.data,
              homepage: e.target.value,
            })}"
          @input=${(/** @type Event & { target: HTMLInputElement } */ e) => {
            const isEmpty = e.target.value.trim() == "";
            const cv = /** @type HTMLInputElement */ (
              this.renderRoot.querySelector("input[name='cv_file']")
            );
            cv.required = this.requiresHomepageOrCV() && isEmpty;
            cv.closest(".field").classList.toggle("required", cv.required);
          }}
        />
      `,
    });

    return html`
      <link rel="stylesheet" href="${window.contentsCss}" />

      <div class="header">
        <h1>Hi, we are excited to meet you!</h1>
        <h2>To sail with Sea-Watch please fill out the following form.</h2>
      </div>

      ${this.notification &&
      renderNotification(this.notification, () => (this.notification = null))}

      <form class="main" novalidate @submit="${this.submitForm}">
        <div class="content">
          <fieldset>
            <legend>Personal data</legend>
            <div class="fields">${personalDataFields}</div>
          </fieldset>
          <fieldset>
            <legend>Which positions are you applying for?</legend>
            <p>
              Our website contains an
              <a href="https://sea-watch.org/en/join-us/crew/">
                overview of positions on the ship.
              </a>
            </p>
            <div class="fields">
              <div class="field required">
                <label for="positions">Select positions</label>
                <small
                  >Unselectable positions are currently not open for
                  applications.</small
                >

                <multi-select
                  name="positions"
                  .options="${position_options}"
                  required
                  data-validation="required"
                  @change="${(e) =>
                    (this.data = { ...this.data, positions: e.target.value })}"
                ></multi-select>
              </div>
              <div class="field">
                <label for="certificates">Select certificates</label>
                <multi-select
                  name="certificates"
                  .options="${certificates_options}"
                  @change="${(e) => this.certificatesChange(e)}"
                ></multi-select>
                <input
                  type="text"
                  id="input_other_certificate"
                  class="other-certificate"
                  placeholder="Other certificate"
                  aria-hidden="${!this.otherCertificateTextboxVisible}"
                  @change="${(e) =>
                    (this.data = { ...this.data, other_certificate: e.target.value })}"
                />
              </div>
            </div>
          </fieldset>

          <fieldset>
            <legend>
              CV & Portfolio (one is mandatory for medics and media coordinators)
            </legend>
            <div class="fields">${cvField} ${homepageField}</div>
          </fieldset>

          <fieldset>
            <legend>Tell us about yourself</legend>
            <div class="fields">
              <div class="required field wide">
                <label for="motivation">Why do you want to work with Seawatch?</label>
                <textarea-counter
                  name="motivation"
                  id="motivation"
                  maxlength="1000"
                  placeholder="Tell us what motivates you to join a search and rescue NGO and why you chose Sea-Watch?"
                  data-validation="required"
                  @change="${(e) =>
                    (this.data = { ...this.data, motivation: e.target.value })}"
                  required
                ></textarea-counter>
              </div>
              <div class="required field wide">
                <label for="qualification"
                  >What qualifies you for the chosen position(s)?</label
                >
                <small>
                  <b>Please refer to the position descriptions in your answer.</b><br />
                  A link to position descriptions for each position you selected above
                  will be listed as buttons next to the text field.
                </small>
                <textarea-counter
                  name="qualification"
                  id="qualification"
                  maxlength="1000"
                  placeholder="Why are you qualified for the positions you apply for? Whats your Background, fitness level, disaster/SAR/NGO experience? If possible provide references and make sure you refer to the points that are mentioned under 'Profile' and 'Desired Skills' in the position description on our website. Use the buttons next to the text field to open the position descriptions."
                  data-validation="required"
                  .buttons="${this.data.positions.map((value) => {
                    const { label, url } =
                      position_options.find((position) => position.value === value) ||
                      {};
                    return { label, url };
                  })}"
                  @change="${(e) =>
                    (this.data = {
                      ...this.data,
                      qualification: e.target.value,
                    })}"
                  required
                ></textarea-counter>
              </div>
            </div>
          </fieldset>
          <div class="buttons">
            <input
              type="submit"
              class="button"
              ?disabled=${this.submitInProgress}
              value=${this.submitInProgress ? "Submitting..." : "Submit"}
            />
          </div>
        </div>
      </form>
    `;
  }
}
customElements.define("application-form", ApplicationForm);

function renderNotification(notification, dismiss) {
  return html`
    <section role="alert">
      <button aria-label="Dismiss" @click="${dismiss}">x</button>
      Something went wrong, sorry about that.<br />
      <small>${notification}</small>
    </section>
  `;
}

function renderLabeled({
  label,
  node,
  target = undefined,
  subLabel = null,
  subLabelId = undefined,
  size = undefined, // "narrow" | "wide" | undefined
  required = false,
}) {
  return html`<div class="field ${required ? " required" : ""} ${size || ""}">
    <label for="${ifDefined(target)}">${label}</label>
    ${subLabel && html`<small id="${ifDefined(subLabelId)}">${subLabel}</small>`}
    ${node}
  </div>`;
}
