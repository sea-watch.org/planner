import { Validator } from "jsonschema";
import availabilitySchema from "common/schema/availability.json";

class ResponseError extends window.Error {
  constructor(status, text) {
    super("ResponseError");
    this.status = status;
    this.text = text;
  }

  toString() {
    return `${super.toString()} (Status ${this.status})\n${this.text}`;
  }

  static async fromResponse(response) {
    return new ResponseError(response.status, await response.text());
  }
}

export async function sendApplicationForm(dataObject) {
  let formData = new FormData();
  for (const [key, value] of Object.entries(dataObject)) {
    if (value != null) {
      formData.append(key, value);
    }
  }
  const response = await fetch("/applications/create/", {
    method: "POST",
    headers: { Accept: "application/json" },
    body: formData,
  });

  if (response.status !== 204) {
    throw await ResponseError.fromResponse(response);
  }
}

/** @typedef {string} UUID */
/** @typedef {("yes" | "no" | "maybe" )} Availability */
/** @typedef {(Availability | "unknown")} AvailabilityOrUnknown */
/** @typedef {{operation: UUID, start_date: string, end_date: string, availability: Availability }} Operation */
/** @typedef {{operation_id: number, start_date: string, end_date: string, previous_answer: ?Availability }} OperationRequest */

/**
 * @param {UUID} token
 * @param {{operation_id: number, answer: Availability}[]} operations
 */
export async function sendAvailabilityForm(token, answers) {
  const data = { token: token, answers };
  const validationResult = new Validator().validate(data, availabilitySchema);
  if (!validationResult.valid) {
    throw validationResult;
  }

  // XXX: This url path isn't great
  const response = await fetch("/inquiries/update/", {
    method: "POST",
    headers: { "Content-Type": "application/json", Accept: "application/json" },
    body: JSON.stringify(data),
  });

  if (response.status !== 204) {
    throw await ResponseError.fromResponse(response);
  }
}
